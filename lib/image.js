import { CloudinaryContext, Transformation, Image } from "cloudinary-react";

const TransformImage = ({ crop, image, width, height,x,y }) => {
  return (
    <CloudinaryContext cloudName="pweb">
      <Image publicId={image} crop={crop} >
        <Transformation width={width} height={height} crop={crop} x={x} y={y} />
      </Image>
    </CloudinaryContext>
  );
};

export default TransformImage;