const cloudinary = require('cloudinary').v2;
const myconfig = cloudinary.config({
    cloud_name: 'pweb',
    api_key: process.env.CLOUDINARY_APIKEY,
    api_secret: process.env.CLOUDINARY_SECRET,
    secure: true
  });
  exports.myconfig = myconfig;