import { MongoClient } from "mongodb";
export async function  connectToDatabase() {
    
        const client = await MongoClient.connect(
            ""
          );
          setTimeout(() => {
            client.close();
          }, 30000);
    return client;
 
}

