import React, { useState } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { translation } from "../../lib/translations";

const heroContent = {
  heroImage: "/assets/img/hero/dark.jpg",
  heroMobileImage: "/assets/img/hero/img-mobile.jpg",
  heroTitleName: "much...",
  heroDesignation: "THE NEW ERA OF DIGITAL CARDS",
  heroDescriptions: `much... is a digital card provider allowing you to immediately share your desire information, with anyone, wherever and whenever you are in a single touch.`,
  heroBtn: "Create Your Card",
};

const Hero = () => {
  const { data: session, status } = useSession();
  const [isOpen, setIsOpen] = useState(false);
  function toggleModalOne() {
    setIsOpen(!isOpen);
  }
  const { locale } = useRouter();
  //=== Translation Function Start
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  return (
    <>
      <div className="row home-details-container align-items-center pt-36">
        
        <div className="col-12 col-lg-8 offset-lg-4 home-details  text-center text-lg-start rounded-xl">
          <div>
            
            <h1 className="poppins-font">
              {heroContent.heroTitleName}
              <span>{heroContent.heroDesignation}</span>
            </h1>
            <p className="open-sans-font">{heroContent.heroDescriptions}</p>
            {!session &&<a href="/create-your-card" className="button">
              <span className="button-text">{heroContent.heroBtn}</span>
              <span className="button-icon fa fa-arrow-right p-3"></span>
            </a>}
            {session &&<a href="/dashboard/" className="button">
              <span className="button-text">{_('Go to Dashboard')}</span>
              <span className="button-icon fa fa-arrow-right p-3"></span>
            </a>}
          </div>
        </div>
      </div>
    </>
  );
};

export default Hero;
