import Head from "next/head";

const SEO = ({ pageTitle }) => (
  <>
    <Head>
      <title>{pageTitle && `${pageTitle}`}</title>
      <meta httpEquiv="x-ua-compatible" content="ie=edge" />
      <meta
        name="description"
        content="The New Era Of Digital Business Cards"
      />
      <meta
        name="keywords"
        content="qr business card,free barcode scanner,qr code business card,scan barcode web,bar scanner,qr code reader app,free qr code reader,qr code generator software,qr and barcode scanner,barcode scanner iphone,barcode scanner android,read barcode,scan code reader,google barcode reader,wifi barcode scanner,baca barcode,square barcode scanner,open barcode scanner,qr code marketing,qr code card,qr code software,barcode reader iphone,scan barcode with phone,apple barcode scanner,free dynamic qr code generator,free qr code scanner app,qr code barcode scanner,ios barcode scanner,qr code and barcode scanner,google qr scanner android,qr code contact card,create own qr code,3d barcode scanner,netum c750,nike qr,best barcode scanner app for android,qr code scanner price,barcode reader android,digital business card qr code,qr code business card free,qr code label,qr code generator business card,qr code visiting card,scannable business card,free digital business card qr code,id card qr code scanner online,barcode business cards,qr code business card app,qr business card app,digital business card qr code free,business card design with qr code,qr code business card iphone,card qr code,barcode visiting card,qr business card free,create digital business card with qr code,visiting card design with qr code,qr business card generator,plastic business cards with qr code,online business card qr code,best qr code business card,card qr,free qr business card,electronic business card qr code,business card maker with qr code,digital business card qr,create barcode for business card,online business card maker with qr code,business card qr code generator with logo,business card in qr code,e business card with qr code,create business card qr code free,modern business card with qr code,create qr code business card online,name card barcode,qr personal card,id card qr,best qr code for business card,free digital business card with qr code,free qr code business card generator,qr business card maker,creative qr code card,business card design barcode,name card qr code design,best qr business cards"
      />
      <meta name="robots" content="all,max-image-preview:standard,notranslate" />
      <meta name="googlebot" content="all" />

      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />

      <link rel="icon" href="/favicon.ico" />
    </Head>
  </>
);

export default SEO;
