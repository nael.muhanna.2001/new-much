import React, { useState } from "react";
import handleSwitchValue from "../../utils/theme";
import { useRive,Layout} from '@rive-app/react-canvas';

const SwitchDark = () => {
  let pv = false;
  if(typeof document !='undefined')
  {
    pv = localStorage.getItem("theme-color")=='dark';
    console.log(pv);
  }
  
  const [isDark, setIsDark] = useState(pv);
  const handleLabelClick = () => {
    if (isDark) {
      handleSwitchValue(true)
      setIsDark(false);
      rive.stop(['idllOff']);
      rive.play(['idllOn','On']);
      
    } else {
      handleSwitchValue(false)
      setIsDark(true);
      rive.stop(['idllOn']);
      rive.play(['idllOff','Off']);
      

    }
  };
  
  const { rive, RiveComponent } = useRive({
    src: '/rive/switch.riv',
    autoplay:true,
    animations:isDark?['idllOff','Off']:['idllOn','On']
  })
  return (
    <label className={`theme-switcher-label d-flex true`}>
      <input
        type="checkbox"
        onClick={handleLabelClick}
        className="theme-switcher"
      />
      <RiveComponent  />
    </label>
  );
};

export default SwitchDark;
