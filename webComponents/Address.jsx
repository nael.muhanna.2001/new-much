import React from "react";

const Address = () => {
  return (
    <>
      <p className="open-sans-font custom-span-contact position-relative text-gray-800 md:text-white">
        <i className="fa-sharp fa-solid fa-location-dot position-absolute  text-blue-900 md:text-green -m-2 !text-2xl"></i>
        3rd Floor Innovation Tower, Riyadh, Saudi Arabia.
      </p>
      {/* End .custom-span-contact */}

      <p className="open-sans-font custom-span-contact position-relative text-gray-800 md:text-white">
        <i className="fa fa-envelope-open position-absolute text-blue-900 md:text-green -m-2 !text-xl"></i>

        <a
          className="text-gray-800 md:text-white mt-5"
          href="mailto:info@much.sa"
        >
          info@much.sa
        </a>
      </p>
      {/* End .custom-span-contact */}

      <p className="open-sans-font custom-span-contact position-relative text-gray-800 md:text-white">
        <i className="fa fa-phone-square position-absolute text-blue-900 md:text-green -m-2 !text-2xl"></i>

        <a className="text-gray-800 md:text-white" href="Tel: +966 53 663 6788">
          +966 53 663 6788
        </a>
      </p>
      {/* End .custom-span-contact */}
    </>
  );
};

export default Address;
