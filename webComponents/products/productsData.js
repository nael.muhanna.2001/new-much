
const Image1 = "/assets/img/portfolio/project-1.jpg";
const Image2 = "/assets/img/portfolio/project-2.jpg";
const Image3 = "/assets/img/portfolio/project-3.jpg";
const Image4 = "/assets/img/portfolio/project-4.jpg";
const Image5 = "/assets/img/portfolio/project-5.jpg";
const Image6 = "/assets/img/portfolio/project-6.jpg";
const Image7 = "/assets/img/portfolio/project-7.jpg";
const Image8 = "/assets/img/portfolio/project-8.jpg";
const Image9 = "/assets/img/portfolio/project-9.jpg";
const PortfolioData = [
  {
    id: 1,
    type: "Printed NFC Card",
    image: Image1,
    tag: ["NFC","QR"],
    delayAnimation: "0",
    modalDetails: [
      {
        project: "Printed NFC Card",
        price: "SAR 138",
        language: "HTML, CSS, Javascript",
        preview: "www.envato.com",
        link: "https://www.envato.com/",
      },
    ],
  },
  {
    id: 2,
    type: "youtube project",
    image: Image2,
    tag: ["NFC"],
    delayAnimation: "100",
    modalDetails: [
      {
        project: "video",
        client: "Videohive",
        language: " Adobe After Effects",
        preview: "www.videohive.net",
        link: "https://www.videohive.net",
      },
    ],
  },
  {
    id: 3,
    type: "youtube project",
    image: Image3,
    tag: ["NFC"],
    delayAnimation: "100",
    modalDetails: [
      {
        project: "video",
        client: "Videohive",
        language: " Adobe After Effects",
        preview: "www.videohive.net",
        link: "https://www.videohive.net",
      },
    ],
  },
  {
    id: 4,
    type: "youtube project",
    image: Image4,
    tag: ["NFC"],
    delayAnimation: "100",
    modalDetails: [
      {
        project: "video",
        client: "Videohive",
        language: " Adobe After Effects",
        preview: "www.videohive.net",
        link: "https://www.videohive.net",
      },
    ],
  },
  
];

export default PortfolioData;
