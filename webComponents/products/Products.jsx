import React, { useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import ModalMain from "./modal/ModalMain";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { translation } from "../../lib/translations";
import { useRive } from "@rive-app/react-canvas";

const Portfolio = () => {
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const { rive, RiveComponent } = useRive({
    src: "/rive/logo.riv",
    autoplay: true,
  });
  const router = useRouter();
  const [getModal, setGetModal] = useState(false);
  const [productsData, setProductsData] = useState([]);

  useEffect(() => {
    fetch("/api/much-products/", {
      method: "POST",
      body: JSON.stringify({ locale: locale }),
    })
      .then((res) => res.json())
      .then((data) => {
        setProductsData(data);
      });
  }, []);
  return (
    <>
      <div className="portfolio-main">
        <Tabs>
          <TabList className="portfolio-tab-list">
            
          </TabList>

          <div className="container">
            <TabPanel>
              <div className="tab-container">
                {productsData.map((item) => {
                  const {
                    _id,
                    name,
                    description,
                    shortDescription,
                    type,
                    image,
                    delayAnimation,
                  } = item;

                  return (
                    <div
                      key={_id}
                      className="card cursor-pointer border"
                      onClick={() => {
                        router.push("/products/" + _id);
                      }}
                    >
                      <div className="tab-content hover:text-white">
                        {item.type == "Product" &&
                        item.images &&
                        item.images.length > 0 ? (
                          <div className="w-full h-full" style={{backgroundImage:'url('+item.images[0].url+')'}}></div>
                        ) : (
                          ""
                        )}
                        {item.type == "Card" && item.cardFront ? (
                          <div className="p-6 md:p-16">
                            <div
                              className="mx-auto drop-shadow-xl"
                              style={{
                                backgroundImage:
                                  "url(" + item.cardFront.imgUrl + ")",
                                width: "220px",
                                height: "139px",
                              }}
                            >
                              <img
                                style={{
                                  position: "relative",
                                  width: "30px",
                                  top: "40px",
                                  left: "180px",
                                }}
                                src={item.muchIcon}
                              />
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="h-auto  px-2 py-2">
                        <h3>
                          <span className="text-lg font-bold  text-gray-700">
                            {name[locale]}
                          </span>
                        </h3>
                        <p className=" text-gray-400">
                          {shortDescription[locale]}
                        </p>
                        <div className="price-tag text-gray-700">
                          {item.originalPrice != item.price && (
                            <span className="decoration-black">
                              <span className="font-bold text-lg">
                                {" "}
                                {item.price} {_(item.currency)}
                              </span>
                              <br />
                              <span className="line-through">
                                {" "}
                                {item.originalPrice} {_(item.currency)}
                              </span>
                            </span>
                          )}
                          {item.originalPrice == item.price && (
                            <span className="decoration-black">
                              {item.price} {_(item.currency)}
                            </span>
                          )}
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </TabPanel>

            <TabPanel>
              <div className="tab-container">
                {productsData
                  .filter((item) => item.type.includes("NFC"))
                  .map((item) => {
                    const {
                      id,
                      name,
                      description,
                      shortDescription,
                      type,
                      image,
                      delayAnimation,
                    } = item;
                    return (
                      <div key={id} className="card cursor-pointer border">
                        <div
                          className="tab-content hover:text-white"
                          onClick={() => handleModal(id)}
                        >
                          {item.images && item.images.length > 0 && (
                            <img src={item.images[0].url} alt={name[locale]} />
                          )}
                        </div>
                        <div className="h-auto  px-2 py-2">
                          <h3>
                            <span className="text-lg font-bold  text-gray-700">
                              {name[locale]}
                            </span>
                          </h3>
                          <p className=" text-gray-400">
                            {shortDescription[locale]}
                          </p>
                          <div className="price-tag text-gray-700">
                            {item.originalPrice != item.price && (
                              <span className="decoration-black">
                                <span className="font-bold text-lg">
                                  {" "}
                                  {item.price} {_(item.currency)}
                                </span>
                                <br />
                                <span className="line-through">
                                  {" "}
                                  {item.originalPrice} {_(item.currency)}
                                </span>
                              </span>
                            )}
                            {item.originalPrice == item.price && (
                              <span className="decoration-black">
                                {item.price} {_(item.currency)}
                              </span>
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </TabPanel>

            <TabPanel>
              <div className="tab-container">
                {productsData
                  .filter((item) => item.type.includes("QR Code"))
                  .map((item) => {
                    const {
                      id,
                      name,
                      description,
                      shortDescription,
                      type,
                      image,
                      delayAnimation,
                    } = item;
                    return (
                      <div key={id} className="card cursor-pointer border">
                        <div
                          className="tab-content hover:text-white"
                          onClick={() => handleModal(id)}
                        >
                          {item.images && item.images.length > 0 && (
                            <img src={item.images[0].url} alt={name[locale]} />
                          )}
                        </div>
                        <div className="h-auto  px-2 py-2">
                          <h3>
                            <span className="text-lg font-bold  text-gray-700">
                              {name[locale]}
                            </span>
                          </h3>
                          <p className=" text-gray-400">
                            {shortDescription[locale]}
                          </p>
                          <div className="price-tag text-gray-700">
                            {item.originalPrice != item.price && (
                              <span className="decoration-black">
                                <span className="font-bold text-lg">
                                  {" "}
                                  {item.price} {_(item.currency)}
                                </span>
                                <br />
                                <span className="line-through">
                                  {" "}
                                  {item.originalPrice} {_(item.currency)}
                                </span>
                              </span>
                            )}
                            {item.originalPrice == item.price && (
                              <span className="decoration-black">
                                {item.price} {_(item.currency)}
                              </span>
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </TabPanel>
          </div>
        </Tabs>
      </div>
      {getModal && <ModalMain modalId={modalId} setGetModal={setGetModal} />}{" "}
    </>
  );
};

export default Portfolio;
