import { useRouter } from "next/router";
import React from "react";
import { translation } from "../../lib/translations";

const index = () => {
  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  return (
    <section  className="main-content ">
      <div className="container">
        <div className="row">
          <div className="col-xl-6 col-lg-5 col-12">
            <div className="row">
              <div className="col-12" style={{paddingTop:'40%'}}>
                <h1 className="ft-wt-600 text-3xl mb-6">
                  {_("Shine wherever you go and stay interactive!")}
                </h1>
                <p className="text-xl">
                  {_(
                    "Be always connected and grow your network wherever & whenever you’re!"
                  )}
                </p>
                
              </div>
              {/* End .col */}
            </div>
          </div>

          <div className="col-xl-6 col-lg-7 col-12 mt-5 mt-lg-5 p-2">
            <img
              src="/assets/img/features/save-to-contacts.gif"
              className="mx-auto"
              style={{ maxHeight: "600px" }}
            />
          </div>
        </div>
        {/* End .row */}

        <hr className="separator" />
        <div className="row">
          <div className="col-xl-6 col-lg-5 col-12">
            <div className="row">
            <div className="col-12" style={{paddingTop:'40%'}}>
                <h1 className="text-3xl ft-wt-600 mb-6">
                  {_("Categorize your contacts by tags!")}
                </h1>
                <p className="text-xl">
                  {_(
                    "Add contact to your much... cardholder and add tags and note to them!"
                  )}
                </p>
                
              </div>
              {/* End .col */}
            </div>
          </div>
          <div className="col-xl-6 col-lg-7 col-12 mt-5 mt-lg-5 p-2">
            <img
              src="/assets/img/features/save-to-wallet.gif"
              className="mx-auto"
              style={{ maxHeight: "600px" }}
            />
          </div>
        </div>
        {/* End .row */}
        <hr className="separator" />
        <div className="row">
          <div className="col-xl-6 col-lg-5 col-12">
            <div className="row">
            <div className="col-12" style={{paddingTop:'40%'}}>
                <h1 className="text-3xl mb-6 ft-wt-600">
                  {_("Dynamic updates of where you’ve  met your contacts")}
                </h1>
                <p className="text-xl font-thin">
                  {_(
                    "Keep your contact meeting location and time up to date by receiving automatic updates once. You meet them again."
                  )}
                </p>
                
              </div>
              {/* End .col */}
            </div>
          </div>

          <div className="col-xl-6 col-lg-7 col-12 mt-5 mt-lg-5 p-2">
            <img
              src="/assets/img/features/location.gif"
              className="mx-auto"
              style={{ maxHeight: "600px" }}
            />
          </div>
        </div>
        {/* End .row */}
        <hr className="separator" />
        <div className="row">
          
          <div className="col-xl-6 col-lg-5 col-12">
            <div className="row">
              <div className="col-12" style={{paddingTop:'40%'}}>
                <h1 className="text-3xl  mb-6 ft-wt-600">
                  {_("Data Insights & analytic")}
                </h1>
                <p className="text-xl">
                  {_(
                    "Calculate the actual ROI on your network with the ability to track real-time results from networking efforts to improve your bottom line."
                  )}
                </p>
                
              </div>
              {/* End .col */}
            </div>
          </div>
          <div className="col-xl-6 col-lg-7 col-12 mt-5 mt-lg-5 p-2">
            <img
              src="/assets/img/features/dashboard.gif"
              className="mx-auto"
              style={{ maxHeight: "600px" }}
            />
          </div>
        </div>
        {/* End .row */}
      </div>
    </section>
  );
};

export default index;
