import { useRouter } from "next/router";
import { translation } from "../../lib/websiteTranslations";
import { useState } from "react";
import { BsCaretDownFill, BsChevronDown } from "react-icons/bs";
import { Dropdown } from "rsuite";
import "rsuite/dist/rsuite.min.css";
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

const SinglePlan = ({ data }) => {
  const [open, setOpen] = useState(false);
  const [plan,setPlan]=useState('monthly');
  return (
    <div className="min-h-[395px] min-w-[315px]   h-fit   max-w-[315px] rounded-3xl relative  bg-gray-100 shadow">
      <div className="h-full">
        <div className=" relative">
          <div
            className=" absolute w-[100.5%] h-[100px] right-[-1px]   rounded-t-3xl"
            style={{ backgroundColor: data.color }}
          >
            <div className="top-[40%] relative">
              <h2 className="  text-white text-2xl font-bold mb-0 tracking-normal ">
                {data.name}
              </h2>
            </div>

            <div className="w-[85px] absolute rotate-180 top-[99%] right-[115px]">
              <svg
                id="a"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 84.2 17"
                className=""
                style={{ fill: data.color }}
              >
                <path d="M0,17c4.52,0,8.96-1.23,12.83-3.56L29.27,3.56c7.9-4.75,17.77-4.75,25.67,0l16.43,9.88c3.88,2.33,8.31,3.56,12.83,3.56H0Z" />
              </svg>
            </div>
          </div>
        </div>
        <div className=" flex flex-col pt-[150px]  ">
          <div className="text-center flex justify-center gap-4">
            {data.annualy!=0 && <Dropdown value={plan} title={plan=='monthly'?_("Monthly Fees"):_("Annually Fees")}>
              <Dropdown.Item value={'monthly'} onClick={(e)=>{setPlan('monthly')}}>{_("Monthly Fees")}</Dropdown.Item>
              <Dropdown.Item value={'annualy'} onClick={(e)=>{setPlan('annualy')}}>{_("Annually Fees")}</Dropdown.Item>
            </Dropdown>
            }
            {data.annualy==0 && <span className="py-2">
              {_('Free for ever!')}</span>}
          </div>
          <div className="flex justify-center">
            <hr className="w-4/5" />
          </div>
          {plan=='monthly' &&<div className="flex justify-center">
            <span className=" text-[30px] font-bold">{data.monthly} {_('SAR')}</span>
          </div>}
          {plan=='annualy' && data.annualy==0 &&<div className="flex justify-center">
            <span className=" text-[30px] font-bold">{data.annualy} {_('SAR')}</span>
          </div>}
          {plan=='annualy' && data.annualy!=0 &&<div className=" text-center">
          <span className=" text-[15px] text-gray-700 line-through ">{data.monthly*12} {_('SAR')}</span>
          <br />
            <span className=" text-[30px] text-red-900 font-bold">{data.annualy} {_('SAR')}</span>
          </div>}
          {plan=='annualy' && data.annualy!=0 &&<div className="flex justify-center">
            <span className=" text-[20px] text-red-800">17% {_("Discount")}</span>
          </div>}
          {/* <div className="flex justify-center mt-4">
            <span className=" text-[20px] ">17 % {_("Discount")}</span>
          </div> */}
          <div id="planDetails" className="flex justify-center mt-4">
            <button
              className="!p-4 rounded-xl w-[80%] text-white  font-bold"
              style={{ backgroundColor: data.color }}
            >
              <span className="drop-shadow shadow-[#000]">
              {_("Sign up for")} {data.name}
              </span>
            </button>
          </div>
          {!open ? (
            <div className="flex justify-center mt-4">
              <button
                className="text-black text-base flex gap-2"
                onClick={() => {
                  setOpen(!open);
                  document.getElementById("planDetails").scrollIntoView({behavior:'smooth'});
                  
                }}
              >
                {_("Learn More")} <BsChevronDown className="mt-1" />
              </button>
            </div>
          ) : null}
          <div className="mt-8">
            <Featurs open={open} data={data} setOpen={setOpen} />
          </div>
        </div>
      </div>
    </div>
  );
};

const Plans = () => {
  const data = [
    {
      monthly:0,
      annualy:0,
      name: _("Freemium"),
      color: "#dfdac7",
      featurs: [
        _("No App Needed"),
        _("Shareable QR Code"),
        _("Unlimited Cardholder Contacts"),
        _("Social Media Links"),
        _("Data Security"),
        _("Basic Real-Time Analytics"),
      ],
    },
    {
      monthly:30,
      annualy:300,
      name: "VIP",
      color: "#7fcaad",
      
      featurs: [
        _("No App Needed"),
        _("Shareable QR Code"),
        _("Unlimited Cardholder Contacts"),
        _("Social Media Links"),
        _("Data Security"),
        _("Basic Real-Time Analytics"),
        _('Customizable Digital Card Design Template (i)'),
        _('Email Signatures'),
        _('View Recent Connections'),
        _('24/7 Support'),
        _('Add Sections'),
        _('Spotify | SoundCloud Embeds'),
        _('Custom Profile Logo'),
        _('Who Visited Your Profile')
      ],
    },
    {
      monthly:45,
      annualy:450,
      name: "Elite",
      color: "#6b5d7b",
      description:
        "18+ yrs experience in advertising, branding, localisation and copywriting. Founder of Moodban creative agency.",
      img: "/images/logo-old.svg",
      featurs: [
        _("No App Needed"),
        _("Shareable QR Code"),
        _("Unlimited Cardholder Contacts"),
        _("Social Media Links"),
        _("Data Security"),
        _("Basic Real-Time Analytics"),
        _('Customizable Digital Card Design Template (i)'),
        _('Email Signatures'),
        _('View Recent Connections'),
        _('24/7 Support'),
        _('Add Sections'),
        _('Spotify | SoundCloud Embeds'),
        _('Custom Profile Logo'),
        _('Who Visited Your Profile'),
        _('Personal Profile Video'),
        _('PDF Uploads'),
        _('Branded QR Code'),
        _('Remove Much Logo'),
        _('Apple Wallet Card'),
        _('Advanced Real-Time Analytics'),
        _('Communication Form'),
        _('Photo Gallery / Portfolio'),
        _('Calendar Integrations'),
        _('One Link'),
        _('Behavioral Analytics'),
        _('Push Notifications')
      ],
    },
    // {
    //   name: "enterprise",
    //   color: "#004c7f",
    //   description:
    //     "18+ yrs experience in advertising, branding, localisation and copywriting. Founder of Moodban creative agency.",
    //   img: "/images/logo-old.svg",
    //   featurs: [
    //     "Seats for team collaboration",
    //     "Data cap: unlimited scans",
    //     "Unlimited custom codes and pages",
    //     "Seats for team collaboration",
    //     "Seats for team collaboration",
    //     "CRM system integrations",
    //     "Reseller licensing",
    //     "Full funnel conversion tracking",
    //     "API Integrations",
    //     "Advanced reporting",
    //     "Advanced security and SSO",
    //   ],
    // },
  ];

  return (
    <div className="bg-white  pb-20" id="pricing">
      <div className=" mt-40  ">
        <div className="text-center ">
          <h2 className="text-3xl md:text-5xl md:font-bold text-green p-5">
            {_("Pricing")}
          </h2>
        </div>
        <div id="plans" className=" flex justify-center flex-col md:flex-row flex-wrap items-center md:items-start  gap-4  pt-20 pb-4">
          {data.map((val, index) => {
            return <SinglePlan key={index} data={val} />;
          })}
        </div>
        <div className=" flex justify-center flex-col md:flex-row flex-wrap items-center md:items-start  gap-4  pt-20 pb-4">
          <PlanTable
            colors={data.map((val) => {
              return val.color;
            })}
          />
        </div>
      </div>
    </div>
  );
};
const Featurs = ({ data, open, setOpen }) => {
  
  return  (
    <div  className={open?"duration-1000 overflow-hidden":"duration-1000 overflow-hidden h-0"} >
      <ul>
        {data.featurs.map((val, index) => {
          return (
            <li key={index}>
              <div className="flex justify-start align-middle">
                <div className="w-1/12 mt-1 p-1">
                  <svg
                    id="a"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 16.05 16.05"
                    style={{ fill: data.color }}
                  >
                    <circle
                      cx="8.02"
                      cy="8.02"
                      r="8.02"
                      transform="translate(-1.18 14.66) rotate(-80.78)"
                    />
                    <polyline
                      points="2.81 8.06 6.26 11.51 13.24 4.53"
                      fill="none"
                      stroke="#fff"
                      strokeMiterlimit="10"
                      strokeWidth="1.5"
                    />
                  </svg>
                </div>
                <p className="w-11/12 pt-[5px]">{val}</p>
              </div>
            </li>
          );
        })}
      </ul>
      <div className="flex justify-center mt-4 mb-4">
        <a
          className="text-black text-base flex gap-2"
          onClick={() => {
            setOpen(!open);
            document.getElementById("plans").scrollIntoView({behavior:'smooth'});
          }}
        >
          <BsChevronDown className="mt-1 rotate-180" /> {_("Show less")}
        </a>
      </div>
    </div>
  );
};
const Row = ({ index, data, colors }) => {
  return (
    <tr style={{ backgroundColor: index % 2 == 0 ? "#f2f2f2" : "white" }}>
      <th
        scope="row"
        className="px-4 py-2 text-left font-medium text-gray-900 whitespace-nowrap "
      >
        {data.PackageType}
      </th>
      <td className="px-4 py-2 text-center">
        {typeof data.Freemium === "string" ? (
          data.Freemium
        ) : (
          <Check color={colors[0]} value={data.Freemium} />
        )}
      </td>
      <td className="px-4 py-2 text-center">
        {typeof data.VIP === "string" ? (
          data.VIP
        ) : (
          <Check color={colors[1]} value={data.VIP} />
        )}
      </td>
      <td className="px-4 py-2 text-center">
        {typeof data.Elite === "string" ? (
          data.Elite
        ) : (
          <Check color={colors[2]} value={data.Elite} />
        )}
      </td>
      {/* <td className="px-4 py-2 text-center">
        {typeof data.Enterprise === "string" ? (
          data.Enterprise
        ) : (
          <Check color={colors[3]} value={data.Enterprise} />
        )}
      </td> */}
    </tr>
  );
};

const Check = ({ value, color }) => {
  return !value ? (
    <div className="text-center">
      <svg
        className="w-4 m-auto"
        id="a"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 11.56 11.56"
        style={{ stroke: color }}
      >
        <line
          x1="10.66"
          y1=".9"
          x2=".9"
          y2="10.66"
          strokeLinecap="round"
          strokeMiterlimit="10"
          strokeWidth="1.8"
        />
        <line
          x1=".9"
          y1=".9"
          x2="10.66"
          y2="10.66"
          strokeLinecap="round"
          strokeMiterlimit="10"
          strokeWidth="1.8"
        />
      </svg>
    </div>
  ) : (
    <div>
      <svg
        className="w-4 m-auto"
        id="a"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 16.79 12.06"
        style={{ stroke: color }}
      >
        <polyline
          points="15.89 .9 5.63 11.16 .9 6.43"
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="1.8"
        />
      </svg>
    </div>
  );
};

const PlanTable = ({ colors }) => {
  const tableData = [
    {
      PackageType: "Monthly Fees",
      Freemium: "0",
      VIP: "30",
      Elite: "45 ",
      Enterprise: "Request",
    },
    {
      PackageType: "Yearly Fees",
      Freemium: "0",
      VIP: "300",
      Elite: "450 ",
      Enterprise: "Request",
    },
    {
      PackageType: "No App Needed",
      Freemium: true,
      VIP: true,
      Elite: true,
      Enterprise: true,
    },
    {
      PackageType: "Business Cards / Profiles",
      Freemium: "2",
      VIP: "3",
      Elite: "6",
      Enterprise: "Request",
    },
    {
      PackageType: "Customizable Digital Card Design Template (i)",
      Freemium: false,
      VIP: true,
      Elite: true,
      Enterprise: true,
    },
    {
      PackageType: "Custom Profile Colors",
      Freemium: false,
      VIP: true,
      Elite: true,
      Enterprise: true,
    },
    {
      PackageType: "Personal Profile Video",
      Freemium: false,
      VIP: false,
      Elite: true,
      Enterprise: true,
    },
    {
      PackageType: "PDF Uploads",
      Freemium: false,
      VIP: false,
      Elite: true,
      Enterprise: true,
    },
    {
      PackageType: "Email Signatures",
      Freemium: false,
      VIP: true,
      Elite: true,
      Enterprise: true,
    },
    {
      PackageType: "Personalized Links",
      Freemium: false,
      VIP: false,
      Elite: true,
      Enterprise: true,
    },
    {
      PackageType: "Shareable QR Code",
      Freemium: true,
      VIP: true,
      Elite: true,
      Enterprise: true,
    },
  ];
  return (
    <div className="container" >
      <div className="relative overflow-x-auto  sm:rounded-lg">
        <table className="w-full text-sm text-left bg-white ">
          <thead className="text-xs text-gray-700 uppercase bg-white dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3 text-center text-black">
                {_("Package Type")}
              </th>
              <th
                scope="col"
                className="px-6 py-3 text-center text-white  "
                style={{ backgroundColor: colors[0] }}
              >
                {_("Freemium")}
              </th>
              <th
                scope="col"
                className="px-6 py-3 text-center text-white "
                style={{ backgroundColor: colors[1] }}
              >
                {_("VIP")}
              </th>
              <th
                scope="col"
                className="px-6 py-3 text-center text-white "
                style={{ backgroundColor: colors[2] }}
              >
                {_("Elite")}
              </th>
              {/* <th
                scope="col"
                className="px-6 py-3 text-center text-white "
                style={{ backgroundColor: colors[3] }}
              >
                Enterprise
              </th> */}
            </tr>
          </thead>
          <tbody>
            {tableData.map((val, index) => {
              return (
                <Row key={index} data={val} colors={colors} index={index} />
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Plans;
