import { translation } from "../../lib/websiteTranslations";
import { useRouter } from "next/router";

const Features = () => {
  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  return (
    <div>
      <div className="container ">
        {/* <!--Section 1--> */}
        {/* <section className="section-1 w-full h-auto pt-28">
        <div className="px-10 sm:px-0">
          <div className="w-full mx-auto flex flex-col sm:flex-row justify-between items-center">
            <div className="w-full sm:w-1/2 h-full items-center !p-6 mx-auto">
              <h2 className="text-left md:text-left text-3xl text-[26px] !p-1 text-green md:text-4xl font-bold !pb-12 w-full">
                Tranform The Way You Share Your Details
              </h2>
              <p className="text-white text-left md:text-left font-light !p-1 md:text-lg text-white">
                Wave Your Card, Connect, & Share Details Instantly
              </p>
              <p className="text-left md:text-left font-light !p-1 md:text-lg text-white">
                Say goodbye to outdated paper cards and take the lead in the new
                era of digital business cards. With just a wave of your card,
                instantly connect and share your details with anyone.
              </p>
              <p className="text-left md:text-left font-light !p-1 md:text-lg text-white">
                Experience seamless networking like never before with much.sa's
                digital business card solutions.
              </p>
            </div>
            <div className="w-full sm:w-1/2 h-full flex flex-col mx-auto items-center pb-5">
              <div className="video-1 h-auto w-64 lg:w-80">
                <video
                  className="rounded-[60px]"
                  autoplay
                  loop
                  muted
                  src="https://dl.dropboxusercontent.com/s/ia26motmxja3f71/qr-share_optimized.mp4?dl=0"
                ></video>
              </div>
            </div>
          </div>
        </div>
      </section> */}
        <section className="section-1 w-full h-auto pt-28 text-justify">
          <div className="px-2 md:px-10">
            <div className="w-full mx-auto flex flex-col-reverse sm:flex-row justify-between items-center">
              <div className="w-full sm:w-1/2  h-full items-center !p-4 mx-auto">
                <h2 className="text-[center] md:text-left text-3xl text-[26px] !p-1 text-green md:text-4xl font-bold !pb-4 w-full ">
                  {_("Tranform The Way You Share Your Details")}
                </h2>
                <p className="text-white md:text-justify   font-bold !p-1 md:text-[24px] text-[center]">
                  {_("Wave Your Card, Connect, & Share Details Instantly")}
                </p>
                <p className="text-[center] md:text-justify font-light !p-1 md:text-lg text-white m-0">
                  {_(
                    "Say goodbye to outdated paper cards and take the lead in the new era of digital business cards. With just a wave of your card, instantly connect and share your details with anyone."
                  )}
                </p>
                <p className="text-[center] md:text-justify font-light !p-1 md:text-lg text-white">
                  {_(
                    "Experience seamless networking like never before with much.sa's digital business card solutions."
                  )}
                </p>
              </div>
              <div className="w-full sm:w-1/2 h-full flex flex-col mx-auto items-center pb-5">
                <div className="video-1 h-auto w-64 lg:w-80 relative">
                  <img
                    className="w-36 lg:w-60 absolute right-3/4 -to!p-1 lg:right-64 lg:top-16 animate-[popup_10s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/1.png"
                    alt=""
                  />
                  <img
                    className="w-36 lg:w-60 absolute left-3/4 -bottom-5 lg:left-64 lg:bottom-16 animate-[popup_10s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/2.png"
                    alt=""
                  />

                  <video
                    className="rounded-[60px]"
                    // controls
                    muted
                    loop
                    autoPlay
                    playsInline
                  >
                    <source
                      src="https://pioneers.network/much/nfc.m4v"
                      type="video/mp4"
                    />
                  </video>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!--Section 2--> */}

        <section className="section-2 w-full h-auto pt-28">
          <div className="px-10 sm:px-0">
            <div className="w-full mx-auto flex flex-col sm:flex-row justify-between items-center">
              <div className="w-full sm:w-1/2 h-full flex flex-col mx-auto items-center pb-5">
                <div className="video-1 h-auto w-64 lg:w-80 relative">
                  <img
                    className="w-16  lg:w-24 absolute right-[80%] -top-1 lg:right-64 lg:top-16 animate-[popup_7s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/4.png"
                    alt=""
                  />
                  <img
                    className="  w-16 lg:w-24 absolute left-[90%] -bottom-[-16rem] lg:left-64 lg:bottom-16 animate-[popup_10s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/5.png"
                    alt=""
                  />
                  <img
                    className=" w-16 lg:w-24 absolute right-[80%]  bottom-0 lg:right-64 lg:bottom-16 animate-[popup_5s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/3.png"
                    alt=""
                  />
                  <video
                    className="rounded-[60px]"
                    muted
                    loop
                    autoPlay
                    playsInline
                  >
                    <source
                      src="https://pioneers.network/much/noapp.m4v"
                      type="video/mp4"
                    />
                  </video>
                </div>
              </div>
              <div className="w-full sm:w-1/2 h-full items-center !p-4 mx-auto">
                <h2 className="text-center md:text-justify text-3xl text-[26px] !p-1 text-green md:text-4xl font-bold !pb-4 w-full">
                  {_("No App? No Problem!")}
                </h2>
                <p className="text-center md:text-justify font-light !p-1 md:text-lg text-white">
                  {_(
                    "Share your business details with potential clients with just a tap using an app free digital business card solution offered by much.sa"
                  )}
                </p>
              </div>
            </div>
          </div>
        </section>

        {/* <!--Section 3--> */}

        <section className="section-3 w-full h-auto pt-28">
          <div className="px-0">
            <div className="w-full mx-auto flex flex-col-reverse sm:flex-row justify-between items-center">
              <div className="w-full sm:w-1/2 h-full items-center !p-4 mx-auto">
                <h2 className="text-center md:text-justify text-3xl text-[26px] !p-1 text-green md:text-4xl font-bold !pb-4 w-full">
                  {_("Track Your Data Like A Pro")}
                </h2>
                <p className="text-center md:text-justify font-light !p-1 md:text-lg text-white">
                  {_(
                    "With much.sa's advanced analytics dashboard, track your digital business card's performance and gain valuable insights on connections made, meeting locations, profile views, and other data."
                  )}
                </p>
              </div>
              <div className="w-full sm:w-1/2 h-full flex flex-col mx-auto items-center pb-5">
                <div className="video-1 h-auto w-64 lg:w-80 relative">
                  <img
                    className="   w-16  lg:w-24 absolute right-[80%] -top-1 lg:right-64 lg:top-16 animate-[popup_7s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/6.png"
                    alt=""
                  />
                  <img
                    className="   w-16 lg:w-24 absolute left-[90%] -bottom-[-16rem] lg:left-64 lg:bottom-16 animate-[popup_10s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/7.png"
                    alt=""
                  />
                  <img
                    className="  w-16 lg:w-24 absolute right-[80%]  bottom-0 lg:right-64 lg:bottom-16 animate-[popup_5s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/8.png"
                    alt=""
                  />

                  <video
                    className="rounded-[60px]"
                    // controls
                    muted
                    loop
                    autoPlay
                    playsInline
                  >
                    <source
                      src="https://pioneers.network/much/db.mp4"
                      type="video/mp4"
                    />
                  </video>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!--Section 4--> */}

        {/* <section className="section-2 w-full h-auto pt-28">
          <div className="px-0">
            <div className="w-full mx-auto flex flex-col-reverse sm:flex-row justify-between items-center">
              <div className="w-full sm:w-1/2 h-full flex flex-col mx-auto items-center pb-5">
                <div className="video-1 h-auto w-64 lg:w-80 relative">
                  <img
                    className=" w-20  lg:w-32 absolute right-[80%] -to!p-1 lg:right-64 lg:top-16 animate-[popup_10s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/3.png"
                    alt=""
                  />
                  <img
                    className=" w-20 lg:w-32 absolute left-[90%] -bottom-[-16rem] lg:left-64 lg:bottom-16 animate-[popup_10s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/4.png"
                    alt=""
                  />
                  <img
                    className="w-20 lg:w-32 absolute right-[80%] -bottom-5 lg:left-64 lg:bottom-16 animate-[popup_10s_ease-in-out_infinite]"
                    src="/images/new/imgmuch/5.png"
                    alt=""
                  />

                  <video
                    className="rounded-[60px]"
                    // controls
                    muted
                    loop
                    autoPlay
                    src="https://pioneers.network/much/nfc.m4v"
                  ></video>
                </div>
              </div>
              <div className="w-full sm:w-1/2 h-full items-center !p-6 mx-auto">
                <h2 className="text-left md:text-left text-3xl text-[26px] !p-1 text-green md:text-4xl font-bold !pb-4 w-full">
                  {_("One-Tap Secure Data Sharing")}
                </h2>
                <p className="text-left md:text-left font-light !p-1 md:text-lg text-white">
                  {_(
                    "Sharing your contact info and data is just one tap away with much.sa's NFC digital business cards and NFC tags. Advanced NFC technology that ensures secure and efficient data transfer for expanding your network."
                  )}
                </p>
              </div>
            </div>
          </div>
        </section> */}


      </div>
    </div>
  );
};

export default Features;
