import { translation } from "../../lib/websiteTranslations";
import { useRouter } from "next/router";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

const Marquee = () => {
  const array = [
    {
      img: "https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg",
    },
    {
      img: "https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg",
    },
    {
      img: "https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg",
    },
    {
      img: "https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg",
    },
    {
      img: "https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg",
    },
    {
      img: "https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg",
    },
  ];
  return (
    <div className="container mt-40">
      <div className="relative flex overflow-x-hidden  ">
        <div className="py-12 animate-marquee whitespace-nowrap flex">
          {/* <span className="text-4xl mx-4">
            <img
              className="text-4xl mx-4 "
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span>
          <span className="text-4xl mx-4">
            <img
              className="text-4xl mx-4"
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span>
          <span className="text-4xl mx-4">
            <img
              className="text-4xl mx-4"
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span>
          <span className="text-4xl mx-4">
            <img
              className="text-4xl mx-4"
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span> */}
          {array.map((val, index) => {
            return (
              <span key={index} className="text-4xl mx-4">
                <img className="text-4xl mx-4" src={val.img} />
              </span>
            );
          })}
        </div>

        <div className="absolute top-0 py-12 animate-marquee2 whitespace-nowrap flex">
          {/* <span className="text-4xl mx-4">
            <img
              className="text-4xl mx-4"
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span>
          <span className="text-4xl mx-4">
            <img
              className="text-4xl mx-4"
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span>
          <span className="text-4xl mx-4">
            {" "}
            <img
              className="text-4xl mx-4"
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span>
          <span className="text-4xl mx-4">
            {" "}
            <img
              className="text-4xl mx-4"
              src="https://assets-global.website-files.com/60b64e5e7e0ea41f8673bebb/61c091153d1b138ed9ada3ca_Group%201022.svg"
            />
          </span>
          <span className="text-4xl mx-4">Marquee Item 4</span> */}
          {array.map((val, index) => {
            return (
              <span key={index} className="text-4xl mx-4">
                <img className="text-4xl mx-4" src={val.img} />
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Marquee;
