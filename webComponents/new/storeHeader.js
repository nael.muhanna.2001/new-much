import { useCallback, useEffect, useState } from "react";
import { BsList, BsX } from "react-icons/bs";

import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import NavRes from "./components/nav";
import Link from "next/link";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { translation } from "../../lib/translations";

const BootstrapCarousel = (items) => {

  const [index, setIndex] = useState(0);
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <Carousel
      controls={false}
      indicators={false}
      className="!absolute top-0 left-0 "
      activeIndex={index}
      onSelect={handleSelect}
      //   className="absolute top-0 left-0  min-w-full min-h-screen max-h-screen object-cover brightness-50"
    >
      {items.items.map((item) => (
        <Carousel.Item key={item.id} className="!h-[300px]" interval={4000}>
          <div className="h-[300px] !w-screen">
            <div
              style={{
                backgroundPositionX: "center",
                background:
                  "linear-gradient(to  bottom, rgba(0, 0, 0, 0.0),rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 1)) 0% 50% / cover, url(" +
                  item.imageUrl +
                  ") 50% 50%",
              }}
              className="!h-full object-cover"
              alt="slides"
            ></div>
          </div>
          <Carousel.Caption className="top-1/4   rounded-lg h-fit">
            <h1 className="mb-4  font-boldold text-[28px]  md:text-[40px]">
              {item.title}
            </h1>
            <h3 className="mb-4 font-semibold text-[18px] md:text-[24px]">
              {item.sub}
            </h3>
            <p className=" text-[14px] md:text-[20px] ">{item.body}</p>
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  );
};

const Header = ({ refresh, setRefresh , items }) => {
 
  const [open, setOpen] = useState(false);
  const [scrollY, setScrollY] = useState(0);
  const { data: session, status } = useSession();
  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener(
        "scroll",
        (_) => setScrollY(document.body.scrollTop),
        true
      );
    }
  }, []);

  const [loading, setLoading] = useState(false);
  const [itemCount, setItemCount] = useState(0);
  const [theHeight, setTheHeight] = useState(0);

  useEffect(() => {
    setLoading(true);
    let user = null;
    if (session) {
      user = session.user;
    } else {
      if (typeof window != "undefined") {
        user = JSON.parse(window.localStorage.getItem("user"));
      }
    }

    fetch("/api/orders/my-orders/", {
      method: "post",
      body: JSON.stringify({ user: user }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.length > 0) {
          setTimeout(() => {
            setTheHeight(1);
          }, 500);
        }
        setItemCount(data.length);
        setLoading(false);
      });
  }, [refresh]);

  return (
    <>
      <NavRes open={open} setOpen={setOpen} />
      <header className="container mx-auto ">
        <div className="containers mx-auto flex flex-col w-full h-[300px]">
          <div
            className={
              scrollY > 450
                ? "fixed left-0 w-full z-[52] bg-blue-900 duration-200 h-[64px] bg-opacity-90"
                : "fixed left-0 w-full z-[52] duration-200"
            }
          >
            <nav
              className={
                " w-full  container  mx-auto py-2  lg:px-3 flex flex-row justify-between h-fit px-1 "
              }
            >
              <div className="">
                <Link href="/">
                  <img
                    style={{ filter: "brightness(100)" }}
                    className="h-6 mt-3 w-auto"
                    src="/images/new/logo.svg"
                    alt="logo"
                  />
                </Link>
              </div>

              <div className="h-fit flex">
                <div className="register mt-1 flex justify-end mx-2">
                  <div
                    className="h-fit rounded-full py-2 pl-3 pr-3  !bg-black !bg-opacity-20 text-white hover:!text-green"
                    //   style={{ backgroundColor: "rgba(255, 255, 255, 0.205)" }}
                    href=""
                  >
                    {!session && (
                      <Link href={`/signin`}>
                        <span className="text-white">{_("Log In")}</span>
                      </Link>
                    )}
                    {session?.user && (
                      <>
                        {session.user && (
                          <Link href={`/dashboard/`}>
                            <span className="text-white">{_("Dashboard")}</span>
                          </Link>
                        )}
                      </>
                    )}
                    <i className="fa fa-chevron-right px-2"></i>
                  </div>
                </div>
                { itemCount > 0 &&  <Link href="/store/cart">
                  <div className=" text-center py-3  mx-4 relative">
                    <div className="absolute bg-blue-900 -top-1 -right-1 rounded-full w-6 h-6 text-center text-white font-bold text-xs py-1">
                      {loading && (
                        <i className="fa fa-spinner animate-spin"></i>
                      )}
                      {!loading && <span>{itemCount}</span>}
                    </div>
                    <i className=" text-white fa fa-shopping-cart text-2xl"></i>
                  </div>
                </Link>}
                <a
                  onClick={() => {
                    setOpen(!open);
                  }}
                  className="mx-0 lg:mx-4 -mt-1 hover:!text-green !text-white"
                >
                  {open ? (
                    <BsX className="text-4xl mt-2 " />
                  ) : (
                    <BsList className="text-4xl mt-2 " />
                  )}
                </a>
              </div>
            </nav>
          </div>

          <BootstrapCarousel items={items} />
        </div>
      </header>
    </>
  );
};

export default Header;
