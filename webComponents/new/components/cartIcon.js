import { useSession } from "next-auth/react";
import Link from "next/link";
import { useEffect, useState } from "react";

export default function CartIcon({ flag }) {
  const [itemCount, setItemCount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [theHeight, setTheHeight] = useState(0);
  const { data: session, status } = useSession();
  useEffect(() => {
    
    setLoading(true);
    let user = null;
    if (session) {
      user = session.user;
    } else {
      if (typeof window != "undefined") {
        user = JSON.parse(window.localStorage.getItem("user"));
      }
    }

    fetch("/api/orders/my-orders/", {
      method: "post",
      body: JSON.stringify({ user: user }),
    })
      .then((res) => res.json())
      .then((data) => {
        if(data.length>0)
        {
            setTimeout(() => {
                setTheHeight(1);
              }, 500);
        }
        setItemCount(data.length);
        setLoading(false);
      });
  }, [flag]);
  return (
    <>
      {itemCount > 0 && (
        <div className="fixed bottom-20 right-8 duration-500" style={{right:theHeight==0?'-200px':''}}>
          <Link href="/store/cart">
            <div className="h-[70px] w-[70px] bg-green text-center py-4 rounded-full text-blue-900 shadow relative">
              <div className="absolute bg-blue-900 -top-1 -right-1 rounded-full w-6 h-6 text-center text-white font-bold text-xs py-1">
                {loading && <i className="fa fa-spinner animate-spin"></i>}
                {!loading && <span>{itemCount}</span>}
              </div>
              <i className="fa fa-shopping-cart text-2xl"></i>
            </div>
          </Link>
        </div>
      )}
    </>
  );
}
