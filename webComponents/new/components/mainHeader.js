import { useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { BsList, BsX } from "react-icons/bs";
import { translation } from "../../../lib/translations";
import NavRes from "./nav";

const Header = () => {
    function _(text) {
        const { locale } = useRouter();
         if (translation[locale][text]) {
           return translation[locale][text];
         }
         return text;
       }

    const [open, setOpen] = useState(false);
    const [scrollY, setScrollY] = useState(0);
    const { data: session, status } = useSession();
  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener(
        "scroll",
        (_) => setScrollY(document.body.scrollTop),
        true
      );
    }
  }, []);
    return (
      <>
        <NavRes open={open} setOpen={setOpen} />
        <header className="container mx-auto  h-16">
          <div className="containers mx-auto flex flex-col w-full  h-16">
          <div
            className={
              scrollY > 450
                ? "fixed left-0 w-full z-[52] bg-blue-900 duration-200 h-[64px] bg-opacity-90"
                : "fixed left-0 w-full z-[52] duration-200"
            }
          >
            <nav
              className={
                " w-full  container  mx-auto py-2  lg:px-3 flex flex-row justify-between h-fit px-1 "
              }
            >
                <div className="pt-2">
                  <Link href="/">
                    <img
                      style={{ filter: "brightness(100)" }}
                      className="h-6 w-auto"
                      src="/images/new/logo.svg"
                      alt="logo"
                    />
                  </Link>
                </div>
                <div className="h-fit flex">
                  <div className="register flex justify-end mx-2">
                    <div
                      className="h-fit rounded-full py-2 pl-3 pr-3 mt-2 !bg-black !bg-opacity-20 text-white hover:!text-green"
                      
                    >
                      {!session && <Link href={`/signin`}><span className="text-white">{_("Log In")}</span></Link>}
                    {session?.user && (
                      <>
                        {session.user && (
                          <Link href={`/dashboard/`}><span className="text-white">{_("Dashboard")}</span></Link>
                        )}
                      </>
                    )}<i className="fa fa-chevron-right px-2"></i>
                    </div>
                  </div>
                  <a
                    onClick={() => {
                      setOpen(!open);
                    }}
                    className="mx-0 lg:mx-4 hover:!text-green !text-white"
                  >
                    {open ? (
                      <BsX className="text-4xl mt-2 " />
                    ) : (
                      <BsList className="text-4xl mt-2 " />
                    )}
                  </a>
                </div>
              </nav>
            </div>
          </div>
        </header>
      </>
    );
  };
  export default Header;