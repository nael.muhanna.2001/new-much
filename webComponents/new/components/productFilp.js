import { useState } from "react";
import ReactCardFlip from "react-card-flip";

export default function ProductFilp({ product }) {
  const [flip, setFlip] = useState(false);
  return (
    <>
      {product.cardFront && product.cardBack && (
        <div className="">
          <ReactCardFlip isFlipped={flip}>
            <img
              className="max-w-[100px] mx-auto"
              src={product.cardFront.imgUrl}
              onMouseEnter={() => {
                setFlip(!flip);
              }}
            />
            <img
              className="max-w-[100px] mx-auto"
              src={product.cardBack.imgUrl}
              onMouseEnter={() => {
                setFlip(!flip);
              }}
            />
          </ReactCardFlip>
        </div>
      )}
      {product.cardFront && !product.cardBack && (
        <img
          className="max-w-[100px] mx-auto"
          src={product.cardFront.imgUrl}
          onMouseEnter={() => {
            setFlip(!flip);
          }}
        />
      )}
      {!product.cardFront && product.cardBack && (
        <img
          className="max-w-[100px] mx-auto"
          src={product.cardBack.imgUrl}
          onMouseEnter={() => {
            setFlip(!flip);
          }}
        />
      )}
      {(!product.cardFront && !product.cardBack && product.images && product.images.length>0) && (
        <img
          className="max-w-[100px] mx-auto"
          src={product.images[0].imgUrl}
          onMouseEnter={() => {
            setFlip(!flip);
          }}
        />
      )}
      {/*  */}
    </>
  );
}
