import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { translation } from "../../../lib/translations";
import ProductFilp from "./productFilp";

export default function CartRow({ order, index, setQty, removeOrder,checkedout }) {
  
  function changeQuantity(val) {
    setQty(index, order.quantity*1+val);
    
  }
  //====== Translation Function =======//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  const { locale } = useRouter();
  return (
    <div className="">
      <div
        className={`flex flex-wrap relative ${
          index % 2 == 0 ? "bg-gray-100" : ""
        }  border-b min-h-[76px]`}
      >
        <div className="w-full lg:w-2/12 p-4  overflow-hidden">
          <ProductFilp product={order.product} />
        </div>
        <div className="w-full lg:w-4/12 text-gray-500 px-4 text-center lg:py-10">
          {order.product.name[locale]
            ? order.product.name[locale]
            : order.product.name["en"]}
        </div>
        <div className="w-full lg:w-2/12 p-2 flex lg:block">
          <div className="mx-auto text-center my-[4px]">
            {!checkedout &&<button
              onClick={() => {
                changeQuantity(1);
              }}
              className="h-6 w-6 p-[2px] text-center rounded-full text-gray-800 mx-auto bg-gray-200"
            >
              <i className="fa fa-plus"></i>
            </button>}
          </div>
          <input disabled={checkedout}
            type="number"
            min="1"
            max="100"
            step={1}
            onChange={(e) => {
              setQty(index, e.target.value);
            }}
            value={order.quantity}
            className={`form-control pl-[20px]  min-w-8 mx-auto text-center max-w-[60px] px-0 font-bold ${checkedout?'my-4 bg-transparent border-none':''}`}
          />

          <div className="mx-auto text-center my-[4px]">
          {!checkedout && order.quantity>1 &&<button
            onClick={() => {
              changeQuantity(-1);
            }}
            className="h-6 w-6 p-[2px] text-center rounded-full text-gray-800 mx-auto bg-gray-200"
          >
            <i className="fa fa-minus"></i>
          </button>}
          {!checkedout && order.quantity==1 &&<button
            onClick={() => {
              removeOrder(index);
            }}
            className="h-6 w-6 p-[2px] text-center rounded-full text-red-800 mx-auto bg-gray-200"
          >
            <i className="fa fa-times"></i>
          </button>}
          </div>
        </div>
        <div className="w-full lg:w-2/12  text-gray-500 text-center lg:py-12 font-bold">
          {Number(order.quantity * order.product.price).toFixed(2)}{" "}
          {_(order.product.currency)}
        </div>
        <div className="w-0 p-2 lg:p-5 text-center">
          <div className="text-center mx-auto absolute lg:relative top-10 right-5">
            {!checkedout &&<button
              onClick={() => {
                removeOrder(index);
              }}
              className="h-6 w-6 p-[2px] text-center rounded-full text-red-600 mx-auto bg-gray-200 font-bold"
            >
              <i className="fa fa-times "></i>
            </button>}
          </div>
        </div>
      </div>
    </div>
  );
}
