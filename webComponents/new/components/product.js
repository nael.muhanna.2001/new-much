
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Carousel } from "react-bootstrap";
import ReactCardFlip from "react-card-flip";
import { translation } from "../../../lib/translations";

export default function Product({ product: product }) {
  const { locale } = useRouter();
  const [height,setHeight]=useState(0);
  const [productCard,setProductCard]=useState({});
  //======= Translation =========//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  useEffect(()=>{
    setTimeout(()=>{setHeight(100)},200);
  },[])
  //====== Flip Card =======
  function flipCard(id)
  {
    setProductCard(prev=>{
        let cards = prev;
        cards[id]=!cards[id];
        return {...cards}
    })
  }
  return (
    <>
      <div
        className="relative border-b-2 border-b-emerald-300 min-h-fit m-3  mx-4 max-w-xs duration-100 overflow-hidden"
        style={{ height: height+"%",opacity:(height/100) }}
      >
        {(product.cardFront && product.cardBack) && (
          <ReactCardFlip  isFlipped={productCard[product._id]}>
            <img  src={product.cardFront.imgUrl} onMouseOver={()=>{flipCard(product._id)}}  />
            <img src={product.cardBack.imgUrl} onMouseOver={()=>{flipCard(product._id)}}  />
          </ReactCardFlip>
        )}
        {(product.cardFront || product.cardBack) && !(product.cardFront && product.cardBack) && (
          <Carousel fade controls={false} indicators={false}>
            {product.cardFront && (
              <Carousel.Item className="max-h-[200px]">
                <img
                  className="mx-auto"
                  
                  src={product.cardFront.imgUrl}
                />
              </Carousel.Item>
            )}
            {product.cardBack && (
              <Carousel.Item className="max-h-[200px]">
                <img
                  className="mx-auto"
                  
                  src={product.cardBack.imgUrl}
                />
              </Carousel.Item>
            )}
          </Carousel>
        )}
        {!product.cardFront &&
          !product.cardBack &&
          product.images &&
          product.images.length > 0 && (
            <Carousel
              fade
              controls={false}
              indicators={false}
            >
              {product.images.map((img) => {
                return (
                  <Carousel.Item className="max-h-[200px]">
                    <img
                      className="mx-auto"
                      
                      src={img.imgUrl}
                    />
                  </Carousel.Item>
                );
              })}
            </Carousel>
          )}
        {product.name && (
          <Link href={"/store/product/"+product._id}>
          <h1 className="px-1 text-start text-lg  max-w-full overflow-ellipsis overflow-hidden my-2 text-gray-600">
            {product.name[locale] ? product.name[locale] : product.name["en"]}
          </h1>
          </Link>
        )}
        {product.shortDescription && (
          <p className="px-1 text-start text-md  max-w-full overflow-ellipsis overflow-hidden text-gray-700 my-2">
            {product.shortDescription[locale]
              ? product.shortDescription[locale]
              : product.shortDescription["en"]}
          </p>
        )}
        <div className="absolute bottom-1 left-0">
        {product.price!=product.originalPrice &&<div >
                <span className=" w-full line-through text-gray-400">{Number(product.originalPrice).toFixed(2)} </span>
                <span className="font-bold text-red-700 px-2">{Number(product.price).toFixed(2)} {product.currency}</ span>
                </div>}
                {product.price==product.originalPrice &&<div className="font-bold text-blue-900">
                {Number(product.price).toFixed(2)} {product.currency}
                </div>}
        </div>
        <div className="absolute bottom-0 right-0 select-none">
            <Link href={"/store/product/"+product._id}>
          <button className="text-xs font-semibold text-blue-900 bg-emerald-300 p-2 rounded-t-md hover:bg-blue-900 hover:text-emerald-300 duration-300">
            {_("View")} <i className="fa fa-arrow-right"></i>
          </button>
          </Link>
        </div>
      </div>
    </>
  );
}
