import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { translation } from "../../../lib/translations";

export default function NavRes({ open, setOpen }) {
  const { locale } = useRouter();

  //======= Translation =========//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  return (
    <div
      className="fixed transition-all duration-700 left-0 z-[51] w-full bg-maincolor opacity-90  "
      style={{ top: open ? "0px" : "-800px", height: open ? "100%" : "auto" }}
    >
      <nav>
        <div className="flex flex-col text-center p-2 pt-24">
          <ul className="mt-36">
            <li className="p-2">
              <Link
                onClick={() => {
                  setOpen(!open);
                }}
                className="hover:!text-green !text-white text-[40px] font-bold"
                href="/"
              >
                {_("Home")}
              </Link>
            </li>
            <li className="p-2">
              <Link
                onClick={() => {
                  setOpen(!open);
                }}
                className="hover:!text-green !text-white text-[40px] font-bold"
                href="/store"
              >
                {_("Store")}
              </Link>
            </li>
            {/* <li className="p-2">
                <Link
                onClick={() => {
                    setOpen(!open);
                  }}
                  className="hover:!text-green !text-white text-[40px] font-bold"
                  href="/#pricing"
                >
                  {_('Pricing')}
                </Link>
              </li> */}
            {/* <li className="p-2">
                <Link
                onClick={() => {
                    setOpen(!open);
                  }}
                  className="hover:!text-green !text-white text-[40px] font-bold"
                  href="enterprise"
                >
                {_('Enterprise')}
                </Link>
              </li> */}
            <li className="p-2">
              <Link
                onClick={() => {
                  setOpen(!open);
                }}
                className="hover:!text-green !text-white text-[40px] font-bold"
                href="/about-us"
              >
                {_("About Us")}
              </Link>
            </li>
            <li className="p-2">
              <Link
                onClick={() => {
                  setOpen(!open);
                }}
                className="hover:!text-green !text-white text-[40px] font-bold"
                href="/teams"
              >
                {_("Our Team")}
              </Link>
            </li>
            <li className="p-2">
              <Link
                onClick={() => {
                  setOpen(!open);
                }}
                className="hover:!text-green !text-white text-[40px] font-bold"
                href="/contact"
              >
                {_("Contact Us")}
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}
