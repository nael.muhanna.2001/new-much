import { useEffect, useState } from "react";
import Link from "next/link";
import NavRes from "./nav";
import { BsList, BsX } from "react-icons/bs";
import { useRouter } from "next/router";
import { translation } from "../../../lib/translations";
import { useSession } from "next-auth/react";

const Header = ({ refresher }) => {
  const [open, setOpen] = useState(false);
  const [scrollY, setScrollY] = useState(0);
  const { locale } = useRouter();
  const { data: session, status } = useSession();
  //======= Translation =========//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener(
        "scroll",
        (_) => setScrollY(document.body.scrollTop),
        true
      );
    }
  }, []);

  const [loading, setLoading] = useState(false);
  const [itemCount, setItemCount] = useState(0);
  const [theHeight, setTheHeight] = useState(0);

  useEffect(() => {
    setLoading(true);
    let user = null;
    if (session) {
      user = session.user;
    } else {
      if (typeof window != "undefined") {
        user = JSON.parse(window.localStorage.getItem("user"));
      }
    }

    fetch("/api/orders/my-orders/", {
      method: "post",
      body: JSON.stringify({ user: user }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.length > 0) {
          setTimeout(() => {
            setTheHeight(1);
          }, 500);
        }
        setItemCount(data.length);
        setLoading(false);
      });
  }, [refresher]);

  return (
    <>
      <NavRes open={open} />
      <header className="container mx-auto ">
        <div className="containers mx-auto flex flex-col w-full h-[64px]">
          <div
            className={
              scrollY >= 0
                ? "fixed left-0 w-full z-[52] bg-[#00487D] duration-200 h-[64px]"
                : "fixed left-0 w-full z-[52] duration-200"
            }
          >
            <nav
              className={
                " w-full  container  mx-auto py-2  lg:px-3 flex flex-row justify-between h-fit px-1 "
              }
            >
              <div className="flex">
                <Link href="/store">
                  <button className="text-white  py-0 mr-2 lg:-ml-[35px] px-2 h-12 border rounded">
                    <i className="fa fa-arrow-left  text-2xl"></i>{" "}
                    <div className="text-xs hidden md:block">
                      {_("Back to store")}
                    </div>
                  </button>
                </Link>
                <Link href="/">
                  <img
                    style={{ filter: "brightness(100)" }}
                    className="h-6 mt-3 w-auto"
                    src="/images/new/logo.svg"
                    alt="logo"
                  />
                </Link>
              </div>

              <div className="h-fit flex">
                <div className="register mt-1 flex justify-end mx-2">
                  <div
                    className="h-fit rounded-full py-2 pl-3 pr-3  !bg-black !bg-opacity-20 text-white hover:!text-green"
                    //   style={{ backgroundColor: "rgba(255, 255, 255, 0.205)" }}
                    href=""
                  >
                    {!session && (
                      <Link href={`/signin`}>
                        <span className="text-white">{_("Log In")}</span>
                      </Link>
                    )}
                    {session?.user && (
                      <>
                        {session.user && (
                          <Link href={`/dashboard/`}>
                            <span className="text-white">{_("Dashboard")}</span>
                          </Link>
                        )}
                      </>
                    )}
                  </div>
                </div>
                { itemCount > 0 &&  <Link href="/store/cart">
                  <div className=" text-center py-3  mx-4 relative">
                    <div className="absolute bg-blue-900 -top-1 -right-1 rounded-full w-6 h-6 text-center text-white font-bold text-xs py-1">
                      {loading && (
                        <i className="fa fa-spinner animate-spin"></i>
                      )}
                      {!loading && <span>{itemCount}</span>}
                    </div>
                    <i className=" text-white fa fa-shopping-cart text-2xl"></i>
                  </div>
                </Link>}
                <a
                  onClick={() => {
                    setOpen(!open);
                  }}
                  className="mx-0 lg:mx-4 -mt-1 hover:!text-green !text-white"
                >
                  {open ? (
                    <BsX className="text-4xl mt-2 " />
                  ) : (
                    <BsList className="text-4xl mt-2 " />
                  )}
                </a>
              </div>
            </nav>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
