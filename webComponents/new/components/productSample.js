import Link from "next/link";
import { useRouter } from "next/router"

export default function ProductSample({product:product})
{
    const {locale} =useRouter();
    return <>
    <Link href={"/store/product/"+product._id}>
        <div className="max-w-[150px] card cursor-pointer z-0">
    <div className="w-full border-b border-b-emerald-300">
        {product.cardFront&&<div className="w-full h-24" style={{backgroundImage:'url('+product.cardFront.imgUrl+')',backgroundSize:'contain',backgroundRepeat:'no-repeat'}} ></div>}
        {(!product.cardFront&& product.images && product.images.length>0) &&<div className="w-full h-24" style={{backgroundImage:'url('+product.images[0].imgUrl+')',backgroundSize:'contain',backgroundRepeat:'no-repeat'}} ></div>}
    </div>
    <div className="card-body my-0 py-0 px-1">
    <p className="text-[10px] max-w-full text-gray-700 whitespace-nowrap my-0 py-0 normal-case overflow-hidden overflow-ellipsis font-bold">{product.name[locale]?product.name[locale]:product.name['en']}</p>
    <p className="text-[12px] text-red-900  font-bold">{product.price} {product.currency}</p>
    </div>
    </div>

    </Link>
    </>

}