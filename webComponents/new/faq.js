import { useRive, Layout } from "@rive-app/react-canvas";
import { translation } from "../../lib/websiteTranslations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const Accordion = (props) => {
  const [isShowing, setIsShowing] = useState(false);

  const toggle = () => {
    setIsShowing(!isShowing);
  };

  return (
    <div
      className="rounded-xl p-4"
      style={{
        width: "100%",
        marginBottom: "15px",
        lineHeight: "15px",
        background: "#2c6e9d",
      }}
    >
      <button
        className="text-2xl  font-semibold flex"
        style={{
          width: "100%",
          position: "relative",
          textAlign: "left",
          padding: "4px",
          border: "none",
          background: "transparent",
          outline: "none",
          cursor: "pointer",
        }}
        onClick={toggle}
        type="button"
      >
        <div className="w-11/12 text-gray-100">{props.title}</div>
        <div className="w-1/12 text-center">
          {" "}
          {!isShowing && <i className="fa fa-chevron-down text-white"></i>}
          {isShowing && <i className="fa fa-chevron-up text-white"></i>}
        </div>
      </button>
      <div
        className="text-lg  duration-50"
        style={{
          opacity: isShowing ? "1" : "0",
          height: isShowing ? "auto" : "0",
          padding: isShowing ? "5px" : "0px",
          fontSize: "17px",
          color: "lightgray",
        }}
        dangerouslySetInnerHTML={{
          __html: props.content,
        }}
      />
    </div>
  );
};

const Faq = () => {
  const { query } = useRouter();
  const router = useRouter();
  const { rive, RiveComponent } = useRive({
    src: "/rive/logo.riv",
    autoplay: true,
  });

  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  const menuItem = [
    { icon: "fa-home", menuName: _("Home"), name: "/" },
    { icon: "fa-star", menuName: _("Features"), name: "features" },
    { icon: "fab fa-product-hunt", menuName: _("Products"), name: "products" },
    { icon: "fas fa-question-circle", menuName: "FAQ", name: "faq" },
    { icon: "fa-envelope", menuName: _("Contact"), name: "contact" },
  ];

  let dm = 3;

  return (
    <div className="container mt-40 mb-40">
      <div className="text-center">
        <h2 className="text-3xl md:text-5xl md:font-bold text-green p-5">
          {_("Frequently Asked Questions")}
        </h2>
      </div>
      <div className="container" style={{ maxWidth: "920px" }}>
        <Accordion
          title={_("What is the benefit of much... digital cards?")}
          content={
            "<ul>" +
            "<li>" +
            _(
              "The ease of sharing your info: A digital business card is the easiest and fastest approach to share your contact details in a professional way. ‍"
            ) +
            "</li><li>" +
            _(
              "Stay always connected: you can have your digital card always with you on your smartphone or wherever you go!"
            ) +
            "</li><li>" +
            _(
              "Always stay up to date: keep your info up to date all the time in case if you change your contact details or updated more info on your much... digital card, those who have your card will receive your updated information on the spot."
            ) +
            "</li></ul>"
          }
        />
        <Accordion
          title={_("Do much... digital cards work in my phone and laptop?")}
          content={_(
            "Of course, if you have a smartphone, either Android or iPhone. You just need to scan the QR code by opening your phone camera."
          )}
        />
        <Accordion
          title={_("Can I have multiple much... digital cards?")}
          content={_(
            "Yes, you can have up to 10 digital cards. much... provide multiple variety of digital card, such as: Business Card, Business Profile, Product Card & Event Card."
          )}
        />
        <Accordion
          title={_("How do much... digital cards work?")}
          content={_(
            "You basically can create your digital card by visiting www.much.sa. After creating your account, you will be able to create your card and design it as you desire! Once you complete your card, you can start sharing it with your potential clients by letting them scan your digital card via the QR Code. Doing so will able them to view your digital card and just hit “save to contact”."
          )}
        />
        <Accordion
          title={_("Can I update my information on my much... digital card?")}
          content={_(
            "Yes, in your account dashboard, you will be able to update/edit your contact information whenever you like. Any changes you make are instantly applied to your card in real-time."
          )}
        />
        <Accordion
          title={_("How do NFC business cards work?")}
          content={_(
            "much... offers NFC or ‘Near Fields Communication’ digital cards. NFC cards have a chip embedded in each card that can be synced with your much... digital card and dashboard. ‍Whenever you want to share your details with someone, simply tap the NFC card on the smartphone which will bring up a link containing your much... digital card. You can purchase NFC cards via our website today!"
          )}
        />
      </div>
    </div>
  );
};

export default Faq;
