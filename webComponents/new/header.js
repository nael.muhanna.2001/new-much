import { useEffect, useState } from "react";
import { BsList, BsX } from "react-icons/bs";

import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import { translation } from "../../lib/websiteTranslations";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import Link from "next/link";
import NavRes from "./components/nav";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

const BootstrapCarousel = () => {
  const router = useRouter();

  const signIn = () => {
    router.replace("/signin");
  };

  const items = [
    {
      id: 1,
      title: { en: "The New Era Of Digital Business Cards", ar: "" },
      sub: {
        en: "Sustainability, Creativity, & Easy Data Sharing With A Single Tap",
        ar: "",
      },
      body: {
        en: "Elevate your professional networking strategy with customizable NFC digital business cards, NFC tags & QR code business card collections at much.sa.",
        ar: "",
      },
      imageUrl: "/images/new/Much-Slider-1.jpg",
    },
    {
      id: 2,
      title: { en: "Wide Range of NFC Business Cards", ar: "" },
      sub: "",
      body: {
        en: "Choose from a wide variety of digital business cards, or design your own NFC business cards to match your brand identity! Get Card Now!",
        ar: "",
      },
      imageUrl: "/images/new/Much-Slider-3.jpg",
    },
    {
      id: 3,
      title: { en: "Get Your NFC Sticker Tags", ar: "" },
      sub: "",
      body: {
        en: "Get a smaller version of NFC technology with much.sa NFC Sticker Tags that come in a wide range of colors and designs. Get Tag Now!",
        ar: "",
      },
      imageUrl: "/images/new/Much-Slider-2.jpg",
    },
  ];
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <Carousel
      className="!absolute top-0 left-0 min-h-screen "
      activeIndex={index}
      onSelect={handleSelect}
      //   className="absolute top-0 left-0  min-w-full min-h-screen max-h-screen object-cover brightness-50"
    >
      {items.map((item) => (
        <Carousel.Item key={item.id} className="!h-screen" interval={4000}>
          <div className="!h-screen !w-screen">
            <div
              style={{
                background:
                  "linear-gradient(to  bottom, rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.3)) , url(" +
                  item.imageUrl +
                  ")",
                backgroundPosition: "center",
              }}
              className="!h-full object-cover !bg-cover bg-center"
              alt="slides"
            ></div>
          </div>
          <Carousel.Caption className="top-1/4 md:top-1/4 rounded-lg h-fit">
            <h1 className="mb-6  font-boldold text-[1.8rem]  md:text-[3rem] ">
              {item.title["en"]}
            </h1>
            <h3 className="mb-12 font-semibold text-[18px] md:text-[1.8rem]">
              {item.sub["en"]}
            </h3>
            <p className="text-[14px] md:text-[1.3rem] lg:text-[1.5rem] lg:px-32 ">
              {item.body["en"]}
            </p>
            {/* <div className="flex flex-col justify-center items-center h-full w-full z-30">
              <div className="w-full flex justify-center">
                <a
                  href="#"
                  className="w-fit text:xl sm:text-2xl relative group"
                >
                  <div className="text-[20px] font-bold top-1/2 right-[2em] bg-green px-3 rounded-full text-maincolor w-52 text-center relative mx-auto group-hover:scale-105 transition-all duration-500 z-50">
                    Create your card
                  </div>
                  <div className=" text-[18px] bg-maincolor text-white px-3  rounded-full w-60 text-right mx-auto group-hover:scale-105 transition-all duration-500 ">
                    Free
                  </div>
                </a>
              </div>
            </div> */}
            <div className="mt-12 flex justify-center">
              {item.id == 1 ? (
                <Link href="/create-your-card">
                  <button className="bg-maincolor rounded-full  h-14 px-1 py-2 relative scale-100 hover:scale-110 ease-in-out duration-300">
                    <span className="bg-green rounded-full py-3 h-14 px-2 relative right-[15px] text-maincolor font-bold text-[20px]">
                      {_("Create your card")}
                    </span>
                    <span className="mr-4 text-white font-bold text-[18px]">
                      {_("Free")}
                    </span>
                  </button>
                </Link>
              ) : item.id == 2 ? (
                <div className="text-center w-fit bg-green font-bold  hover:bg-emerald-300 hover:text-white transition-all duration-500 rounded-3xl text-gray-700 px-3 py-2 group-hover:scale-105 shadow-sm hover:!shadow-lg shadow-stone-200">
                  <Link href="/store/?type=card" className="text-lg px-3 py-2">
                    <span className="text-blue-900 text-xl">
                      {_("Show Now")}
                    </span>
                  </Link>
                </div>
              ) : (
                <div className="text-center w-fit bg-green font-bold  hover:bg-emerald-300 hover:text-white transition-all duration-500 rounded-3xl text-gray-700 px-3 py-2 group-hover:scale-105 shadow-sm hover:!shadow-lg shadow-stone-200">
                  <Link href="/store/?type=tag" className="text-lg px-3 py-2">
                    <span className="text-blue-900 text-xl">
                      {_("Show Now")}
                    </span>
                  </Link>
                </div>
              )}
            </div>
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  );
};
const Header = () => {
  const { data: session, status } = useSession();

  const [open, setOpen] = useState(false);
  const [scrollY, setScrollY] = useState(0);

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener(
        "scroll",
        (_) => setScrollY(document.body.scrollTop),
        true
      );
    }
  }, []);
  return (
    <>
      <NavRes open={open} setOpen={setOpen} />
      <header className="container mx-auto h-screen">
        <div className="containers mx-auto flex flex-col w-full h-screen">
          <div
            className={
              scrollY > 450
                ? "fixed left-0 w-full z-[52] bg-blue-900 duration-200 h-[64px] bg-opacity-90"
                : "fixed left-0 w-full z-[52] duration-200"
            }
          >
            <nav className=" w-full  container  mx-auto py-2  lg:px-3 flex flex-row justify-between h-fit px-3 ">
              <div className="pt-2">
                <Link href="/">
                  <img
                    style={{ filter: "brightness(100)" }}
                    className="h-6 mt-1 w-auto"
                    src="/images/new/logo.svg"
                    alt="logo"
                  />
                </Link>
              </div>
              {/* <div className="main-nav flex justify-between pt-2 w-3/5">
            <div>
              <a className="text-md" href="#">HomePage</a>
            </div>
            <div>
              <a className="text-md" href="#">Products</a>
            </div>
            <div>
              <a className="text-md" href="#">Pricing</a>
            </div>
            <div>
              <a className="text-md" href="#">Much Enterprise</a>
            </div>
            <div>
              <a className="text-md" href="#">About</a>
            </div>
            <div>
              <a className="text-md" href="#">Contact</a>
            </div>
          </div>  */}
              <div className="h-fit flex">
                <div className="register flex justify-end mx-2">
                  <span
                    className="h-fit rounded-full py-2 pl-3 pr-3 mt-2 !bg-black !bg-opacity-20 text-white hover:!text-green"
                    //   style={{ backgroundColor: "rgba(255, 255, 255, 0.205)" }}
                  >
                    {!session && (
                      <Link href={`/signin`}>
                        <span className="text-white">{_("Log In")}</span>
                      </Link>
                    )}
                    {session?.user && (
                      <>
                        {session.user && (
                          <Link href={`/dashboard/`}>
                            <span className="text-white">{_("Dashboard")}</span>
                          </Link>
                        )}
                      </>
                    )}
                    <i className="fa fa-chevron-right px-2"></i>
                  </span>
                </div>
                <a
                  onClick={() => {
                    setOpen(!open);
                  }}
                  className="mx-0 lg:mx-4 hover:!text-green !text-white"
                >
                  {open ? (
                    <BsX className="text-4xl mt-2 " />
                  ) : (
                    <BsList className="text-4xl mt-2 " />
                  )}
                </a>
              </div>
            </nav>
          </div>
          {/* <div className="w-full h-full">
            <div
              id="carouselExampleCaptions"
              className="relative"
              data-te-carousel-init
              data-te-carousel-slide
            >
              <div
                className="absolute bottom-0 left-0 right-0 z-[2] mx-[15%] mb-4 flex list-none justify-center p-0"
                data-te-carousel-indicators
              >
                <button
                  type="button"
                  data-te-target="#carouselExampleCaptions"
                  data-te-slide-to="0"
                  data-te-carousel-active
                  className="mx-[3px] box-content h-[3px] w-[30px] flex-initial cursor-pointer border-0 border-y-[10px] border-solid border-transparent bg-white bg-clip-padding p-0 -indent-[999px] opacity-50 transition-opacity duration-[600ms] ease-[cubic-bezier(0.25,0.1,0.25,1.0)] motion-reduce:transition-none"
                  aria-current="true"
                  aria-label="Slide 1"
                ></button>
                <button
                  type="button"
                  data-te-target="#carouselExampleCaptions"
                  data-te-slide-to="1"
                  className="mx-[3px] box-content h-[3px] w-[30px] flex-initial cursor-pointer border-0 border-y-[10px] border-solid border-transparent bg-white bg-clip-padding p-0 -indent-[999px] opacity-50 transition-opacity duration-[600ms] ease-[cubic-bezier(0.25,0.1,0.25,1.0)] motion-reduce:transition-none"
                  aria-label="Slide 2"
                ></button>
                <button
                  type="button"
                  data-te-target="#carouselExampleCaptions"
                  data-te-slide-to="2"
                  className="mx-[3px] box-content h-[3px] w-[30px] flex-initial cursor-pointer border-0 border-y-[10px] border-solid border-transparent bg-white bg-clip-padding p-0 -indent-[999px] opacity-50 transition-opacity duration-[600ms] ease-[cubic-bezier(0.25,0.1,0.25,1.0)] motion-reduce:transition-none"
                  aria-label="Slide 3"
                ></button>
              </div>
            </div>

            <div
              id="default-carousel"
              className="relative w-full h-full sm:mt-10"
              data-carousel="slide"
            >
              <div className="relative w-full h-full overflow-hidden rounded-lg md:h-96">
                <div
                  className="hidden duration-700 ease-in-out h-full"
                  data-carousel-item
                >
                  <div className="container flex flex-col justify-center items-center w-1/2 h-full mx-auto">
                    <h1 className="text-[40px] sm:text-5xl font-boldold text-center leading-snug">
                      The New Era Of Digital Business Cards
                    </h1>
                    <p className="sm:text-lg font-boldold mt-8 text-center leading-snug">
                      Elevate your professional networking strategy with
                      customizable NFC digital business cards, NFC tags & QR
                      code business card collections at much.sa.
                    </p>
                  </div>
                </div>
                <div
                  className="hidden duration-700 ease-in-out h-full"
                  data-carousel-item
                >
                  <div className="container flex flex-col justify-center items-center w-1/2 h-full mx-auto">
                    <h1 className="text-[40px] sm:text-5xl font-boldold text-center leading-snug">
                      Wide Range of NFC Business Cards
                    </h1>
                    <p className="sm:text-lg font-boldold mt-8 text-center leading-snug">
                      Choose from a wide variety of digital business cards, or
                      design your own NFC business cards to match your brand
                      identity! Get Card Now!
                    </p>
                  </div>
                </div>
                <div
                  className="hidden duration-700 ease-in-out h-full"
                  data-carousel-item
                >
                  <div className="container flex flex-col justify-center items-center w-1/2 h-full mx-auto">
                    <h1 className="text-[40px] sm:text-5xl font-boldold text-center leading-snug">
                      Get Your NFC Sticker Tags
                    </h1>
                    <p className="sm:text-lg font-boldold mt-8 text-center leading-snug">
                      Get a smaller version of NFC technology with much.sa NFC
                      Sticker Tags that come in a wide range of colors and
                      designs. Get Tag Now!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div> */}
          {/* <video
            autoplay
            loop
            muted
            className="absolute top-0 left-0 z-10 min-w-full min-h-screen max-h-screen object-cover brightness-50"
            src="https://assets-global.website-files.com/617ac0d059899a9a3c8216e9/629eb92c558636538ee2a08a_SharePage_Blinq_46MB-transcode.mp4"
          ></video> */}
          <BootstrapCarousel />
        </div>
      </header>
    </>
  );
};

export default Header;
