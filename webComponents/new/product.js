import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { Carousel } from "react-bootstrap";
import { translation } from "../../lib/translations";

export default function Product({ product: product }) {
  const { locale } = useRouter();
  //======= Translation =========//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  return (
    <>
      <div
        className="relative border-b-2 border-b-emerald-300 min-h-fit m-3  mx-4 max-w-xs"
        style={{ height: "100%" }}
      >
        {(product.cardFront || product.cardBack) && (
          <Carousel onMouseOver={() => {}} controls={false} indicators={false}>
            {product.cardFront && (
              <Carousel.Item className="max-h-[200px]">
                <img
                  className="mx-auto"
                  width={200}
                  height={150}
                  src={product.cardFront.imgUrl}
                />
              </Carousel.Item>
            )}
            {product.cardBack && (
              <Carousel.Item className="max-h-[200px]">
                <img
                  className="mx-auto"
                  width={200}
                  height={150}
                  src={product.cardBack.imgUrl}
                />
              </Carousel.Item>
            )}
          </Carousel>
        )}
        {!product.cardFront &&
          !product.cardBack &&
          product.images &&
          product.images.length > 0 && (
            <Carousel
              onMouseOver={() => {
                console.log("test");
              }}
              controls={false}
              indicators={false}
            >
              {product.images.map((img) => {
                return (
                  <Carousel.Item className="max-h-[200px]">
                    <img
                      className="mx-auto"
                      width={200}
                      height={150}
                      src={img.imgUrl}
                    />
                  </Carousel.Item>
                );
              })}
            </Carousel>
          )}
        {product.name && (
          <h1 className="p-1 text-start text-sm  max-w-full overflow-ellipsis overflow-hidden mb-4">
            {product.name[locale] ? product.name[locale] : product.name["en"]}
          </h1>
        )}
        {product.shortDescription && (
          <p className="text-start text-xs  max-w-full overflow-ellipsis overflow-hidden text-gray-500">
            {product.shortDescription[locale]
              ? product.shortDescription[locale]
              : product.shortDescription["en"]}
          </p>
        )}
        <div className="absolute bottom-1 left-0">
          {product.price != product.originalPrice && (
            <div>
              <span className=" w-full line-through text-gray-400">
                {product.originalPrice}{" "}
              </span>
              <span className="font-bold text-red-700 px-2">
                {product.price} {product.currency}
              </span>
            </div>
          )}
          {product.price == product.originalPrice && (
            <div className="font-bold text-blue-900">
              {product.price} {product.currency}
            </div>
          )}
        </div>
        <div className="absolute bottom-0 right-0">
          <Link href={"/store/product/" + product._id}>
            <button className="text-xs font-semibold text-blue-900 bg-emerald-300 p-2 rounded-t-md hover:bg-blue-900 hover:text-emerald-300 duration-300">
              {_("Buy Now")} <i className="fa fa-arrow-right"></i>
            </button>
          </Link>
        </div>
      </div>
    </>
  );
}
