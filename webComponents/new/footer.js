import { BsFacebook, BsInstagram, BsLinkedin, BsYoutube } from "react-icons/bs";
import { translation } from "../../lib/websiteTranslations";

import { useRouter } from "next/router";
import Link from "next/link";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
const Footer = () => {
  return (
    <div className="bg-[#00487D]">
      <footer className="container ">
        <div className="pb-10  pt-4" id="contact">
          <div className="w-full flex flex-wrap">
            <div className="">
              <div className="logo">
                <img
                  src="/images/logo.svg"
                  alt="much.sa"
                  className="max-w-[200px] my-2 "
                />
              </div>
              <div className="slug">
                <h2 className="ltr:text-left text-sm text-white font-bold">
                  {_("The New Era of Digital Cards")}
                </h2>
              </div>
              <div className="w-full flex flex-col justify-between lg:flex-row ">
                <div className="lg:w-5/12 mt-10">
                  <p className="text-white text-sm mt-2 mr-5 pe-5">
                    {_(
                      "much is a leading digital business cards and NFC tags provider in KSA. We revolutionize the way individuals and businesses communicate in the digital era while providing innovative sustainable solutions with contactless and effortless data sharing."
                    )}
                  </p>
                </div>


                <div className=" lg:w-3/12 text-white  text-sm mt-10 mx-5">
                  <div className="flex mt-2">
                    <div className="flex mt-1">
                      <i className="far fa-registered mr-5"></i>
                    </div>
                    <span className="px-1 ">
                      {_("Much More For Information Technology LLC")}{" "}
                      <span className="font-semibold">(1010815381)</span>
                    </span>
                  </div>
                  <div className=" flex justify-start mt-2">
                    <div className="flex mt-1">
                      <i className="fa fa-envelope-open mr-5"></i>
                    </div>
                    <a className="text-white px-2" href="mailto:info@much.sa">
                      info@much.sa
                    </a>
                  </div>
                  <div className=" flex justify-start mt-2">
                    <div className="flex mt-1">
                      <i className="fa fa-phone-square mr-5"></i>
                    </div>
                    <a className="text-white px-2" href="Tel: +966 53 663 6788">
                      +966 53 663 6788
                    </a>
                  </div>
                </div>


                <div className=" w-full flex justify-between mt-10 lg:w-4/12 ">
                  <div className="w-1/2 flex">
                    <div className="mb-6">
                      <div className="text-white !sm:text-left font-bold  mb-3 text-base">
                        {_("Store")}
                      </div>
                      <div className="flex flex-col justify-center">
                        <Link
                          href="/store"
                          className="w-full mb-2  text-white  !sm:text-left font-semibold text-base"
                        >
                          {_("All Products")}
                        </Link>
                        <Link
                          href="/store/?type=card"
                          className="w-full mb-2  text-white !sm:text-left font-semibold"
                        >
                          {_("Business Cards")}
                        </Link>
                        <Link
                          href="/store/?type=tag"
                          className="w-full mb-2  text-white !sm:text-left font-semibold"
                        >
                        {_("NFC Tags")}
                        </Link>
                        <Link
                          href="/store/?type=custom-cards"
                          className="w-full mb-3  text-white !sm:text-left font-semibold"
                        >
                          {_("Custom Business Cards")}
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="w-1/2 ">
                    <div className="mb-6">
                      <div className="text-white !sm:text-left font-bold  mb-3 text-base">
                        {_("Enterprise")}
                      </div>
                      <div className="flex flex-col">
                        <Link
                          href="/#"
                          className="w-full mb-2  text-white  !sm:text-left font-semibold text-base"
                        >
                          {_("Enterprise Features")}
                        </Link>
                        <Link
                          href="/#"
                          className="w-full mb-3  text-white !sm:text-left font-semibold"
                        >
                          {_("Request a quote")}
                        </Link>
                        <Link
                          href="/#"
                          className="w-full mb-3  text-white !sm:text-left font-semibold"
                        >
                          {_("Our Team")}
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex justify-start text-left">
              <div className="mr-4">
                <Link
                  target="_blank"
                  href="https://www.linkedin.com/company/much-sa/"
                >
                  <i className="fab fa-linkedin text-2xl text-white"></i>
                </Link>
              </div>
              <div className="mx-3">
                <Link target="_blank" href="https://twitter.com/much_digital">
                  <i className="fab fa-twitter text-2xl text-white"></i>
                </Link>
              </div>

              <div className="mx-3">
                <Link
                  target="_blank"
                  href="https://www.instagram.com/much_digital/"
                >
                  <i className="fab fa-instagram text-2xl text-white"></i>
                </Link>
              </div>
              <div className="mx-3">
                <Link
                  target="_blank"
                  href="https://www.facebook.com/much.sa.digital"
                >
                  <i className="fab fa-facebook text-2xl text-white"></i>
                </Link>
              </div>
            </div>
            <div className=" w-full">
              <hr className=" border-white border-2" />
            </div>
            <div className="w-full mx-auto text-center">
              <div className=" text-white">
                {_("Terms & Conditions | Privacy Policy")}
              </div>
              <div className=" text-white">
                {_("All Rights Reserved © much.sa")}
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
