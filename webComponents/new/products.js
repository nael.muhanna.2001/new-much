import Link from "next/link";
import { useEffect, useState } from "react";

import { translation } from "../../lib/websiteTranslations";
import { useRouter } from "next/router";
import Product from "./components/product";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

const Tap1 = ({ tap, cards, locale }) => {
  return (
    <div
      className={` pt-4 pb-4 rounded-lg transition-all aria ${
        tap == 1 ? "block" : "hidden"
      }`}
      role="tabpanel"
      aria-labelledby="profile-tab"
    >
      <div className="cards flex flex-wrap justify-center pt-2 w-full">
        {cards.map((val, index) => {
          if (index < 3)
            return (
              <div className="w-full md:w-4/12">
                <Product product={val} key={index} />
              </div>
            );
        })}
      </div>
    </div>
  );
};

const Tap2 = ({ tap, cards, locale }) => {
  return (
    <div
      className={` pt-4 pb-4 rounded-lg transition-all aria ${
        tap == 2 ? "block" : "hidden"
      }`}
      id="tab-2"
      role="tabpanel"
      aria-labelledby="dashboard-tab"
    >
      <div className="cards flex flex-wrap justify-center pt-2">
        {cards.map((val, index) => {
          if (index < 3)
            return (
              <div className="w-full md:w-4/12">
                <Product product={val} key={index} />
              </div>
            );
        })}
      </div>
    </div>
  );
};

const Products = () => {
  const { locale } = useRouter();
  const [productsData, setProductsData] = useState([]);

  useEffect(() => {
    fetch("/api/much-products/", {
      method: "POST",
      body: JSON.stringify({ locale: locale }),
    })
      .then((res) => res.json())
      .then((data) => {
        setProductsData(data);
        console.log(data);
      });
  }, []);
  const [tap, setTap] = useState(1);
  return (
    <div className="bg-[url('/images/bgs/pbg.jpg')] bg-contain" id="products">
      <div className="container ">
        <section className="section-5 w-full h-auto pt-28">
          <div className="">
            <div className="text-center">
              <h2 className="text-3xl md:text-5xl md:font-bold text-maincolor">
                {_("Share Your Details with Style")}
              </h2>
              <p className="font-light !p-5 md:text-lg text-maincolor">
                {_("Customize your card the way you desire")}
              </p>
            </div>
            <div className="mb-4 border-b border-green dark:border-whitew">
              <ul
                className="flex flex-wrap justify-around -mb-px text-sm font-medium text-center"
                id="myTab"
                data-tabs-toggle="#myTabContent"
                role="tablist"
              >
                <li className="mr-2" role="presentation">
                  <button
                    className={`inline-block p-4 border-b-2 border-transparent rounded-t-lg text-2xl text-maincolor hover:text-maincolor  dark:hover:text-maincolor aria-selected:text-green transition-all duration-500 ${
                      tap == 1
                        ? "border-maincolor bg-green"
                        : "border-transparent bg-gray-200 bg-opacity-50 "
                    }`}
                    id="profile-tab"
                    data-tabs-target="#tab-1"
                    type="button"
                    role="tab"
                    aria-controls="profile"
                    aria-selected="false"
                    onClick={() => {
                      setTap(1);
                    }}
                  >
                    {_("Cards")}
                  </button>
                </li>
                <li className="mr-2" role="presentation">
                  <button
                    className={`inline-block p-4 border-b-2 border-transparent rounded-t-lg text-2xl text-maincolor hover:text-maincolor  dark:hover:text-maincolor aria-selected:text-green transition-all duration-500 ${
                      tap == 2
                        ? "border-maincolor bg-green"
                        : "border-transparent bg-gray-200 bg-opacity-50 "
                    }`}
                    id="dashboard-tab"
                    data-tabs-target="#tab-2"
                    type="button"
                    role="tab"
                    aria-controls="dashboard"
                    aria-selected="false"
                    onClick={() => {
                      setTap(2);
                    }}
                  >
                    {_("Tags")}
                  </button>
                </li>
              </ul>
            </div>
            <div id="myTabContent" className=" pb-4">
              <Tap1
                tap={tap}
                cards={productsData.filter((val, index) => {
                  return val.category == "card";
                })}
                locale={locale}
              />
              <Tap2
                tap={tap}
                cards={productsData.filter((val, index) => {
                  return val.category == "tag";
                })}
                locale={locale}
              />
              <div className="flex justify-end pt-8 pr-20">
                {/* <div className=" w-fit text-center bg-green hover:bg-emerald-600 hover:text-white transition-all duration-500 rounded-md text-gray-700 px-3 py-2"> */}
                <Link href={"/store"} className="text-md font-bold px-3 py-2">
                  {_("All products")}
                  <i className="fa fa-arrow-right pl-2"></i>
                </Link>
                {/* </div> */}
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default Products;
