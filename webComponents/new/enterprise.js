import { translation } from "../../lib/websiteTranslations";
import { useRouter } from "next/router";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

const Enterprise = () => {
  return (
    <div className="mt-0 ">
      <section className="section-8 w-full h-auto m-0 bg-white pb-4 ">
        <div className="flex flex-col pt-8 lg:pt-0 w-full lg:w-fit container ">
          <div className="flex flex-col justify-center items-center mt-5">
            <div className="flex flex-col justify-between bg-white ">
              <div className="py-4">
                <h2 className="text-maincolor text-4xl font-bold  text-center md:text-5xl md:font-bold">
                  {_("Much Enterprise")}
                </h2>
                <p className="font-light p-5 text-lg text-center text-gray-700 leading-8">
                  {_("Attract and retain customers with")}{" "}
                  <span>
                    <a href="#" className="text-green font-bold">
                      {'much.sa'}
                    </a> 
                  </span>
                  {(" customizable NFC digital business cards and NFC tags. Our trackable products and advanced analytics feature assist businesses in generating sales leads, making valuable connections, and driving revenue.")}
                </p>
                <ol
                  type="1"
                  className="font-light p-12 pt-12 leading-8 text-lg text-left list-disc"
                >
                  <li className="text-gray-700">{_("Elevate Brand Presence")}</li>
                  <li className="text-gray-700">
                    {_("Sustainable & Cost-effective")}
                  </li>
                  <li className="text-gray-700">
                    {_("Fully Customizable Card Designs")}
                  </li>
                  <li className="text-gray-700">{_("Trackable Team Performance")}</li>
                  <li className="text-gray-700">{_("Geolocation Records")}</li>
                </ol>
              </div>
              {/* <!-- <div className="flex justify-end mt-8 group">
            <div
              className="text-center bg-green hover:bg-emerald-400 hover:text-white transition-all duration-500 rounded-xl text-gray-700 px-3 py-2 group-hover:scale-105 shadow-sm hover:!shadow-lg shadow-stone-200">
              <a href="#" className="text-lg px-3 py-2">Get Started</a>
            </div>
          </div> --> */}
            </div>
            <div className="w-fit h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 md:w-3/4">
              <img
                className="w-[85%] h-auto pt-0 lg:pt-4 mx-auto"
                src="https://www.much.sa/images/BDB.png"
                alt=""
              />
            </div>
          </div>

          <div className="flex flex-col lg:flex-row pt-8 lg:pt-0 w-full">
            <div className="flex">
              <div className="flex flex-col mt-5 w-1/2 lg:w-auto">
                <div className="lg:w-1/2 mx-auto">
                  <div className="h-auto">
                    <img
                      className="pt-0 lg:pt-4 mx-auto"
                      src="https://www.much.sa/images/elevatebrand.png"
                      alt=""
                    />
                  </div>
                  <div className="flex flex-col text-center lg:text-left">
                    <h3 className="text-lg lg:text-sm font-bold  text-maincolor text-center">
                      {_('Elevate Brand Presence')}
                    </h3>
                  </div>
                </div>
              </div>
              <div className="flex flex-col mt-5 w-1/2 lg:w-auto">
                <div className="lg:w-1/2 mx-auto">
                  <div className="h-auto">
                    <img
                      className="pt-0 lg:pt-4 mx-auto"
                      src="https://www.much.sa/images/sustainable.png"
                      alt=""
                    />
                  </div>
                  <div className="flex flex-col text-center lg:text-left">
                    <h3 className="text-lg lg:text-sm font-bold  text-maincolor text-center">
                      {_('Sustainable & Cost-effective')}
                    </h3>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex">
              <div className="flex flex-col mt-5 w-1/2 lg:w-auto">
                <div className="lg:w-1/2 mx-auto">
                  <div className="h-auto">
                    <img
                      className="pt-0 lg:pt-4 mx-auto"
                      src="https://www.much.sa/images/customizable.png"
                      alt=""
                    />
                  </div>
                  <div className="flex flex-col text-center lg:text-left">
                    <h3 className="text-lg lg:text-sm font-bold  text-maincolor text-center">
                      {_('Fully Customizable Card Designs')}
                    </h3>
                  </div>
                </div>
              </div>
              <div className="flex flex-col mt-5 w-1/2 lg:w-auto">
                <div className="lg:w-1/2 mx-auto">
                  <div className="h-auto">
                    <img
                      className="pt-0 lg:pt-4 mx-auto"
                      src="https://www.much.sa/images/trackable.png"
                      alt=""
                    />
                  </div>
                  <div className="flex flex-col text-center lg:text-left">
                    <h3 className="text-lg lg:text-sm font-bold  text-maincolor text-center">
                      {_('Trackable Team Performance')}
                    </h3>
                  </div>
                </div>
              </div>
            </div>

            <div className="flex">
              <div className="flex flex-col mt-5 w-1/2 lg:w-auto">
                <div className="lg:w-1/2 mx-auto">
                  <div className="h-auto">
                    <img
                      className="pt-0 lg:pt-4 mx-auto"
                      src="https://www.much.sa/images/geolocation.png"
                      alt=""
                    />
                  </div>
                  <div className="flex flex-col text-center lg:text-left">
                    <h3 className="text-lg lg:text-sm font-bold  text-maincolor text-center">
                      {_('Geolocation Records')}
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Enterprise;
