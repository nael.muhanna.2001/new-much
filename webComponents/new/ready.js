import { translation } from "../../lib/websiteTranslations";
import { useRouter } from "next/router";
import Link from "next/link";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

const Ready = () => {
  return (
    <div
      className="mt-0 pb-20  object-cover !bg-cover "
      style={{
        background:
          "linear-gradient(to  bottom, rgba(255, 255, 255, 0.3),rgba(255, 255, 255, 0.7), rgba(0, 72, 125, 0.3))-100px 0px, url('/images/bgs/rbg.jpg') 0px -110px",
      }}
    >
      <div className="w-screen  md:h-[60vh] -mb-2">
        <div className="container ">
          <section className="section-5 w-full h-auto pt-20 m-0 text-blue-900">
            <div className="px-10 sm:px-0">
              <div className="w-full  flex flex-col justify-center items-center ">
                <div className="text-center">
                  <h2 className="!text-[28px]  md:!text-[3rem] md:font-bold  text-gray-600 readyText">
                    {_("Ready To Go Digital?")}
                  </h2>
                  <p className="font-light p-5 md:text-lg md:font-bold text-[18px] md:text-[1.8rem]">
                    {_(
                      "Create your account now to start designing your very own digital business card."
                    )}
                  </p>
                </div>
                <div className="flex justify-center mt-4 group">
                  <div className="text-center bg-green font-bold  hover:bg-emerald-300 hover:text-white transition-all duration-500 rounded-3xl text-gray-700 px-3 py-2 group-hover:scale-105 shadow-sm hover:!shadow-lg shadow-stone-200">
                    <Link
                      href="/create-your-card/"
                      className="text-lg px-3 py-2"
                    >
                      <span className="text-blue-900 text-xl">
                        {_("Create Account")}
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export default Ready;
