import AdminNavbar from "../components/Navbars/businessNavbar";
import Sidebar from "../components/Sidebar/businessSidebar";
import FooterAdmin from "../components/Footers/FooterAdmin.js";
import { useSession } from "next-auth/react";
import Image from "next/image";

export default function Admin({ children }) {
  const { data: session, status } = useSession();
  if(status=='loading')
  {
    return(<>
    <div className="pt-72 flex justify-center">
        <div className="mx-auto animate-pulse">
    <Image
                  src="/images/logo.svg"
                  alt="Logo"
                  width={"260"}
                  height={"160"}
                  className="cursor-pointer mx-auto"
                />
    </div>
    </div>
    </>)
  }
  if(!session)
  window.location.href='/signin';
  if(session && status!='loading')
  return (
    <>
    
    <div  style={{position:'relative',top:'-4px'}}>
      <Sidebar />
      <div  className="mainBody relative  py-1 bg-gray-100">
        
        <AdminNavbar />
        
        <div className="w-full  min-h-screen">
          
          {children}
          
          <FooterAdmin />
        </div>
      </div>
      </div>
    </>
  );
}
