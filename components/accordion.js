import { useState } from "react";

export default function Accordion(props) {
  const [isShowing, setIsShowing] = useState(false);

  const toggle = () => {
    setIsShowing(!isShowing);
  };

  return (
    <div
    className="rounded-xl p-4"
      style={{
        width: "100%",
        marginBottom: "15px",
        lineHeight: "15px",
        border: "1px solid rgba(209, 213, 219, 0.5)"
      }}
    >
      <button
      className="text-2xl  font-semibold flex"
        style={{
          width: "100%",
          position: "relative",
          textAlign: "left",
          padding: "4px",
          border: "none",
          background: "transparent",
          outline: "none",
          cursor: "pointer"
        }}
        onClick={toggle}
        type="button"
      >
        <div className="w-11/12">
        {props.title}
        </div>
        <div className="w-1/12 text-center"> {!isShowing &&<i className="fa fa-chevron-down"></i>}
        {isShowing &&<i className="fa fa-chevron-up"></i>}
        </div>
      </button>
      <div
      className="text-lg border-t duration-500"
        style={{ opacity: isShowing ? "1" : "0",height:isShowing ? "auto" : "0", padding:isShowing?"5px":"0px" }}
        dangerouslySetInnerHTML={{
          __html: props.content
        }}
      />
    </div>
  );
}
