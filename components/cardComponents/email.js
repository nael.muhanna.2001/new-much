export default function EmailComponent({field,handler})
{
    return <div className="flex   pt-2 w-full cursor-pointer" onClick={handler}>
        <i className={field.field.icon+" text-2xl mx-2 text-gray-400 w-1/12 text-center outline-gray-400"}></i>
        <div className="text-xl whitespace-nowrap text-ellipsis overflow-hidden  w-11/12">{field.email}
        {field.label && <div><small className="text-gray-400 text-sm">{field.label}</small></div>}
        </div>
        
    </div>
    
}