export default function PhoneComponent({field,handler})
{
    return <div className="flex  py-1 w-full cursor-pointer" onClick={handler}>
        <i className={field.field.icon+" text-2xl mx-2  text-gray-400 w-1/12 text-center outline-gray-400"}></i>
        <div className="text-xl  whitespace-nowrap text-ellipsis overflow-hidden  w-11/12">({field.prefix.value}) {field.phone}
        {field.label && <div><small className="text-gray-400 text-sm">{field.label}</small></div>}
        </div>
        
    </div>
    
}