export default function TextComponent({field})
{
    return <div className="flex flex-wrap my-4 p-4">
        <i className={field.field.icon+" text-2xl text-gray-400 w-1/12 text-center outline-gray-400"}></i>
        <div className="text-xl w-11/12">
            {!field.title &&<span>{field.text}</span>}
            {field.title &&<span>{field.title}</span>}
        {field.label && <div><small className="text-gray-400 text-sm">{field.label}</small></div>}
        </div>
        
    </div>
    
}