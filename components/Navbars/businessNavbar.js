  import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React from "react";
import { translation } from "../../lib/translations";
import NotificationDropdown from "../Dropdowns/NotificationDropdown";

import UserDropdown from "../Dropdowns/UserDropdown";
//=== Translation Function Start
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function Navbar() {
  const { data: session, status } = useSession();
  return (
    <>
    <div className="px-8 absolute w-full">
      {/* Navbar */}
      <nav style={{backgroundColor:"#6B5D7B"}} className="rounded-b-xl hidden top-0 left-0 w-full z-10 md:flex-row md:flex-nowrap md:justify-start lg:flex items-center p-2 ">
      
        <div className="w-full mx-auto items-center flex justify-between md:flex-nowrap flex-wrap md:px-10">
          {/* Brand */}
          <h4
            className="text-white text-md  hidden lg:inline-block font-bold"
            href="#pablo"
            onClick={(e) => e.preventDefault()}
          >
            {_('Hi')} 👋 {session.user?.name}
          </h4>
          
          
        </div>
      </nav>
      </div>
      {/* End Navbar */}
    </>
  );
}
