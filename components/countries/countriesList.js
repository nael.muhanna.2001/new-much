import { useEffect, useState } from "react";
import Select from 'react-select'
import {countries} from "../../lib/countries";
export default  function CountryList(props) {
  const options = [];
  countries.map((country)=>{
    options.push({value:country.ISO2,label:country.name+' ('+country.dial+')'})
  })
  
  return (
    <Select className={props.className} ref={props.ref} options={options}>
       
    </Select>
  );
}
