export const merchantIdentifier = 'merchant.apple_pay';
export const merchantDisplay = 'much...';
export const paymentRequestApi = process.env.REACT_APP_PAYMENT_REQUEST_API === "yes";

export const APPLE_PAY_VERSION_NUMBER = 14;
