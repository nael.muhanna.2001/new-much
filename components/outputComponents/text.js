export default function TextOutput({ field }) {
  return (
    <div className="flex flex-wrap">
      <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md ">
        <i className={field.field.icon + " text-xl"}></i>
      </button>
      <h5 className="inline-block font-bold whitespace-nowrap text-ellipsis overflow-hidden pt-4 w-8/12">
        {!field.field.withTitle ? field.text : field.title}
        {field.field.withLabel && (
          <div>
            <small className="text-gray-400">{field.label}</small>
          </div>
        )}
      </h5>
    </div>
  );
}
