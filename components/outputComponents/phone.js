export default function PhoneOutput({ field,handler }) {

  return (
    <div className="flex flex-wrap cursor-pointer selection:none" onClick={handler} >
      <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md ">
        <i className={field.field.icon + " text-xl"}></i>
      </button>
      <h5 className="inline-block font-bold whitespace-nowrap text-ellipsis overflow-hidden pt-4 w-8/12">
        {!field.field.withTitle && (
          <div>
            {field.prefix != null ? "(" + field.prefix.value + ") " : ""}
            { field.phone }
          </div>
        )}
        {field.field.withTitle && (
          <div>
            {field.title}
          </div>
        )}

        {field.field.withLabel && (
          <div>
            <small className="text-gray-400">{field.label}</small>
          </div>
        )}
      </h5>
    </div>
  );
}
