import { useState } from "react";
import { Row, Col, Card, Form, Button } from "react-bootstrap";
import InputField from "../../components/payment/CardValidator/InputField";
import CardIconsList from "../../components/payment/CardValidator/CardIconsList";
import { handleNumbersOnly, getCardType } from "../../utils/validateCard";

const CreditCard = ({ defaultName, setIsCreated }) => {
  const [contact, setContact] = useState({
    name: defaultName,
    number: "",
    expiry: "",
    cvc: "",
    errors: {},
  });

  // Input field onChange handler
  const handleChange = (e) => {
    setContact({ ...contact, [e.target.name]: e.target.value });
  };

  // Input card expiry onKeyUp handler
  const handleCardExpiry = (e) => {
    let expiryDate = e.target.value;

    if (e.keyCode !== 8) {
      if (expiryDate > 1 && expiryDate.length === 1) {
        expiryDate = "0" + expiryDate + "/";
      } else if (expiryDate.length === 2) {
        expiryDate = expiryDate + "/";
      }

      setContact({ ...contact, expiry: expiryDate });
    } else {
      setContact({ ...contact, expiry: "" });
    }
  };

  // Input fields validation handler
  const handleValidation = () => {
    const { name, number, expiry, cvc, errors } = contact;
    let formIsValid = true;

    if (!name) {
      formIsValid = false;
      errors["name"] = "Cardholder name is required";
    } else {
      errors["name"] = "";
    }

    if (!number) {
      formIsValid = false;
      errors["number"] = "Card number is required";
    } else {
      errors["number"] = "";
    }

    if (!expiry) {
      formIsValid = false;
      errors["expiry"] = "Expiry is required";
    } else {
      errors["expiry"] = "";
    }

    if (!cvc) {
      formIsValid = false;
      errors["cvc"] = "CVV is required";
    } else {
      errors["cvc"] = "";
    }

    setContact({ ...contact, errors: errors });
    return formIsValid;
  };

  // Form onSubmit handler
  const handleSubmit = (e) => {
    e.preventDefault();
    const { name, number, expiry, cvc } = contact;

    if (handleValidation()) {
      setContact({ ...contact, errors: {} });
      fetch("/api/payment/credit-cards/create/", {
        method: "post",
        body: JSON.stringify(contact),
      })
        .then((res) => res.json())
        .then((data) => console.log(data));
    }
  };

  const { name, number, expiry, cvc, errors } = contact;

  return (
    <Row>
      <Col>
        <div className="CardPaymentForm">
          <Card className="shadow-sm">
            <Card.Body>
              <CardIconsList type={getCardType(number)} />
              <Form onSubmit={handleSubmit}>
                <InputField
                  unique="cardholderName"
                  label="Cardholder name"
                  name="name"
                  value={name}
                  onChange={handleChange}
                  error={errors.name}
                />
                <InputField
                  unique="cardNumber"
                  label="Card number"
                  maxLength="16"
                  name="number"
                  value={number}
                  onKeyDown={handleNumbersOnly}
                  onChange={handleChange}
                  error={errors.number}
                />
                <Row>
                  <Col>
                    <InputField
                      unique="cardExpiry"
                      label="MM/YY"
                      maxLength="5"
                      name="expiry"
                      value={expiry}
                      onKeyDown={handleNumbersOnly}
                      onKeyUp={handleCardExpiry}
                      onChange={handleChange}
                      error={errors.expiry}
                    />
                  </Col>
                  <Col>
                    <InputField
                      unique="cardCvv"
                      label="CVV"
                      maxLength="4"
                      name="cvc"
                      value={cvc}
                      onKeyDown={handleNumbersOnly}
                      onChange={handleChange}
                      error={errors.cvc}
                    />
                  </Col>
                </Row>
                <Button
                  block
                  variant="primary"
                  className="text-uppercase mb-3"
                  type="submit"
                >
                  Pay Now
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </div>
      </Col>
    </Row>
  );
};

export default CreditCard;
