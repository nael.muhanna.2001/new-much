import { Image } from 'react-bootstrap';

const CardIconsList = ( props ) => {
  return (
    <ul className="CardPaymentForm-CardIcons flex">
      <li className={`mx-3 ${props.type === 'amex' && 'border-2 border-blue-900 p-1 rounded'}`}>
        <Image 
          
          height="24"
          width="48"
          src="/static/amex.png" 
          alt="Amex" 
        />
      </li>
      <li className={`mx-3 ${props.type === 'jcb' && 'border-2 border-blue-900 p-1 rounded'}`}>
        <Image 
          height="24"
          width="48"
          src="/static/jcb.png" 
          alt="JCB" 
        />
      </li>
      <li className={`mx-3 ${props.type === 'mastercard' && 'border-2 border-blue-900 p-1 rounded'}`}>
        <Image 
          height="24"
          width="48"
          src="/static/mastercard.png" 
          alt="MasterCard" 
        />
      </li>
      <li className={`mx-3 ${props.type === 'visa' && 'border-2 border-blue-900 p-1 rounded'}`}>
        <Image 
          height="24"
          width="48"
          src="/static/visa.png" 
          alt="VISA" 
        />
      </li>
    </ul>
  );
}

export default CardIconsList;