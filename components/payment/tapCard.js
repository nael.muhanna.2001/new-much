import { useRouter } from "next/router";
import { useEffect } from "react";
import { translation } from "../../lib/translations";

export default function TapCard({ processPayment }) {
    const { locale } = useRouter();
    function _(text) {
    
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  useEffect(() => {
    //pass your public key from tap's dashboard
    var tap = window.Tapjsli("pk_live_S7jeFulUfsWkhgb3o28nzTJd");

    var elements = tap.elements({});

    var style = {
      base: {
        color: "#535353",
        lineHeight: "18px",
        fontFamily: "sans-serif",
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "rgba(0, 0, 0, 0.26)",
          fontSize: "15px",
        },
      },
      invalid: {
        color: "red",
      },
    };
    // input labels/placeholders
    var labels = {
      cardNumber: "Card Number",
      expirationDate: "MM/YY",
      cvv: "CVV",
      cardHolder: "Card Holder Name",
    };
    //payment options
    var paymentOptions = {
      currencyCode: ["KWD", "USD", "SAR"],
      labels: labels,
      TextDirection: "ltr",
    };
    //create element, pass style and payment options
    var card = elements.create("card", { style: style }, paymentOptions);
    //mount element
    card.mount("#element-container");
    //card change event listener
    card.addEventListener("change", function (event) {
      if (event.loaded) {
        console.log("UI loaded :" + event.loaded);
        console.log("current currency is :" + card.getCurrency());
      }
      var displayError = document.getElementById("error-handler");
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = "";
      }
      // Handle form submission
      var form = document.getElementById("form-container");
      form.addEventListener("submit", function (event) {
        event.preventDefault();

        tap.createToken(card).then(function (result) {
          processPayment(result);
          if (result.error) {
            // Inform the user if there was an error
            var errorElement = document.getElementById("error-handler");
            errorElement.textContent = result.error.message;
          } else {
            // Send the token to your server

            tapTokenHandler(token);
          }
        });
      });
    });
  }, []);
  return (
    <>
      <form id="form-container" method="post" action="/charge">
        <div id="element-container"></div>
        <div id="error-handler" role="alert"></div>
        <div id="success" style={{ display: "none" }}>
          Success! Your token is <span id="token"></span>
        </div>

        <button className="p-2 rounded border bg-blue-900 text-green font-bold w-full" id="tap-btn">{_("Pay Now")}</button>
      </form>
    </>
  );
}
