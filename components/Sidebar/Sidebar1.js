import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";
import NotificationDropdown from "../Dropdowns/NotificationDropdown";
import UserDropdown from "../Dropdowns/UserDropdown.js";
import { translation } from "../../lib/translations";
import { useSession } from "next-auth/react";
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function Sidebar() {
  const { data: session, status } = useSession();
  const [collapseShow, setCollapseShow] = useState("hidden");
  const router = useRouter();

  return (
    <>
      <nav className="ltr:md:left-0 rtl:md:right-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden  bg-white flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 px-6">
        <div className="md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto">
          {/* Toggler */}
          <button
            className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
            type="button"
            onClick={() => setCollapseShow("bg-white m-2 py-3 px-6")}
          >
            <i className="fas fa-bars"></i>
          </button>
          {/* Brand */}
          <Link className="navbar-brand" href="/dashboard">
            <Image src="/images/logo.svg" alt="Logo" width={60} height={40} />
          </Link>
          {/* User */}
          <ul className="md:hidden items-center flex flex-wrap list-none">
            {/* <li className="inline-block relative text-gray-600 text-2xl">
              <NotificationDropdown />
            </li> */}
            <li className="inline-block relative">
              <UserDropdown />
            </li>
          </ul>
          {/* Collapse */}
          <div
            className={
              "md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded " +
              collapseShow
            }
          >
            {/* Collapse header */}
            <div className="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-blueGray-200">
              <div className="flex flex-wrap">
                <div className="w-6/12">
                  <Link href="/">
                    <Image
                      src="/images/logo.svg"
                      alt="Logo"
                      width="160px"
                      height="60px"
                    />
                  </Link>
                </div>
                <div className="w-6/12 flex justify-end">
                  <button
                    type="button"
                    className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                    onClick={() => setCollapseShow("hidden")}
                  >
                    <i className="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </div>

            <h6 className="md:min-w-full text-blueGray-500 text-xs uppercase font-bold block pt-1 pb-4 no-underline">
              {_("Main Menu")}
            </h6>
            {/* Navigation */}
            <ul className="md:flex-col md:min-w-full flex flex-col list-none">
              <li className="items-center">
                <Link href="/dashboard">
                  <a
                    className={
                      "text-xs uppercase py-3 font-bold block " +
                      (router.pathname.indexOf("/dashboard") === 0 &&
                      router.pathname.length === 10
                        ? "text-emerald-300 hover:text-emerald-400"
                        : "text-blueGray-700 hover:text-emerald-500")
                    }
                  >
                    <i
                      className={
                        "fas fa-tv mr-2 text-sm " +
                        (router.pathname.indexOf("/dashboard") === 0 &&
                        router.pathname.length == 10
                          ? "opacity-75"
                          : "text-blueGray-300")
                      }
                    ></i>{" "}
                    {_("Dashboard")}
                  </a>
                </Link>
              </li>
              <li className="items-center">
                <Link href="/dashboard/wallet/">
                  <a
                    className={
                      "text-xs uppercase py-3 font-bold block " +
                      (router.pathname.indexOf("/dashboard/wallet") !== -1
                        ? "text-emerald-300 hover:text-emerald-400"
                        : "text-blueGray-700 hover:text-emerald-500")
                    }
                  >
                    <i
                      className={
                        "fas fa-wallet mr-2 text-sm " +
                        (router.pathname.indexOf("/dashboard/wallet") !== -1
                          ? "opacity-75"
                          : "text-blueGray-300")
                      }
                    ></i>{" "}
                    {_("Cardholder")}
                  </a>
                </Link>
              </li>

              <li className="items-center">
                <Link href="/dashboard/cards">
                  <a
                    className={
                      "text-xs uppercase py-3 font-bold block " +
                      (router.pathname.indexOf("/dashboard/cards") !== -1
                        ? "text-emerald-300 hover:text-emerald-400"
                        : "text-blueGray-700 hover:text-blueGray-500")
                    }
                  >
                    <i
                      className={
                        "fas fa-id-card mr-2 text-sm " +
                        (router.pathname.indexOf("/dashboard/cards") !== -1
                          ? "opacity-75"
                          : "text-blueGray-300")
                      }
                    ></i>{" "}
                    {_("My Cards")}
                  </a>
                </Link>
              </li>

              <li className="items-center">
                <Link href="/dashboard/qr-codes">
                  <a
                    className={
                      "text-xs uppercase py-3 font-bold block " +
                      (router.pathname.indexOf("/dashboard/qr-codes") !== -1
                        ? "text-emerald-300 hover:text-emerald-400"
                        : "text-blueGray-700 hover:text-blueGray-500")
                    }
                  >
                    <i
                      className={
                        "fas fa-qrcode mr-2 text-sm " +
                        (router.pathname.indexOf("/dashboard/qr-codes") !== -1
                          ? "opacity-75"
                          : "text-blueGray-300")
                      }
                    ></i>{" "}
                    {_("My QR Codes")}
                  </a>
                </Link>
              </li>

              {/* <li className="items-center">
                <Link href="/admin/maps">
                  <a
                    href="#pablo"
                    className={
                      "text-xs uppercase py-3 font-bold block " +
                      (router.pathname.indexOf("/admin/maps") !== -1
                        ? "text-emerald-300 hover:text-emerald-400"
                        : "text-emerald-400 hover:text-emerald-300")
                    }
                  >
                    <i
                      className={
                        "fas fa-map-marked mr-2 text-sm " +
                        (router.pathname.indexOf("/admin/maps") !== -1
                          ? "opacity-75"
                          : "text-blueGray-300")
                      }
                    ></i>{" "}
                    Maps
                  </a>
                </Link>
              </li> */}
            </ul>
            {session && session.user && session.user.permissions && session.user.permissions.includes('admin') &&<div>
            <h6 className="md:min-w-full text-blueGray-500 text-xs uppercase font-bold block pt-1 pb-4 no-underline">
              {_("Administation Menu")}
            </h6>
            <ul className="md:flex-col md:min-w-full flex flex-col list-none">
              <li className="items-center">
                <Link href="/dashboard/admin/field-types">
                  <div 
                  className={
                    "text-xs uppercase py-3 font-bold block " +
                    (router.pathname.indexOf("/dashboard/admin/field-type") !== -1
                      ? "text-emerald-300 hover:text-emerald-400"
                      : "text-blueGray-700 hover:text-blueGray-500")
                  }>
                    <i
                      className={
                        "fas fa-link mr-2 text-sm " +
                        (router.pathname.indexOf("/dashboard/admin/field-type") !== -1
                          ? "opacity-75"
                          : "text-blueGray-300")
                      }
                    ></i>
                    {_('Field Types')}
                  </div>
                </Link>
              </li>
              <li className="items-center">
                <Link href="/dashboard/admin/products">
                  <a href="#"
                  className={
                    "text-xs uppercase py-3 font-bold block " +
                    (router.pathname.indexOf("/dashboard/admin/products") !== -1
                      ? "text-emerald-300 hover:text-emerald-400"
                      : "text-blueGray-700 hover:text-blueGray-500")
                  }>
                    <i
                      className={
                        "fas fa-link mr-2 text-sm " +
                        (router.pathname.indexOf("/dashboard/admin/Products") !== -1
                          ? "opacity-75"
                          : "text-blueGray-300")
                      }
                    ></i>
                    {_('Products')}
                  </a>
                </Link>
                <ol>
                  <li className="items-center">
                  <Link href="/dashboard/admin/products">
                    <a className={"text-xs uppercase py-3 font-bold block " +
                    (router.pathname.indexOf("/dashboard/admin/products") !== -1
                      ? "text-emerald-300 hover:text-emerald-400"
                      : "text-blueGray-700 hover:text-blueGray-500")
                  }>TEST</a></Link></li>
                </ol>
              </li>
            </ul>

            </div>}
          </div>
        </div>
      </nav>
    </>
  );
}
