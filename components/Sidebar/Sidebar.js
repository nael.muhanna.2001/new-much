import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";
import NotificationDropdown from "../Dropdowns/NotificationDropdown";
import UserDropdown from "../Dropdowns/UserDropdown.js";
import { translation } from "../../lib/translations";
import { useSession } from "next-auth/react";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function Sidebar() {
  const { data: session, status } = useSession();
  const [collapseShow, setCollapseShow] = useState("hidden");
  const router = useRouter();

  return (
    <>
      <nav className="sidebar rounded-r-xl  ltr:md:left-0 rtl:md:right-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden    flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 pl-10">
        <div className="md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto">
          {/* Toggler */}
          <button
            className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
            type="button"
            onClick={() => setCollapseShow("bg-white m-2 py-3 px-6")}
          >
            <i className="fas fa-bars"></i>
          </button>
          {/* Brand */}
          <div className="md:mx-auto">
            <UserDropdown />
          </div>
          {/* User */}

          {/* Collapse */}
          <div
            className={
              "md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded " +
              collapseShow
            }
          >
            {/* Collapse header */}
            <div className="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-blueGray-200">
              <div className="flex flex-wrap">
                <div className="w-6/12">
                  <Link href="/">
                    <Image
                      src="/images/logo.svg"
                      alt="Logo"
                      width="160"
                      height="60"
                    />
                  </Link>
                </div>
                <div className="w-6/12 flex justify-end">
                  <button
                    type="button"
                    className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                    onClick={() => setCollapseShow("hidden")}
                  >
                    <i className="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </div>

            <h6 className="md:min-w-full text-gray-500 lg:text-white text-md uppercase font-bold block pt-1 pb-2 no-underline">
              {_("Main Menu")}
            </h6>
            {/* Navigation */}
            <ul className="md:flex-col md:min-w-full flex flex-col list-none">
              <li
                className={
                  router.pathname.indexOf("/dashboard") === 0 &&
                  router.pathname.length === 10
                    ? " pl-5  md:rounded-l-full -mr-1 my-2 border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center  pl-5 my-1 hover:bg-emerald-200 md:rounded-l-full duration-300 "
                }
              >
                <Link href="/dashboard">
                  <div
                    className={
                      "text-lg py-1 block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/dashboard") === 0 &&
                      router.pathname.length === 10
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-dashboard mr-2 text-lg "}></i>
                    {_("Dashboard")}
                  </div>
                </Link>
              </li>
              <li
                className={
                  router.pathname.indexOf("/dashboard/wallet") !== -1
                    ? " pl-5 md:rounded-l-full -mr-1 my-2 border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center pl-5 my-1 text-lg hover:bg-emerald-200  md:rounded-l-full duration-300 "
                }
              >
                <Link href="/dashboard/wallet/">
                  <div
                    className={
                      "text-md py-1  block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/dashboard/wallet") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-wallet mr-2 text-lg "}></i>{" "}
                    {_("Cardholder")}
                  </div>
                </Link>
              </li>

              <li
                className={
                  router.pathname.indexOf("/dashboard/cards") !== -1
                    ? "pl-5 md:rounded-l-full -mr-1 my-1 border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center pl-5 my-1 hover:bg-emerald-200 md:rounded-l-full  duration-300 "
                }
              >
                <Link href="/dashboard/cards">
                  <div
                    className={
                      "text-lg py-1  block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/dashboard/cards") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-id-card mr-2 text-lg "}></i>{" "}
                    {_("My Cards")}
                  </div>
                </Link>
              </li>

              <li
                className={
                  router.pathname.indexOf("/dashboard/qr-codes") !== -1
                    ? "pl-5 md:rounded-l-full -mr-1 my-1 border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center pl-5 my-1 hover:bg-emerald-200 md:rounded-l-full  duration-300 "
                }
              >
                <Link href="/dashboard/qr-codes">
                  <div
                    className={
                      "text-lg py-1  block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/dashboard/qr-codes") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-qrcode mr-2 text-lg "}></i>{" "}
                    {_("My QR Codes")}
                  </div>
                </Link>
              </li>
              <li
                className={
                  router.pathname.indexOf("/dashboard/products") !== -1
                    ? "pl-5 md:rounded-l-full -mr-1 my-1 border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center pl-5 my-1 hover:bg-emerald-200 md:rounded-l-full  duration-300 "
                }
              >
                <Link href="/dashboard/products">
                  <div
                    className={
                      "text-lg py-1  block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/dashboard/products") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fab fa-product-hunt mr-2 text-lg "}></i>{" "}
                    {_("My Products")}
                  </div>
                </Link>
              </li>
            </ul>
            {session &&
              session.user &&
              session.user.permissions &&
              session.user.permissions.includes("admin") && (
                <div>
                  <h6 className="md:min-w-full text-gray-500 lg:text-white text-md uppercase font-bold block pt-1 pb-2 no-underline">
                    {_("Administation Menu")}
                  </h6>
                  <ul className="md:flex-col md:min-w-full flex flex-col list-none">
                    <li
                      className={
                        router.pathname.indexOf(
                          "/dashboard/admin/field-type"
                        ) !== -1
                          ? "pl-5 md:rounded-l-full -mr-1 my-1 border bg-gray-100 text-lg md:border-white duration-300 "
                          : "items-center pl-5 my-1 hover:bg-emerald-200 md:rounded-l-full  duration-300 "
                      }
                    >
                      <Link href="/dashboard/admin/field-types">
                        <div
                          href="#"
                          className={
                            "text-md py-1  block decoration-transparent hover:decoration-transparent duration-300 " +
                            (router.pathname.indexOf(
                              "/dashboard/admin/field-type"
                            ) !== -1
                              ? "md:text-black font-bold  md:hover:text-blue-800"
                              : "md:text-white md:hover:text-black")
                          }
                        >
                          <i className={"fas fa-link mr-2 text-sm "}></i>
                          {_("Field Types")}
                        </div>
                      </Link>
                    </li>
                    <li
                      className={
                        router.pathname.indexOf("/dashboard/admin/store/products") !=
                        -1
                          ? "pl-5 md:rounded-l-full -mr-1 my-1 border bg-gray-100 text-lg md:border-white duration-300 "
                          : "items-center pl-5 my-1 hover:bg-emerald-200 md:rounded-l-full  duration-300 "
                      }
                    >
                      <Link href="/dashboard/admin/store/products">
                        <div
                          href="#"
                          className={
                            "text-md py-1  block decoration-transparent hover:decoration-transparent duration-300 " +
                            (router.pathname.indexOf(
                              "/dashboard/admin/store/products"
                            ) !== -1
                              ? "md:text-black font-bold  md:hover:text-blue-800"
                              : "md:text-white md:hover:text-black")
                          }
                        >
                          <i className={"fas fa-link mr-2 text-md "}></i>
                          {_("Products")}
                        </div>
                      </Link>
                    </li>
                    <li
                      className={
                        router.pathname.indexOf("/dashboard/admin/pre-active-links") !=
                        -1
                          ? "pl-5 md:rounded-l-full -mr-1 my-1 border bg-gray-100 text-lg md:border-white duration-300 "
                          : "items-center pl-5 my-1 hover:bg-emerald-200 md:rounded-l-full  duration-300 "
                      }
                    >
                      <Link href="/dashboard/admin/pre-active-links">
                        <div
                          href="#"
                          className={
                            "text-md py-1  block decoration-transparent hover:decoration-transparent duration-300 " +
                            (router.pathname.indexOf(
                              "/dashboard/admin/pre-active-links"
                            ) !== -1
                              ? "md:text-black font-bold  md:hover:text-blue-800"
                              : "md:text-white md:hover:text-black")
                          }
                        >
                          <i className={"fas fa-link mr-2 text-md "}></i>
                          {_("Pre Active Links")}
                        </div>
                      </Link>
                    </li>
                  </ul>
                </div>
              )}
          </div>
        </div>
      </nav>
    </>
  );
}
