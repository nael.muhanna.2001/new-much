import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";
import NotificationDropdown from "../Dropdowns/NotificationDropdown";
import UserDropdown from "../Dropdowns/UserDropdown.js";
import { translation } from "../../lib/translations";
import { useSession } from "next-auth/react";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function Sidebar() {
  const { data: session, status } = useSession();
  const [collapseShow, setCollapseShow] = useState("hidden");
  const router = useRouter();

  return (
    <>
      <nav className="sidebar rounded-r-xl  ltr:md:left-0 rtl:md:right-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden    flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 pl-4">
        <div className="md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto">
          {/* Toggler */}
          <button
            className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
            type="button"
            onClick={() => setCollapseShow("bg-white m-2 py-3 px-6")}
          >
            <i className="fas fa-bars"></i>
          </button>
          {/* Brand */}
          <div className="md:mx-auto">
            <UserDropdown className="mx-2" />
          </div>
          {/* User */}

          {/* Collapse */}
          <div
            className={
              "md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded " +
              collapseShow
            }
          >
            {/* Collapse header */}
            <div className="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-blueGray-200">
              <div className="flex flex-wrap">
                <div className="w-6/12">
                  <Link href="/">
                    <Image
                      src="/images/logo.svg"
                      alt="Logo"
                      width="160"
                      height="60"
                    />
                  </Link>
                </div>
                <div className="w-6/12 flex justify-end">
                  <button
                    type="button"
                    className="cursor-pointer text-black opacity-50 md:hidden  py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                    onClick={() => setCollapseShow("hidden")}
                  >
                    <i className="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </div>

            {/* Navigation */}
            <ul className="md:flex-col md:min-w-full flex flex-col list-none">
              <li
                className={
                  router.pathname.indexOf("/business/dashboard") === 0 &&
                  router.pathname.length === 19
                    ? "py-1 pl-3 my-1  md:rounded-l-full -mr-1  border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center py-1 pl-3 my-1 text-lg hover:bg-emerald-200  md:rounded-l-full duration-300 "
                }
              >
                <Link href="/business/dashboard">
                  <div
                    className={
                      "text-lg py-2 block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/business/dashboard") === 0 &&
                      router.pathname.length === 19
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-dashboard mr-2 text-lg "}></i>
                    {_("Dashboard")}
                  </div>
                </Link>
              </li>
              <li
                className={
                  router.pathname.indexOf("/business/departments") !== -1
                    ? " py-1 pl-3 my-1  md:rounded-l-full -mr-1  border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center py-1 pl-3 my-1 text-lg hover:bg-emerald-200  md:rounded-l-full duration-300 "
                }
              >
                <Link href="/business/departments/">
                  <div
                    className={
                      "text-lg py-2 block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/business/departments") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-users mr-2 text-lg "}></i>{" "}
                    {_("Departments")}
                  </div>
                </Link>
              </li>
              <li
                className={
                  router.pathname.indexOf("/business/cards") !== -1
                    ? " py-1 pl-3 my-1  md:rounded-l-full -mr-1  border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center py-1 pl-3 my-1 text-lg hover:bg-emerald-200  md:rounded-l-full duration-300 "
                }
              >
                <Link href="/business/cards/">
                  <div
                    className={
                      "text-lg py-2 block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/business/cards") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-id-card mr-2 text-lg "}></i>{" "}
                    {_("Cards")}
                  </div>
                </Link>
              </li>

              <li
                className={
                  router.pathname.indexOf("/business/signatures") !== -1
                    ? "py-1 pl-3 my-1  md:rounded-l-full -mr-1  border bg-gray-100 text-lg md:border-white duration-300 "
                    : "items-center py-1 pl-3 my-1 hover:bg-emerald-200 md:rounded-l-full  duration-300 "
                }
              >
                <Link href="/business/signatures">
                  <div
                    className={
                      "text-lg  py-2 block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/business/signatures") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-signature mr-2 text-lg "}></i>{" "}
                    {_("Email Signatures")}
                  </div>
                </Link>
              </li>

              <li
                className={
                  router.pathname.indexOf("/business/leads") !== -1
                    ? "py-1 pl-3 my-1  md:rounded-l-full -mr-1  border bg-gray-100 text-lg md:border-white duration-300"
                    : "items-center py-1 pl-3 my-1 hover:bg-emerald-200 md:rounded-l-full duration-300  "
                }
              >
                <Link href="/business/leads">
                  <div
                    className={
                      "text-lg  py-2  block decoration-transparent hover:decoration-transparent duration-300 " +
                      (router.pathname.indexOf("/business/leads") !== -1
                        ? "md:text-black font-bold  md:hover:text-blue-800"
                        : "md:text-white md:hover:text-black")
                    }
                  >
                    <i className={"fas fa-user-friends mr-2 text-lg "}></i>{" "}
                    {_("Leads")}
                  </div>
                </Link>
              </li>

              
            </ul>
            <hr className="mt-24 border-1 border-blue-700 mr-6" />
            <div>
              <ul className="md:flex-col md:min-w-full flex flex-col list-none">
                <li
                  className={
                    router.pathname.indexOf("/business/billing") !== -1
                      ? "py-1 pl-3 my-1  md:rounded-l-full -mr-1  border bg-gray-100 text-lg md:border-white duration-300"
                      : "items-center py-1 pl-3 my-1 hover:bg-emerald-200 md:rounded-l-full duration-300  "
                  }
                >
                  <Link href="/business/billing">
                    <div
                      className={
                        "text-lg  py-2  block decoration-transparent hover:decoration-transparent duration-300 " +
                        (router.pathname.indexOf("/business/billing") !== -1
                          ? "md:text-black font-bold  md:hover:text-blue-800"
                          : "md:text-white md:hover:text-black")
                      }
                    >
                      <i className={"fas fa-file-invoice mr-2 text-lg "}></i>{" "}
                      {_("Payment")}
                    </div>
                  </Link>
                </li>
                <li
                  className={
                    router.pathname.indexOf("/business/settings") !== -1
                      ? "py-1 pl-3 my-1  md:rounded-l-full -mr-1  border bg-gray-100 text-lg md:border-white duration-300"
                      : "items-center py-1 pl-3 my-1 hover:bg-emerald-200 md:rounded-l-full duration-300  "
                  }
                >
                  <Link href="/business/settings">
                    <div
                      className={
                        "text-lg  py-2  block decoration-transparent hover:decoration-transparent duration-300 " +
                        (router.pathname.indexOf("/business/settings") !== -1
                          ? "md:text-black font-bold  md:hover:text-blue-800"
                          : "md:text-white md:hover:text-black")
                      }
                    >
                      <i className={"fas fa-cog mr-2 text-lg "}></i>{" "}
                      {_("Settings")}
                    </div>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}
