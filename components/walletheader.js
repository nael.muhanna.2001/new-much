import Link from "next/link";
import { getCsrfToken, signIn, signOut, useSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { Menu } from "@headlessui/react";
import { translation } from "../lib/translations";
import { useState } from "react";

export default function Header({className}) {
  const { data: session, status } = useSession();

  const loading = status === "loading";
  const { locale, locales, defaultLocale, asPath } = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }


  return (
    <div className={className}>
    <header id="header-wrap" className="relative">
      <div className="navigation  top-0 left-0 w-full z-50 duration-300">
        <div className="container">
          <nav
            className={
              "navbar py-2 navbar-expand-lg w-full flex justify-between items-center relative duration-300 "
            }
          >
            <div className="ltr:pr-10 rtl:pl-10">
            <Link className="navbar-brand" href="/">
              <Image
                src="/images/logo.svg"
                alt="Logo"
                
                width={160}
                height={90}
              />
            </Link>
            </div>
            <div className="grid grid-cols-2">
            <div className="header-btn mt-2.5 rtl:ml-4 ltr:mr-4" >
              {!session && (
                <>
                  <a
                    href={`/signin`}
                    className="whitespace-nowrap w-12 h-12 text-sm text-emerald-300 border border-emerald-300 px-4 py-2.5 mx-3 rounded-full duration-300 hover:bg-emerald-300 hover:text-white"
                  >
                    <i className="fa fa-sign-in"></i>
                  </a>
                </>
              )}
              {session?.user && (
                <>
                  <div className="text-emerald-300 md:border md:border-emerald-300  -mt-3  h-12 w-12 cursor-pointer rounded-full duration-300 hover:bg-emerald-300 hover:text-white">
                    {session.user.image && (
                      <div
                      onClick={(e) => {
                        e.preventDefault();
                        signOut();
                      }}
                        style={{
                          backgroundImage: `url('${session.user.image}')`,
                          backgroundSize: "cover",
                        }}
                        className="p-0 w-12 h-12 py-2.5 rounded-full"
                      ></div>
                    )}
                    <a
                      href={`/api/auth/signout`}
                      onClick={(e) => {
                        e.preventDefault();
                        signOut();
                      }}
                    >
                      
                    </a>
                  </div>
                </>
              )}
            </div>
            <Menu >
              <Menu.Button  className="ltr:ml-auto rtl:mr-auto ltr:lg:ml-2 rtl:lg:mr-2 button  md:block  capitalize mx-4 border text-gray-900  border-emerald-400 hover:border-emerald-500 focus:ring-4 focus:outline-none focus:ring-emerald-100 font-medium rounded-full text-sm px-4 text-center  items-center dark:border-emerald-300 dark:hover:border-emerald-400 w-12 h-12">
                {locale === "ar" ? "ع" : locale}
              </Menu.Button>
              <Menu.Items className="z-10   bg-white divide-y divide-gray-100 rounded shadow  dark:bg-gray-700 w-20 absolute top-20 ltr:right-1 rtl:left-1 rtl:lg:left-0 ltr:lg:right-0">
                {locales.map((lang, idx) => {
                  return (
                    <Menu.Item
                      className="block capitalize py-2 text-center text-sm text-gray-700 dark:text-gray-200"
                      key={idx}
                    >
                      {({ active }) => (
                        <a
                          className={`${active && "bg-emerald-200 w-full"}`}
                          href={"/" + lang+'/wallet'}
                        >
                          {lang === "ar" ? "ع" : lang}
                        </a>
                      )}
                    </Menu.Item>
                  );
                })}
              </Menu.Items>
            </Menu>
            </div>
          </nav>
        </div>
      </div>
    </header>
    </div>
  );
}
