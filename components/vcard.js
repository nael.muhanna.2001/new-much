import Link from "next/link";
import { useRouter } from "next/router";
import { translation } from "../lib/translations";
import PhoneOutput from "../components/cardComponents/phone"
import EmailOutput from "../components/cardComponents/email"
import UrlOutput from "../components/cardComponents/url"
import AddressOutput from "../components/cardComponents/address"
import TextOutput from "../components/cardComponents/text"
import { useSession } from "next-auth/react";


export default function Vcard({ card }) {
  const { data: session, status } = useSession();
  const { locale } = useRouter();
  
  //=== Translation Function Start
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  async function newClick(card_id, user_id, link) {
    let vistor=window.localStorage.getItem('user')?JSON.parse(window.localStorage.getItem('user')):null;
    console.log(card_id);
    const click = await fetch(
      "/api/link-click/?card_id=" +
        card_id +
        "&user_id=" +
        user_id +
        "&link=" +
        link+"&visitor_id="+vistor?._id,
      { method: "GET" }
    );
  }
  return (
    <>
      <section className="bg-white z-50  px-2">
        <div className="md:flex flex-wrap">
          {card.pic && (
            <div>
              <div className="relative m-2 text-center">
                <div className=" rounded-full w-32 shadow overflow-hidden mx-auto ">
                  <img src={card.pic} className="h-full w-full  shadow-xl mx-auto md:mx-0" />
                </div>
              </div>
            </div>
          )}
          <div className="m-2  text-center md:ltr:text-left md:rtl:text-right w-full md:w-auto">
            <div className="text-center md:text-start mx-2 mt-10   ltr:md:mb-6">
              <h3 className="text-2xl font-semibold leading-normal mb-1 text-blueGray-700 ">
                {card.name}
              </h3>
              {card.jobTitle && (
                <h4 className="text-lg font-semibold mb-1 md:inline-block md:ltr:mr-5 md:text-md md:text-start">
                  {card.jobTitle.label}
                </h4>
              )}
              {card.department && (
                <h4 className="text-lg leading-normal mt-0 mb-2 text-blueGray-600  md:inline-block">
                  {card.department.label}
                </h4>
              )}
              <br />
              {card.company && (
                <h4 className="mb-2 text-lg text-blueGray-600 mt-2 md:inline-block">
                  <Link href={"/" + card.company.slug} target="_blank">
                    <div>
                      {!card.company.pic && (
                        <i className="fas fa-building  text-lg text-blueGray-400 px-2"></i>
                      )}
                      {card.company.pic && (
                        <img
                          src={card.company.pic}
                          style={{
                            height: "32px",
                            width: "32px",
                            borderRadius: "100px",
                            display: "inline",
                            margin: "0 4px",
                          }}
                        />
                      )}
                      {card.company.label}
                    </div>
                  </Link>
                </h4>
              )}
            </div>
            <div className="my-4">
              <a href={"/api/vcard/?_id="+card._id} className="bg-white rounded-xl uppercase font-bold border p-3 hover:no-underline">{_('Save To Contacts')}</a>
            </div>
          </div>
          <div className="w-full  px-1 ltr:text-left rtl:text-right">
          {card && (
                      <div className="ltr:text-left rtl:text-right justify-center flex flex-wrap">
                        {card.fields.map((item, k) => {
                          return (
                            <div className="w-full border rounded my-1 py-1" key={k}>
                              {item.field.fieldType == "phone" &&
                                item.published && (
                                  <PhoneOutput
                                    handler={() => {
                                      if (item.field.name == "Whatsapp") {
                                        newClick(
                                          card._id,
                                          card.user_id,
                                          "whatsapp://send?" +
                                            item.prefix.value +
                                            "" +
                                            item.phone
                                        ).then(() => {
                                          window.location.href =
                                            "whatsapp://send?" +
                                            item.prefix.value +
                                            "" +
                                            item.phone;
                                        });
                                      } else {
                                        newClick(
                                          card._id,
                                          card.user_id,
                                          "tel:+" +
                                            item.prefix.value +
                                            "" +
                                            item.phone
                                        ).then(() => {
                                          window.location.href =
                                            "tel:+" +
                                            item.prefix.value +
                                            "" +
                                            item.phone;
                                        });
                                      }
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "email" &&
                                item.published && (
                                  <EmailOutput
                                    handler={() => {
                                      newClick(
                                        card._id,
                                        card.user_id,
                                        "mailto:" + item.email
                                      ).then(() => {
                                        window.location.href =
                                          "mailto:" + item.email;
                                      });
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "url" &&
                                item.published && (
                                  <UrlOutput
                                    handler={() => {
                                      newClick(
                                        card._id,
                                        card.user_id,
                                        item.url
                                      ).then(() => {
                                        window.location.href = item.url;
                                      });
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "address" &&
                                item.published && (
                                  <AddressOutput
                                    handler={() => {
                                      newClick(
                                        card._id,
                                        card.user_id,
                                        "Address"
                                      );
                                      window.location.href =
                                        "https://www.google.com/maps/dir/?api=1&destination=" +
                                        item.address?.formatted_address +
                                        "&destination_place_id=" +
                                        item.address.place_id;
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "text" &&
                                item.published && <TextOutput field={item} />}
                            </div>
                          );
                        })}
                      </div>
                    )}
          </div>
          {card.first_meet && (
            <div className="w-full  px-1 ltr:text-left rtl:text-right">
              <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                <i className="fas fa-handshake-alt text-xl"></i>
              </button>
              <h5 className="inline-block font-bold">
                <div className="text-xs uppercase text-gray-500">
                  {_("When we met")}
                </div>
                {new Date(card.first_meet.created_at).toLocaleDateString(
                  locale,
                  {
                    month: "short",
                    day: "numeric",
                    year: "numeric",
                    hour: "numeric",
                    minute: "numeric",
                  }
                )}
              </h5>
            </div>
          )}
          
          {card.first_meet && (card.first_meet.geoLocation || card.first_meet.location) && (
            <div className="w-full  px-1 ltr:text-left rtl:text-right">
            <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
              <i className="fas fa-map-marker text-xl"></i>
            </button>
            <h5 className="inline-block font-bold">
              <div className="text-xs uppercase text-gray-500">
                {_("Where we met")}
              </div>
              {card.first_meet.location.city},{" "}
                    {card.first_meet.location.country_name}
            </h5>
            {(card.first_meet && card.first_meet.geoLocation) &&<div className="m-4 w-10/12 h-48 border rounded-xl overflow-hidden">
                <div className="mapouter">
                  <div className="gmap_canvas">
                    <iframe
                      className="w-full h-48"
                      id="gmap_canvas"
                      src={
                        "https://maps.google.com/maps?q=" +
                        card.first_meet.geoLocation.lat +
                        "," +
                        card.first_meet.geoLocation.lng +
                        "&t=&z=16&ie=UTF8&iwloc=&output=embed"
                      }
                      frameBorder="0"
                      scrolling="no"
                      marginHeight="0"
                      marginWidth="0"
                    ></iframe>
                  </div>
                </div>
              </div>}
          </div>
            
          )}
        </div>
      </section>
    </>
  );
}
