import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Placeholder } from "rsuite";
import { translation } from "../../lib/translations";
import "rsuite/dist/rsuite.min.css";
export default function StoreHeader() {
  const [orderCounts, setOrderCounts] = useState();
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  useEffect(() => {
    fetch("/api/dashboard/store/orders/")
      .then((res) => res.json())
      .then((data) => setOrderCounts(data));
  }, []);
  if (orderCounts)
    return (
      <>
        <div className="w-full flex flex-wrap">
          <div className="w-6/12 lg:w-3/12">
            <div className="p-2 mr-2 border rounded bg-[#6A5E7A]">
              <h3 className="text-lg text-gray-300">{_("Total Orders")}</h3>
              <div className="text-white text-4xl font-bold">
                {orderCounts.totalOrders}
              </div>
            </div>
          </div>
          <div className=" w-6/12 lg:w-3/12 ">
            <div className="p-2 mr-2 border rounded bg-[#6A5E7A]">
              <h3 className="text-lg text-gray-300">{_("New Orders")}</h3>
              <div className="text-white text-4xl font-bold">
                {orderCounts.newOrders}
              </div>
            </div>
          </div>
          <div className=" w-6/12 lg:w-3/12 ">
            <div className="p-2 mr-2 border rounded bg-[#6A5E7A]">
              <h3 className="text-lg text-gray-300">{_("Paid Orders")}</h3>
              <div className="text-white text-4xl font-bold">
                {orderCounts.paidOrders}
              </div>
            </div>
          </div>
          <div className="w-6/12 lg:w-3/12">
            <div className="p-2 border rounded bg-[#6A5E7A]">
              <h3 className="text-lg text-gray-300">{_("Cancelled Orders")}</h3>
              <div className="text-white text-4xl font-bold">
                {orderCounts.cancelledOrders}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  return (
    <>
      <div className="w-full flex flex-wrap">
        <div className="w-6/12 lg:w-3/12">
          <div className="p-2 mr-2 border rounded bg-[#6A5E7A]">
            <Placeholder.Paragraph rows={1} rowHeight={20} active />
            <Placeholder.Paragraph rows={1} rowHeight={30} active />
          </div>
        </div>
        <div className="w-6/12 lg:w-3/12">
          <div className="p-2 mr-2 border rounded bg-[#6A5E7A]">
            <Placeholder.Paragraph rows={1} rowHeight={20} active />
            <Placeholder.Paragraph rows={1} rowHeight={30} active />
          </div>
        </div>
        <div className="w-6/12 lg:w-3/12">
          <div className="p-2 mr-2 border rounded bg-[#6A5E7A]">
            <Placeholder.Paragraph rows={1} rowHeight={20} active />
            <Placeholder.Paragraph rows={1} rowHeight={30} active />
          </div>
        </div>
        <div className="w-6/12 lg:w-3/12">
          <div className="p-2 border rounded bg-[#6A5E7A]">
            <Placeholder.Paragraph rows={1} rowHeight={20} active />
            <Placeholder.Paragraph rows={1} rowHeight={30} active />
          </div>
        </div>
      </div>
    </>
  );
}
