import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Select from "react-select";
import { countries } from "../../lib/countries";
import { translation } from "../../lib/translations";
const options = [];
countries.map((country) => {
  options.push({
    value: country.dial,
    label: country.name + " (" + country.dial + ")",
  });
});
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function URL({ field_id,cardId,urlValue,titleValue,labelValue,viewAccessValue, field,isDone }) {
  const [defaultCountry, setDefaultCountry] = useState();
  const [url, setUrl] = useState(urlValue || field.initialValue);
  const [label, setLabel] = useState(labelValue);
  const [title, setTitle] = useState(titleValue);
  const [viewAccess, setViewAccess] = useState(viewAccessValue || "public");
  const [isLoading, setIsLoading] = useState(false);
  let countryName = null;
  function updateField() {
    if (
      url &&
      url
        .toLowerCase()
        .match(
          /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/
        )
    ) {
      setIsLoading(true);
      fetch("/api/card-field/create/", {
        method: "POST",
        body: JSON.stringify({
          _id:field_id,
          field: field,
          cardId: cardId,
          url: url,
          label: label,
          title: title,
          viewAccess: viewAccess,
          order: 0,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setIsLoading(false);
          isDone(data);
        });
    }
  }


  return (
    <>
      <div className="mt-4 flex">
        <div className=" text-gray-500 px-2 w-1/12">
          <i className={field.icon + " text-3xl"}></i>
        </div>

        <input
          type="url"
          value={url}
          onChange={(e) => setUrl(e.target.value)}
          placeholder={_("URL")}
          className="border w-full mx-2  p-2 h-10 border-gray-400 rounded-xl  placeholder-blueGray-400 text-blueGray-600 bg-white   ease-linear transition-all duration-150"
        />
      </div>
      {field.withLabel && (
        <div className="mt-4 p-2 flex flex-wrap">
          <label className="w-2/12 text-gray-400 pt-1">
            {_("Label")} <small className="text-xs">({_("Optional")})</small>
          </label>
          <input
            value={label}
            onChange={(e) => {
              setLabel(e.target.value);
            }}
            className="border border-gray-400 rounded-xl w-10/12 p-2"
          />
          <div className="w-2/12"></div>
          {field.labelSuggestions.map((e, k) => {
            return (
              <div
                onClick={() => {
                  setLabel(e);
                }}
                key={k}
                className="p-2 m-2 bg-gray-200 cursor-pointer rounded-xl"
              >
                {e}
              </div>
            );
          })}
        </div>
      )}
      {field.withTitle && (
        <div className="mt-4 p-2 flex flex-wrap">
          <label className="w-2/12 text-gray-400 pt-1">
            {_("Title")} <small className="text-xs">({_("Optional")})</small>
          </label>
          <input
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
            className="border border-gray-400 rounded-xl w-10/12 p-2"
          />
          <div className="w-2/12"></div>
          {field.titleSuggestions.map((e, k) => {
            return (
              <div
                onClick={() => {
                  setTitle(e);
                }}
                key={k}
                className="p-2 m-2 bg-gray-200 cursor-pointer rounded-xl"
              >
                {e}
              </div>
            );
          })}
        </div>
      )}
      <div className="mt-4 p-2 flex">
        <label className="text-sm w-2/12 text-gray-400 pt-1">
          {_("View Access")}
        </label>
        <select
          value={viewAccess}
          onChange={(e) => {
            setViewAccess(e.target.value);
          }}
          className=" w-10/12 p-1 rounded-xl border border-gray-400"
        >
          <option value="public">{_("Public")}</option>
          <option value="connections">{_("Connections")}</option>
          <option value="private">{_("Only Me")}</option>
        </select>
      </div>
      <div className="mt-4 p-2 flex justify-end">
        <button
          onClick={updateField}
          className="border-2 border-emerald-300 p-4 rounded-xl"
        >
          {_("Save")}
        </button>
      </div>
    </>
  );
}
