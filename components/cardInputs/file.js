import { useState } from "react";

export default function File({field_id,cardId,titleValue,labelValue,viewAccessValue, field,isDone }) {
    
    const [defaultCountry, setDefaultCountry] = useState();
    const [phone, setPhone] = useState(phoneValue);
    const [prefix, setPrefix] = useState(prefixValue);
    const [label, setLabel] = useState(labelValue);
    const [title, setTitle] = useState(titleValue);
    const [viewAccess, setViewAccess] = useState(viewAccessValue || "public");
    const [isLoading, setIsLoading] = useState(false);
    let countryName = null;
    function updateField() {
      if(phone && prefix)
      {
        setIsLoading(true);
        
        fetch("/api/card-field/create/", {
          method: "POST",
          body: JSON.stringify({
            _id:field_id,
            field: field,
            cardId: cardId,
            prefix: prefix,
            phone:phone,
            label: label,
            title:title,
            viewAccess:viewAccess,
            order:0
          }),
        }).then(res=>res.json()).then(data=>{
          
          setIsLoading(false);
          isDone(data)
        });
      }
      
    }
    return <>

    </>

  }