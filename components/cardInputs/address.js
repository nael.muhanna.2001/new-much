import { useRouter } from "next/router";
import Script from "next/script";
import { useEffect, useState } from "react";
import Select from "react-select";
import { countries } from "../../lib/countries";
import { translation } from "../../lib/translations";
import Autocomplete from "react-google-autocomplete";
const options = [];
countries.map((country) => {
  options.push({
    value: country.ISO2,
    label: country.name,
  });
});
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function Address({
  field_id,
  cardId,
  countryValue,
  addressValue,
  titleValue,
  labelValue,
  viewAccessValue,
  field,
  isDone,
}) {
  const [defaultCountry, setDefaultCountry] = useState();
  const [country, setCountry] = useState(countryValue);
  const [address, setAddress] = useState(addressValue);
  const [label, setLabel] = useState(labelValue);
  const [title, setTitle] = useState(titleValue);
  const [viewAccess, setViewAccess] = useState(viewAccessValue || "public");
  const [isLoading, setIsLoading] = useState(false);
  let countryName = null;
  function updateField() {
    if (address && country) {
      setIsLoading(true);
      fetch("/api/card-field/create/", {
        method: "POST",
        body: JSON.stringify({
          _id: field_id,
          field: field,
          cardId: cardId,
          country: country,
          address: address,
          label: label,
          title: title,
          viewAccess: viewAccess,
          order: 0,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setIsLoading(false);
          isDone(data);
        });
    }
  }
  async function getLocation() {
    
    const location = await fetch(
      "https://ipgeolocation.abstractapi.com/v1/?api_key=8981df21aa7d4dfe9f299632ae1472d8",
      {
        method: "GET",
      }
    );
    let data = await location.json();
    countries.map((country) => {
      if (country.ISO2 == data.country_code) {
        countryName = {
          value: country.ISO2,
          label: country.name,
        };
      }
    });
    return countryName;
  }
  useEffect(() => {
    getLocation().then((data) => {
      if(!country)
      setCountry(data);
      setDefaultCountry(data);
    });
  }, []);

  return (
    <>
      <div className="mt-4 flex">
        <div className=" text-gray-500 px-2 w-1/12">
          <i className={field.icon + " text-3xl"}></i>
        </div>
        <Select
          value={countryValue || defaultCountry}
          onChange={(e) => {
            setCountry(e);
          }}
          className="text-xs w-4/12  rounded-xl outline-0 focus:outline-0"
          options={options}
        ></Select>
        <Autocomplete
        defaultValue={address?.formatted_address}
          apiKey={"AIzaSyBeoXqvPbcjNUkp6FOjmYNkqx6oLmYvcYg"}
          options={{
            types: ["geocode", "establishment"],
            componentRestrictions: { country: country?.value },
          }}
          onPlaceSelected={(place) => {
            setAddress(place);
          }}
          className="border w-8/12 mx-2  p-2 h-10 border-gray-400 rounded-xl  placeholder-blueGray-400 text-blueGray-600 bg-white   ease-linear transition-all duration-150"
        />
      </div>
      {field.withLabel && (
        <div className="mt-4 p-2 flex flex-wrap">
          <label className="w-2/12 text-gray-400 pt-1">
            {_("Label")} <small className="text-xs">({_("Optional")})</small>
          </label>
          <input
            value={label}
            onChange={(e) => {
              setLabel(e.target.value);
            }}
            className="border border-gray-400 rounded-xl w-10/12 p-2"
          />
          <div className="w-2/12"></div>
          {field.labelSuggestions.map((e, k) => {
            return (
              <div
                onClick={() => {
                  setLabel(e);
                }}
                key={k}
                className="p-2 m-2 bg-gray-200 cursor-pointer rounded-xl"
              >
                {e}
              </div>
            );
          })}
        </div>
      )}
      {field.withTitle && (
        <div className="mt-4 p-2 flex flex-wrap">
          <label className="w-2/12 text-gray-400 pt-1">
            {_("Title")} <small className="text-xs">({_("Optional")})</small>
          </label>
          <input
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
            className="border border-gray-400 rounded-xl w-10/12 p-2"
          />
          <div className="w-2/12"></div>
          {field.titleSuggestions.map((e, k) => {
            return (
              <div
                onClick={() => {
                  setTitle(e);
                }}
                key={k}
                className="p-2 m-2 bg-gray-200 cursor-pointer rounded-xl"
              >
                {e}
              </div>
            );
          })}
        </div>
      )}
      <div className="mt-4 p-2 flex">
        <label className="text-sm w-2/12 text-gray-400 pt-1">
          {_("View Access")}
        </label>
        <select
          value={viewAccess}
          onChange={(e) => {
            setViewAccess(e.target.value);
          }}
          className=" w-10/12 p-1 rounded-xl border border-gray-400"
        >
          <option value="public">{_("Public")}</option>
          <option value="connections">{_("Connections")}</option>
          <option value="private">{_("Only Me")}</option>
        </select>
      </div>
      <div className="mt-4 p-2 flex justify-end">
        <button
          onClick={updateField}
          className="border-2 border-emerald-300 p-4 rounded-xl"
        >
          {_("Save")}
        </button>
      </div>
    </>
  );
}
