import Link from "next/link";
async function newClick(card_id, user_id, link) {
  const click = await fetch(
    "/api/link-click/?card_id=" +
      card_id +
      "&user_id=" +
      user_id +
      "&link=" +
      link,
    { method: "GET" }
  );
}
export default function Card({card})
{
return (
    <>
    <main
        className="profile-page bg-blueGray-200 relative"
        style={{ minHeight: "100vh" ,minWidth:'360px'}}
      >
          <style jsx>
        {`
          h1,
          h2,
          h3,
          h4,
          h5,
          h6,
          p,
          i {
            color: ${card.color?.primary};
          }
          .card,
          button {
            background-color: ${card.color?.secondary};
          }

          .avatar {
            border-radius: ${card.avatar};
          }
          button {
            border-radius: ${card.buttonShape};
          }
        `}
      </style>
        <section>
          <div
            className={
              card.cover
                ? "relative block h-96 filter bt-top blur-lg grayscale brightness-200"
                : "relative block h-60"
            }
            style={{
              minHeight: "250px",
              background: "url(" + card.cover + ") 0 0 / cover",
            }}
          ></div>
          <div
            className="absolute top-0 w-full h-96 bg-top bg-fixed  bg-no-repeat"
            style={{
              backgroundAttachment: "fixed",
              backgroundSize: "100%",
              backgroundImage: "url(" + card.cover + ")",
              boxShadow: "inset 0 -200px 100px #00000088",
            }}
          >

          </div>
        </section>
        <section
          className="relative py-16 bg-blueGray-200"
          style={{ height: "100%" }}
        >
          <div className="container mx-auto px-4 w-full">
            <div
              className={"card relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg duration-200 -mt-64"}
            >
              <div className="px-6">
                <button className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute  ltr:right-3 rtl:left-3 top-3">
                  <i className="fas text-md fa-share-alt"></i>
                </button>
                {card.pic && (
                  <div className="flex flex-wrap justify-center md:justify-start md:w-fit md:ltr:float-left ltr:md:m-10">
                    <div className="w-full md:w-3/12 px-4 md:order-2 flex justify-center pb-10">
                      <div className="relative">
                        <div className="avatar w-28 overflow-hidden rounded-full h-auto align-middle absolute md:relative -m-16 ltr:-ml-16 rtl:-mr-16 rtl:md:mr-4 rtl:md:mt-4 ltr:md:-ml-16 md:-mt-8 max-w-150-px">
                          <img
                            src={card.pic}
                            className="h-full w-full  shadow-xl "
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                <div className="text-center md:text-start mx-2 mt-10  rtl:md:mr-32 ltr:md:mt-4 rtl:md:-mt-20 ltr:md:mb-24">
                  <h3 className="text-4xl font-semibold leading-normal mb-1 text-blueGray-700 ">
                    {card.name}
                  </h3>
                  {card.jobTitle && (
                    <h4 className="text-xl font-semibold mb-1 md:inline-block md:ltr:mr-5 md:text-md md:text-start">
                      {card.jobTitle.label}
                    </h4>
                  )}
                  {card.department && (
                    <h4 className="text-lg leading-normal mt-0 mb-2 text-blueGray-600  md:inline-block">
                      {card.department.label}
                    </h4>
                  )}
                  {card.company && (
                    <h4 className="mb-2 text-lg text-blueGray-600 mt-2 md:inline-block">
                      <Link href={"/" + card.company.slug}>
                        <div>
                          {!card.company.pic && (
                            <i className="fas fa-building  text-lg text-blueGray-400 px-2"></i>
                          )}
                          {card.company.pic && (
                            <img
                              src={card.company.pic}
                              style={{
                                height: "32px",
                                width: "32px",
                                borderRadius: "100px",
                                display: "inline",
                                margin: "0 4px",
                              }}
                            />
                          )}
                          {card.company.label}
                          </div>
                      </Link>
                    </h4>
                  )}
                </div>
                <div className="mt-2 py-5 border-t border-blueGray-200 text-center">
                  <div>
                    {card.description && (
                      <p className="whitespace-pre-line">
                        {card.description}
                      </p>
                    )}
                  </div>
                  {!card && (
                    <div className="text-center justify-center flex flex-wrap">
                      <div className="h-12 w-12 rounded-full p-3 m-2 leading-relaxed nimate-pulse bg-gray-200 shadow-md"></div>
                      <div className="h-12 w-12 rounded-full p-3 m-2 leading-relaxed nimate-pulse bg-gray-200 shadow-md"></div>
                      <div className="h-12 w-12 rounded-full p-3 m-2 leading-relaxed nimate-pulse bg-gray-200 shadow-md"></div>
                      <div className="h-12 w-12 rounded-full p-3 m-2 leading-relaxed nimate-pulse bg-gray-200 shadow-md"></div>
                    </div>
                  )}
                  {card &&
                    (card.contactShape == "horizontal" ||
                      !card.contactShape) && (
                      <div className="flex flex-wrap justify-center">
                        <div className="w-full  px-1">
                          {card.telephonePrefix && card.telephone && (
                            <button
                              onClick={(e) => {
                                e.preventDefault();
                                newClick(
                                  card._id,
                                  card.user_id,
                                  "tel:+" +
                                    card.telephonePrefix.value +
                                    "" +
                                    card.telephone
                                ).then(() => {
                                  window.location.href =
                                    "tel:+" +
                                    card.telephonePrefix.value +
                                    "" +
                                    card.telephone;
                                });
                              }}
                              className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md"
                            >
                              <i className="fa fa-phone text-xl"></i>
                            </button>
                          )}
                          {card.mobilePrefix && card.mobile && (
                            <button
                              onClick={(e) => {
                                e.preventDefault();
                                newClick(
                                  card._id,
                                  card.user_id,
                                  "tel:+" +
                                    card.mobilePrefix.value +
                                    "" +
                                    card.mobile
                                ).then(() => {
                                  window.location.href =
                                    "tel:+" +
                                    card.mobilePrefix.value +
                                    "" +
                                    card.mobile;
                                });
                              }}
                              className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md"
                            >
                              <i className="fa fa-mobile text-xl"></i>
                            </button>
                          )}
                          {card.whatsappPrefix && card.whatsapp && (
                            <button
                              onClick={(e) => {
                                e.preventDefault();
                                newClick(
                                  card._id,
                                  card.user_id,
                                  "https://wa.me/" +
                                    card.whatsappPrefix.value +
                                    "" +
                                    card.whatsapp
                                ).then(() => {
                                  window.location.href =
                                    "https://wa.me/" +
                                    card.whatsappPrefix.value +
                                    "" +
                                    card.whatsapp;
                                });
                              }}
                              className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md"
                            >
                              <i className="fab fa-whatsapp text-xl"></i>
                            </button>
                          )}
                          {card.email && (
                            <button
                              onClick={(e) => {
                                e.preventDefault();
                                newClick(
                                  card._id,
                                  card.user_id,
                                  "mailto:+" + card.email
                                ).then(() => {
                                  window.location.href =
                                    "mailto:" + card.email;
                                });
                              }}
                              className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md"
                            >
                              <i className="fas fa-envelope text-xl"></i>
                            </button>
                          )}
                        </div>
                      </div>
                    )}
                  {card && card.contactShape == "vertical" && (
                    <div className="w-full  px-1 ltr:text-left rtl:text-right">
                      {card.telephonePrefix && card.telephone && (
                        <div
                          onClick={(e) => {
                            e.preventDefault();
                            newClick(
                              card._id,
                              card.user_id,
                              "tel:+" +
                                card.telephonePrefix.value +
                                "" +
                                card.telephone
                            ).then(() => {
                              window.location.href =
                                "tel:+" +
                                card.telephonePrefix.value +
                                "" +
                                card.telephone;
                            });
                          }}
                        >
                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                            <i className="fa fa-phone text-xl"></i>
                          </button>
                          <h5 className="inline-block font-bold">
                            {"(" +
                              card.telephonePrefix.value +
                              ") " +
                              card.telephone}
                          </h5>
                        </div>
                      )}
                      {card.mobilePrefix && card.mobile && (
                        <div
                          onClick={(e) => {
                            e.preventDefault();
                            newClick(
                              card._id,
                              card.user_id,
                              "tel:+" +
                                card.mobilePrefix.value +
                                "" +
                                card.mobile
                            ).then(() => {
                              window.location.href =
                                "tel:+" +
                                card.mobilePrefix.value +
                                "" +
                                card.mobile;
                            });
                          }}
                        >
                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                            <i className="fa fa-mobile text-xl"></i>
                          </button>
                          <h5 className="inline-block font-bold">
                            {"(" +
                              card.mobilePrefix.value +
                              ") " +
                              card.mobile}
                          </h5>
                        </div>
                      )}
                      {card.whatsappPrefix && card.whatsapp && (
                        <div
                          onClick={(e) => {
                            e.preventDefault();
                            newClick(
                              card._id,
                              card.user_id,
                              "whatsapp://send?phone=" +
                                card.whatsappPrefix.value +
                                "" +
                                card.whatsapp
                            ).then(() => {
                              window.location.href =
                                "whatsapp://send?phone=" +
                                card.whatsappPrefix.value +
                                "" +
                                card.whatsapp;
                            });
                          }}
                        >
                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                            <i className="fab fa-whatsapp text-xl"></i>
                          </button>
                          <h5 className="inline-block font-bold">
                            {"(" +
                              card.whatsappPrefix.value +
                              ") " +
                              card.whatsapp}
                          </h5>
                        </div>
                      )}
                      {card.email && (
                        <div
                          onClick={(e) => {
                            e.preventDefault();
                            newClick(
                              card._id,
                              card.user_id,
                              "mailto:+" + card.email
                            ).then(() => {
                              window.location.href = "mailto:" + card.email;
                            });
                          }}
                        >
                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                            <i className="fas fa-envelope text-xl"></i>
                          </button>
                          <h5 className="inline-block font-bold">
                            {card.email}
                          </h5>
                        </div>
                      )}
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          
        </section>
      </main>
      </>)
}