import { useSession } from "next-auth/react";
import React from "react";
import { translation } from "../../lib/translations";
import { signOut } from "next-auth/react";
import { useRouter } from "next/router";

const UserDropdown = ({className}) => {
  const { locale } = useRouter();
  const { data: session, status } = useSession();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  // dropdown props
  const [dropdownPopoverShow, setDropdownPopoverShow] = React.useState(false);

  return (
    <>
      <a
        className={"text-blueGray-500 block "+className }
        href="#pablo"
        onClick={(e) => {
          e.preventDefault();
          setDropdownPopoverShow(!dropdownPopoverShow);
        }}
      >
        <div className="items-center flex">
          <span className="w-16 h-16 text-sm  text-white bg-blueGray-200 inline-flex items-center justify-center rounded-full">
            {session?.user && (
              <>
                {session.user.image && (
                  <img
                    className="w-full rounded-full align-middle border-none shadow-lg"
                    src={session.user.image}
                  />
                )}
                {!session.user.image && (
                  <img
                  className="w-full rounded-full align-middle border-none shadow-lg"
                    src={
                      "https://ui-avatars.com/api/?name=" + session.user.name
                    }
                  />
                )}
              </>
            )}
          </span>
        </div>
      </a>
      <div
        className={
          (dropdownPopoverShow ? "block " : "hidden ") +
          "bg-white absolute ltr:left-32 rtl:right-32 ltr:md:-left-10 rtl:md:-right-10 text-base z-50  py-2 list-none ltr:text-left rtl:text-right rounded shadow-lg min-w-48 ltr:lg:left-auto rtl:lg:right-auto ltr:md:right-10 rtl:md:left-10 md:top-16 md:w-48"
        }
      >
        {/* <div className="h-0 my-2 border border-solid border-blueGray-100" /> */}
        <a
          href={`/api/auth/signout`}
          className={
            "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent text-blueGray-700"
          }
          onClick={(e) => {
            e.preventDefault();
            signOut();
          }}
        >
          {_("Sign Out")}
        </a>
      </div>
    </>
  );
};

export default UserDropdown;
