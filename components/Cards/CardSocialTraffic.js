import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { translation } from "../../lib/translations";
//=== Translation Function Start
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
// components

export default function CardSocialTraffic({ dateRange, selectedCardIds }) {
  const [trafficList, setTrafficList] = useState([]);
  const [totalVists,SetTotalVisits]=useState(0);
  useEffect(() => {
    fetch("/api/dashboard/traffic-source/", {
      method: "post",
      body: JSON.stringify({
        dateRange: dateRange,
        selectCards: selectedCardIds,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setTrafficList(data.trafficList);
        SetTotalVisits(data.totalVists);
      });
  }, [dateRange, selectedCardIds]);
  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6  rounded-xl" style={{height:'335px',borderBottom:'solid 3px #00487d'}}>
        <div className="rounded-t mb-0  border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full  max-w-full flex-grow">
              <h3 className="font-semibold text-base text-white px-4 py-3 rounded-t-xl text-center" style={{backgroundColor:'#00487d'}}>
                {_('View Sources')}
              </h3>
            </div>
            <div className="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
              {/* <button
                className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
              >
                See all
              </button> */}
            </div>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          {/* Projects table */}
          <table className="items-center w-full bg-transparent border-collapse" >
            <thead className="thead-light">
              <tr>
                <th className="px-4 uppercase bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs  border-l-0 border-r-0 whitespace-nowrap font-semibold ltr:text-left rtl:text-right">
                  {_('Source')}
                </th>
                <th className="px-2 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold ltr:text-left rtl:text-right">
                  {_('View')}
                </th>
                <th className="px-2 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold ltr:text-left rtl:text-right min-w-140-px"></th>
              </tr>
            </thead>
            <tbody>
              {trafficList.map((e, k) => {
                return (
                  e.count>0 && (
                    <tr key={k}>
                      <th width="30%" className="border-t-0 uppercase px-4 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 ltr:text-left rtl:text-right">
                        {e.referrer}
                      </th>
                      <td width="20%" className="border-t-0 px-2 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                        {e.count}
                      </td>
                      <td width="40%" className="border-t-0 px-2 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                        <div className="flex items-center">
                          <span className="mr-2">{ Math.round((e.count/totalVists)*100)}%</span>
                          <div className="relative w-full">
                            <div className="overflow-hidden h-2 text-xs flex rounded bg-emerald-200">
                              <div
                                style={{ width: Math.round((e.count/totalVists)*100) }}
                                className="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-emerald-400"
                              ></div>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  )
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
