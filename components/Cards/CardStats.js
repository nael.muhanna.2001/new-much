import React from "react";
import PropTypes from "prop-types";

export default function CardStats({
  statSubtitle,
  statTitle,
  statArrow,
  statPercent,
  statPercentColor,
  statDescripiron,
  statIconName,
  statIconColor,
  stateBGColor,
  stateTextColor
}) {
  return (
    <>
      <div style={{backgroundColor:stateBGColor,color:stateTextColor}} className="relative select-none flex flex-col min-w-0 break-words  rounded-xl mb-4 md:mb-6 h-32 xl:mb-0">
        <div className="flex-auto p-3">
          <div className="flex flex-wrap">
            <div className="relative w-full  max-w-full flex-grow flex-1">
              <h5 className="font-bold uppercase text-sm text-center">
                {statSubtitle}
              </h5>
              {(statTitle!='null' || statTitle=="0") &&<div className="font-bold text-5xl lg:text-5xl text-right">
                {statTitle}
              </div>}
              {!(statTitle!='null' || statTitle=="0") &&<div className="font-bold text-2xl text-left text-blueGray-700 bg-gray-300 h-6 mx-auto w-15 rounded-md">
                
              </div>}
            </div>
            {statIconName &&<div className="relative w-auto pl-4 flex-initial">
              <div
                className={
                  "text-white leading-relaxed nimate-pulse duration-100 p-1 text-center inline-flex  w-12 h-12 shadow-lg rounded-full " +
                  statIconColor
                }
              >
                <i className={statIconName}></i>
              </div>
            </div>}
          </div>
          <p className="text-sm text-blueGray-400 mt-2">
            {statPercent && statPercent!=NaN && <span className={statPercentColor + " mr-2"}>
              <i
                className={
                  statArrow === "up"
                    ? "fas fa-arrow-up"
                    : statArrow === "down"
                    ? "fas fa-arrow-down"
                    : ""
                }
              ></i>{" "}
              {statPercent<10000?Math.round(statPercent):">100"+""}%
            </span>}
            {!statPercent  && statPercent!=NaN &&<span className={statPercentColor+ " mr-2"}><i></i> </span>}
            <span className="whitespace-nowrap">{statDescripiron}</span>
          </p>
        </div>
      </div>
    </>
  );
}

CardStats.defaultProps = {
  statSubtitle: "",
  statTitle: "",
  statArrow: "",
  statPercent: "Laoding ...",
  statPercentColor: "",
  statDescripiron: "",
  statIconName: "",
  statIconColor: "",
};

CardStats.propTypes = {
  statSubtitle: PropTypes.string,
  statTitle: PropTypes.string,
  statArrow: PropTypes.oneOf(["up", "down"]),
  statPercent: PropTypes.string,
  // can be any of the text color utilities
  // from tailwindcss
  statPercentColor: PropTypes.string,
  statDescripiron: PropTypes.string,
  statIconName: PropTypes.string,
  // can be any of the background color utilities
  // from tailwindcss
  statIconColor: PropTypes.string,
};
