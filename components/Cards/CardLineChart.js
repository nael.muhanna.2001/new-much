// build 82
import Chart from "chart.js";
import { useRouter } from "next/router";
import { useEffect } from "react";

import "rsuite/dist/rsuite.min.css";
import { translation } from "../../lib/translations";
//=== Translation Function Start
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function CardLineChart({
  dateRange,
  selectedCardIds,
  assemblyBy,
  chartShap,
  cards,
  labels,
  visits,
  datasets,
  cardCheck,
  getChartInfo,
  isLoading,
}) {
  useEffect(() => {
    if (labels.length == 0) getChartInfo();
    var ctx = document.getElementById("line-chart").getContext("2d");
    var config = {
      
      type: chartShap,
      
      data: {
        labels: labels,
        datasets: datasets,
      },
      options: {
        
        animation:{
          easing:"easeInOutExpo",
          duration:2000
        },
        
        maintainAspectRatio: false,
        responsive: true,
        title: {
          
          display: false,
          fontColor: "gray",
        },
        legend: {
          
          labels: {
            fontSize:10,
            fontColor: "gray",
            usePointStyle:true
          },
          align: "start",
          position: "bottom",
        },
        
        tooltips: {
          
          mode: "index",
          intersect: false,
        },

        scales: {
          xAxes: [
            {
              ticks: {
                fontColor: "rgba(0,0,0,.7)",
                fontSize:8,
              },
              display: true,
              
              scaleLabel: {
                
                display: false,
                labelString: "Month",
                fontColor: "gray",
                
              },
              gridLines: {
                
                display: true,
                borderDash: [10],
                borderDashOffset: [50],
                color: "rgba(33, 37, 41, 0.1)",
                zeroLineColor: "rgba(0, 0, 0, 0)",
                zeroLineBorderDash: [2],
                zeroLineBorderDashOffset: [2],
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                fontSize:8,
                fontColor: "rgba(0,0,0,0.5)",
              },
              display: true,
              scaleLabel: {
                display: false,
                labelString: "Value",
                fontColor: "white",
              },
              gridLines: {
                borderDash: [4],
                borderDashOffset: [4],
                drawBorder: false,
                color: "rgba(30,30, 0, 0.1)",
                zeroLineColor: "rgba(33, 37, 41, 0)",
                zeroLineBorderDash: [1],
                zeroLineBorderDashOffset: [1],
              },
            },
          ],
        },
      },
    };
    if (window.myLine) {
      window.myLine.destroy();
    }
    window.myLine = new Chart(ctx, config);
  }, [chartShap, labels]);

  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words w-full">
        <div className="rounded-t mb-0  bg-transparent">
          <div className="flex flex-wrap items-center">
            
          </div>
        </div>
        <div className="p-4 flex-auto">
          {/* Chart */}
          <div className="relative h-350-px">
            {isLoading && (
              <div className="w-full h-full absolute  left-0 bg-white text-center py-16 bg-opacity-50">
                <div className="text-lg animate-pulse border p-2 w-fit rounded-lg font-bold mx-auto">
                  {_("Loading...")}
                </div>
              </div>
            )}
            <canvas id="line-chart"></canvas>
          </div>
        </div>
      </div>
    </>
  );
}
