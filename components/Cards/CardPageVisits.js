import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { translation } from "../../lib/translations";
//=== Translation Function Start
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
// components

export default function CardPageVisits({ dateRange, selectedCardIds }) {
  const [list, setList] = useState([]);
  useEffect(() => {
    
      fetch("/api/dashboard/card-visit/", {
        method: "post",
        body: JSON.stringify({
          dateRange: dateRange,
          selectCards: selectedCardIds,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          let list = [];
          let visits = 0;
          data.forEach((e, i) => {
            e.bounce_rate = Math.round((((e.unique_users.length)/e.users.length)*100));
            e.unique_visits = e.unique_users.length;
            list.push(e);
          });
          setList(list);
        });
  }, [dateRange, selectedCardIds]);
  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-2 md:mb-4 rounded-xl" style={{height:'335px',borderBottom:'solid #00487d 3px'}}>
        <div className="rounded-t mb-0  border-0">
          <div className="flex flex-wrap items-center">
            <div className="relative w-full max-w-full flex-grow text-center">
              <h3 className="font-semibold text-base text-white px-4 py-3 rounded-t-xl" style={{backgroundColor:'#00487d'}}>
                {_('Card Views')}
              </h3>
            </div>
            <div className="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
              {/* <button
                className="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
              >
                See all
              </button> */}
            </div>
          </div>
        </div>
        <div className="block w-full overflow-x-auto h-96">
          {/* Projects table */}
          <table className="items-center w-full bg-transparent border-collapse">
            <thead>
              <tr>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold ltr:text-left rtl:text-right">
                  {_('Card')}
                </th>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold ltr:text-left rtl:text-right">
                  {_('Views')}
                </th>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold ltr:text-left rtl:text-right">
                  {_('Visitors')}
                </th>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold ltr:text-left rtl:text-right">
                {_('Returned Visitors Rate')}
                </th>
              </tr>
            </thead>
            <tbody>
              {list.map((e) => {
                return (
                  <tr key={e._id}>
                    <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 ltr:text-left rtl:text-right">
                      <Link href={"/"+(e.card.slug?e.card.slug:e.card._id)} target="_blank">
                        <div target="_blank">
                      {e.card.slug?e.card.slug:e.card.name} <i className="fa fa-external-link"> </i>
                      </div>
                      </Link>
                    </th>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {e.count}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {e.unique_visits}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {e.bounce_rate>50 &&<i className="fas fa-arrow-up text-green-500 mr-4"></i>}
                      {e.bounce_rate<=50 &&<i className="fas fa-arrow-down text-red-500 mr-4"></i>}
                      {e.bounce_rate}%
                    </td>
                  </tr>
                );
              })}
              
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
