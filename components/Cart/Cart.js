import Link from "next/link";
import { useEffect, useState } from "react"

export default function Cart()
{
    const [orderCount,setOrderCount]=useState(0);
    useEffect(()=>{
        fetch('/api/orders/my-orders/').then(res=>res.json()).then(data=>{
            setOrderCount(data.length);
        })
    },[])
    return <>
    <Link href="/orders/">
    <div className="cart rounded-full p-1 flex  fixed text-gray-400  mt-7 border bg-white right-32">
        <i className="fa fa-shopping-cart text-lg mx-2 my-1"></i> <div className="text-xs text-center rounded-full bg-emerald-300 py-1 w-6 h-6">{orderCount}</div>
    </div>
    </Link>
    </>
}