import { signIn } from "next-auth/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Select from "react-select";
import { translation } from "../../lib/translations";
import { countries } from "../../lib/countries";
const options = [];
countries.map((country) => {
  options.push({
    value: country.dial,
    label: country.name + " (" + country.dial + ")",
  });
});
export default function NewCard({ name, setCard, setCredentials }) {
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [cardType, setCardType] = useState("business_card");
  const [mobilePrefix, setMobilePrefix] = useState();
  const [mobile, setMobile] = useState();
  const [error, setError] = useState();
  const [isRegistering, setIsRegistering] = useState(false);
  //====== get Location =======
  async function getLocation() {
    const location = await fetch(
      "https://api.ipgeolocation.io/ipgeo?apiKey=8981df21aa7d4dfe9f299632ae1472d8",
      {
        method: "GET",
      }
    );
    let data = await location.json();
    countries.map((country) => {
      if (country.ISO2 == data.country_code2) {
        setMobilePrefix({
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        });
      }
    });
    return location;
  }
  useEffect(() => {
    getLocation();
  }, []);

  // ===== Create User =========//
  async function createUser(name, email, password) {
    const response = await fetch("/api/auth/signup/", {
      method: "POST",
      body: JSON.stringify({ name, email, password }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response;
  }

  onsubmit = async (e) => {
    e.preventDefault();
    if (name && email && password) {
      setIsRegistering(true);
      setError();
      if (!isRegistering) {
        const res = await createUser(name, email, password);
        const data = await res.json();

        if (res.status == "422") {
          setError(data["message"]);
          setIsRegistering(false);
        }
        if (res.status == "201") {
          let credentials = { email: email, password: password };
          setCredentials(credentials);
          let user_id = data._id;
          let slug = name
            .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
            .toLowerCase();
          let contactShape = "vertical";
          const cardRes = await fetch("/api/cards/create/", {
            method: "POST",
            body: JSON.stringify({
              user_id: user_id,
              cardType:cardType,
              slug: slug,
              contactShape: contactShape,
              name: name,
              email: email,
              mobilePrefix: mobilePrefix,
              mobile: mobile,
            }),
          });
          if (res.status == "201") {
            const card = await cardRes.json();
            setCard(card);
            setIsRegistering(false);
          }
        }
      } else {
        setIsRegistering(false);
        console.log(name, email, password);
      }
    }
  };
  return (
    <>
      <form onSubmit={onsubmit}>
        <h1 className="text-lg mb-4">{_("Create an Account")}</h1>
        <div className="flex flex-wrap mb-4">
          <label className="uppercase font-bold text-gray-600 text-sm w-full md:w-2/12">
            {_("Card Type")}
          </label>
          <div className="w-full md:w-10/12">
            <select
              className="form-control"
              value={cardType}
              onChange={(e) => {
                setCardType(e.target.value);
              }}
            >
              <option value="business_card">{_("business_card")}</option>
              <option value="company_profile">{_("company_profile")}</option>
            </select>
          </div>
        </div>
        <div className="flex flex-wrap mb-4">
          <label className="uppercase font-bold text-gray-600 text-sm w-full md:w-2/12">
            {_("Email")}
          </label>
          <div className="w-full md:w-10/12">
            <input
              type="email"
              className="form-control"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-4">
          <label className="uppercase font-bold text-gray-600 text-sm w-full md:w-2/12">
            {_("Mobile")}
          </label>
          <div className="flex w-10/12">
            <Select
              value={mobilePrefix}
              onChange={(e) => {
                setMobilePrefix(e);
              }}
              className="text-xs w-6/12  pt-1 ltr:mr-1 rtl:ml-1  rounded-sm outline-0 focus:outline-0"
              options={options}
            ></Select>
            <input
              value={mobile}
              placeholder={_("5XXXXXXXX")}
              type="number"
              className="w-full border border-gray-300 h-9.5 mt-1 text-md rounded p-2"
              onChange={(e) => setMobile(e.target.value)}
              onBlur={(e) => {
                setMobile(e.target.value);
              }}
              required
            />
          </div>
        </div>
        <div className="flex flex-wrap mb-4">
          <label className="uppercase font-bold text-gray-600 text-sm w-full md:w-2/12">
            {_("Password")}
          </label>
          <div className="w-full md:w-10/12">
            <input
              type="password"
              className="form-control"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </div>
        </div>
        <div className="mb-4">
          {error && <span className="text-red-500">{error}</span>}
          <button
            disabled={isRegistering}
            className="bg-emerald-300 text-gray-50 p-2 rounded-xl font-bold hover:bg-emerald-200 hover:text-gray-600 duration-200 float-right"
          >
            {_("Register")}{" "}
            {isRegistering && <i className="fa fa-spinner animate-spin"></i>}
          </button>
        </div>
      </form>
    </>
  );
}
