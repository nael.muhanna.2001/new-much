import Link from "next/link";
import { getCsrfToken, signIn, signOut, useSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { Menu } from "@headlessui/react";
import { translation } from "../lib/translations";
import { useState } from "react";

export default function Header({ id }) {
  const { data: session, status } = useSession();

  const loading = status === "loading";
  const { locale, locales, defaultLocale, asPath } = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const menuitems = [
    {
      name: {
        ar: "الرئيسية",
        en: "Home",
      },
      link: "home",
    },
    {
      name: {
        ar: "المميزات",
        en: "Features",
      },
      link: "features",
    },
    {
      name: {
        ar: "المنتجات",
        en: "Products",
      },
      link: "products",
    },
    {
      name: {
        ar: "اسئلة شائعة",
        en: "FAQ",
      },
      link: "faq",
    },
  ];

  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  // return (
  //   <header>
  //     <noscript>
  //       <style>{`.nojs-show { opacity: 1; top: 0; }`}</style>
  //     </noscript>
  //     <div className={styles.signedInStatus}>
  //       <p
  //         className={`nojs-show ${
  //           !session && loading ? styles.loading : styles.loaded
  //         }`}
  //       >
  // {!session && (
  //   <>
  //     <span className={styles.notSignedInText}>
  //       You are not signed in
  //     </span>
  //     <a
  //       href={`/api/auth/signin`}
  //       className={styles.buttonPrimary}
  //       onClick={(e) => {
  //         e.preventDefault()
  //         signIn()
  //       }}
  //     >
  //       Sign in
  //     </a>
  //   </>
  // )}
  // {session?.user && (
  //   <>
  //     {session.user.image && (
  //       <span
  //         style={{ backgroundImage: `url('${session.user.image}')` }}
  //         className={styles.avatar}
  //       />
  //     )}
  //     <span className={styles.signedInText}>
  //       <small>Signed in as</small>
  //       <br />
  //       <strong>{session.user.email ?? session.user.name}</strong>
  //     </span>
  //     <a
  //       href={`/api/auth/signout`}
  //       className={styles.button}
  //       onClick={(e) => {
  //         e.preventDefault()
  //         signOut()
  //       }}
  //     >
  //       Sign out
  //     </a>
  //   </>
  // )}
  //       </p>
  //     </div>

  //   </header>
  // )

  return (
    <header id={id} className={"fixed z-50 w-full"}>
      <div className="navigation  top-0 left-0 w-full z-50 duration-300 bg-white drop-shadow-lg">
        <div className="container">
          <nav
            className={
              "navbar py-2 navbar-expand-lg w-full md:flex  items-center relative duration-300 "
            }
          >
            <div className="w-12/12 md:w-2/12">
              <Link className="navbar-brand" href="/">
                <img
                  src="/images/logo.svg"
                  alt="Logo"
                  className="logo duration-500 w-32 mx-auto"
                />
              </Link>
            </div>
            <div
              className={
                !isOpen
                  ? "hidden lg:block w-full md:w-8/12 duration-300 shadow absolute top-100 left-0  bg-white z-20 md:px-5 py-3  lg:static lg:bg-transparent lg:shadow-none"
                  : "block duration-300 shadow absolute -top-24 mt-40 lg:block left-0 mt-full bg-white z-20 px-5 py-3 w-full lg:static lg:bg-transparent lg:shadow-none"
              }
            >
              <ul className="navbar-nav mr-auto justify-center items-center lg:flex ">
                {menuitems.map((item, id) => {
                  return (
                    <li
                      onClick={() => setIsOpen(false)}
                      data-menuanchor={item.link}
                      key={id}
                      className={
                        asPath == item.link
                          ? "nav-item border text-emerald-600 rounded-lg"
                          : "nav-item"
                      }
                    >
                      <a
                        className={"page-scroll "}
                        href={"#" + item.link}
                        locale={locale}
                      >
                        {item.name[locale]}
                      </a>
                    </li>
                  );
                })}
              </ul>
            </div>
            <div className="header-btn ltr:md:ml-96 rtl:md:mr-96 -mx-1 md:w-2/12 flex">
              {!session && (
                <div className="dashboard-button text-emerald-500 border md:border-emerald-600 overflow-hidden  h-12 w-fit p-3 rounded-full duration-300 hover:bg-emerald-300 hover:text-white">
                  <a
                    href={`/signin`}
                    className=" hover:bg-transparent hover:no-underline hover:font-bold"
                  >
                    {_("Sign In")}
                  </a>
                </div>
              )}
              {session?.user && (
                <>
                  <div className="dashboard-button text-emerald-500 border md:text-emerald-600 overflow-hidden  h-12 rounded-full duration-300 hover:bg-emerald-300 hover:text-white">
                    {session.user && (
                    <a className=" hover:bg-transparent hover:no-underline hover:font-bold" href={`/dashboard/`}>
                    <div className=" md:block  whitespace-nowrap my-3 px-3">
                      {_("Dashboard")}
                    </div>
                  </a>
                    )}
                    
                  </div>
                </>
              )}
              <Menu>
                <Menu.Button className="lang-button ltr:ml-auto rtl:mr-auto ltr:lg:ml-2 rtl:lg:mr-2 button  md:block  capitalize mx-4 border text-gray-900  border-emerald-400 hover:border-emerald-500 focus:ring-4 focus:outline-none focus:ring-emerald-200 font-medium rounded-full text-sm px-4 py-2.5 text-center  items-center dark:text-emerald-600 dark:hover:border-emerald-400 w-12 h-12">
                  {locale === "ar" ? "ع" : locale}
                </Menu.Button>
                <Menu.Items className="z-10 px-4  bg-white divide-y divide-gray-100 rounded shadow  dark:bg-gray-700 w-20 absolute top-20 ltr:right-8 rtl:left-8 rtl:lg:left-0 ltr:lg:right-0">
                  {locales.map((lang, idx) => {
                    return (
                      <Menu.Item
                        className="block capitalize py-2 text-center text-sm text-gray-700 dark:text-gray-200"
                        key={idx}
                      >
                        {({ active }) => (
                          <a
                            className={`${active && "bg-emerald-300 w-full"}`}
                            href={"/" + lang}
                          >
                            {lang === "ar" ? "ع" : lang}
                          </a>
                        )}
                      </Menu.Item>
                    );
                  })}
                </Menu.Items>
              </Menu>
              <button
                onClick={() => setIsOpen(!isOpen)}
                className="focus:outline-none block relative rtl:-left-5 ltr:-right-5 rtl:md:left-auto ltr:md:right-auto lg:hidden"
                type="button"
              >
                <span className="toggler-icon"></span>
                <span className="toggler-icon"></span>
                <span className="toggler-icon"></span>
              </button>
            </div>
          </nav>
        </div>
      </div>
    </header>
  );
}
