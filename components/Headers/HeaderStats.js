

// components

import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { translation } from "../../lib/translations";
import CardStats from "../Cards/CardStats";
//=== Translation Function Start
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

export default function HeaderStats() {
  const [isLoading,setIsLoading]=useState(false);
  const [dayVisit,setDayVisit]= useState(null);
  const [pastDayVisit,setPastDayVisit]= useState(null);
  const [weekVisit,setWeekVisit]= useState(null);
  const [pastWeekVisit,setPastWeekVisit]= useState(null);
  const [monthVisit,setMonthVisit]= useState(null);
  const [pastMonthVisit,setPastMonthVisit]= useState(null);
  const [allVisit,setAllVisit]= useState(null);
  useEffect(()=>{
    if(!isLoading)
    {
      fetch('/api/dashboard/visits').then(res=>res.json()).then(data=>{
        setDayVisit(data.day_visit_count);
        setPastDayVisit(data.past_day_visit_count)
        setWeekVisit(data.week_visit_count)
        setPastWeekVisit(data.past_week_visit_count)
        setMonthVisit(data.month_visit_count)
        setPastMonthVisit(data.past_month_visit_count)
        setAllVisit(data.all_visit_count)
      })
    }
  },[])
  return (
    <>
      {/* Header */}
      <div className="-mx-1">
        <div className=" mb-24 mx-auto   w-full rounded-xl py-1 lg:py-3">
          <div>
            {/* Card stats */}
            <div className="flex flex-wrap">
              <div className="w-6/12 lg:w-6/12 xl:w-3/12 px-2">
                <CardStats
                  statSubtitle={_('Today Visits')}
                  statTitle={dayVisit+""}
                  statArrow={dayVisit>pastDayVisit?"up":"down"}
                  statPercent={(((dayVisit/pastDayVisit)-1)*100)+""}
                  statPercentColor={dayVisit>pastDayVisit?"text-emerald-500":"text-red-500"}
                  statDescripiron=""
                  statIconName=""
                  stateBGColor="#084874"
                  stateTextColor="#fff"
                />
              </div>
              <div className="w-6/12 lg:w-6/12 xl:w-3/12 px-2 ">
                <CardStats
                  statSubtitle={_("Week Visits")}
                  statTitle={weekVisit+""}
                  statArrow={weekVisit>pastWeekVisit?"up":"down"}
                  statPercent={(((weekVisit/pastWeekVisit)-1)*100)+""}
                  statPercentColor={weekVisit>pastWeekVisit?"text-emerald-500":"text-red-500"}
                  statDescripiron=""
                  statIconName=""
                  statIconColor="bg-transparent"
                  stateBGColor="#d0d0d0"
                  stateTextColor="#333"
                />
              </div>
              <div className="w-6/12 lg:w-6/12 xl:w-3/12 px-2">
                <CardStats
                  statSubtitle={_("Month Visits")}
                  statTitle={monthVisit+""}
                  statArrow={monthVisit>pastMonthVisit?"up":"down"}
                  statPercent={(((monthVisit/pastMonthVisit)-1)*100)+""}
                  statPercentColor={monthVisit>pastMonthVisit?"text-emerald-500":"text-red-500"}
                  statDescripiron=""
                  statIconName=""
                  statIconColor=""
                  stateBGColor="#084874"
                  stateTextColor="#fff"
                />
              </div>
              <div className="w-6/12 lg:w-6/12 xl:w-3/12 px-2">
                <CardStats
                  statSubtitle={_("All Visits")}
                  statTitle={allVisit+""}
                  statPercent=""
                  statPercentColor=""
                  statDescripiron=""
                  statIconName=""
                  statIconColor=""
                  stateBGColor="#d0d0d0"
                  stateTextColor="#333"
                />
              </div>
            </div>
          </div>
        </div>
        </div> 
    </>
  );
}
