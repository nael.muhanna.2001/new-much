// Input onFocus append class handler
export const handleFocus = ( e ) => {
  e.target.parentElement.classList.add('focused');
}

// Input onBlur remove class handler
export const handleFocusOut = ( e ) => {
  const hasValue = e.target.value;

  if (!hasValue) {
    e.target.parentElement.classList.remove('focused');
  }
}

// Input onKeyDown numbers only handler
export const handleNumbersOnly = ( e ) => {
  let flag;

  if((e.keyCode === 8) ||
      (e.keyCode === 9) || 
      (e.keyCode === 16 && e.keyCode >= 9) ||
      (e.keyCode === 37) ||
      (e.keyCode === 39) ||
      (e.keyCode === 46) || 
      (e.keyCode >= 48 && e.keyCode <= 57) || 
      (e.keyCode >= 96 && e.keyCode <= 105)) {
    flag = false;
  } else {
    flag = true;
  }

  if(flag) {
    e.preventDefault();
  }
}

// Get card type based on card number
export const getCardType = ( number ) => {
  if (number !== '' || number !== null) {
    const amexReg   = new RegExp('^3[47][0-9]{13}$');
    const jbcReg    = new RegExp('^(?:2131|1800|35\d{3})\d{11}$');
    const masterReg = new RegExp('^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$');
    const visaReg   = new RegExp('^4[0-9]{12}(?:[0-9]{3})?$');
    const madaReg = new RegExp('/(4(0(0861|1757|7(197|395)|9201)|1(0685|7633|9593)|2(281(7|8|9)|8(331|67(1|2|3)))|3(1361|2328|4107|9954)|4(0(533|647|795)|5564|6(393|404|672))|5(5(036|708)|7865|8456)|6(2220|854(0|1|2|3))|8(301(0|1|2)|4783|609(4|5|6)|931(7|8|9))|93428)|5(0(4300|8160)|13213|2(1076|4(130|514)|9(415|741))|3(0906|1095|2013|5(825|989)|6023|7767|9931)|4(3(085|357)|9760)|5(4180|7606|8848)|8(5265|8(8(4(5|6|7|8|9)|5(0|1))|98(2|3))|9(005|206)))|6(0(4906|5141)|36120)|9682(0(1|2|3|4|5|6|7|8|9)|1(0|1)))\d{10}$/');

    if (number.toString().match(amexReg)) {
      return 'amex';
    } else if (number.toString().match(jbcReg)) {
      return 'jcb';
    } else if (number.toString().match(masterReg)) {
      return 'mastercard';
    } else if (number.toString().match(visaReg)) {
      return 'visa';
    } 
     else {
      return 'invalid';
    }
  }
}