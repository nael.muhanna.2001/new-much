import { useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { ToastContainer } from "react-toastify";
import { translation } from "../lib/translations";

const Wrapper = ({ children }) => {
  const { locale } = useRouter();
  const { data: session, status } = useSession();

  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  return (
    <>
      {status!='loading' &&<div className="sign-button z-40 md:fixed p-2 px-3 text-sm bg-white bg-opacity-10 rounded-full" style={{top:'27px',right:'130px'}}>
        {!session && (
          <Link href="/signin">
            <button>
              {_("Sign in")}{" "}
              {locale != "ar" ? (
                <i className="fa fa-chevron-right"></i>
              ) : (
                <i className="fa fa-chevron-left"></i>
              )}
            </button>
          </Link>
        )}
        {session && (
          <Link href="/dashboard">
            <button>
              {_("Dashboard")}{" "}
              {locale != "ar" ? (
                <i className="fa fa-chevron-right"></i>
              ) : (
                <i className="fa fa-chevron-left"></i>
              )}
            </button>
          </Link>
        )}
      </div>}
      {children}
      <ToastContainer />
    </>
  );
};

export default Wrapper;
