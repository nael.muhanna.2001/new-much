/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./webComponents/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundOpacity: {
        '10': '0.1',
        '20': '0.2',
        '30': '0.3',
        '40': '0.4',
        '50': '0.5',
        '60': '0.6',
        '30': '0.3',
        '30': '0.3',
        '30': '0.3',
        '30': '0.3',
       },
      colors: {
        maincolor: "#00487D",
        green: "#8CCAAE",
      },
      animation: {
        marquee: "marquee 3s linear",
        marquee2: "marquee2 25s linear infinite",
        popup: "popup 10s ease-in-out infinite",
      },
      keyframes: {
        popup: {
          "0%,40%,100%": {
            transform: "scale(0,0)",
          },
          "42%": {
            transform: "scale(1,1)",
          },
          "43%": {
            transform: "rotate(-5deg)",
          },
          "44%": {
            transform: "rotate(0deg)",
          },
          "98%": {
            transform: "scale(1,1)",
          },
        },
        marquee2: {
          "0%": { transform: "translateX(0%)" },
          "100%": { transform: "translateX(-150%)" },
        },
        marquee: {
          
          "0%": { transform: "translateX(-100%)" },
          "100%": { transform: "translateX(0%)" },
        },
      },
    },
  },
  plugins: [],
};
