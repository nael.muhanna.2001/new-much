import Link from "next/link";
import React from "react";
import { useCallback, useEffect, useRef, useState } from "react";
import QRCode from "easyqrcodejs";
import Head from "next/head";
import { translation } from "../lib/translations";
import { useRouter } from "next/router";
import cookie from "js-cookie";
import { useSession } from "next-auth/react";
import { useGeolocated } from "react-geolocated";
import { Dropdown, IconButton } from "rsuite";
import ShareOutline from "@rsuite/icons/ShareOutline";
import "rsuite/dist/rsuite.min.css";
import {
  WhatsappShareButton,
  WhatsappIcon,
  TelegramShareButton,
  TelegramIcon,
  FacebookMessengerShareButton,
  FacebookMessengerIcon,
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  LinkedinShareButton,
  LinkedinIcon,
} from "next-share";
import PhoneOutput from "../components/outputComponents/phone";
import EmailOutput from "../components/outputComponents/email";
import UrlOutput from "../components/outputComponents/url";
import TextOutput from "../components/outputComponents/text";
import AddressOutput from "../components/outputComponents/address";

export default function Profile({ slug, staticCard, src }) {
  const router = useRouter();

  const { locale, query } = useRouter();

  //=== Translation Function Start
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  //=== Translation Function End
  const renderIconButton = (props, ref) => {
    return (
      <IconButton
        {...props}
        ref={ref}
        icon={<ShareOutline />}
        circle
        appearance="link"
      />
    );
  };
  const { data: session, status } = useSession();
  const rounter = useRouter();
  const { coords, isGeolocationAvailable, isGeolocationEnabled } =
    useGeolocated({
      positionOptions: {
        enableHighAccuracy: false,
      },
      userDecisionTimeout: 5000,
    });
  const [isLoading, setIsLoading] = useState(false);
  const [saveMenuOpen, setSaveMenuOpen] = useState(false);
  const [card, setCard] = useState();
  const qrCodeRef = useRef(null);
  const [showQR, setShowQR] = useState(false);
  const [QRcolorDark, setQRcolorDark] = useState("#000000");
  const [QRcolorLight, setQrColorLight] = useState("#ffffff");
  const [qrLogo, setQrLogo] = useState();
  const [qrlogoWidth, setQrlogoWidth] = useState();
  const [qrlogoBackgroundTransparent, setQrLogoBackgroundTransparent] =
    useState(false);
  const [qrbackgroundImage, setQrbackgroundImage] = useState();
  const [qrbackgroundImageAlpha, setQrbackgroundImageAlpha] = useState(1);
  const [isSavingToWallet, setIsSavingToWallet] = useState(false);
  const [walletClass, setWalletClass] = useState("");
  const [isSaved, setIsSaved] = useState(false);
  const [cardFields, setCardFields] = useState([]);
  let user = null;
  async function saveToWallet(card_id) {
    setIsSavingToWallet(true);
    fetch("/api/wallet/save-to-wallet/?card_id=" + card_id)
      .then((res) => res.json())
      .then((data) => {
        setIsSaved(true);
        setWalletClass("animate-ping duration-200");
        setTimeout(() => {
          setWalletClass("");
        }, 400);
        setIsSavingToWallet(false);
      });
  }
  async function getLocation() {
    const location = await fetch(
      "https://ipgeolocation.abstractapi.com/v1/?api_key=8981df21aa7d4dfe9f299632ae1472d8",
      {
        method: "GET",
      }
    );

    return location;
  }
  async function newClick(card_id, user_id, link) {
    let visitor = window.localStorage.getItem('user')?JSON.parse(window.localStorage.getItem('user')):null;
    const click = await fetch(
      "/api/link-click/?card_id=" +
        card_id +
        "&user_id=" +
        user_id +
        "&link=" +
        link+"&visitor_id="+visitor?._id,
      { method: "GET" }
    );
  }
  async function downloadVcard(card_id) {
    fetch("/api/cards/vcard", {
      method: "POST",
      body: JSON.stringify({ card_id: card_id }),
    })
      .then((res) => res.json())
      .then((data) => {});
  }
  useEffect(() => {
    let srcq = rounter.query["src"];

    if (coords && card && card.visit) {
      fetch("/api/update-visit/", {
        method: "POST",
        body: JSON.stringify({
          _id: card.visit._id,
          geolocation: { lat: coords.latitude, lng: coords.longitude },
          src: srcq,
        }),
      });
    }
  }, [coords, card]);

  useEffect(() => {
    let action = rounter.query["action"];
    let referrer = null;
    if (!isLoading && status != "loading") {
      setIsLoading(true);
      setCard(null);
      setShowQR(false);
      getLocation().then((location) => {
        location.json().then((data) => {
          if (window.localStorage.getItem("user") && !session) {
            user = window.localStorage.getItem("user");
          } else {
            user =
              session && session.user ? JSON.stringify(session.user) : null;
          }
          if (typeof document !== "undefined") {
            referrer = document.referrer ? document.referrer : null;
          }
          fetch("/api/post-user/", { method: "post", body: user })
            .then((res) => res.json())
            .then((userData) => {
              window.localStorage.setItem("user", JSON.stringify(userData));
              user  =window.localStorage.getItem('user');
              if (action == "save") {
                saveToWallet(staticCard._id);
              }
              if (userData.wallet) {
                setIsSaved(
                  userData.wallet.filter((e) => e.card_id == staticCard._id)
                    .length
                );
              }
              src =
                window && window.location.hash
                  ? window.location.hash.split("#")[1]
                  : null;
              console.log(src);

              fetch("/api/cards/" + slug+'/', {
                method: "POST",
                body: JSON.stringify({
                  card_id: staticCard._id,
                  location: data,
                  src: src,
                  referrer: referrer,
                  user:JSON.parse(user)
                }),
              })
                .then((res) => res.json())
                .then((data) => {
                  setCard(data);
                  setCardFields(data.fields);
                  setIsLoading(false);
                  if (src != null) {
                    router.replace("/" + slug);
                  }
                  return;
                });
            });
        });
      });
    }
  }, [slug, status]);
  const showQrFn = async () => {
    setShowQR(!showQR);

    let opt = {
      text: "https://" + window.location.host + "/qr/" + slug,
      width: 160,
      height: 160,
      logoMaxWidth: 40,
      logoMaxHeight: 40,
      drawer: "canvas",
      autoColor: true,
      colorDark: staticCard.QRcolorDark ? staticCard.QRcolorDark : "#000000",
      colorLight: staticCard.QRcolorLight ? staticCard.QRcolorLight : "#ffffff",
      correctLevel: QRCode.CorrectLevel.H,
      dotScaleTiming: 1,
      dotScale: 1,
      dotScaleTiming_V: 1,
      dotScaleTiming_h: 0.9,
      dotScaleA: 1,
      dotScaleAO: 1,
      dotScaleAI: 1,
      quietZone: 10,
      logo: staticCard.qrLogo,
      logoWidth: staticCard.qrlogoWidth,
      logoHeight: staticCard.qrlogoWidth,
      logoBackgroundTransparent: staticCard.qrlogoBackgroundTransparent,
      backgroundImage: staticCard.qrbackgroundImage,
      backgroundImageAlpha: staticCard.qrbackgroundImageAlpha,
    };

    qrCodeRef.current.innerHTML = null;
    new QRCode(qrCodeRef.current, opt);
    let rectArr = [];
    let rects = document.getElementsByTagName("rect");
    setTimeout(() => {
      rectArr = [...rects];
      rectArr.map((e) => {
        e.setAttribute("rx", "10");
        e.setAttribute("ry", "10");
      });
    }, 100);
  };
  if (!src)
    return (
      <>
        <Head>
          <title>
            much... | {staticCard.name}{" "}
            {staticCard.cardType == "business_card"
              ? _("Business Card")
              : staticCard.cardType == "company_profile"
              ? _("Profile")
              : staticCard.cardType == "product_card"
              ? "Card"
              : ""}
          </title>
          <meta name="robots" content="all" />
          <meta
            property="og:title"
            content={
              staticCard.cardType == "business_card"
                ? staticCard.name + " " + _("Business Card")
                : staticCard.cardType == "company_profile"
                ? staticCard.name + " " + _("Profile")
                : staticCard.cardType == "product_card"
                ? staticCard.name + " " + _("Card")
                : staticCard.name
            }
          />
          <meta property="og:type" content="business.card" />
          <meta property="og:url" content={"https://much.sa/" + slug} />
          <meta property="og:image" content={staticCard.pic} />
          <meta name="twitter:title" content={staticCard.name} />
          <meta
            name="twitter:description"
            content={
              staticCard.name +
              " | " +
              staticCard.jobTitle?.label +
              "@" +
              staticCard.company?.label
            }
          />
          <meta
            name="description"
            content={
              staticCard.name +
              " | " +
              staticCard.jobTitle?.label +
              "@" +
              staticCard.company?.label
            }
          />
          <meta name="twitter:image" content={staticCard.pic} />
          <meta name="twitter:card" content="summary_large_image"></meta>
          <meta
            property="og:site_name"
            content="much... | Where Professionals meet"
          ></meta>
        </Head>
        <style jsx>
          {`
            h1 {
              color: #333;
            }
            h2,
            h3,
            h4,
            h5,
            h6,
            p,
            i {
              color: ${staticCard.color?.primary};
            }
            .card,
            button {
              background-color: ${staticCard.color?.secondary};
            }

            .avatar {
              border-radius: ${staticCard.avatar};
            }
            button {
              border-radius: ${staticCard.buttonShape};
            }
          `}
        </style>
        <main
          className="profile-page  bg-blueGray-200"
          style={{ minHeight: "100vh" }}
        >
          <section className="z-50">
            <div
              className={
                staticCard.cover
                  ? "relative block h-96 filter blur-lg bg-top grayscale brightness-200"
                  : "relative block h-96"
              }
              style={{
                background: "url(" + staticCard.cover + ") 0 0 / cover",
              }}
            ></div>
            <div
              className="absolute top-0 w-full h-96 bg-top bg-fixed  bg-no-repeat"
              style={{
                backgroundAttachment: "fixed",
                backgroundSize: "100%",
                backgroundImage: "url(" + staticCard.cover + ")",
                boxShadow: "inset 0 -200px 100px #00000088",
              }}
            >
              {card && (
                <button
                  onClick={() => {
                    showQrFn();
                  }}
                  className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute ltr:right-3 rtl:left-3 top-3"
                >
                  <i className="fas text-md fa-qrcode"></i>
                </button>
              )}
              {session && (
                <button
                  onClick={() => {
                    window.location.href = "/dashboard/wallet/";
                  }}
                  className={
                    walletClass +
                    " bg-white  shadow-md h-10 w-10 border-gray-500 rounded-full absolute ltr:left-3 rtl:right-3 top-3"
                  }
                >
                  <i className="fas text-md fa-wallet"></i>
                </button>
              )}
              {!session && (
                <button
                  onClick={() => {
                    let link =
                      "/signin/?callback=" +
                      (typeof window != "undefined"
                        ? window.location.href
                        : null);
                    window.location.href = link;
                  }}
                  className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute ltr:left-3 rtl:right-3 top-3"
                >
                  <i className="fas text-md fa-wallet"></i>
                </button>
              )}
              <div
                className={
                  showQR
                    ? "relative mx-auto mt-10 p-4   qrCodeContainer"
                    : "relative mx-auto mt-10 h-32 w-32 overflow-hidden qrCodeContainer hidden"
                }
                ref={qrCodeRef}
                style={{
                  width: "250px",
                  height: "250px",
                }}
              ></div>
            </div>
          </section>
          <section
            className="relative py-16 bg-blueGray-200"
            style={{ height: "100%" }}
          >
            <div className="container mx-auto px-4">
              <div
                className={
                  showQR
                    ? "card z-50 relative flex flex-col min-w-0 break-words bg-white w-full mb-48 shadow-xl rounded-xl duration-200 -mt-16"
                    : "card z-50 relative flex flex-col min-w-0 break-words bg-white w-full mb-48 shadow-xl rounded-xl duration-200 -mt-64"
                }
              >
                <div className="px-6">
                  <div className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute  ltr:right-3 rtl:left-3 top-3">
                    <Dropdown
                      renderToggle={renderIconButton}
                      placement={locale == "ar" ? "bottomStart" : "bottomEnd"}
                      className="rtl:text-right ltr:text-left  top-1 md:flex"
                    >
                      <Dropdown.Item>
                        <WhatsappShareButton
                          url={
                            typeof window != "undefined"
                              ? window.location.href
                              : ""
                          }
                          quote={
                            "much... | " +
                            staticCard.name +
                            " " +
                            (staticCard.cardType == "business_card"
                              ? _("Business Card")
                              : staticCard.cardType == "company_profile"
                              ? _("Profile")
                              : staticCard.cardType == "product_card"
                              ? "Card"
                              : "")
                          }
                        >
                          <div className="flex">
                            <WhatsappIcon size={32} round />
                          </div>
                        </WhatsappShareButton>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <TelegramShareButton
                          url={
                            typeof window != "undefined"
                              ? window.location.href
                              : ""
                          }
                          title={
                            "much... | " +
                            staticCard.name +
                            " " +
                            (staticCard.cardType == "business_card"
                              ? _("Business Card")
                              : staticCard.cardType == "company_profile"
                              ? _("Profile")
                              : staticCard.cardType == "product_card"
                              ? "Card"
                              : "")
                          }
                        >
                          <div className="flex">
                            <TelegramIcon size={32} round />
                          </div>
                        </TelegramShareButton>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <LinkedinShareButton
                          url={
                            typeof window != "undefined"
                              ? window.location.href
                              : ""
                          }
                          title={
                            "much... | " +
                            staticCard.name +
                            " " +
                            (staticCard.cardType == "business_card"
                              ? _("Business Card")
                              : staticCard.cardType == "company_profile"
                              ? _("Profile")
                              : staticCard.cardType == "product_card"
                              ? "Card"
                              : "")
                          }
                          separator=":: "
                        >
                          <div className="flex">
                            <LinkedinIcon size={32} round />
                          </div>
                        </LinkedinShareButton>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <TwitterShareButton
                          url={
                            typeof window != "undefined"
                              ? window.location.href
                              : ""
                          }
                          title={
                            "much... | " +
                            staticCard.name +
                            " " +
                            (staticCard.cardType == "business_card"
                              ? _("Business Card")
                              : staticCard.cardType == "company_profile"
                              ? _("Profile")
                              : staticCard.cardType == "product_card"
                              ? "Card"
                              : "")
                          }
                        >
                          <div className="flex">
                            <TwitterIcon size={32} round />
                          </div>
                        </TwitterShareButton>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <FacebookShareButton
                          url={
                            typeof window != "undefined"
                              ? window.location.href
                              : ""
                          }
                          quote={
                            "much... | " +
                            staticCard.name +
                            " " +
                            (staticCard.cardType == "business_card"
                              ? _("Business Card")
                              : staticCard.cardType == "company_profile"
                              ? _("Profile")
                              : staticCard.cardType == "product_card"
                              ? "Card"
                              : "")
                          }
                          hashtag={"#much..."}
                        >
                          <div className="flex">
                            <FacebookIcon size={32} round />
                          </div>
                        </FacebookShareButton>
                      </Dropdown.Item>
                      <Dropdown.Item>
                        <EmailShareButton
                          url={
                            typeof window != "undefined"
                              ? window.location.href
                              : ""
                          }
                          subject={
                            "much... | " +
                            staticCard.name +
                            " " +
                            (staticCard.cardType == "business_card"
                              ? _("Business Card")
                              : staticCard.cardType == "company_profile"
                              ? _("Profile")
                              : staticCard.cardType == "product_card"
                              ? "Card"
                              : "")
                          }
                        >
                          <div className="flex">
                            <EmailIcon size={32} round />
                          </div>
                        </EmailShareButton>
                      </Dropdown.Item>
                    </Dropdown>
                  </div>

                  {staticCard.pic && (
                    <div className="flex flex-wrap justify-center md:justify-start md:w-fit md:ltr:float-left md:ltr:m-10 ltr:md:-mr-10  ">
                      <div className="w-full md:w-3/12 px-4 md:order-2 flex justify-center pb-10">
                        <div className="relative">
                          <div className="avatar w-28 overflow-hidden rounded-full h-auto align-middle    ltr:-ml-16    rtl:md:mt-4  md:-mt-8 max-w-150-px">
                            <img
                              src={staticCard.pic}
                              className="h-full w-full  shadow-xl "
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="name-card md:text-start  mt-10 ltr:md:-ml-32  ltr:md:mt-4 rtl:md:-mt-20 mb-10 rtl:md:mr-28">
                    {staticCard.pic && (
                      <h1 className="text-4xl font-semibold leading-normal mb-1 text-blueGray-700 ">
                        {staticCard.name}
                      </h1>
                    )}
                    {!staticCard.pic && (
                      <h3 className="text-4xl font-semibold leading-normal mb-1 ltr:md:ml-32 rtl:md:-mr-28 rtl:md:mt-24 text-blueGray-700 ">
                        {staticCard.name}
                      </h3>
                    )}
                    {staticCard.jobTitle && (
                      <h4
                        className={
                          staticCard.pic
                            ? "text-xl font-semibold mb-1 md:inline-block md:ltr:mr-5 md:text-md md:text-start"
                            : "text-xl font-semibold mb-1 md:inline-block md:ltr:mr-5 md:text-md md:text-start ltr:md:ml-32 rtl:md:-mr-28 rtl:md:mt-24"
                        }
                      >
                        {staticCard.jobTitle.label}
                      </h4>
                    )}
                    {staticCard.department && (
                      <h4 className="text-lg leading-normal mt-0 mb-2 text-blueGray-600  md:inline-block">
                        {staticCard.department.label}
                      </h4>
                    )}
                    {staticCard.company && (
                      <h4 className="mb-2 text-lg text-blueGray-600 mt-2 md:inline-block">
                        <Link href={"/" + staticCard.company.value + "/"}>
                          
                            <h6>
                              {!staticCard.company.pic && (
                                <i className="fas fa-building  text-lg text-blueGray-400 px-2"></i>
                              )}
                              {staticCard.company.pic && (
                                <img
                                  src={staticCard.company.pic}
                                  style={{
                                    height: "32px",
                                    width: "32px",
                                    borderRadius: "100px",
                                    display: "inline",
                                    margin: "0 4px",
                                  }}
                                />
                              )}
                              {staticCard.company.label}
                            </h6>
                          
                        </Link>
                      </h4>
                    )}
                  </div>
                  <div className="mt-2 py-5 border-t border-blueGray-200 text-center">
                    <div>
                      {staticCard.description && (
                        <p className="whitespace-pre-line mb-6">
                          {staticCard.description}
                        </p>
                      )}
                    </div>
                    {!card && typeof staticCard.fields != "undefined" && (
                      <div className="ltr:text-left rtl:text-right justify-center flex flex-wrap">
                        {staticCard.fields.map((item, k) => {
                          return (
                            <div className="w-full" key={k}>
                              {item.field.fieldType == "phone" &&
                                item.published && <PhoneOutput field={item} />}
                              {item.field.fieldType == "email" &&
                                item.published && <EmailOutput field={item} />}
                              {item.field.fieldType == "url" &&
                                item.published && <UrlOutput field={item} />}
                              {item.field.fieldType == "text" &&
                                item.published && <TextOutput field={item} />}
                            </div>
                          );
                        })}
                      </div>
                    )}
                    {card && (
                      <div className="ltr:text-left rtl:text-right justify-center flex flex-wrap">
                        {card.fields.map((item, k) => {
                          return (
                            <div className="w-full" key={k}>
                              {item.field.fieldType == "phone" &&
                                item.published && (
                                  <PhoneOutput
                                    handler={() => {
                                      if (item.field.name == "Whatsapp") {
                                        newClick(
                                          staticCard._id,
                                          card.user_id,
                                          "https://wa.me/" +
                                            item.prefix.value +
                                            "" +
                                            item.phone
                                        ).then(() => {
                                          window.location.href =
                                            "https://wa.me/" +
                                            item.prefix.value +
                                            "" +
                                            item.phone;
                                        });
                                      } else {
                                        newClick(
                                          staticCard._id,
                                          card.user_id,
                                          "tel:+" +
                                            item.prefix.value +
                                            "" +
                                            item.phone
                                        ).then(() => {
                                          window.location.href =
                                            "tel:+" +
                                            item.prefix.value +
                                            "" +
                                            item.phone;
                                        });
                                      }
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "email" &&
                                item.published && (
                                  <EmailOutput
                                    handler={() => {
                                      newClick(
                                        staticCard._id,
                                        card.user_id,
                                        "mailto:" + item.email
                                      ).then(() => {
                                        window.location.href =
                                          "mailto:" + item.email;
                                      });
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "url" &&
                                item.published && (
                                  <UrlOutput
                                    handler={() => {
                                      newClick(
                                        staticCard._id,
                                        card.user_id,
                                        item.url
                                      ).then(() => {
                                        window.location.href = item.url;
                                      });
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "address" &&
                                item.published && (
                                  <AddressOutput
                                    handler={() => {
                                      newClick(
                                        staticCard._id,
                                        card.user_id,
                                        "Address"
                                      );
                                      window.location.href =
                                        "https://www.google.com/maps/dir/?api=1&destination=" +
                                        item.address?.formatted_address +
                                        "&destination_place_id=" +
                                        item.address.place_id;
                                    }}
                                    field={item}
                                  />
                                )}
                              {item.field.fieldType == "text" &&
                                item.published && <TextOutput field={item} />}
                            </div>
                          );
                        })}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>
          <footer className="text-center">
            <div className="opacity-50 hover:opacity-100 focus:opacity-100 duration-500 md:fixed sticky w-full -bottom-5 xs:text-center pb-28 pt-10 border-t-8 rounded-3xl bg-gray-100  border-emerald-300">
              {staticCard.user_id != session?.user._id && (
                <a
                  href="/create-your-card/"
                  className="p-2 border-b-2 border-emerald-300 font-bold rounded-xl text-xs text-gray-800 hover:text-gray-800 hover:no-underline visited:no-underline visited:text-gray-600"
                  target="_blank"
                  rel="noopener"
                >
                  {_("Create Your Card")}
                </a>
              )}
              {staticCard.user_id == session?.user._id && (
                <a
                  href={"/dashboard/cards/create/" + staticCard._id}
                  className="p-2 border-b-2 border-emerald-300 font-bold rounded-xl text-xs text-gray-800 hover:text-gray-800 hover:no-underline visited:no-underline visited:text-gray-600"
                >
                  {_("Edit Your Card")}
                </a>
              )}

              <p className="py-8">
                {_("Powered by")}{" "}
                <a href="https://much.sa" target="_blank" rel="noreferrer">
                  <img src="/images/logo.svg" className="w-12 inline-block" />
                </a>
              </p>
            </div>
          </footer>
        </main>
        <div className="fixed bottom-0 left-0 justify-center text-center z-50  w-screen shadow">
          <Dropdown
            open={saveMenuOpen}
            noCaret
            title={_("Save")}
            placement="topStart"
            size="lg"
            className="w-full md:w-96 font-bold save_menu"
            onClick={() => setSaveMenuOpen(!saveMenuOpen)}
          >
            <Dropdown.Item
              key={1}
              className={!isSaved ? "uppercase text-center font-bold" : "h-0"}
              style={{ display: isSaved ? "none" : "" }}
            >
              {!isSaved && !isLoading && session && (
                <a
                  href="#"
                  className="text-gray-800 text-center hover:no-underline font-bold"
                  onClick={() => {
                    setSaveMenuOpen(false);
                    setTimeout(() => {
                      saveToWallet(staticCard._id);
                    }, 200);
                  }}
                >
                  <i className="fa fa-wallet"> </i> {_("Save To Cardholder")}
                </a>
              )}
            </Dropdown.Item>

            {!isSaved && !isLoading && !session && (
              <Dropdown.Item
                key={1}
                className="uppercase text-center font-bold"
              >
                <a
                  className="text-gray-800 text-center hover:no-underline font-bold"
                  href={
                    "/signin/?callback=" +
                    (typeof window != "undefined"
                      ? window.location.href + "?action=save"
                      : null)
                  }
                >
                  <i className="fa fa-wallet"> </i> {_("Save To Cardholder")}
                </a>
              </Dropdown.Item>
            )}

            <Dropdown.Item className="uppercase text-center font-bold" key={2}>
              <a
                href={"/api/vcard/?_id=" + staticCard._id}
                className="text-gray-800 text-center hover:no-underline font-bold"
              >
                <i className="fa fa-id-card"></i> {_("Save To Contacts")}{" "}
              </a>
            </Dropdown.Item>
          </Dropdown>
        </div>
        {/* {!isSaved && !isLoading && session && (
          <div className="fixed bottom-2 left-0 h-12 w-screen shadow bg-white">
            <button
              className="p-2 uppercase w-full"
              onClick={() => {
                saveToWallet(staticCard._id);
              }}
            >
              {_("Save To Wallet")}
            </button>
          </div>
        )}
        {!isSaved && !isLoading && !session && (
          <div className="fixed bottom-2 left-0 h-12 w-screen shadow bg-white">
            <Link
              href={
                "/signin/?callback=" +
                (typeof window != "undefined"
                  ? window.location.href + "?action=save"
                  : null)
              }
            >
              <button className="p-2 uppercase w-full">
                {_("Save To Wallet")}
              </button>
            </Link>
          </div>
        )} */}
      </>
    );
}
export const getStaticPaths = async () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking", //indicates the type of fallback
  };
};
export async function getStaticProps(context) {
  let { slug } = context.params;

  if (slug.length > 1) {
    const res = await fetch("https://much.sa/api/cards/" + slug[1] + "/", {
      method: "POST",
    });
    const staticCard = await res.json();
    if (res.status == 200 && staticCard) {
      const src = slug[0];
      //slug=slug[1];
      return {
        redirect: {
          destination: "/" + staticCard._id + "#" + slug[0],
        },
      };
    } else {
      return {
        NotFound: true,
      };
    }

    if (res.status == 200 && staticCard) {
      const src = slug[0];
      slug = slug[1];
      return {
        props: { slug, staticCard, src },
        revalidate: 60,
      };
    } else {
      return {
        NotFound: true,
      };
    }
  } else {
    const res = await fetch("https://much.sa/api/cards/" + slug + "/", {
      method: "POST",
    });
    const staticCard = await res.json();

    if (res.status == 200 && staticCard) {
      const src = null;
      return {
        props: { slug, staticCard, src },
        revalidate: 60,
      };
    } else {
      return {
        NotFound: true,
      };
    }
  }
}
