import Address from "../webComponents/Address";
import Contact from "../webComponents/Contact";
import SEO from "../webComponents/Seo";
import { useRouter } from "next/router";
import Header from "../webComponents/new/storeHeader";
import { translation } from "../lib/websiteTranslations";
import Footer from "../webComponents/new/footer";

const items = [
  {
    id: 1,
    title: "Privacy Policy",
    sub: "",
    body: "",
    imageUrl: "/images/new/Much-Slider-1.jpg",
  },
];

export default function PrivacyPolicy() {
  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  return (
    <>
      <SEO pageTitle={_("much | Privacy Policy")} />
      <Header items={items} />
      <div className="bg-blue-900 -mt-14">
        <section className="section-6 w-full h-auto pt-10">
          <div
            active
            className="contact react-tabs__tab-panel--selected container"
          >
            <div className="title-section text-start text-sm-center">
              <h2 className="text-center text-3xl font-medium p-5 md:text-4xl md:font-bold w-full text-white">
                {_("Privacy ")}{" "}
                <span className="text-green">{_("Policy")}</span>
              </h2>
            </div>
            <div className="container">
              <div className=" text-white">
                {_(
                  "We care about data privacy and security. Please review our Privacy Policy: https://much.sa/privacy-policy."
                )}
              </div>
              <div className=" text-white mt-4">
                {_(
                  "By using the Website or purchasing Products, you consent to the practices disclosed in our Privacy Policy, which is incorporated into these Terms of Use."
                )}
              </div>
              <div className=" text-white my-4">
                {_(
                  "Please be advised the Website is hosted in the Kingdom of Saudi Arabia. If you access the Website or purchase Products from the European Union, Asia, or any other region of the world with laws or other requirements governing personal data collection, use, or disclosure that differ from applicable laws in the Kingdom of Saudi Arabia, then through your continued use of the Website, you are transferring your data to the Kingdom of Saudi Arabia, and you expressly consent to have your data transferred to and processed in the Kingdom of Saudi Arabia. Further, we do not knowingly accept, request, or solicit information from children or knowingly market to children. Therefore, in accordance with the U.S. Children’s Online Privacy Protection Act, if we receive actual knowledge that anyone under the age of 13 has provided personal information to us without the request and verifiable parental consent, we will delete that information from the Website as quickly as is reasonably practical.."
                )}
              </div>
              <div className=" text-green text-4xl mt-4 font-bold">
                {_("INTELLECTUAL PROPERTY INFRINGEMENTS")}
              </div>
              <div className=" text-white my-4">
                {_(
                  "We respect the intellectual property rights of others. If you believe that any material available on or through the Website infringes upon any copyright, trademark, or other proprietary right you own or control, please immediately notify us using the contact information provided below (a “Notification”). A copy of your Notification will be sent to the person who posted or stored the material addressed in the Notification. Please be advised that pursuant to federal law you may be held liable for damages if you make material misrepresentations in a Notification. Thus, if you are not sure that material located on or linked to by the Website infringes your copyright, you should consider first contacting an attorney."
                )}
              </div>
              <div className=" text-white my-4">
                {_(
                  "much... Card will process notices of alleged infringement which it receives and will take appropriate action as required by the Digital Millennium Copyright Act (“DMCA”) and other applicable intellectual property laws."
                )}
              </div>
              <div className=" text-white my-4">
                {_(
                  "To be effective, the Notification must be in writing and contain the following information:"
                )}
              </div>
              <ul className="text-white mt-3 ">
                <li className=" my-2">
                  <span className=" text-green text-xl m-1">1.</span>
                  {_(
                    "Physical or electronic signature of a person authorized to act on behalf of the owners of an exclusive right that is allegedly infringed."
                  )}
                </li>
                <li className=" my-2">
                  <span className=" text-green text-xl m-1">2.</span>
                  {_(
                    "Identification of the copyrighted work or other proprietary material claimed to have been infringed, or, if multiple works or materials are alleged to have been infringed, a representative list of such works."
                  )}
                </li>
                <li className=" my-2">
                  <span className=" text-green text-xl m-1">3.</span>
                  {_(
                    "Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which to be disabled, and information reasonably sufficient to permit us to locate the material."
                  )}
                </li>
                <li className=" my-2">
                  <span className=" text-green text-xl m-1">4.</span>
                  {_(
                    "Information reasonably sufficient to permit us to contact the complaining party, such as an address, telephone number, and if available, an electronic mail address at which the complaining party may be contacted;"
                  )}
                </li>
                <li className=" my-2">
                  <span className=" text-green text-xl m-1">5.</span>
                  {_(
                    "A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the owner of such material, or its agent or the law; A statement, made under penalty of perjury, that the information in the notification is accurate and that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed."
                  )}
                </li>
              </ul>
            </div>
            {/* End .container */}
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
