import Address from "../webComponents/Address";
import Contact from "../webComponents/Contact";
import SEO from "../webComponents/Seo";
import { useRouter } from "next/router";
import Header from "../webComponents/new/storeHeader";
import { translation } from "../lib/websiteTranslations";
import Footer from "../webComponents/new/footer";

const items = [
  {
    id: 1,
    title: "Terms & Conditions",
    sub: "",
    body: "",
    imageUrl: "/images/new/Much-Slider-1.jpg",
  },
];

export default function TermsAndConditions() {
  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  return (
    <>
      <SEO pageTitle={_("much | Terms & Conditions")} />
      <Header items={items} />
      <div className="bg-blue-900 -mt-14">
        <section className="section-6 w-full h-auto pt-10">
          <div
            active
            className="contact react-tabs__tab-panel--selected container"
          >
            <div className="title-section text-start text-sm-center">
              <h2 className="text-center text-3xl font-medium p-5 md:text-4xl md:font-bold w-full text-white">
                {_("Terms ")} <span className="text-green">{_("&")}</span>
                {_(" Conditions")}
              </h2>
            </div>
            <div className="container">
              <div className=" text-white before:content-['Much'] before:text-green before:text-xl">
                {_(
                  " assists not only private individuals in buying or selling a luxury time share in the private resale market but also worldwide travel. These Terms are important together with your booking confirmation set out the legal terms on which services are made available to you. They also cover any interactions or communications you have with us. Your use of our Service is conditioned upon your acceptance of these Terms. To book our services, you must also accept these Terms. We may change these Terms at any time and your future use of our Service following changes to these Terms is subject to you accepting those updated Terms."
                )}
              </div>
              <div className=" text-green font-bold text-xl mt-8">
                {_(
                  "Please read this agreement carefully before using the Web Site:"
                )}
              </div>
              <ol className="list-decimal text-white mt-3">
                <li>
                  {_(
                    "The access to this Web Site is subject to the terms and conditions set forth below."
                  )}
                </li>
                <li>
                  {_(
                    "Your use of the Web Site signifies your acceptance of this agreement. If you do not agree to comply with this agreement, please do not use the Web Site."
                  )}
                </li>
                <li>
                  {_(
                    "Much reserves the right to make changes in the programs, policies, products and services described in this Web Site at any time without notice."
                  )}
                </li>
                <li>
                  {_(
                    "A user who uses or makes decisions based on information contained in this Web Site does so at the user’ s own risk."
                  )}
                </li>
              </ol>
              <div className=" text-white my-3 mt-5">
                {_(
                  "By accessing this Web Site, the user agrees to hold Much and its affiliates harmless against any claims for damages arising from any decisions that the user makes based on such information."
                )}
              </div>
              <div className=" text-white my-4">
                {_(
                  "Much and its affiliated websites involves pooling advertising resources with those of other sellers / buyers to maximize exposure to potential buyers / sellers of timeshare properties. Although your timeshares will be exposed to millions of potential buyers because of our advertising program."
                )}
              </div>
              <div className=" text-white my-4">
                {_(
                  "Much is not affiliated with any resort, or any third-party organization. It does extensive advertising in print, the radio, search engines, billboards and other advertising mediums, to drive buyers and sellers to our web site or to call us by phone for information about our advertiser’s timeshare properties."
                )}
              </div>
              <div className=" text-white my-4">
                {_(
                  "Much has made no representations as to the period that it will take to sell your timeshare, the success rate of our advertising program or the sales price that you may receive. Market condition and the size and location determines the marketing period and sales price of the week that you desire to sell."
                )}
              </div>
              <div className=" text-green font-bold text-4xl mt-8">
                {_("Limitation of Liability")}
              </div>
              <div className=" text-white my-4">
                {_(
                  "Under no shall Much be liable for any damages, direct, special, incidental, consequential, punitive, or exemplary or otherwise, that result from the use of, or the inability to use, the materials in this Web Site, even if Much or an authorized representative has been advised of the possibility of such damages."
                )}
              </div>
              <div className=" text-white my-4">
                {_(
                  "As the Web Site is operated within UAE, this agreement shall be governed by and enforced in accordance with the UAE laws. If a court of competent jurisdiction finds any part of this agreement void or unenforceable, the remainder should be enforced, and the court should use its authority to amend it to fulfill the stated purposes of this agreement to the fullest extent permitted by law."
                )}
              </div>
            </div>
            {/* End .container */}
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
