import Wrapper from "../../layout/wrapper";
import SEO from "../../webComponents/Seo";
import { useRive, Layout } from "@rive-app/react-canvas";
import SwitchDark from "../../webComponents/switch/SwitchDark";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import QRCode from "easyqrcodejs";
import { useRouter } from "next/router";
import { translation } from "../../lib/translations";
import { useEffect, useRef, useState } from "react";
import { Carousel, Slider, Toggle } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import Script from "next/script";
import { signIn, useSession } from "next-auth/react";
import NewCard from "../../components/newCard/NewCard";
const Product = ({ params }) => {
  const { id } = params;
  const { query } = useRouter();
  const router = useRouter();
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  // const { rive, RiveComponent } = useRive({
  //   src: "/rive/logo.riv",
  //   autoplay: true,
  // });

  const menuItem = [
    { icon: "fa-home", menuName: _("Home"), name: "/" },
    { icon: "fa-star", menuName: _("Features"), name: "/features" },
    { icon: "fab fa-product-hunt", menuName: _("Products"), name: "/products" },
    { icon: "fas fa-question-circle", menuName: "FAQ", name: "/faq" },
    { icon: "fa-envelope", menuName: _("Contact"), name: "/contact" },
  ];
  let dm = 2;
  const { data: session, status } = useSession();
  const qrCodeRef = useRef(null);
  const [product, setProduct] = useState(null);
  const [cards, setCards] = useState([]);
  const [selectedCard, setSelectedCard] = useState({});
  const [name, setName] = useState();
  const [jobTitle, setJobTitle] = useState();
  const [showName, setShowName] = useState(true);
  const [showQr, setShowQr] = useState(false);
  const [qrWidth, setQrWith] = useState(0);
  const [qrColor, setQrColor] = useState("#FFFFFF");
  const [qrDarkColor, setQrDarkColor] = useState("#000000");
  const [fontSize, setFontSize] = useState("sm");
  const [textColor, setTextColor] = useState();
  const [textDirection, setTextDirection] = useState("ltr");
  const [qrDirection, setQrDirection] = useState("left");
  const [logo, setLogo] = useState();
  const [logoWidth, setLogoWidth] = useState(50);
  const [logoHeight, setLogoHeight] = useState(50);
  const [removeLogo, setRemoveLogo] = useState(false);
  const [totalPrice, setTotalPrice] = useState();
  const [disableLogoButton, setDisableLogoButton] = useState(false);
  const [isPlacingOrder, setIsPlacingOrder] = useState(false);
  const [credentials, setCredentials] = useState();
  const showQrFn = async (card) => {
    let opt = {
      text: "https://" + window.location.host + "/qr/" + card.slug,
      width: 160,
      height: 160,
      logoMaxWidth: 40,
      logoMaxHeight: 40,
      drawer: "canvas",
      autoColor: true,
      colorDark: qrDarkColor,
      colorLight: qrColor,
      correctLevel: QRCode.CorrectLevel.H,
      dotScaleTiming: 1,
      dotScale: 1,
      dotScaleTiming_V: 1,
      dotScaleTiming_h: 0.9,
      dotScaleA: 1,
      dotScaleAO: 1,
      dotScaleAI: 1,
      quietZone: 10,
      logo: card.qrLogo,
      logoWidth: card.qrlogoWidth,
      logoHeight: card.qrlogoWidth,
      logoBackgroundTransparent: card.qrlogoBackgroundTransparent,
      backgroundImage: card.qrbackgroundImage,
      backgroundImageAlpha: card.qrbackgroundImageAlpha,
    };
    
    
      
      if(qrCodeRef && qrCodeRef.current) 
      {
        qrCodeRef.current.innerHTML = null;
        new QRCode(qrCodeRef.current, opt);
      }
      
    
    
    
    
    let rectArr = [];
    let rects = document.getElementsByTagName("rect");
    setTimeout(() => {
      rectArr = [...rects];
      rectArr.map((e) => {
        e.setAttribute("rx", "10");
        e.setAttribute("ry", "10");
      });
    }, 100);
  };

  const openLogoWidget = () => {
    // create the widget

    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 5,
        multiple: true,
        cropping: true,

        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 940,
        maxImageHeight: 520,
        theme: "white",
        form: "coverPic",
        showPoweredBy: false,
        ocr: "adv_ocr",
      },

      (error, result) => {
        setDisableLogoButton(false);

        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let bg = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            bg +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          bg += "/" + result.info.public_id;
          let theImage = { ...result.info };
          theImage.imgUrl = bg;
          setLogo(theImage);
          setDisableLogoButton(false);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };

  useEffect(() => {
    fetch("/api/much-products/" + id + "/", {
      method: "post",
      body: JSON.stringify({ id: id }),
    })
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
        setTotalPrice(data.price);
        setTextColor(data.defaultTextColor);
        setQrColor(data.defaultQrColor);
        setQrDarkColor(data.defaultQrDarkColor);
      });
    fetch("/api/cards/", { method: "post" })
      .then((res) => res.json())
      .then((data) => {
        setCards(data);
        if (data.length) {
          setSelectedCard(data[0]);
          setName(data[0].name);
          if (data[0].jobTitle) {
            let jobTitle = data[0].jobTitle.value;
            if (data[0].company) {
              jobTitle += " - " + data[0].company.label;
            }
            setJobTitle(jobTitle);
          }
          showQrFn(data[0]);
        }
      });
  }, [id]);

  //========== Place Order ============
  const placeOrder = async () => {
    let order = {
      user_id: selectedCard.user_id,
      product_id: product._id,
      card:selectedCard,
      product:product,
      name: name,
      jobTitle: jobTitle,
      showName: showName,
      showQr: showQr,
      qrColor: qrColor,
      qrDarkColor: qrDarkColor,
      fontSize: fontSize,
      textColor: textColor,
      textDirection: textDirection,
      qrDirection: qrDirection,
      qrWidth:qrWidth,
      logo: logo,
      logoWidth: logoWidth,
      removeLogo: removeLogo,
      totalPrice: totalPrice,
      currency:product.currency,
      payment_status:'unpaid',
      status:'new'
    };
    setIsPlacingOrder(true);
    const orderRes = await fetch("/api/orders/place-order/", {
      body: JSON.stringify(order),
      method: "POST",
    });
    const returnOrder = await orderRes.json();
    console.log(returnOrder);
    
    setIsPlacingOrder(false);
    if(orderRes.status=='201')
    {
      
    }
  };
  const checkOut = async()=>{
    if(!session && credentials)
      {
        signIn("credentials",credentials).then(()=>{
          window.location.href = '/orders/';
        })
      }
      else
      {
        window.location.href='/orders/';
      }
  }
  return (
    <>
      <Script
        src="https://widget.Cloudinary.com/v2.0/global/all.js"
        type="text/javascript"
      ></Script>
      <Wrapper>
        <SEO pageTitle={"much... | Digital Cards"} />
        <div className="container hidden lg:block">
          <nav className="fixed">
            <div className="logo w-32 h-20">{/* <RiveComponent /> */}</div>
          </nav>
        </div>
        <div className="blue">
          {/* <SwitchDark /> */}

          {/* End Switcher */}
          <Tabs defaultIndex={dm}>
            <div className="header">
              <TabList className=" icon-menu  revealator-slideup revealator-once revealator-delay1">
                {menuItem.map((item, i) => (
                  <Tab
                    className="icon-box"
                    key={i}
                    onClick={() => {
                      router.push(item.name);
                    }}
                  >
                    <i className={`fa ${item.icon}`}></i>
                    <h2>{item.menuName}</h2>
                  </Tab>
                ))}
              </TabList>
            </div>
            {/* End Menu Content */}

            <div className="tab-panel_list">
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
              {/* Products Content Starts */}
              <TabPanel className="portfolio professional min-h-screen">
                {product && (
                  <div className="container">
                    <div className="flex-row flex-wrap card mt-40 p-4">
                      <div className="w-full lg:w-5/12 lg:border-r border-gray-500">
                        {product.images.length || product.videos.length ? (
                          <Carousel
                            // autoplay
                            autoplayInterval="5000"
                            key={"bars-left"}
                            placement="left"
                            shape="bar"
                            className="custom-slider h-36"
                          >
                            {product.images.map((image, index) => {
                              console.log(image);
                              return (
                                <div
                                  key={"i" + index}
                                  style={{ height: "400px", width: "100%" }}
                                >
                                  <img
                                    className="rounded-xl"
                                    src={image.imgUrl}
                                    style={{ width: "100%" }}
                                  />
                                </div>
                              );
                            })}
                            {product.videos.map((video, index) => {
                              return (
                                <video
                                  key={"v" + index}
                                  loop
                                  autoPlay
                                  muted
                                  preload="auto"
                                  className="rounded-xl"
                                  style={{
                                    height: "auto !important",
                                    width: "100%",
                                  }}
                                >
                                  <source src={video.url}></source>
                                </video>
                              );
                            })}
                          </Carousel>
                        ) : (
                          ""
                        )}
                        {product.type == "Card" && product.cardFront ? (
                          <div
                            className="lg:py-10 py-6 lg:fixed lg:mx-10 xl:mx-20"
                            style={{
                              zoom: "1.25",
                            }}
                          >
                            <div className="p-0 md:p-16 ">
                              <div
                                className="mx-auto drop-shadow-xl rounded-lg overflow-hidden"
                                style={{
                                  backgroundImage:
                                    "url(" + product.cardBack.imgUrl + ")",
                                  width: "220px",
                                  height: "139px",
                                }}
                              >
                                {showName && (
                                  <div
                                    style={{
                                      zIndex:10,
                                      position: "relative",
                                      top: "10px",
                                      padding: "0 10px",
                                      width: "100%",
                                      
                                      direction: textDirection,
                                      color: textColor,
                                    }}
                                    className={"NameContaine  text-" + fontSize}
                                  >
                                    {" "}
                                    <span className="font-bold">{name}</span>
                                    <br />
                                    {jobTitle && (
                                      <small
                                        className="whitespace-pre"
                                        style={{ zoom: "0.85" }}
                                      >
                                        {jobTitle}
                                      </small>
                                    )}
                                  </div>
                                )}
                                {showQr && (
                                  <div
                                    className="qrCodeContainer rounded-lg p-2"
                                    style={{
                                      position: "relative",
                                      zIndex:10,
                                      top: "20px",
                                      left:
                                        qrDirection == "left" ? "0px" : "140px",
                                      width: 80 + qrWidth + "px",
                                      height: 80 + qrWidth + "px",
                                    }}
                                    ref={qrCodeRef}
                                  ></div>
                                )}
                              </div>
                            </div>
                            <div className="p-0 md:p-16 my-12">
                              <div
                                className="mx-auto drop-shadow-xl rounded-lg p-2 overflow-hidden"
                                style={{
                                  backgroundImage:
                                    "url(" + product.cardFront.imgUrl + ")",
                                  width: "220px",
                                  height: "139px",
                                }}
                              >
                                {logo && (
                                  <div
                                    style={{
                                      maxWidth: "200px",
                                      maxHeight: "100px",
                                      width: logoWidth,
                                      height: logoHeight,
                                    }}
                                  >
                                    <img src={logo.imgUrl} />
                                  </div>
                                )}
                                {!removeLogo && (
                                  <img
                                    style={{
                                      position: "relative",
                                      width: "30px",
                                      top: "95px",
                                      left: "177px",
                                    }}
                                    src={product.muchIcon}
                                  />
                                )}
                              </div>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                      <div className="w-full lg:w-7/12 ">
                        <div className="p-4">
                          <h3 className="pb-3">
                            <span>{_(product.name[locale])}</span>
                          </h3>
                          <p className="text-muted pb-3">
                            {product.shortDescription[locale]}
                          </p>
                          <h4>
                            {totalPrice} {_(product.currency)}
                          </h4>
                          <small>{"Price includes VAT"}</small>
                          <p className="pt-3 whitespace-pre-wrap">
                            {product.description[locale]}
                          </p>
                          {product.type=='Card' && <div className="my-4">
                            <h1 className="text-xl mb-6">
                              {_("Customize Your Card")}
                            </h1>
                            {session && session.user && (
                              <div className="flex flex-wrap mb-4">
                                <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 py-2">
                                  {_("Select Card")}
                                </label>
                                <select
                                  className="form-control w-full md:w-10/12"
                                  onChange={(e) => {
                                    setSelectedCard(
                                      cards.filter(
                                        (c) => c._id == e.target.value
                                      )[0]
                                    );
                                    showQrFn(
                                      cards.filter(
                                        (c) => c._id == e.target.value
                                      )[0]
                                    );
                                    setName(
                                      cards.filter(
                                        (c) => c._id == e.target.value
                                      )[0].name
                                    );
                                    if (
                                      cards.filter(
                                        (c) => c._id == e.target.value
                                      )[0].jobTitle
                                    ) {
                                      let jobTitle = cards.filter(
                                        (c) => c._id == e.target.value
                                      )[0].jobTitle.value;
                                      if (
                                        cards.filter(
                                          (c) => c._id == e.target.value
                                        )[0].company
                                      ) {
                                        jobTitle +=
                                          " - " +
                                          cards.filter(
                                            (c) => c._id == e.target.value
                                          )[0].company.label;
                                      }
                                      setJobTitle(jobTitle);
                                    }
                                  }}
                                >
                                  {cards.map((e) => {
                                    return (
                                      <option key={e._id} value={e._id}>
                                        {e.name} ({_(e.cardType)})
                                      </option>
                                    );
                                  })}
                                </select>
                              </div>
                            )}
                            <div className="flex flex-wrap mb-4">
                              <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                {_("Title")}
                              </label>
                              <div className="w-full md:w-10/12">
                                <input
                                  className="form-control mb-2"
                                  value={name}
                                  onChange={(e) => {
                                    setName(e.target.value);
                                  }}
                                />
                              </div>
                            </div>
                            <div className="flex flex-wrap mb-4">
                              <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                {_("Sub-title")}
                              </label>
                              <div className="w-full md:w-10/12">
                                <textarea
                                  className="form-control mb-2"
                                  value={jobTitle}
                                  onChange={(e) => {
                                    setJobTitle(e.target.value);
                                  }}
                                ></textarea>
                              </div>
                            </div>
                            <div className="flex flex-wrap mb-4">
                              <Toggle
                                className="mx-2"
                                checked={showName}
                                unCheckedChildren="Hide Title"
                                checkedChildren="Show Title"
                                onChange={(e) => {
                                  setShowName(e);
                                }}
                              ></Toggle>
                              <Toggle
                                className="mx-2"
                                checked={showQr}
                                unCheckedChildren="Hide QR Code"
                                checkedChildren="Show QR Code"
                                onChange={(e) => {
                                  setShowQr(e);
                                  e
                                    ? setTimeout(() => {
                                        showQrFn(selectedCard);
                                      }, 200)
                                    : "";
                                }}
                              ></Toggle>
                              <Toggle
                                className="mx-2"
                                checked={removeLogo}
                                unCheckedChildren={_("Keep much logo")}
                                checkedChildren={
                                  _("Remove much logo +") +
                                  product.brandlessPrice +
                                  " " +
                                  product.currency
                                }
                                onChange={(e) => {
                                  setRemoveLogo(e);
                                  e
                                    ? setTotalPrice(
                                        (prev) =>
                                          prev * 1 + product.brandlessPrice * 1
                                      )
                                    : setTotalPrice(
                                        (prev) =>
                                          prev * 1 - product.brandlessPrice * 1
                                      );
                                }}
                              ></Toggle>
                            </div>
                            {showQr && (
                              <div>
                                <div className="flex flex-wrap mb-4">
                                  <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                    {_("QR Code Color")}
                                  </label>
                                  <div className="w-full md:w-10/12 flex flex-wrap">
                                    <div className="w-full md:w-6/12">
                                      <input
                                        className="form-control"
                                        type="color"
                                        value={qrColor}
                                        onChange={(e) => {
                                          setQrColor(e.target.value);
                                          showQrFn(selectedCard);
                                        }}
                                      />
                                    </div>
                                    <div className="w-full md:w-6/12">
                                      <input
                                        className="form-control"
                                        type="color"
                                        value={qrDarkColor}
                                        onChange={(e) => {
                                          setQrDarkColor(e.target.value);
                                          showQrFn(selectedCard);
                                        }}
                                      />
                                    </div>
                                  </div>
                                </div>{" "}
                                <div className="flex flex-wrap mb-4">
                                  <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                    {_("QR Code Direction")}
                                  </label>
                                  <div className="w-full md:w-10/12">
                                    <button
                                      className={
                                        qrDirection == "left"
                                          ? "p-2 border rounded-md  bg-gray-400"
                                          : "p-2 border rounded-md"
                                      }
                                      onClick={() => setQrDirection("left")}
                                    >
                                      <i className="fa fa-long-arrow-left"></i>
                                    </button>
                                    <button
                                      className={
                                        qrDirection == "right"
                                          ? "p-2 border rounded-md  bg-gray-400"
                                          : "p-2 border rounded-md"
                                      }
                                      onClick={() => setQrDirection("right")}
                                    >
                                      <i className="fa fa-long-arrow-right"></i>
                                    </button>
                                  </div>
                                </div>
                              </div>
                            )}
                            {showName && (
                              <div>
                                <div className="flex flex-wrap mb-4">
                                  <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                    {_("Text Color")}
                                  </label>
                                  <div className="w-full md:w-10/12">
                                    <input
                                      className="form-control"
                                      type="color"
                                      value={textColor}
                                      onChange={(e) =>
                                        setTextColor(e.target.value)
                                      }
                                    />
                                  </div>
                                </div>
                                <div className="flex flex-wrap mb-4">
                                  <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                    {_("Text Direction")}
                                  </label>
                                  <div className="w-full md:w-10/12">
                                    <button
                                      className={
                                        textDirection == "ltr"
                                          ? "p-2 border rounded-md  bg-gray-400"
                                          : "p-2 border rounded-md"
                                      }
                                      onClick={() => setTextDirection("ltr")}
                                    >
                                      <i className="fa fa-long-arrow-left"></i>
                                    </button>
                                    <button
                                      className={
                                        textDirection == "rtl"
                                          ? "p-2 border rounded-md  bg-gray-400"
                                          : "p-2 border rounded-md"
                                      }
                                      onClick={() => setTextDirection("rtl")}
                                    >
                                      <i className="fa fa-long-arrow-right"></i>
                                    </button>
                                  </div>
                                </div>
                                <div className="flex flex-wrap mb-4">
                                  <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                    {_("Font Size")}
                                  </label>
                                  <div className="w-full md:w-10/12">
                                    <label className="checkbox">
                                      <input
                                        className="form-control mx-2"
                                        type="radio"
                                        checked={fontSize == "xs"}
                                        name="font-size"
                                        value="xs"
                                        onChange={(e) => {
                                          setFontSize(e.target.value);
                                        }}
                                      />{" "}
                                      {_("Small")}
                                    </label>
                                    <label className="checkbox mx-2">
                                      <input
                                        className="form-control"
                                        type="radio"
                                        checked={fontSize == "sm"}
                                        name="font-size"
                                        value="sm"
                                        onChange={(e) => {
                                          setFontSize(e.target.value);
                                        }}
                                      />{" "}
                                      {_("Medium")}
                                    </label>
                                    <label className="checkbox mx-2">
                                      <input
                                        className="form-control"
                                        type="radio"
                                        checked={fontSize == "lg"}
                                        name="font-size"
                                        value="lg"
                                        onChange={(e) => {
                                          setFontSize(e.target.value);
                                        }}
                                      />{" "}
                                      {_("Large")}
                                    </label>
                                    <label className="checkbox mx-2">
                                      <input
                                        className="form-control"
                                        type="radio"
                                        name="font-size"
                                        checked={fontSize == "xl"}
                                        value="xl"
                                        onChange={(e) => {
                                          setFontSize(e.target.value);
                                        }}
                                      />{" "}
                                      {_("X Large")}
                                    </label>
                                  </div>
                                </div>
                                <div className="flex flex-wrap mb-4">
                                  <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                    {_("Logo")}
                                  </label>
                                  <div className="w-full md:w-10/12">
                                    <button
                                      type="button"
                                      disabled={disableLogoButton}
                                      onClick={() => {
                                        setDisableLogoButton(true);
                                        openLogoWidget();
                                      }}
                                      className="border rounded-sm p-2 "
                                    >
                                      {_("Upload Logo")}{" "}
                                      {disableLogoButton && (
                                        <i className="fa fa-spinner"></i>
                                      )}
                                    </button>
                                    {logo && (
                                      <button
                                        type="button"
                                        className="border rounded-sm p-2 mx-2"
                                        onClick={() => {
                                          setLogo();
                                        }}
                                      >
                                        {_("Clear Logo")}{" "}
                                        <i className="text-red-500 fa fa-times"></i>
                                      </button>
                                    )}
                                  </div>
                                </div>
                                {logo && (
                                  <div className="flex flex-wrap mb-4">
                                    <label className="uppercase font-bold text-sm text-gray-600 w-full md:w-2/12">
                                      {_("Logo Width")}
                                    </label>
                                    <div className="w-full md:w-10/12 py-2">
                                      <Slider
                                        value={logoWidth}
                                        step={10}
                                        max={200}
                                        min={50}
                                        onChange={(e) => setLogoWidth(e)}
                                        graduated
                                        progress
                                      />
                                    </div>
                                  </div>
                                )}
                              </div>
                            )}
                            {(!session || (session && !session.user)) &&
                              !selectedCard._id && (
                                <NewCard
                                  name={name}
                                  setCard={setSelectedCard}
                                  setCredentials={setCredentials}
                                />
                              )}
                            
                          </div>}
                          <div className="p-4">
                          {selectedCard._id && (
                              <div className="flex flex-wrap mb-4">
                                <button
                                  onClick={() => placeOrder()}
                                  disabled={isPlacingOrder}
                                  className="bg-emerald-300 text-gray-50 p-2 rounded-xl font-bold hover:bg-emerald-200 hover:text-gray-600 duration-200 float-right"
                                >
                                  {_("Add To Cart")}{" "}
                                  {isPlacingOrder && (
                                    <i className="fa fa-spinner animate-spin"></i>
                                  )}
                                </button>
                                <button
                                  onClick={() => checkOut()}
                                  
                                  className="mx-2 bg-emerald-300 text-gray-50 p-2 rounded-xl font-bold hover:bg-emerald-200 hover:text-gray-600 duration-200 float-right"
                                >
                                  {_("Checkout")}{" "}
                                  
                                </button>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {/* End title */}
              </TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
              {/* Products Content Ends */}
            </div>
          </Tabs>
        </div>
      </Wrapper>
    </>
  );
};
export default Product;

export function getServerSideProps(context) {
  return {
    props: { params: context.params },
  };
}
