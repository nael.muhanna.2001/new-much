import { useRive } from "@rive-app/react-canvas";
import { useRouter } from "next/router";
import Wrapper from "../../layout/wrapper";
import { translation } from "../../lib/translations";
import SEO from "../../webComponents/Seo";
import SwitchDark from "../../webComponents/switch/SwitchDark";
import QRCode from "easyqrcodejs";
import "rsuite/dist/rsuite.min.css";
import { useEffect, useRef, useState } from "react";
import { toPng } from "html-to-image";
import Link from "next/link";
import Autocomplete from "react-google-autocomplete";
import Select from "react-select";
import { countries } from "../../lib/countries";
import { useSession } from "next-auth/react";


import Head from "next/head";
import Script from "next/script";
import CreditCard from "../../components/payment/Card";
const options = [];
countries.map((country) => {
  options.push({
    value: country.dial,
    label: country.name + " (" + country.dial + ")",
  });
});
export default function Orders() {
  const { rive, RiveComponent } = useRive({
    src: "/rive/logo.riv",
    autoplay: true,
  });
  const { locale } = useRouter();
  const router = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const { data: session, status } = useSession();
  const qrCodeRef = useRef(null);
  const [orders, setOrders] = useState([]);
  const [address, setAddress] = useState();
  const [country, setCountry] = useState();
  const [mobilePrefix, setMobilePrefix] = useState();
  const [mobile, setMobile] = useState();
  useEffect(() => {
    fetch("/api/orders/my-orders/")
      .then((res) => res.json())
      .then((data) => {
        setOrders(data);
      });
  }, []);
  const deleteOrder = (order) => {
    fetch("/api/orders/delete/", {
      method: "post",
      body: JSON.stringify({ _id: order._id }),
    }).then((res) => {
      if (res.status == "202") {
        setOrders((prev) => {
          return prev.filter((e) => e._id != order._id);
        });
      }
    });
  };

  //====== Show QR function =====//
  async function showQrFn(item) {
    let opt = {
      text: "https://" + window.location.host + "/" + item.card.slug,
      width: 200,
      height: 200,
      logoMaxWidth: 40,
      logoMaxHeight: 40,
      drawer: "canvas",
      autoColor: true,
      colorDark: item.qrDarkColor ? item.qrDarkColor : "#000000",
      colorLight: item.qrColor ? item.qrColor : "#ffffff",
      correctLevel: QRCode.CorrectLevel.H,
      dotScaleTiming: 0.9,
      dotScale: 1,
      dotScaleTiming_V: 0.9,
      dotScaleTiming_h: 0.9,
      dotScaleA: 0.9,
      dotScaleAO: 0.9,
      dotScaleAI: 0.5,
      quietZone: 3,
      logo: item.card.qrLogo,
      logoWidth: item.card.qrlogoWidth,
      logoHeight: item.card.qrlogoWidth,
      logoBackgroundTransparent: item.card.qrlogoBackgroundTransparent,
      backgroundImage: item.card.qrbackgroundImage,
      backgroundImageAlpha: item.card.qrbackgroundImageAlpha,
    };

    if (document.getElementById(item._id)) {
      document.getElementById(item._id).innerHTML = null;
      new QRCode(document.getElementById(item._id), opt);
    }
  }
  //====== convert html to png ===//
  const convertDivToPng = async (div) => {
    const data = await toPng(div, {
      cacheBust: true,
      canvasWidth: 880,
      canvasHeight: 556,
    });
    return data;
  };

  //====== Download PNG =========//
  const handleDownloadTest = async (e, item) => {
    try {
      const data = await convertDivToPng(
        document.getElementById("f-" + item._id)
      );
      if (data) {
        const link = document.createElement("a");
        link.href = data;
        link.download = item._id + "f.png";
        link.click();
      }
    } catch (e) {
      console.log(e, "ini errornya");
    }
    try {
      const data = await convertDivToPng(
        document.getElementById("b-" + item._id)
      );
      if (data) {
        const link = document.createElement("a");
        link.href = data;
        link.download = item._id + "b.png";
        link.click();
      }
    } catch (e) {
      console.log(e, "ini errornya");
    }
  };

  if (status == "loading") return _("Loading...");
  if (status != "loading" && status == "unauthenticated") {
    router.replace("/products");
  }
  return (
    <>
    
      <Wrapper>
        <SEO pageTitle={"much... | Checkout"} />
        <Head>
        
        </Head>
        <div className="container hidden lg:block">
          <nav className="fixed">
            {/* <div className="logo w-32 h-20 fixed top-0"><RiveComponent /></div> */}
          </nav>
        </div>
        <div className="blue">{/* <SwitchDark /> */}</div>
        <div className="container  mt-28 z-50">
          <div className="portfolio professional min-h-screen card">
            <div className="card-header">
              <div>
                <Link href="/products">
                  <button className="fa fa-arrow-left py-2 text-lg absolute"></button>
                </Link>
              </div>
              <h2 className="text-gray-500 card-title text-xl">
                {_("Order Summary")}
              </h2>
            </div>
            <div className="card-body">
              {orders.map((item, index) => {
                return (
                  <div
                    key={index}
                    className="flex flex-wrap mb-4  border-b-4 p-1 rounded-xl"
                    style={{ zoom: 0.65 }}
                  >
                    <div className="w-1/12 border-t border-l p-4 hidden lg:block">
                      <h1>{index + 1}</h1>
                    </div>
                    {item.product.type=='Product' &&<div className="w-full lg:w-4/12 p-4 border">
                    {item.product.images && item.product.images.length && <div
                        id={"f-" + item._id}
                        className="drop-shadow-xl w-full h-full rounded-lg overflow-hidden"
                        style={{
                          backgroundImage:
                            "url(" + item.product.images[0].imgUrl + ")",
                          backgroundSize:'contain',
                          backgroundRepeat:'no-repeat',
                          width: "440px",
                          height: "280px",
                        }}
                      >
                        </div>}
                      </div>}
                    {item.product.type =='Card' &&<div className="w-full lg:w-2/12 p-4 border ">
                      <div
                        id={"f-" + item._id}
                        className="drop-shadow-xl rounded-lg overflow-hidden"
                        style={{
                          backgroundImage:
                            "url(" + item.product.cardBack.imgUrl + ")",
                          width: "220px",
                          height: "139px",
                        }}
                      >
                        <div style={{ width: "220px", height: "139px" }}>
                          {item.showName && (
                            <div
                              style={{
                                position: "relative",
                                top: "10px",
                                padding: "0 10px",
                                width: "100%",
                                direction: item.textDirection,
                                color: item.textColor,
                              }}
                              className={"NameContaine  text-" + item.fontSize}
                            >
                              {" "}
                              <span className="font-bold">{item.name}</span>
                              <br />
                              {item.jobTitle && (
                                <small
                                  className="whitespace-pre"
                                  style={{ zoom: "0.85" }}
                                >
                                  {item.jobTitle}
                                </small>
                              )}
                            </div>
                          )}
                          {item.showQr && (
                            <div
                              id={item._id}
                              onLoad={setTimeout(() => {
                                showQrFn(item);
                              }, 200)}
                              className="qrCodeContainer rounded-lg p-2"
                              style={{
                                position: "relative",
                                bottom: "10px",
                                left:
                                  item.qrDirection == "left" ? "0px" : "140px",
                                width: 80 + item.qrWidth + "px",
                                height: 80 + item.qrWidth + "px",
                              }}
                              ref={qrCodeRef}
                            ></div>
                          )}
                        </div>
                      </div>
                    </div>}
                    {item.product.type=='Card' &&<div className="w-full lg:w-2/12 p-4 border">
                      <div
                        id={"b-" + item._id}
                        className="drop-shadow-xl rounded-lg p-2 overflow-hidden"
                        style={{
                          backgroundImage:
                            "url(" + item.product.cardFront.imgUrl + ")",
                          width: "220px",
                          height: "139px",
                        }}
                      >
                        {item.logo && (
                          <div
                            style={{
                              maxWidth: "200px",
                              maxHeight: "100px",
                              width: item.logoWidth,
                              height: item.logoHeight,
                            }}
                          >
                            <img src={item.logo.imgUrl} />
                          </div>
                        )}
                        {!item.removeLogo && (
                          <img
                            style={{
                              position: "relative",
                              width: "30px",
                              top: "95px",
                              left: "177px",
                            }}
                            src={item.product.muchIcon}
                          />
                        )}
                      </div>
                    </div>}
                    <div className="w-full lg:w-2/12 p-4 border">
                      <h4>{item.product.name[locale]}</h4>
                      {_("Linked Card")} <br />
                      <a
                        target="_Blank"
                        rel="noreferrer"
                        href={"/" + item.card.slug}
                      >
                        {item.card.name} ({_(item.card.cardType)}){" "}
                        <i className="fa fa-link"></i>
                      </a>
                    </div>
                    <div className="w-full lg:w-2/12 p-4 border">
                      <h4 className="font-bold text-md">
                        {_("Price")} {item.totalPrice} {item.currency}
                      </h4>
                    </div>
                    <div className="w-full lg:w-2/12 p-4 border">
                      <h4>{_("Created at")}</h4>
                      {new Date(item.created_at).toUTCString()}
                    </div>
                    <div className="w-full lg:w-1/12 border">
                      <button
                        className="hidden md:block bg-emerald-300 p-3 w-full text-gray-600 mb-2 rounded"
                        onClick={() => handleDownloadTest(event, item)}
                      >
                        {_("Download")}
                        <br /> <i className="fa fa-download"></i>
                      </button>

                      <button
                        onClick={() => {
                          deleteOrder(item);
                        }}
                        className="bg-red-500 p-3 w-full text-white rounded"
                      >
                        {_("Remove")} <br /> <i className="fa fa-trash"></i>
                      </button>
                    </div>
                  </div>
                );
              })}
              <div>
                <h2 className="text-xl border-b-2 mb-4 ltr:float-left rtl:float-right">
                  {_("Delivery Address")}
                </h2>
                <form>
                  <div className="flex flex-wrap w-full mb-4">
                    <label className="uppercase text-gray-600 text-sm font-bold py-2 w-full lg:w-2/12">
                      {_("Full Address")}
                    </label>
                    <div className="w-full lg:w-10/12">
                      <Autocomplete
                        defaultValue={address?.formatted_address}
                        apiKey={"AIzaSyBeoXqvPbcjNUkp6FOjmYNkqx6oLmYvcYg"}
                        options={{
                          types: ["geocode", "establishment"],
                          componentRestrictions: { country: country?.value },
                        }}
                        onPlaceSelected={(place) => {
                          setAddress(place);
                        }}
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="flex flex-wrap w-full mb-4">
                    <div className="w-full lg:w-6/12 flex flex-wrap">
                      <label className="uppercase text-gray-600 text-sm font-bold py-1 w-full lg:w-2/12">
                        {_("Street")}
                      </label>
                      <div className="w-full lg:w-10/12">
                        <input className="form-control" />
                      </div>
                    </div>
                    <div className="w-full lg:w-6/12 flex flex-wrap p-1">
                      <label className="uppercase text-gray-600 text-sm font-bold py-1 w-full lg:w-2/12">
                        {_("Building")}
                      </label>
                      <div className="w-full lg:w-10/12">
                        <input className="form-control" />
                      </div>
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 text-sm w-full md:w-2/12">
                      {_("Mobile")}
                    </label>
                    <div className="flex w-10/12">
                      <Select
                        value={mobilePrefix}
                        onChange={(e) => {
                          setMobilePrefix(e);
                        }}
                        className="text-xs w-6/12  pt-1 ltr:mr-1 rtl:ml-1  rounded-sm outline-0 focus:outline-0"
                        options={options}
                      ></Select>
                      <input
                        value={mobile}
                        placeholder={_("5XXXXXXXX")}
                        type="number"
                        className="w-full border border-gray-300 h-9.5 mt-1 text-md rounded p-2"
                        onChange={(e) => setMobile(e.target.value)}
                        onBlur={(e) => {
                          setMobile(e.target.value);
                        }}
                        required
                      />
                    </div>
                  </div>
                </form>
                <CreditCard />
              </div>
            </div>
          </div>
        </div>
      </Wrapper>
     

    </>
  );
}
