import Image from "next/image";
import Link from "next/link";
import { translation } from "../lib/translations";
import { useRouter } from "next/router";
import { getCsrfToken } from "next-auth/react";
import { getProviders, signIn, useSession } from "next-auth/react";
import { useRef, useState } from "react";
import Head from "next/head";
import SEO from "../webComponents/Seo";
export default function SigninPage({ csrfToken, providers, query }) {
  const { locale, locales, defaultLocale, asPath } = useRouter();
  const { data: session, status } = useSession();
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  const [error, setError] = useState();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  if (status == "loading") {
    return (
      <>
      <SEO pageTitle={"much... | Sign in"} />
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }
  if (session) window.location.href = "/dashboard";
  if (!session)
    return (
      <>
      <div className="bg-white min-h-screen">
        <header id="header-wrap" className="relative">
          <div className="navigation  top-0 left-0 w-full z-50 duration-300">
            <div className="container p-4 text-center">
              <nav
                className={
                  "navbar p-4 navbar-expand-lg flex justify-center md:justify-between items-center relative duration-300"
                }
              >
                <Link className="navbar-brand left-10 right-10" href="/">
                  <Image
                    src="/images/logo.svg"
                    alt="Logo"
                    width={"160"}
                    height={"60"}
                  />
                </Link>
              </nav>
            </div>
          </div>
        </header>
        <main>
          <div className="signin-page text-center container h-fit ">
            <div className="lg:grid lg:grid-cols-2">
              <div></div>
              <div className="my-12 lg:my-20 lg:px-28  lg:bg-gray-50 lg:shadow-md lg:rounded-xl lg:mx-8 md:mx-28">
                <h1 className="text-2xl pt-5 font-bold text-gray-600">{_("Sign In!")}</h1>

                <div>
                  <div className="justify-center">
                    <form method="post" action="/api/auth/callback/credentials">
                      <input
                        name="csrfToken"
                        type="hidden"
                        defaultValue={csrfToken}
                      />
                      {query && query.callback&&<input
                        name="callbackUrl"
                        type="hidden"
                        defaultValue={query.callback}
                      />}
                      <div className="mt-4">
                        <input
                          name="email"
                          ref={emailInputRef}
                          className="border text-gray-600 border-gray-500 h-12 w-full rounded-md p-1 placeholder-gray-500"
                          placeholder={_("Email")}
                          type="email"
                        ></input>
                      </div>
                      <div className="mt-4">
                        <input
                          name="password"
                          ref={passwordInputRef}
                          className="border text-gray-600 border-gray-500 h-12 w-full rounded-md p-1 placeholder-gray-500"
                          placeholder={_("Password")}
                          type="password"
                        ></input>
                        <div className="my-3"><Link href="/reset-password"><div className="text-gray-600 decoration-transparent"> {_('Forget your password?')}</div></Link></div>
                      </div>
                      <div className="mt-4">
                        <button className="bg-emerald-300 text-white w-full h-12 rounded-md hover:bg-emerald-500 duration-200">
                          {_("Sign in")}
                        </button>
                      </div>
                    </form>
                  </div>

                  <div className="mt-4 justify-start grid grid-cols-12">
                    <div className="col-span-5 pt-3">
                      <hr className="border-gray-500"></hr>
                    </div>
                    <div className="col-span-2 text-gray-500">{_("OR")}</div>
                    <div className="col-span-5 pt-3">
                      <hr className="border-gray-500"></hr>
                    </div>
                  </div>
                  <div className="mt-4  flex justify-center">
                    {Object.values(providers).map((provider) =>
                      provider.id == "facebook" ? (
                        <button
                          onClick={() =>
                            signIn(provider.id, { callbackUrl: query.callback })
                          }
                          key={provider.name}
                          className="facebook w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i
                            className={
                              "fab fa-facebook-f text-blue-600  text-lg"
                            }
                          ></i>
                        </button>
                      ) : provider.id == "google" ? (
                        <button
                          key={provider.name}
                          onClick={() =>
                            signIn(provider.id, { callbackUrl: query.callback })
                          }
                          className="facebook w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-google text-red-500  text-lg"></i>
                        </button>
                      ) : provider.id == "linkedin" ? (
                        <button
                          key={provider.name}
                          onClick={() =>
                            signIn(provider.id, { callbackUrl: query.callback })
                          }
                          className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-linkedin-in text-blue-800  text-lg"></i>
                        </button>
                      ) : provider.id == "apple" ? (
                        <button
                          key={provider.name}
                          onClick={() =>
                            signIn(provider.id, { callbackUrl: query.callback })
                          }
                          className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-apple text-gray-800  text-lg"></i>
                        </button>
                      ) : provider.id == "twitter" ? (
                        <button
                          key={provider.name}
                          onClick={() =>
                            signIn(provider.id, { callbackUrl: query.callback })
                          }
                          className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-twitter text-cyan-400  text-lg"></i>
                        </button>
                      ) : (
                        ""
                      )
                    )}
                  </div>
                  <div className="my-4">
                    <p className="text-sm text-gray-600">
                      {_("Don't have a Card?")}{" "}
                      <span className="text-blue-600 font-bold">
                        <Link href={"/create-your-card/"}>{_("Create Now!")}</Link>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        </div>
      </>
    );
}

export async function getServerSideProps(context) {
  const providers = await getProviders();

  return {
    props: {
      query: context.query,
      csrfToken: await getCsrfToken(context),
      providers,
    },
  };
}
