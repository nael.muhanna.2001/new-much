import Image from "next/image";
import Link from "next/link";
import { translation } from "../lib/translations";
import { useRouter } from "next/router";
import { getCsrfToken } from "next-auth/react";
import { getProviders, signIn, useSession } from "next-auth/react";
import { useEffect, useRef, useState } from "react";
import Head from "next/head";
export default function SigninPage({ csrfToken, providers, query }) {
  const { locale, locales, defaultLocale, asPath } = useRouter();
  const { data: session, status } = useSession();

  const emailInputRef = useRef({current:{value:query.email}});
  const password = useRef();
  const passwordConf = useRef();

  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const [mailsent, setmailsent] = useState(query.token ? true : false);
  const [checkingToken, setCheckingToken] = useState(false);
  const [badToken, setBadtoken] = useState();
  const [user, setUser] = useState();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  useEffect(() => {
    if (query.email && query.token) {
      setCheckingToken(true);
      fetch("/api/reset-password/check-token/", {
        body: JSON.stringify({ email: query.email, token: query.token }),
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setCheckingToken(false);
          if (data.success) {
            setUser(data.user);
            setBadtoken(false);
          } else {
            setBadtoken(true);
            setError(data.message);
          }
        });
    }
  }, [query]);

  const setPassword = (e) => {
    e.preventDefault();
    if (password.current.value === passwordConf.current.value) {
      setLoading(true);
      fetch("/api/reset-password/set-password/", {
        method: "POST",
        body: JSON.stringify({ password: password.current.value, user: user }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }).then(res=>res.json()).then(data=>{
        console.log(data);
        if(data.success)
        {
            signIn("credentials",{email:user.email,password:password.current.value}).then(()=>{
                window.location.href='/dashboard';
            });
        }
        else
        {
            setError(data.message);
        }
      });
    } else {
      setError("Passwords don't matched!");
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setError();
    setLoading(true);
    fetch("/api/reset-password/request/", {
      method: "POST",
      body: JSON.stringify({ email: emailInputRef.current.value }),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setLoading(false);
        console.log(data);
        if (data.success) {
          setmailsent(true);
        }
      });
  };

  if (status == "loading") {
    return (
      <>
        <Head>
          <title>much... | {_("Reset Password")}</title>
        </Head>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }

  if (session) window.location.href = "/dashboard";
  if (!session)
    return (
      <>
        <div className="bg-white min-h-screen">
          <header id="header-wrap" className="relative">
            <div className="navigation  top-0 left-0 w-full z-50 duration-300">
              <div className="container p-4 text-center">
                <nav
                  className={
                    "navbar p-4 navbar-expand-lg flex justify-center md:justify-between items-center relative duration-300"
                  }
                >
                  <Link className="navbar-brand left-10 right-10" href="/">
                    <Image
                      src="/images/logo.svg"
                      alt="Logo"
                      width={"160"}
                      height={"60"}
                    />
                  </Link>
                </nav>
              </div>
            </div>
          </header>
          <main>
            {checkingToken && (
              <div className="contianer">
                <div className="flex flex-wrap">
                  <div className="w-full md:w-6/12">
                    <img src="/images/checking.svg" />
                  </div>
                  <div className="w-full md:w-6/12 text-center mt-12">
                    <h2 className="text-xl text-gray-600 my-5">
                      {_("Checking...")}
                    </h2>
                  </div>
                </div>
              </div>
            )}
            {query.email && query.token && badToken == false && (
              <div className="forgetpassowrd-page text-center container h-fit min-h-screen mt-10">
                <div className="lg:grid lg:grid-cols-2">
                  <div></div>
                  <div className="lg:my-36 lg:px-28  lg:bg-gray-50 lg:shadow-md lg:rounded-xl lg:mx-8 md:mx-28">
                    <h1 className="text-2xl pt-5 font-bold text-gray-600">
                      {_("Reset Password")}
                    </h1>

                    <div>
                      <div className="justify-center pb-12">
                        <form method="post" onSubmit={() => setPassword(event)}>
                          <div className="mt-4">
                            <input
                              name="password"
                              ref={password}
                              className="border text-gray-600 border-gray-500 h-12 w-full rounded-md p-1 placeholder-gray-500"
                              placeholder={_("New Password")}
                              type="password"
                              required
                            ></input>
                            <input
                              name="passwordConf"
                              ref={passwordConf}
                              className="border text-gray-600 border-gray-500 h-12 w-full rounded-md p-1 placeholder-gray-500"
                              placeholder={_("Confirm new password")}
                              type="password"
                              required
                            ></input>
                          </div>
                          <div className="mt-4">
                            <span className="text-red-500">{_(error)}</span>
                            <button
                              disabled={loading}
                              className="bg-emerald-300 text-white w-full h-12 rounded-md hover:bg-emerald-500 duration-200"
                            >
                              {_("Save Password & Sign in")}{" "}
                              {loading && (
                                <i className="fa fa-spinner animate-spin"></i>
                              )}
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {mailsent && !query.token && (
              <div className="container">
                <div className="flex flex-wrap">
                  <div className="w-full md:w-6/12">
                    <img src="/images/mailsent.svg" />
                  </div>
                  <div className="w-full md:w-6/12 text-center mt-12">
                    <h2 className="text-xl text-gray-600 my-5">
                      {_("Reset Link sent to your email")}
                    </h2>
                    <p className="text-lg text-gray-600 my-6">
                      {_(
                        "Please check your email and use the link as soon as possible!"
                      )}
                    </p>
                    <Link href="/">
                      <button className="bg-emerald-300 p-2 text-lg rounded-xl mt-6">
                        {_("Home")}
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            )}
            {(!mailsent || badToken) && (
              <div className="forgetpassowrd-page text-center container h-fit min-h-screen mt-10">
                <div className="lg:grid lg:grid-cols-2">
                  <div></div>
                  <div className="lg:my-36 lg:px-28  lg:bg-gray-50 lg:shadow-md lg:rounded-xl lg:mx-8 md:mx-28">
                    <h1 className="text-2xl pt-5 font-bold text-gray-600">
                      {_("Reset Password")}
                    </h1>

                    <div>
                      <div className="justify-center pb-12">
                        <form
                          method="post"
                          onSubmit={() => handleSubmit(event)}
                        >
                          <div className="mt-4">
                            <input
                              name="email"
                              ref={emailInputRef}
                              className="border text-gray-600 border-gray-500 h-12 w-full rounded-md p-1 placeholder-gray-500"
                              placeholder={_("Email")}
                              type="email"
                              required
                            ></input>
                          </div>
                          <div className="mt-4">
                            <span className="text-red-500">{_(error)}</span>
                            <button
                              disabled={loading}
                              className="bg-emerald-300 text-white w-full h-12 rounded-md hover:bg-emerald-500 duration-200"
                            >
                              {_("Send reset link")}{" "}
                              {loading && (
                                <i className="fa fa-spinner animate-spin"></i>
                              )}
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </main>
        </div>
      </>
    );
}

export async function getServerSideProps(context) {
  const providers = await getProviders();

  return {
    props: {
      query: context.query,
      csrfToken: await getCsrfToken(context),
      providers,
    },
  };
}
