import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import Hero from "../webComponents/hero/Hero";
import Wrapper from "../layout/wrapper";
import SEO from "../webComponents/Seo";
import SwitchDark from "../webComponents/switch/SwitchDark";
import { useRive, Layout } from "@rive-app/react-canvas";
import { translation } from "../lib/websiteTranslations";
import { useRouter } from "next/router";
import Footer from "../webComponents/new/footer";
import Header from "../webComponents/new/header";
import Features from "../webComponents/new/features";
import Marquee from "../webComponents/new/marquee";
import Products from "../webComponents/new/products";
import Ready from "../webComponents/new/ready";
import Faq from "../webComponents/new/faq";
import Teams from "../webComponents/new/teams";
import Plans from "../webComponents/new/plans";
import Enterprise from "../webComponents/new/enterprise";
import { useEffect } from "react";

const HomeDark = () => {
  const { query } = useRouter();
  const router = useRouter();
  const { rive, RiveComponent } = useRive({
    src: "/rive/logo.riv",
    autoplay: true,
  });

  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  useEffect(() => {
    document.body.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <>
      <SEO pageTitle={"much... | Digital Cards"} />
      <div className="bg-maincolor">
        <Header />
        <Features />
        <Products />
        {/* <Marquee /> */}
        <Faq />
        {/* <Enterprise /> */}
        {/* <Teams /> */}
        {/* <Plans /> */}
        <Ready />
        <Footer />
      </div>
    </>
  );
};

export default HomeDark;
