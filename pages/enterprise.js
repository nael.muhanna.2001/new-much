import { BsList, BsX } from "react-icons/bs";
import { useState } from "react";
import { translation } from "../lib/websiteTranslations";
import { useRouter } from "next/router";
import Header from "../webComponents/new/components/mainHeader";
import SEO from "../webComponents/Seo";


function _(text) {
  const { locale } = useRouter();
   if (translation[locale][text]) {
     return translation[locale][text];
   }
   return text;
 }



const Enterprise = () => {
  return (
    <>
    <SEO pageTitle={"much... | Enterprise"} />
    <div className="w-full bg-maincolor">
      <div className="container">
        <Header />
        <section className="section-8 w-full h-auto m-0 bg-maincolor">
          <div className="flex flex-col pt-8 lg:pt-0 w-full">
            <div className="flex flex-col justify-center items-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="py-4">
                  <h2 className="text-green text-4xl font-bold  text-center md:text-5xl md:font-bold">
                    {_('Much Enterprise')}
                  </h2>
                  <p className=" p-5 text-lg text-left text-white leading-8">
                    {_('Much recognizes the business requirements for advanced and customized solutions, thus providing a range of unique services to different organizations. Our NFC digital business cards and NFC tags with performance tracking and analytics offer clear insights on customer behavior, enabling you to tailor your strategies accordingly. Here is how our solutions help your business succeed.')}
                  </p>
                </div>
              </div>
            </div>
            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('Customized Packages')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Built To Fit Your Business')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {_('Much.sa offers customized packages to fit your business needs. Whether you require a few NFC digital business cards or tags, or thousands of them, we build a plan that suits you.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>

            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('Data Collection')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Increase Efficiency In Your Business')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {_('Our NFC digital business cards and NFC tags offer businesses valuable insights into customer behavior and preferences, making it easier to target marketing campaigns more effectively. The data collected includes information on user profiles, usage, and user feedback.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>

            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('Enhanced Analytics Tools')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Track And Make Use Of Your Data')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {_('Our NFC digital business cards and NFC tags also come with advanced built-in analytics tools, including key performance metrics such as customer engagement, user retention, and customer lifetime value. This information will offer you insights into customer engagement and help you optimize your marketing campaigns accordingly.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>

            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('Team Collaboration')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Manage Teams Effectively')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {_('Much.sa provides seamless and effective team collaboration, ensuring that your team can work together seamlessly. You can share contacts and other information with your team members, ensuring everyone has access to the same information, streamlining workflows, and boosting productivity.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>

            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('Custom Branding')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Be Unique & Recognizable')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {('Our NFC digital business cards and NFC tags can be customized to reflect your brand’s identity. You can choose the colors, design, and add your logo, ensuring consistency with your overall brand image.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>

            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('Advanced Security')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Consumer Trust Is Key')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {_('Much.sa values the importance of data security and data privacy. Our NFC digital business cards and NFC tags are equipped with advanced security features to ensure confidentiality, integrity, and authenticity of the data exchanged.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>

            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('24/7 Customer Support')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Constant Customer Care')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {('At Much.sa, customer satisfaction is a top priority. We offer 24/7 customer support to help you resolve any queries related to our products and solutions.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>
            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('CRM Integration')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Smooth Syncing Process')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {_('Our NFC digital business cards and NFC tags are fully compatible with all leading CRM solutions, including Salesforce, Zoho, Hubspot, Microsoft Dynamics, and much more.')}
                  </p>
                </div>
              </div>
              <div className="w-full h-auto mx-auto lg:mx-0 lg:mr-4 px-4 py-12 flex-col items-center">
                <img
                  className="w-[70%] h-auto pt-0 lg:pt-4 mx-auto"
                  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ0NDw8PDQ0NDQ0NDQ0NDw8NDQ0NFREWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDisZFRkrKy0tLS0rKystNzctKystLTctNy0rLSstLSstKysrNys3LSstKystLSstLSs3Ny0tK//AABEIAMIBAwMBIgACEQEDEQH/xAAXAAEBAQEAAAAAAAAAAAAAAAAAAQcC/8QAFhABAQEAAAAAAAAAAAAAAAAAAAER/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDERFAQAVAAAAAAKKCKggoAoioqCooACCoAKIooCKAAgAACggIAKgAKCAoIAAqCCgAAKoAggoICCigIoAAAqAAAAKAgiKigKAJioCgCooAgqCKAgACgAAAIKgiiKoAIAAACgCoACiAAgoCKgCgCoqKgIqAAAoigAAKgCoAIAqACCoAKIqgAACoqAAAAgoqICiiAgAACoAACiKAAAAAigIKggKAAAigoaoCoAgAKAAIAIoioqAAAKACAAIKgKoAAigAAAAAAAKgAgACgAAACKAgAAAAAgAKACACgAiqAAAAAAAAAAKAgCoAIoAAABUUUQBEAAAAAAAFABBRAVQAAAAAAAUQAAUAAFQQAFABBBUEAAAFAAAAAAAAFEVFAAAAAAAAAUEFARUAAFAABFRAAVAAABAAAAAAUFQQUAAAVUABUUEVBRUUQQAAAAAEAVAAABAAUAEAAAUBBUUUBFAAFRQRQAQFQAFABAAAQAAAAAAAAAAQFAABVAEABUFRUUAAVFBAFEKAAAhSABEABQBFUFc0AQigAAgEAAUFKgARQBFBQFEEigK//2Q=="
                  alt=""
                />
              </div>
            </div>

            <div className="flex flex-col justify-center mt-5">
              <div className="flex flex-col justify-between  px-4 py-4 m-4">
                <div className="p-4">
                  <h2 className="text-green text-xl font-bold  text-left md:text-3xl md:font-bold">
                    {_('Request A Quote')}
                  </h2>
                  <p className="text-green text-left md:text-left  !py-5 md:text-lg">
                    {_('Smooth Syncing Process')}
                  </p>
                  <p className=" text-lg text-left text-white leading-8">
                    {_('Unlock your team\'s potential with Much Enterprise! Request a customized quote tailored to your organization\'s needs. Getstarted today!')}
                  </p>
                </div>
                <div className="flex justify-center mt-8 group">
                  <div className="text-left bg-green hover:bg-emerald-400 hover:text-white transition-all duration-500 rounded-3xl text-white px-3 py-2 group-hover:scale-105 shadow-sm hover:!shadow-lg shadow-stone-200">
                    <a href="#" className="text-lg px-3 py-2">
                      {_('Request A Quote')}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    </>
  );
};

export default Enterprise;
