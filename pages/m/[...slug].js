export default function MasterLink({props})
{

}
export const getStaticPaths = async () => {
    return {
      paths: [], //indicates that no page needs be created at build time
      fallback: "blocking", //indicates the type of fallback
    };
  };
export async function getStaticProps(context) {
    let { slug } = context.params;
    const linkReq = await fetch('http://much.sa/api/master-link/'+slug);
    if(linkReq.status==200)
    {
        const link = await linkReq.json();
        if(link.type=='master-link')
        {
            return {
                redirect: {
                  destination: "/" + link.slug + "#" + link.src,
                },
              };
        }
        if(link.type=='new-link')
        {
            return {
                redirect:{
                    destination:'/new-link-activation/'+link.link
                }
            }
        }
    }
}