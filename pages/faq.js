
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import Hero from "../webComponents/hero/Hero";
import FeaturesMain from "../webComponents/features";
import Wrapper from "../layout/wrapper";
import SEO from "../webComponents/Seo";
import Products from "../webComponents/products/Products";
import Address from "../webComponents/Address";
import Contact from "../webComponents/Contact";
import SwitchDark from "../webComponents/switch/SwitchDark";
import { useRive,Layout} from '@rive-app/react-canvas';
import { translation } from "../lib/translations";
import { useRouter } from "next/router";
import Accordion from "../components/accordion";
import { useEffect, useState } from "react";

const HomeDark = () => {
  const {query} =useRouter();
  const router = useRouter();
  const { rive, RiveComponent } = useRive({
    src: '/rive/logo.riv',
    autoplay:true,
  })

  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  
  const menuItem = [
    { icon: "fa-home", menuName: _("Home"),name:"/" },
    { icon: "fa-star", menuName: _("Features"),name:"features" },
    { icon: "fab fa-product-hunt", menuName: _("Products"),name:'products' },
    { icon: "fas fa-question-circle", menuName: "FAQ",name:'faq' },
    { icon: "fa-envelope", menuName: _("Contact"),name:'contact' },
    
  ];
  
  let dm =3;

    
    

  return (
    <Wrapper>
      <SEO pageTitle={"much... | Digital Cards"} />
      <div className="container hidden lg:block">
        <nav className="fixed">
          <div className="logo w-32 h-20"><RiveComponent /></div>
        </nav>
      </div>
      <div className="blue">
        <SwitchDark />

        {/* End Switcher */}
        <Tabs defaultIndex={dm}>
          <div className="header">
            <TabList className=" icon-menu  revealator-slideup revealator-once revealator-delay1" >
              {menuItem.map((item, i) => (
                <Tab className="icon-box" key={i} onClick={()=>{
                  router.push(item.name);
                }}>
                  <i className={`fa ${item.icon}`}></i>
                  <h2>{item.menuName}</h2>
                </Tab>
              ))}
            </TabList>
          </div>
          {/* End Menu Content */}

          <div className="tab-panel_list">
            
            <TabPanel className="home ">
            </TabPanel>
            <TabPanel className="about">
            </TabPanel>
            {/* About Content Ends */}

            {/* Products Content Starts */}
            <TabPanel className="portfolio professional">
            </TabPanel>
            {/* Products Content Ends */}
            {/* Products Content Starts */}
            <TabPanel className="faq professional">
              <div className="title-section lg:text-start text-center">
                <h1>
                  <span>{_('FAQ')}</span>
                </h1>
                <span className="title-bg text-6xl">{_('Frequently Asked Question')}</span>
              </div>
              <div className="container" style={{maxWidth:'920px'}}>
              <Accordion
              title={_("What is the benefit of much... digital cards?")}
              content={
                "<ul>" +
                "<li>" +
                _(
                  "The ease of sharing your info: A digital business card is the easiest and fastest approach to share your contact details in a professional way. ‍"
                ) +
                "</li><li>" +
                _(
                  "Stay always connected: you can have your digital card always with you on your smartphone or wherever you go!"
                ) +
                "</li><li>" +
                _(
                  "Always stay up to date: keep your info up to date all the time in case if you change your contact details or updated more info on your much... digital card, those who have your card will receive your updated information on the spot."
                ) +
                "</li></ul>"
              }
            />
            <Accordion
              title={_("Do much... digital cards work in my phone and laptop?")}
              content={_(
                "Of course, if you have a smartphone, either Android or iPhone. You just need to scan the QR code by opening your phone camera."
              )}
            />
            <Accordion
              title={_("Can I have multiple much... digital cards?")}
              content={_(
                "Yes, you can have up to 10 digital cards. much... provide multiple variety of digital card, such as: Business Card, Business Profile, Product Card & Event Card."
              )}
            />
            <Accordion
              title={_("How do much... digital cards work?")}
              content={_(
                "You basically can create your digital card by visiting www.much.sa. After creating your account, you will be able to create your card and design it as you desire! Once you complete your card, you can start sharing it with your potential clients by letting them scan your digital card via the QR Code. Doing so will able them to view your digital card and just hit “save to contact”."
              )}
            />
            <Accordion
              title={_("Can I update my information on my much... digital card?")}
              content={_(
                "Yes, in your account dashboard, you will be able to update/edit your contact information whenever you like. Any changes you make are instantly applied to your card in real-time."
              )}
            />
            <Accordion
              title={_("How do NFC business cards work?")}
              content={_(
                "much... offers NFC or ‘Near Fields Communication’ digital cards. NFC cards have a chip embedded in each card that can be synced with your much... digital card and dashboard. ‍Whenever you want to share your details with someone, simply tap the NFC card on the smartphone which will bring up a link containing your much... digital card. You can purchase NFC cards via our website today!"
              )}
            />
            </div>
            </TabPanel>
            <TabPanel className="contact">
            </TabPanel>
            

            
          </div>
        </Tabs>
      </div>
    </Wrapper>
  );
};

export default HomeDark;
