import { signOut, useSession } from "next-auth/react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { translation } from "../../lib/translations";

export default function NewLinkActivation({ newLink }) {
  const { locale, query } = useRouter();
  const router = useRouter();
  const [selectType, setSelectType] = useState();
  const [isNew, setIsNew] = useState();
  const [isChecking, setIsChecking] = useState(false);
  const [wrongPin, setWrongPin] = useState(false);
  const [inputPin, setInputPin] = useState();
  const { data: session, status } = useSession();
  //=== Translation Function Start
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  //===== Check Pin ======
  const checkPin = async (pin) => {
    setWrongPin(false);
    if (pin.length == 4) {
      setIsChecking(true);
      const checker = await fetch("/api/pre-active/check-pin/", {
        method: "POST",
        body: JSON.stringify({ link: newLink.link, pin: pin }),
      });
      setIsChecking(false);
      if (checker.status == 200) {
        setSelectType(true);
      }
      if (checker.status == 204) {
        setInputPin("");
        setWrongPin(true);
      }
    }
  };

  return (
    <>
    <Head>
      <title>{_('Product Activation')}</title>
    </Head>
      <div className="container">
        <div className="mx-auto max-w-xl mt-6">
          <Image
            className="brightness-200"
            src="/images/logo.svg"
            width="160"
            height="60"
          />
        </div>
        {!selectType && (
          <div className="bg-white mt-6 p-4 max-w-lg mx-auto rounded">
            <div className="">
              <h1 className="text-lg text-gray-600 text-center">{_('Thank you for choosing our product')}</h1>
            </div>
            {newLink.product &&<div className="text-center mx-auto w-full">
              {newLink.product.cardFront &&<img className="mx-auto" src={newLink.product.cardFront.imgUrl}  />}
              {newLink.product.images && newLink.product.images.length &&<img className="mx-auto" src={newLink.product.images[0].imgUrl}  />}
              <h3 className="text-lg text-gray-500">{newLink.product.name[locale]?newLink.product.name[locale]:newLink.product.name['en']}</h3>
              </div>}
          <div className=" mx-auto max-w-xl my-28">
            <div className="mt-2 text-gray-700">
              <h1 className="text-lg font-bold text-center">
                {_("To activate the product please insert the activation code associated with the product")}
              </h1>
            </div>
            <div className="card-body">
              <div className="mt-12">
                <input
                  type="number"
                  min="1000"
                  max="9999"
                  style={{ border: wrongPin ? "red 2px solid" : "2px solid #ccc" }}
                  value={inputPin}
                  disabled={isChecking}
                  className="form-control text-6xl text-center"
                  placeholder="- - - -"
                  onChange={(e) => {
                    setInputPin(e.target.value);
                    checkPin(e.target.value);
                  }}
                />
                {isChecking && (
                  <span className="absolute z-50 text-2xl top-36 right-8 text-gray-500">
                    <i className="fa fa-spinner animate-spin"></i>
                  </span>
                )}
                {wrongPin && (
                  <span className="text-red-500">{_("Wrong code! Please try again")}</span>
                )}
              </div>
            </div>
          </div>
          </div>
        )}
        {selectType && (
          <div className="card mx-auto max-w-xl my-28">
            <div className="card-header text-gray-700">
              <h1 className="text-lg font-bold text-center px-6">
                {_("Do you have a much... account?")}
              </h1>
            </div>
            <div className="card-body text-center">
              <div className="mt-12">
                {!session &&<Link
                  href={
                    "/signin/?callback=/dashboard/products/create/" +
                    newLink.link
                  }
                >
                  <button className="w-full p-3 bg-emerald-300 rounded font-bold">
                    {_("Yes, sign-in")}
                  </button>
                </Link>}
                {session &&
                <div>
                <Link
                  href={
                    "/dashboard/products/create/" +
                    newLink.link
                  }
                >
                  <button className="w-full p-3 bg-emerald-300 rounded">
                    {_("Yes, linked to exist digital card in my account")}
                  </button>
                </Link>
                <div className="mt-1  text-sm text-gray-500"><span className="text-gray-700">{_('You\'er sign-in as: ')}</span>{session && session.user && session.user.image && <div className="flex"><img src={session.user.image} className="rounded-full h-6 w-6 mx-2" /> <span className="text-gray-500 mx-1">{session.user.name}</span> <a className="text-blue-600" onClick={()=>signOut()}>{_('Sign-out')}</a></div>}</div>
                </div>
                }
              </div>
              <div className="mt-12">
                <Link href={"/create-your-card/?masterlink=" + newLink.link}>
                  <button className="w-full p-3 bg-emerald-300 rounded font-bold">
                    {_("No, create an account")}
                  </button>
                </Link>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
}
export async function getServerSideProps(context) {
  const { link } = context.query;
  const linkReq = await fetch("http://much.sa/api/master-link/" + link);
  if (linkReq.status == 200) {
    const newLink = await linkReq.json();
    if (newLink.type == "new-link") {
      return {
        props: { newLink },
      };
    }
  }
  return {
    redirect: {
      destination: "/m/"+link,
    },
  };
}
