
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import Hero from "../webComponents/hero/Hero";
import Wrapper from "../layout/wrapper";
import SEO from "../webComponents/Seo";
import SwitchDark from "../webComponents/switch/SwitchDark";
import { useRive,Layout} from '@rive-app/react-canvas';
import { translation } from "../lib/translations";
import { useRouter } from "next/router";


const HomeDark = () => {
  
  const {query} =useRouter();
  const router = useRouter();
  const { rive, RiveComponent } = useRive({
    src: '/rive/logo.riv',
    autoplay:true,
  })

  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
 
  const menuItem = [
    { icon: "fa-home", menuName: _("Home"),name:"home" },
    { icon: "fa-star", menuName: _("Features"),name:"features" },
    { icon: "fab fa-product-hunt", menuName: _("Products"),name:'products' },
    { icon: "fas fa-question-circle", menuName: "FAQ",name:'faq' },
    { icon: "fa-envelope", menuName: _("Contact"),name:'contact' },
    
  ];
  
  let dm =0;
    

  return (
    <Wrapper id="main">
      <SEO pageTitle={"much... | Digital Cards"} />
      <div className="container hidden lg:block">
        <nav className="fixed">
          <div className="logo w-32 h-20"><RiveComponent /></div>
        </nav>
      </div>
      <div className="blue">
        <SwitchDark />
        
        {/* End Switcher */}
        <Tabs defaultIndex={dm}>
          <div className="header">
            <TabList className=" icon-menu  revealator-slideup revealator-once revealator-delay1" >
              {menuItem.map((item, i) => (
                <Tab className="icon-box" key={i} onClick={()=>{
                  router.push(item.name);
                }}>
                  <i className={`fa ${item.icon}`}></i>
                  <h2>{item.menuName}</h2>
                </Tab>
              ))}
            </TabList>
          </div>
          {/* End Menu Content */}

          <div className="tab-panel_list">
            {/* Hero Content Starts */}
            <TabPanel className="home ">
              <div
                className="container-fluid main-container container-home p-0"
                data-aos-duration="1200"
              >
                
                <div
                  className="color-block"
                  style={{ zIndex: "-1" }}
                >
                  <img className="block lg:hidden w-52 -mt-5 mx-1" src="/images/logo-white.svg" />
                  <video
                    loop
                    autoPlay
                    muted
                    preload="auto"
                    style={{ width: "100%" }}
                    className="hidden lg:block"
                  >
                    <source src="https://pioneers.network/much/intro-new.mp4" type="video/mp4" />
                    Your browser does not support the video tag.
                  </video>
                  
                </div>
                <Hero />
              </div>
            </TabPanel>
            {/* Hero Content Ends */}

            {/* About Content Starts */}
            <TabPanel className="about">
            </TabPanel>
            {/* About Content Ends */}

            {/* Products Content Starts */}
            <TabPanel className="portfolio professional">
            </TabPanel>
            {/* Products Content Ends */}
            {/* Products Content Starts */}
            <TabPanel className="faq professional">
            </TabPanel>
            {/* Products Content Ends */}

            {/* Contact Content Starts */}
            <TabPanel className="contact">
            </TabPanel>
            {/* Contact Content Ends */}

            
          </div>
        </Tabs>
      </div>
    </Wrapper>
  );
};

export default HomeDark;
