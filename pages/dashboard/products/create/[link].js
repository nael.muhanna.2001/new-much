import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { translation } from "../../../../lib/translations";

export default function CreateProduct({ params }) {
    const { locale } = useRouter();
  //=== Translation Function Start
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
    const router = useRouter();
    const [loading,setLoading]=useState(false);
    const [cards,setCards]=useState([]);
    useEffect(() => {
        setLoading(true);
        fetch("/api/cards/", { method: "POST" })
          .then((res) => res.json())
          .then((data) => {
            setCards(data);
            setLoading(false);
          });
      }, []);
      //===== Select Card ======//
      const selectCard = async (id)=>{
        console.log(id);
        const linkCard = await fetch('/api/master-link/create/'+params.link,{method:'post',body:JSON.stringify({
            card_id:id
        })});
        const LinkRes = await linkCard.json();
        if(LinkRes.success)
        {
            router.replace('/dashboard/products');
        }
      }
      return <>
      <div className="container max-w-xl mt-28">
      <div className="card">
        <div className="card-header">
            <h2 className="text-lg font-bold normal-case">{_('Select the digital card you wish to link product to')}</h2>
        </div>
        <div className="card-body mx-auto">
            <div className="flex flex-wrap">
                {cards.map((e)=>{
                    return <div
                    style={{minWidth:'250px'}}
                    key={e._id}
                    className={
                      e.cardType == "mx-auto business_card "
                        ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white m-2  cursor-pointer border-b-2 border-emerald-300"
                        : e.cardType == "company_profile"
                        ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white m-2  cursor-pointer border-b-2 border-blue-600"
                        : e.cardType == "product_card"
                        ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white  m-2  cursor-pointer border-b-2 border-orange-600"
                        : e.cardType == "event_card"
                        ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white  m-2  cursor-pointer border-b-2 border-pink-600"
                        : "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white  m-2  cursor-pointer"
                    }
                  >
                    
                    <div className="card-body">
                      {
                        <div
                          className={
                            e.cardType == "business_card"
                              ? "pr-2 text-emerald-300 text-xs"
                              : e.cardType == "company_profile"
                              ? "pr-2 text-gray-600 text-xs"
                              : e.cardType == "product_card"
                              ? "pr-2 text-orange-600 text-xs"
                              : e.cardType == "event_card"
                              ? "pr-2 text-pink-600 text-xs"
                              : ""
                          }
                        >
                          {e.cardType == "business_card"
                            ? _("Businees Card")
                            : e.cardType == "company_profile"
                            ? _("Company Profile")
                            : e.cardType == "product_card"
                            ? _("Product Card")
                            : ""}
                        </div>
                      }
                      
                        <div>
                          <h1 className="text-sm">
                            {!e.pic && (
                              <i
                                className={
                                  e.cardType == "business_card"
                                    ? "pr-2 text-emerald-300 mx-1 fa fa-user"
                                    : e.cardType == "company_profile"
                                    ? "pr-2 text-emerald-300 mx-1 fa fa-building"
                                    : e.cardType == "product_card"
                                    ? "pr-2 text-orange-600 mx-1 fa fa-tag"
                                    : e.cardType == "event_card"
                                    ? "pr-2 text-pink-600  mx-1 fa-light fa-calendar-days"
                                    : ""
                                }
                              ></i>
                            )}
                            {e.pic && (
                              <img
                                className="w-10 h-10 ltr:float-left rtl:float-right mx-1 rounded-full"
                                src={e.pic}
                              />
                            )}
                            <h2 className="text-sm normal-case">{e.name ? e.name : "Untitle"}</h2>
                            {e.cardType == "business_card" &&
                              e.company && (
                                <h3 className="text-gray-400 text-xs">
                                  {e.company.label}
                                </h3>
                              )}
                          </h1>
                        </div>
                      
                    </div>
                    <div className="card-footer">
                      <div className="btn-group">
                        <button onClick={()=>{selectCard(e._id)}} className="border bg-emerald-300 rounded p-2 px-4">{_('Select')}</button>
                      </div>
                    </div>
                  </div>
                })}
            </div>
        </div>
      </div>
      </div>
      </>
}
export async function getServerSideProps(context) {
  const masterLinkReq = await fetch(
    "http://much.sa/api/master-link/" + context.params.link
  );
  const masterLink = await masterLinkReq.json();
  
  if (masterLink.type == "master-link") {
    return {
      redirect: {
        destination: "/dashboard/products/",
      },
    };
  }
  if(masterLink.type=='new-link')
  {
    return {
        props:{params:context.params}
    }
  }
  
}
