import Head from "next/head";
import { useRouter } from "next/router";
import Admin from "../../../layouts/Admin";
import { translation } from "../../../lib/translations";
import "rsuite/dist/rsuite.min.css";
import Link from "next/link";
import { useEffect, useState } from "react";
import { Placeholder } from "rsuite";
import ReactCardFlip from "react-card-flip";
export default function MyProductsIndex() {
  const { locale } = useRouter();
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [cards, setCards] = useState([]);
  const [productCard, setProductCard] = useState({});
  //====== Translation =========
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  useEffect(() => {
    setLoading(true);
    fetch("/api/master-link/")
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
        fetch("/api/cards/", { method: "POST" })
          .then((res) => res.json())
          .then((data) => {
            setCards(data);
            setLoading(false);
          });
      });
  }, []);
  return (
    <>
      <Head>
        <title>{_("My Products")}</title>
      </Head>

      <Admin>
        <div className="container px-6">
          <div className="hidden lg:block lg:-m-1  bg-white">
            <div className="text-emerald-800 text-sm hidden lg:block p-4">
              {" "}
              {_("My Products")}
            </div>
          </div>
          {loading && (
            <div className=" mt-4 bg-white px-6 p-6">
              <Placeholder.Graph
                height={500}
                color="#ccc"
                rows={10}
                columns={6}
                active
              />
            </div>
          )}
          {loading == false && products.length == 0 && (
            <div className="flex flex-wrap mt-4 bg-white px-6">
              <div className="w-full lg:w-6/12">
                <div className="mx-auto text-center my-36">
                  <div className="my-12">
                    <h1 className="text-2xl mb-2">{"Products"}</h1>
                    <p className="text-lg">
                      {_("Share your Share Your Details with one Tap")}
                    </p>
                  </div>
                  <Link href="/store">
                    <button className="bg-emerald-300 p-2 px-4 rounder font-bold text-gray-600 rounded hover:bg-blue-600 hover:text-white duration-300">
                      {_("Buy Now")} <i className="fa fa-arrow-right"></i>
                    </button>
                  </Link>
                </div>
                <div className="flex flex-wrap my-2">
                  <div className=" rounded-xl border p-2 px-3 mx-1">
                    <span className="text-sm">{_("NFC Cards")}</span>
                  </div>
                  <div className=" rounded-xl border p-2 px-3 mx-1">
                    <span className="text-sm">{_("NFC Tags")}</span>
                  </div>

                  <div className=" rounded-xl border p-2 px-3 mx-1">
                    <span className="text-sm">{_("Branded Cards")}</span>
                  </div>
                  <div className="p-2 px-3">&much...</div>
                </div>
              </div>
              <div className="w-full lg:w-6/12">
                <img src="/images/products.png" />
              </div>
            </div>
          )}
          {products.length > 0 && (
            <div className="mx-auto flex flex-wrap mt-4 bg-white px-6">
              {products.map((e) => {
                return (
                  <ReactCardFlip key={e._id} isFlipped={productCard[e._id]}>
                    <div
                      key={e._id}
                      className="card m-2 md:max-w-xs p-2 lg:min-w-[320px] min-w-[251px] min-h-[254px]  lg:min-h-[279px]"
                    >
                      <div
                        className="absolute w-full"
                        onClick={() => {
                          setProductCard((prev) => {
                            let newVal = prev;
                            newVal[e._id] = !newVal[e._id];
                            console.log(newVal);
                            return { ...newVal };
                          });
                        }}
                      >
                        <span className="float-right cursor-pointer m-3"><i className="fas fa-info-circle"></i> {_('info')}</span>
                        
                      </div>
                      {e.product && (
                        <div className="mb-4">
                          <div className="m-2">
                            {e.product && e.product.cardFront && (
                              <img src={e.product.cardFront.imgUrl} />
                            )}
                            {e.product &&
                              e.product.images &&
                              e.product.images.length && (
                                <img src={e.product.images[0].imgUrl} />
                              )}
                          </div>
                          <h2 className="text-xs normal-case mb-1 w-full whitespace-nowrap text-ellipsis overflow-hidden max-w-full">
                            {e.product.name[locale]
                              ? e.product.name[locale]
                              : e.product.name["en"]}
                          </h2>
                          {cards.filter((c) => c._id == e.slug).length > 0 && (
                            <span className="text-xs normal-case flex">
                              {cards.filter((c) => c._id == e.slug)[0].pic && (
                                <img
                                  className="h-6 w-6 mx-1 rounded-full"
                                  src={
                                    cards.filter((c) => c._id == e.slug)[0].pic
                                  }
                                />
                              )}
                              {cards.filter((c) => c._id == e.slug)[0].name}{" "}
                              <br />(
                              {_(
                                cards.filter((c) => c._id == e.slug)[0].cardType
                              )}
                              )
                            </span>
                          )}
                        </div>
                      )}
                    </div>
                    <div
                      key={e._id}
                      className="card m-2 md:max-w-xs p-2 lg:min-w-[320px] min-w-[251px] min-h-[254px]  lg:min-h-[279px]"
                    >
                      <div
                        className="absolute w-full"
                        onClick={() => {
                          setProductCard((prev) => {
                            let newVal = prev;
                            newVal[e._id] = !newVal[e._id];
                            console.log(newVal);
                            return { ...newVal };
                          });
                        }}
                      >
                        <i className="fas fa-arrow-right float-right cursor-pointer m-3"></i>
                      </div>
                      <div className="w-full mt-6">
                        <div className="flex">
                          <label className="w-5/12">{_("Created At")}:</label>
                          <h3 className="w-7/12 text-sm font-normal">
                            {new Date(e.created_at).toLocaleString()}
                          </h3>
                        </div>
                        <div className="flex mt-2">
                          <label className="w-5/12">{_("Product")}:</label>
                          <h3 className="w-7/12 text-sm font-normal">
                            {e.product.name[locale]
                              ? e.product.name[locale]
                              : e.product.name["en"]}
                          </h3>
                        </div>
                        <div className="flex mt-2">
                          <label className="w-5/12">
                            {_("Product price")}:
                          </label>
                          <h3 className="w-7/12 text-sm font-normal">
                            {e.product.price} {e.product.currency}
                          </h3>
                        </div>
                        <div className="flex flex-wrap mt-2">
                          <label className="w-full">{_("Linked to")}:</label>
                          <h3 className="w-full text-sm font-normal">
                            {cards.filter((c) => c._id == e.slug).length >
                              0 && (
                              <span className="text-xs normal-case flex">
                                {cards.filter((c) => c._id == e.slug)[0]
                                  .pic && (
                                  <img
                                    className="h-6 w-6 mx-1 rounded-full"
                                    src={
                                      cards.filter((c) => c._id == e.slug)[0]
                                        .pic
                                    }
                                  />
                                )}
                                {cards.filter((c) => c._id == e.slug)[0].name}{" "}
                                <br />(
                                {_(
                                  cards.filter((c) => c._id == e.slug)[0]
                                    .cardType
                                )}
                                )
                                <button className="text-blue-500 p-2 font-bold">{_('Change')}<i className="fas fa-crown text-yellow-400"></i></button>
                              </span>
                            )}
                          </h3>
                        </div>
                      </div>
                    </div>
                  </ReactCardFlip>
                );
              })}
            </div>
          )}
        </div>
      </Admin>
    </>
  );
}
