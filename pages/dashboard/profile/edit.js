import { useSession } from "next-auth/react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import Admin from "../../../layouts/Admin";
import { translation } from "../../../lib/translations";
import "rsuite/dist/rsuite.min.css";
import { useEffect, useState } from "react";
import Link from "next/link";
import "rsuite/dist/rsuite.min.css";
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function EditProfile() {
    const { data: session, status } = useSession();
    
    const [name,setName]=useState(session?.user?.name);
    const [email,setEmail]=useState();
    const [img,setImg]=useState();
    const [password,setPasswrod]=useState();
    const [passwordConfirm,setPasswrodConfirm]=useState();
  useEffect(()=>{
    setName(session?.user?.name)
    setEmail(session?.user?.email)
    setImg(session?.user?.image)
  },[session])
  if (status == "loading") {
    return (
      <>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }
  if (!session) window.location.href = "/signin";
  if (session && status != "loading")
    return (
      <>
        <Head>
          <title>
            {_("Edit") + " " + session.user.name + " " + _("Profile")}
          </title>
        </Head>
        <Admin>
          <div className="mt-24 p-4 bg-white rounded-xl mx-3">
            <div>
              <h3 className="text-gray-700">
                {_("Edit") + " " + session.user.name + " " + _("Profile")}
              </h3>
              <hr className="broder border-2" />
            </div>
            
              <div className="card-body">
                <div>
                  <div className="mb-8 text-center">
                    <div className="mx-auto">
                    {img && (
                      <div className="mx-auto w-32 h-32 rounded-full overflow-hidden border mb-1">
                        <img className="w-full" src={img} />
                      </div>
                    )}
                    {!img && (
                      <div className="mx-auto w-32 h-32 rounded-full overflow-hidden border mb-1">
                        <img
                          className="w-full"
                          src="https://ui-avatars.com/api/?name=Omran Karajeh"
                        />
                      </div>
                    )}
                    </div>
                    <div className="btn-group h-10 rounded-xl" style={{direction:'ltr'}}>
                        <button type="button" className="rounded-l-xl  border p-2 bg-emerald-300" onClick={()=>{}}>{_('Change')}</button>
                        <button type="button" className="rounded-r-xl border p-2 bg-emerald-300" onClick={()=>{setImg()}}>{_('Remove')}</button>
                        </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <div className="w-full md:w-2/12">
                      <label className="uppercase font-bold text-sm text-gray-700">
                        {_("Name")}
                      </label>
                    </div>
                    <div className="w-full md:w-10/12">
                      <input value={name} className="form-control" onChange={(e)=>{
                        setName(e.target.value);
                      }} />
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <div className="w-full md:w-2/12">
                      <label className="uppercase font-bold text-sm text-gray-700">
                        {_("Email")}
                      </label>
                    </div>
                    <div className="w-full md:w-10/12">
                      <input type='email' value={email} className="form-control" onChange={(e)=>{
                        setEmail(e.target.value);
                      }} />
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <div className="w-full md:w-2/12">
                      <label className="uppercase font-bold text-sm text-gray-700">
                        {_("Password")}
                      </label>
                    </div>
                    <div className="w-full md:w-10/12">
                      <input type='password' className="form-control" onChange={(e)=>{
                        setPasswrod(e.target.value);
                      }} />
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <div className="w-full md:w-2/12">
                      <label className="uppercase font-bold text-sm text-gray-700">
                        {_("Password Confirmation")}
                      </label>
                    </div>
                    <div className="w-full md:w-10/12">
                      <input type='password' className="form-control" onChange={(e)=>{
                        setPasswrodConfirm(e.target.value);
                      }} />
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <div className="p-4 text-center">
                    <div className="btn-group md:ltr:right-10 md:rtl:left-10 block md:absolute " style={{direction:'ltr'}}>
                    <button type="button" className="btn font-bold">{_('Save')} </button>
                    <Link href="/dashboard/profile">
                    <button  type="button" className="btn font-bold bg-red-400 hover:bg-red-300 decoration-transparent hover:decoration-transparent">{_('Cancel')}</button>
                    </Link>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
        </Admin>
      </>
    );
}
