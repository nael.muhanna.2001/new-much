import { useSession } from "next-auth/react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import Admin from "../../../layouts/Admin";
import { translation } from "../../../lib/translations";
import "rsuite/dist/rsuite.min.css";
import Link from "next/link";
function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
export default function Profile() {
  const { data: session, status } = useSession();
  if (status == "loading") {
    return (
      <>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }
  if (!session) window.location.href = "/signin";
  if (session && status != "loading")
  return <>
  <Head>
    <title>{_('Profile')}</title>
  </Head>
  <Admin>
    <div className="mt-24 p-4 bg-white rounded-xl mx-3">
    <div>
        <h3>{session.user?.name +" "+ _('Profile')}</h3>
        <hr className="broder border-2" />
        <div className="card-body">
            <div className="">
                <div className="w-full">
                    {session.user.image && <div className="w-32 h-32 rounded-full overflow-hidden border mb-4">
                        <img className="w-full" src={session.user.image} />
                        </div>}
                        {!session.user.image && <div className="w-32 h-32 rounded-full overflow-hidden border mb-4">
                        <img className="w-full" src="https://ui-avatars.com/api/?name=Omran Karajeh" />
                        </div>}
                </div>
                <div className="mb-4 flex flex-wrap">
                <div className="w-full md:w-2/12">
                    <label className="uppercase font-bold text-sm">{_('Name')}</label>
                </div>
                <div className="w-full md:w-10/12">
                    {session.user.name}
                </div>
                </div>
                <div className="mb-4 flex flex-wrap">
                <div className="w-full md:w-2/12">
                    <label className="uppercase font-bold text-sm">{_('Email')}</label>
                </div>
                <div className="w-full md:w-10/12">
                    {session.user.email}
                </div>
                </div>
                <div className="mb-4 flex flex-wrap">
                <div className="w-full md:w-2/12">
                    <label className="uppercase font-bold text-sm">{_('Registered at')}</label>
                </div>
                <div className="w-full md:w-10/12">
                    {new Date(session.user.created_at).toLocaleDateString()}
                </div>
                </div>
                <div className="mb-4 flex flex-wrap">
                <div className="w-full md:w-2/12">
                    <label className="uppercase font-bold text-sm">{_('Cards in your wallet')}</label>
                </div>
                <div className="w-full md:w-10/12">
                    <Link href="/dashboard/wallet">
                    
                    {session.user.wallet && session.user.wallet.length + " " + _('Cards')}
                    
                    </Link>
                </div>
                </div>
                <div>
                    <Link href='/dashboard/profile/edit'>
                    {_('Edit')}
                    </Link>
                </div>
            </div>
        </div>
    </div>
    </div>
  </Admin>
  </>
}
