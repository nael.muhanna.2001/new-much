import Head from "next/head";
import Admin from "../../../layouts/Admin";
import { translation } from "../../../lib/translations";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import QRCode from "easyqrcodejs";
import { toPng } from "html-to-image";
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

export default function CreateBackground() {
  const { data: session, status } = useSession();
  const [cards, setCards] = useState([]);
  const [isLoading, setLoading] = useState();
  //====== convert html to png ===//
  const convertDivToPng = async (div) => {
    const data = await toPng(div, {
      cacheBust: true,
      canvasWidth: 842,
      canvasHeight: 595,
    });
    return data;
  };
  //====== Download PNG =========//
  const handleDownloadTest = async (e, card) => {
    try {
      const data = await convertDivToPng(document.getElementById(card._id));
      if (data) {
        const link = document.createElement("a");
        link.href = data;
        link.download = "Image B.png";
        link.click();
      }
    } catch (e) {
      console.log(e, "ini errornya");
    }
  };
  // ===== Downlaod QR Code =====//
  const downloadQRCode = (card) => {
    // Generate download with use canvas and stream
    const canvas = document.getElementById(card._id).firstChild;
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = `${card.name}.png`;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };
  //====== Show QR function =====//
  async function showQrFn(card) {
    let opt = {
      text: "https://" + window.location.host + "/" + card.slug,
      width: 160,
      height: 160,
      logoMaxWidth: 40,
      logoMaxHeight: 40,
      drawer: "canvas",
      autoColor: true,
      colorDark: card.QRcolorDark ? card.QRcolorDark : "#000000",
      colorLight: card.QRcolorLight ? card.QRcolorLight : "#ffffff",
      correctLevel: QRCode.CorrectLevel.H,
      dotScaleTiming: 0.9,
      dotScale: 1,
      dotScaleTiming_V: 0.9,
      dotScaleTiming_h: 0.9,
      dotScaleA: 0.9,
      dotScaleAO: 0.9,
      dotScaleAI: 0.5,
      quietZone: 3,
      logo: card.qrLogo,
      logoWidth: card.qrlogoWidth,
      logoHeight: card.qrlogoWidth,
      logoBackgroundTransparent: card.qrlogoBackgroundTransparent,
      backgroundImage: card.qrbackgroundImage,
      backgroundImageAlpha: card.qrbackgroundImageAlpha,
    };
    if (card?.QRcolorDark) {
      opt.colorDark = card.QRcolorDark;
    }
    if (card?.QRcolorLight) {
      opt.colorLight = card.QRcolorLight;
    }

    document.getElementById(card._id).innerHTML = null;
    new QRCode(document.getElementById(card._id), opt);
  }
  if (status == "loading") {
    return (
      <>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }
  if (!session) window.location.href = "/signin";
  if (session && status != "loading")
    return (
      <>
        <Head>
          <title>{_("My QR Codes")}</title>
        </Head>

        <Admin>
          <div className="-mb-24 md:-m-1 md:-mb-20 h-24 bg-white">
            <div className="text-emerald-800 text-sm hidden lg:block pt-14 ltr:pl-16">
              {" "}
              {_("Create Mobile Background")}
            </div>
          </div>
          <div className="flex flex-wrap mt-24">
            <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-4">
              <div className="bg-white mt-4 shadow min-h-fit rounded">
                <div className="card-header border-b-2 p-6">
                  <h1 className="text-2xl">{_("QR Codes")}</h1>
                </div>
                <div className="card-body p-6 flex flex-wrap">
                  {cards.map((c, k) => {
                    return (
                      <div key={k} className="w-full md:w-4/12 lg:w-3/12 xl:w-2/12 border rounded-xl m-3 p-1">
                        <h1 className="text-center font-bold">{c.name}</h1>
                        <div
                          className={
                            "relative mx-auto mb-6 overflow-hidden qrCodeContainer border"
                          }
                          id={c._id}
                          onLoad={
                            setTimeout(()=>{
                              showQrFn(c)
                            },200)}
                          
                        ></div>
                        <div  className="text-center">
                          <button onClick={()=>{handleDownloadTest(event,c)}}>Download PNG</button>
                          <button onClick={()=>{downloadQRCode(c)}} className="rounded-xl p-4 font-bold text-white bg-emerald-300 ">{_('Download')}</button></div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </Admin>
      </>
    );
}
