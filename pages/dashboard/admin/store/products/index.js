import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import StoreHeader from "../../../../../components/Store/Header";
import Admin from "../../../../../layouts/Admin";
import { translation } from "../../../../../lib/translations";
const { Column, HeaderCell, Cell } = Table;
export default function Products() {
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const [products, setProducts] = useState([]);
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [loading, setLoading] = useState(false);
  const [updating,setUpdating]=useState(false);
const deleteProduct = (id)=>{
  if(confirm(_('Are sure you want to delete the porduct?')))
  {
    fetch('/api/dashboard/much-products/delete/'+id+'/').then(res=>res.json()).then(data=>{
      setUpdating(true)
    })
  }
}
  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    let arr = products.sort((a, b) => {
      if (sortColumn == "name")
        return sortType == "desc"
          ? a[sortColumn]["en"] < b[sortColumn]["en"]
            ? -1
            : 1
          : a[sortColumn]["en"] > b[sortColumn]["en"]
          ? -1
          : 1;
      else
        return sortType == "desc"
          ? a[sortColumn] < b[sortColumn]
            ? -1
            : 1
          : a[sortColumn] > b[sortColumn]
          ? -1
          : 1;
    });
    setProducts(arr);
    setSortColumn(sortColumn);
    setSortType(sortType);
    setLoading(false);
  };

  useEffect(() => {
    fetch("/api/dashboard/much-products")
      .then((res) => res.json())
      .then((data) => {
        setUpdating(false);
        console.log(data);
        setProducts(data);
      });
  }, [updating]);

  const NameCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <div>{_(rowData.name["en"])}</div>
    </Cell>
  );
  const ImageCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props} className="h-16">
      <div>
        {rowData.images.length > 0 ? (
          <img
            className="w-10 h-8 rounded border border-gray-600 mx-6"
            src={rowData.images[0].imgUrl}
          />
        ) : (
          <div className="w-10 h-8 rounded border border-gray-600 mx-6 pt-1">
            <i className="fa fa-image"></i>
          </div>
        )}
      </div>
    </Cell>
  );
  const PriceCell = ({ rowData, dataKey, ...props }) => (
    <Cell height={300} {...props} className="link-group">
      <div>
        {rowData.currency}
        {rowData.price != rowData.originalPrice ? (
          <span>
            (<span className="line-through">{rowData.originalPrice}</span>)
            <br />
            <span className="font-bold"> {rowData.price}</span>
          </span>
        ) : (
          <span>{rowData.price}</span>
        )}
      </div>
    </Cell>
  );
  const TypeCell = ({ rowData, dataKey, ...props }) => (
    <Cell height={300} {...props} className="link-group">
      <div>{rowData.type}</div>
    </Cell>
  );
  const LangCell = ({ rowData, dataKey, ...props }) => (
    <Cell height={300} {...props} className="link-group">
      <div>{rowData.languages ? rowData.languages.join(", ") : null}</div>
    </Cell>
  );
  const TagsCell = ({ rowData, dataKey, ...props }) => (
    <Cell height={300} {...props} className="link-group">
      <div>{rowData.tags ? rowData.tags.join(", ") : null}</div>
    </Cell>
  );
  const EditCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <Link href={"/dashboard/admin/store/products/edit/" + rowData._id}>
        <div className="bg-emerald-300 p-2 rounded-xl text-white duration-300 hover:text-white hover:no-underline hover:font-bold">
          {_("Edit")}
        </div>
      </Link>
    </Cell>
  );
  const DeleteCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      
        <button onClick={()=>deleteProduct(rowData._id)} className="bg-red-500 p-2  rounded-xl text-white duration-300 hover:text-white hover:no-underline hover:font-bold">
          {_("Delete")}
        </button>
      
    </Cell>
  );
  const DateCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <div>
        {rowData[dataKey]
          ? new Date(rowData[dataKey]).toLocaleDateString()
          : ""}
        <br />
        {rowData[dataKey]
          ? new Date(rowData[dataKey]).toLocaleTimeString()
          : ""}
      </div>
    </Cell>
  );
  return (
    <>
      <Head>
        <title>{_("Products")}</title>
      </Head>

      <Admin>
      <div className="container px-6">
        <StoreHeader />
        <div className="flex flex-wrap mt-4">
          <div className="w-full xl:w-12/12 mb-12 xl:mb-0">
            <div className="py-10  md:py-0 md:pb-12 mb-4">
              <Link href="/dashboard/admin/store/products/create">
                <div className="w-full md:w-fit md:rounded-md btn font-bold ltr:float-right">
                  {_("Create New")}
                </div>
              </Link>
            </div>
          </div>
          <div className="card-body w-full">
            <Table
              height="800"
              data={products}
              sortColumn={sortColumn}
              sortType={sortType}
              onSortColumn={handleSortColumn}
              loading={loading}
              rowHeight={80}
            >
              <Column align="center">
                <HeaderCell>{_("Image")}</HeaderCell>
                <ImageCell dataKey="images" />
              </Column>
              <Column width={150} align="center" sortable>
                <HeaderCell>{_("Name")}</HeaderCell>
                <NameCell dataKey="name" />
              </Column>
              <Column align="center" sortable>
                <HeaderCell>{_("Price")}</HeaderCell>
                <PriceCell dataKey="price" />
              </Column>
              <Column width={150} align="center" sortable>
                <HeaderCell>{_("Type")}</HeaderCell>
                <TypeCell dataKey="type" />
              </Column>
              <Column width={150} align="center" sortable>
                <HeaderCell>{_("Language")}</HeaderCell>
                <LangCell dataKey="language" />
              </Column>
              <Column width={300} align="center" sortable>
                <HeaderCell>{_("Tags")}</HeaderCell>
                <TagsCell dataKey="tags" />
              </Column>
              <Column width={300} align="center" sortable>
                <HeaderCell>{_("Create date")}</HeaderCell>
                <DateCell dataKey="created_at" />
              </Column>
              <Column width={300} align="center" sortable>
                <HeaderCell>{_("Updated date")}</HeaderCell>
                <DateCell dataKey="updated_at" />
              </Column>
              <Column fixed="right" width={60}>
                <HeaderCell>{_("")}</HeaderCell>
                <EditCell dataKey="_id" />
              </Column>
              <Column fixed="right">
                <HeaderCell></HeaderCell>
                <DeleteCell dataKey="_id" />
              </Column>
            </Table>
          </div>
        </div>
        </div>
      </Admin>
    </>
  );
}
