import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import Script from "next/script";
import { useEffect, useRef, useState } from "react";
import ReactModal from "react-modal";
import { Toggle, TagInput, Message, CheckPicker } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import SpinnerIcon from "@rsuite/icons/legacy/Spinner";
import Admin from "../../../../../layouts/Admin";
import { translation } from "../../../../../lib/translations";
function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export default function CreateProduct() {
  const languages = [
    { language: "en", isChecked: true },
    { language: "ar", isChecked: false },
  ];
  const countries = [
    { code: "ALL", name: "ALL", isChecked: false },
    { code: "SA", name: "Saudi Arabia", isChecked: false },
    { code: "TR", name: "Turkey", isChecked: false },
  ];
  const [cards, setCards] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const [name, setName] = useState({});
  const [price, setPrice] = useState();
  const [originalPrice, setOriginalPrice] = useState();
  const [currency, setCurrency] = useState("SAR");
  const [shortDescription, setShortDescription] = useState({});
  const [description, setDescription] = useState({});
  const [tags, setTags] = useState([]);
  const [extraFields, setExtraFields] = useState([]);
  const [cardFront, setCardFront] = useState();
  const [cardBack, setCardBack] = useState();
  const [tagImage, setTagImage] = useState();
  const [images, setImages] = useState([]);
  const [videos, setVideos] = useState([]);
  const [products, setProducts] = useState([]);
  const [productType, setProductType] = useState("Card");
  const [brandlessPrice, setbrandlessPrice] = useState(10);
  const [defaultTextColor, setDefaultTextColor] = useState("#FFFFFF");
  const [defaultQrColor, setDefaultQrColor] = useState("#FFFFFF");
  const [defaultQrDarkColor, setDefaultQrDarkColor] = useState("#000000");
  const [selectedProductsIds,setSelectedProductsIds]=useState([]);
  const [muchIcon, setMuchIcon] = useState("/assets/img/icon.svg");
  const [usedLanguages, setUsedLanguages] = useState(
    languages.map((time) => ({ ...time }))
  );
  const [usedCountries, setUsedCountries] = useState(
    countries.map((time) => ({ ...time }))
  );
  //====== get Product ====== //
  useEffect(()=>{updateData()},[])
  const updateData=()=>{
    setProducts([]);
    fetch("/api/dashboard/much-products/")
      .then((res) => res.json())
      .then(data => {
        setProducts([...data.map((e)=>({label:e.name[locale]?e.name[locale]:e.name['en'],value:e._id}))])
        console.log(products);
      });
  }
  const renderMenu = (menu) => {
    if (products.length === 0) {
      return (
        <p style={{ padding: 4, color: "#999", textAlign: "center" }}>
          <SpinnerIcon spin /> loading...
        </p>
      );
    }
    return menu;
  };

  const SubmitProduct = (e) => {
    let theForm = document.getElementById("productForm");
    if (theForm.checkValidity()) {
      setIsLoading(true);
      setHasError(false);
      e.preventDefault();
      fetch("/api/dashboard/much-products/create/", {
        method: "POST",
        body: JSON.stringify({
          type: productType,
          name: name,
          price: price,
          originalPrice: originalPrice,
          brandlessPrice: brandlessPrice,
          currency: currency,
          shortDescription: shortDescription,
          description: description,
          images: images,
          cardFront: cardFront,
          cardBack: cardBack,
          tagImage: tagImage,
          videos: videos,
          defaultTextColor: defaultTextColor,
          defaultQrColor: defaultQrColor,
          defaultQrDarkColor: defaultQrDarkColor,
          muchIcon: muchIcon,
          selectedProductsIds:selectedProductsIds,
          tags: tags,
          languages: usedLanguages
            .filter((e) => e.isChecked == true)
            .map((e) => e.language),
          extraFields: extraFields,
        }),
      }).then((res) => {
        console.log(res);
        if (res.ok) {
          setIsLoading(false);
          window.location.href = "/dashboard/admin/store/products";
        } else {
          setIsLoading(false);
          setHasError(true);
        }
      });
    }
  };
  const onChangeName = (e) => {
    const lang = e.target.name;
    const val = e.target.value;

    setName((prev) => {
      let tempName = { ...prev };
      tempName[lang] = val;
      return tempName;
    });
  };
  const onChangePrice = (e) => {
    const name = e.target.name;
    const val = e.target.value;
    if (name == "price") {
      setPrice(val);
    }
    if (name == "original_price") {
      setOriginalPrice(val);
    }
  };
  const priceBlur = (e) => {
    const name = e.target.name;
    const val = e.target.value;
    if (name == "price") {
      setPrice(val);
      if (!originalPrice) setOriginalPrice(val);
    }
    if (name == "original_price") {
      setOriginalPrice(val);
    }
  };
  const onChangeShortDescription = (e) => {
    const lang = e.target.name;
    const val = e.target.value;
    setShortDescription((prev) => {
      let tempSDesc = { ...prev };
      tempSDesc[lang] = val;
      return tempSDesc;
    });
  };
  const onChangeDescription = (e) => {
    const lang = e.target.name;
    const val = e.target.value;
    setDescription((prev) => {
      let tempDesc = { ...prev };
      tempDesc[lang] = val;
      return tempDesc;
    });
  };

  const onChangeCheckBox = (text, e) => {
    setUsedLanguages((prev) =>
      prev.map((ct) => {
        if (ct.language === text) ct.isChecked = e;
        if (!ct.isChecked) {
          let tempName = name;
          if (tempName[text]) {
            delete tempName[text];
          }
          setName(tempName);
          let tempSDesc = shortDescription;
          if (tempSDesc[text]) {
            delete tempSDesc[text];
          }
          setShortDescription(tempSDesc);
        }

        return ct;
      })
    );
  };
  const onChangeCountries = (text, e) => {
    setUsedCountries((prev) =>
      prev.map((ct) => {
        if (ct.code === text) ct.isChecked = e;
        return ct;
      })
    );
  };
  const newExtraField = () => {
    setExtraFields((prev) => {
      let tempExtraField = [...prev];

      tempExtraField.push({ id: makeid(5), name: {} });
      console.log(tempExtraField);
      return tempExtraField;
    });
  };

  const deleteExtraField = (id) => {
    setExtraFields((prev) => {
      let tempExtraField = [...prev];
      let newTemp = tempExtraField.filter((e) => e != id);
      return [...newTemp];
    });
  };
  const onChangeExtraField = (name, fieldIndex, lang, e) => {
    setExtraFields((prev) => {
      let tempExtraFields = [...prev];
      tempExtraFields.map((f, i) => {
        if (i == fieldIndex && lang) {
          if (typeof tempExtraFields[i][name] != "undefined") {
            tempExtraFields[i][name][lang] = e.target.value;
          } else {
            tempExtraFields[i][name] = {};
            tempExtraFields[i][name][lang] = e.target.value;
          }
        } else if (i == fieldIndex && !lang) {
          if (typeof tempExtraFields[i][name] != "undefined") {
            tempExtraFields[i][name] = e.target.value;
          } else {
            tempExtraFields[i][name] = {};
            tempExtraFields[i][name] = e.target.value;
          }
        }
      });

      return tempExtraFields;
    });
  };
  const addFiledOption = (id) => {
    setExtraFields((prev) => {
      let tempExtraFields = [...prev];
      if (tempExtraFields[id]["options"]) {
        tempExtraFields[id]["options"].push({ name: "" });
      } else {
        tempExtraFields[id]["options"] = [{ name: "" }];
      }
      console.log(tempExtraFields);
      return tempExtraFields;
    });
  };
  const deleteFieldOption = (field, option, oindex) => {
    setExtraFields((prev) => {
      let tempExtraFields = [...prev];
      tempExtraFields.forEach((e, k) => {
        if (e.id == field.id) {
          e.options.splice(oindex, 1);
        }
      });
      return tempExtraFields;
    });
  };
  const onChangeFieldOption = (name, fieldId, optionId, lang, optionValue) => {
    console.log(fieldId, optionId, lang, optionValue.target.value);
    setExtraFields((prev) => {
      let tempExtraFields = [...prev];
      tempExtraFields.forEach((e, k) => {
        if (e.id == fieldId && lang) {
          if (e.options[optionId][name])
            tempExtraFields[k]["options"][optionId][name][lang] =
              optionValue.target.value;
          else {
            tempExtraFields[k]["options"][optionId][name] = {};
            tempExtraFields[k]["options"][optionId][name][lang] =
              optionValue.target.value;
          }
        } else {
          tempExtraFields[k]["options"][optionId][name] =
            optionValue.target.value;
        }
      });
      return tempExtraFields;
    });
  };
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const openFrontWidget = (color) => {
    // create the widget

    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 1,
        multiple: false,
        cropping: true,
        croppingAspectRatio: 1.697,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 1003,
        maxImageHeight: 591,
        theme: "white",
        form: "coverPic",
        showPoweredBy: false,
        ocr: "adv_ocr",
      },

      (error, result) => {
        console.log(result);
        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let image = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            image +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          image += "/" + result.info.public_id;
          let theImage = { ...result.info };
          theImage.imgUrl = image;
          setCardFront(theImage);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };
  const openBackWidget = () => {
    // create the widget

    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 5,
        multiple: true,
        cropping: true,
        croppingAspectRatio: 1.697,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 1003,
        maxImageHeight: 591,
        theme: "white",
        form: "coverPic",
        showPoweredBy: false,
        ocr: "adv_ocr",
      },

      (error, result) => {
        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let bg = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            bg +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          bg += "/" + result.info.public_id;
          let theImage = { ...result.info };
          theImage.imgUrl = bg;
          setCardBack(theImage);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };
  const openTagWidget = () => {
    // create the widget

    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 5,
        multiple: true,
        cropping: true,
        croppingAspectRatio: 1,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 594,
        maxImageHeight: 594,
        theme: "white",
        form: "coverPic",
        showPoweredBy: false,
        ocr: "adv_ocr",
      },

      (error, result) => {
        console.log(result);

        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let bg = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            bg +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          bg += "/" + result.info.public_id;
          let theImage = { ...result.info };
          theImage.imgUrl = bg;
          setTagImage(theImage);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };
  const openImageWidget = () => {
    // create the widget

    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 5,
        multiple: true,
        cropping: true,
        croppingAspectRatio: 1.697,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 1003,
        maxImageHeight: 591,
        theme: "white",
        form: "coverPic",
        showPoweredBy: false,
        ocr: "adv_ocr",
      },

      (error, result) => {
        console.log(result);
        if (
          result.event === "success" &&
          result.info.resource_type === "image" &&
          result.info.format != "pdf"
        ) {
          let image = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            image +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          image += "/" + result.info.public_id;
          let theImage = { ...result.info };
          theImage.imgUrl = image;
          setImages((prev) => {
            let tempImageArray = [...prev];
            tempImageArray.push(theImage);
            return tempImageArray;
          });
        }
        if (
          result.event === "success" &&
          result.info.resource_type === "video"
        ) {
          let image = result.info.thumbnail_url.replace(
            "/c_limit,h_60,w_90",
            ""
          );
          let theImage = { ...result.info };
          theImage.imgUrl = image;
          setVideos((prev) => {
            let tempImageArray = [...prev];
            tempImageArray.push(theImage);
            return tempImageArray;
          });
        }
        if (
          result.event === "success" &&
          result.info.resource_type === "image" &&
          result.info.format === "pdf"
        ) {
          let image = result.info.thumbnail_url.replace(
            "/c_limit,h_60,w_90",
            ""
          );
          let theImage = { ...result.info };
          theImage.imgUrl = image;
          setImages((prev) => {
            let tempImageArray = [...prev];
            tempImageArray.push(theImage);
            return tempImageArray;
          });
        }
      }
    );
    widget.open(); // open up the widget after creation
  };
  return (
    <>
      <Head>
        <title>{_("Create Product")}</title>
      </Head>
      <Script
        src="https://widget.Cloudinary.com/v2.0/global/all.js"
        type="text/javascript"
      ></Script>

      <Admin>
        <div className="-mb-24 md:-m-1 md:-mb-20 h-24 bg-white">
          <div className="text-emerald-800 text-sm hidden lg:block pt-14 ltr:pl-16">
            {" "}
            {_("Create Product")}
          </div>
        </div>
        <div className="flex flex-wrap mt-24">
          <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-4">
            <div className="bg-white mt-4 shadow min-h-fit rounded-xl">
              <div className=" border-b-2 p-6">
                <h1 className="text-2xl">{_("Create Product")}</h1>
              </div>
              {hasError && (
                <Message className="font-bold" type="error">
                  <span className="text-red-500 font-bold">
                    <i className="fa fa-times w-10 text-center h-10 text-xl p-2 m-1 border-2 border-red-500 rounded-full "></i>
                    {_("Something went wrong!")}
                  </span>
                </Message>
              )}
              <div className="card-body p-6">
                <form id="productForm">
                  <div className="flex flex-wrap checkbox mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Product Type")}
                    </label>
                    <Toggle
                      className="mx-2"
                      size="lg"
                      checkedChildren="Card"
                      unCheckedChildren="Card"
                      checked={productType == "Card"}
                      onChange={(e) => {
                        if (e) {
                          setProductType("Card");
                        }
                      }}
                    />
                    <Toggle
                      className="mx-2"
                      size="lg"
                      checkedChildren="Tag"
                      unCheckedChildren="Tag"
                      checked={productType == "Tag"}
                      onChange={(e) => {
                        if (e) {
                          setProductType("Tag");
                        }
                      }}
                    />
                    <Toggle
                      className="mx-2"
                      size="lg"
                      checkedChildren="Product"
                      unCheckedChildren="Product"
                      checked={productType == "Product"}
                      onChange={(e) => {
                        if (e) {
                          setProductType("Product");
                        }
                      }}
                    />
                  </div>
                  {productType == "Card" && (
                    <div>
                      <div className="flex flex-wrap mb-4">
                        <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                          {_("Card Front")}
                        </label>
                        <div className="w-full md:w-10/12 flex flex-wrap">
                          {cardFront && (
                            <div className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-left overflow-hidden cursor-pointer">
                              <button
                                type="button"
                                onClick={() => {
                                  setCardFront(null);
                                }}
                                className="bg-white text-sm text-red-500 border border-gray-700 w-6 h-6  rounded-full absolute"
                              >
                                <i className="fa fa-times"></i>
                              </button>
                              {cardFront && (
                                <img
                                  className="w-full"
                                  onClick={() => {
                                    openFrontWidget();
                                  }}
                                  src={cardFront.imgUrl}
                                />
                              )}
                            </div>
                          )}

                          {!cardFront && (
                            <div
                              className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-center overflow-hidden cursor-pointer"
                              onClick={() => {
                                openFrontWidget();
                              }}
                            >
                              {!cardFront && (
                                <i className="far pt-6 fa-images text-6xl"></i>
                              )}
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="flex flex-wrap mb-4">
                        <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                          {_("Card Back")}
                        </label>
                        <div className="w-full md:w-10/12 flex flex-wrap">
                          {cardBack && (
                            <div className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-left overflow-hidden cursor-pointer">
                              <button
                                type="button"
                                onClick={() => {
                                  setCardBack(null);
                                }}
                                className="bg-white text-sm text-red-500 border border-gray-700 w-6 h-6  rounded-full absolute"
                              >
                                <i className="fa fa-times"></i>
                              </button>
                              {cardBack && (
                                <img
                                  className="w-full"
                                  onClick={() => {
                                    openBackWidget();
                                  }}
                                  src={cardBack.imgUrl}
                                />
                              )}
                            </div>
                          )}

                          {!cardBack && (
                            <div
                              className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-center overflow-hidden cursor-pointer"
                              onClick={() => {
                                openBackWidget();
                              }}
                            >
                              {!cardBack && (
                                <i className="far pt-6 fa-images text-6xl"></i>
                              )}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  )}
                  {productType == "Tag" && (
                    <div className="flex flex-wrap mb-4">
                      <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                        {_("Tag Images")}
                      </label>
                      <div className="w-full md:w-10/12 flex flex-wrap">
                        {tagImage && (
                          <div className="w-48 h-48 m-2 rounded-xl border border-gray-500 text-left overflow-hidden cursor-pointer">
                            <button
                              type="button"
                              onClick={() => {
                                setTagImage(null);
                              }}
                              className="bg-white text-sm text-red-500 border border-gray-700 w-6 h-6  rounded-full absolute"
                            >
                              <i className="fa fa-times"></i>
                            </button>
                            {tagImage && (
                              <img
                                className="w-full"
                                onClick={() => {
                                  openTagWidget();
                                }}
                                src={tagImage.imgUrl}
                              />
                            )}
                          </div>
                        )}

                        {!tagImage && (
                          <div
                            className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-center overflow-hidden cursor-pointer"
                            onClick={() => {
                              openTagWidget();
                            }}
                          >
                            {!tagImage && (
                              <i className="far pt-6 fa-images text-6xl"></i>
                            )}
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {productType == "Product" && (
                    <div className="flex flex-wrap mb-4">
                      <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                        {_("Product Images")}
                        <br />
                        <small className="text-muted">
                          {_("5 Images maximum")}
                        </small>
                      </label>
                      <div className="w-full md:w-10/12 flex flex-wrap">
                        {images.map((img, k) => {
                          return (
                            <div
                              key={k}
                              className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-left overflow-hidden cursor-pointer"
                            >
                              <button
                                type="button"
                                onClick={() => {
                                  setImages((prev) => {
                                    let tempImages = [...prev];
                                    tempImages.splice(k, 1);
                                    return tempImages;
                                  });
                                }}
                                className="bg-white text-sm text-red-500 border border-gray-700 w-6 h-6  rounded-full absolute"
                              >
                                <i className="fa fa-times"></i>
                              </button>
                              {img && (
                                <img
                                  className="w-full"
                                  onClick={() => {
                                    openImageWidget();
                                  }}
                                  src={img.imgUrl}
                                />
                              )}
                            </div>
                          );
                        })}
                        {videos.map((video, k) => {
                          return (
                            <div
                              key={k}
                              className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-left overflow-hidden cursor-pointer"
                            >
                              <button
                                style={{ zIndex: "1000" }}
                                type="button"
                                onClick={() => {
                                  setVideos((prev) => {
                                    let tempImages = [...prev];
                                    tempImages.splice(k, 1);
                                    return tempImages;
                                  });
                                }}
                                className="bg-white text-sm text-red-500 border border-gray-700 w-6 h-6  rounded-full absolute"
                              >
                                <i className="fa fa-times"></i>
                              </button>
                              <video
                                loop
                                autoPlay
                                muted
                                preload="auto"
                                style={{ width: "100%" }}
                              >
                                <source src={video.url}></source>
                              </video>
                            </div>
                          );
                        })}
                        {(!images.length || images.length < 5) && (
                          <div
                            className="w-48 h-28 m-2 rounded-xl border border-gray-500 text-center overflow-hidden cursor-pointer"
                            onClick={() => {
                              openImageWidget();
                            }}
                          >
                            {images.length == 0 && (
                              <i className="far pt-6 fa-images text-6xl"></i>
                            )}
                            {images.length > 0 && (
                              <i className="far pt-6 fa-plus text-6xl"></i>
                            )}
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {productType == "Card" && (
                    <div>
                      <div className="flex flex-wrap mb-4">
                        <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                          {_("Default Text Color")}
                        </label>
                        <div className="w-full md:w-10/12">
                          <input
                            className="form-control"
                            value={defaultTextColor}
                            type="color"
                            onChange={(e) =>
                              setDefaultTextColor(e.target.value)
                            }
                          />
                        </div>
                      </div>
                      <div className="flex flex-wrap mb-4">
                        <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                          {_("Default QR Code Color")}
                        </label>
                        <div className="w-full md:w-5/12">
                          <input
                            className="form-control"
                            value={defaultQrColor}
                            type="color"
                            onChange={(e) => setDefaultQrColor(e.target.value)}
                          />
                        </div>
                        <div className="w-full md:w-5/12">
                          <input
                            className="form-control"
                            value={defaultQrDarkColor}
                            type="color"
                            onChange={(e) =>
                              setDefaultQrDarkColor(e.target.value)
                            }
                          />
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Related products")}
                    </label>
                    <CheckPicker
                    placement="bottomStart"
                    value={selectedProductsIds}
                      data={products}
                      
                      style={{ width: 248 }}
                      onChange={(e) => {
                        setSelectedProductsIds(e);}}
                        renderMenu={renderMenu} 
                    />
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("much icon")}
                    </label>
                    <div className="w-full md:w-10/12 flex flex-wrap">
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon.svg");
                        }}
                        src="/assets/img/icon.svg"
                      />
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon-1.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon-1.svg");
                        }}
                        src="/assets/img/icon-1.svg"
                      />
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon-2.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon-2.svg");
                        }}
                        src="/assets/img/icon-2.svg"
                      />
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon-3.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon-3.svg");
                        }}
                        src="/assets/img/icon-3.svg"
                      />
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon-4.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon-4.svg");
                        }}
                        src="/assets/img/icon-4.svg"
                      />
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon-5.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon-5.svg");
                        }}
                        src="/assets/img/icon-5.svg"
                      />
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon-6.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon-6.svg");
                        }}
                        src="/assets/img/icon-6.svg"
                      />
                      <img
                        className="w-12 m-2 rounded-lg duration-300"
                        style={{
                          borderWidth:
                            muchIcon == "/assets/img/icon-7.svg" ? "3px" : "",
                          borderColor: "#333",
                          borderStyle: "solid",
                        }}
                        onClick={() => {
                          setMuchIcon("/assets/img/icon-7.svg");
                        }}
                        src="/assets/img/icon-7.svg"
                      />
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Available in Languages")}
                    </label>
                    {usedLanguages.map((e, k) => {
                      return (
                        <Toggle
                          readOnly={e.language == "en"}
                          key={k}
                          checked={e.isChecked}
                          className="mx-2"
                          size="lg"
                          checkedChildren={e.language}
                          unCheckedChildren={e.language}
                          onChange={(val) => {
                            onChangeCheckBox(e.language, val);
                          }}
                        />
                      );
                    })}
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Available in Countries")}
                    </label>
                    {usedCountries.map((e, k) => {
                      return (
                        <Toggle
                          key={k}
                          checked={e.isChecked}
                          className="mx-2"
                          size="lg"
                          checkedChildren={e.name}
                          unCheckedChildren={e.name}
                          onChange={(val) => {
                            onChangeCountries(e.code, val);
                          }}
                        />
                      );
                    })}
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Name")}
                    </label>
                    <div className="flex flex-wrap w-full md:w-10/12">
                      {usedLanguages.map((e, k) => {
                        return (
                          <>
                            {e.isChecked && (
                              <input
                                key={k}
                                name={e.language}
                                value={name[e?.language]}
                                placeholder={"Name [" + e.language + "]"}
                                className="form-control md:w-6/12 my-1"
                                onChange={onChangeName}
                                required
                              />
                            )}
                          </>
                        );
                      })}
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Price")}
                    </label>
                    <div className="flex flex-wrap md:w-10/12">
                      <select
                        value={currency}
                        className="form-control md:w-2/12 h-9 bg-gray-100 font-semibold pt-1 mt-1 max-w-fit mx-1"
                        onChange={(e) => {
                          setCurrency(e.target.value);
                        }}
                      >
                        <option value="AED">{_("AED")}</option>
                        <option value="BHD">{_("BHD")}</option>
                        <option value="EGP">{_("EGP")}</option>
                        <option value="EUR">{_("EUR")}</option>
                        <option value="GBP">{_("GBP")}</option>
                        <option value="KWD">{_("KWD")}</option>
                        <option value="OMR">{_("OMR")}</option>
                        <option value="QAR">{_("QAR")}</option>
                        <option value="SAR">{_("SAR")}</option>
                        <option value="USD">{_("USD")}</option>
                      </select>
                      <input
                        value={price}
                        type="number"
                        step="0.1"
                        min="0"
                        name="price"
                        placeholder={_("Price")}
                        className="form-control md:w-5/12 my-1"
                        onChange={onChangePrice}
                        onBlur={priceBlur}
                        required
                      />
                      <input
                        value={originalPrice}
                        type="number"
                        step="0.1"
                        min="0"
                        name="original_price"
                        placeholder={_("Original Price")}
                        className="form-control md:w-5/12 my-1"
                        onChange={onChangePrice}
                        onBlur={priceBlur}
                      />
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12">
                      {_("Remove much logo")}
                    </label>
                    <div className="w-full md:w-10/12 flex">
                      <label className="form-control w-2/12 h-9 bg-gray-100 font-semibold pt-1 mt-1 max-w-fit mx-2">
                        {currency}
                      </label>
                      <input
                        type="number"
                        className="form-control w-10/12"
                        value={brandlessPrice}
                        onChange={(e) => {
                          setbrandlessPrice(e.target.value);
                        }}
                      />
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Short Description")}
                    </label>
                    <div className="flex flex-wrap w-full md:w-10/12">
                      {usedLanguages.map((e, k) => {
                        return (
                          <>
                            {e.isChecked && (
                              <textarea
                                key={k}
                                name={e.language}
                                value={shortDescription[e?.language]}
                                placeholder={
                                  "Short Description [" + e.language + "]"
                                }
                                className="form-control my-1 w-full md:w-6/12"
                                onChange={onChangeShortDescription}
                              />
                            )}
                          </>
                        );
                      })}
                    </div>
                  </div>
                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Description")}
                    </label>
                    <div className="flex flex-wrap w-full md:w-10/12">
                      {usedLanguages.map((e, k) => {
                        return (
                          <>
                            {e.isChecked && (
                              <textarea
                                key={k}
                                name={e.language}
                                value={description[e?.language]}
                                placeholder={"Description [" + e.language + "]"}
                                className="form-control w-full md:w-6/12 my-1"
                                onChange={onChangeDescription}
                              />
                            )}
                          </>
                        );
                      })}
                    </div>
                  </div>

                  <div className="flex flex-wrap mb-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Tags")}
                    </label>
                    <div className="w-full md:w-10/12">
                      <TagInput
                        value={tags}
                        className="w-full"
                        onChange={(e) => {
                          setTags(e);
                        }}
                      />
                    </div>
                  </div>
                  
                  <div className="flex flex-wrap mb-4">
                    <div className="w-full">
                      <button
                        type="button"
                        onClick={() => {
                          newExtraField();
                        }}
                        className="bg-emerald-300 text-gray-800 p-2 rounded-xl btn-sm text-sm font-bold uppercase"
                      >
                        {_("New Extra Field")} <i className="fa fa-plus"></i>
                      </button>
                      <div className="w-full">
                        {extraFields.map((field, findex) => {
                          return (
                            <div className="flex" key={findex}>
                              <div className="w-1/12 text-gray-200 text-6xl border-l-2 rounded-lg my-2 p-1">
                                <button
                                  type="button"
                                  className="absolute text-gray-500 border rounded-full text-xs w-5 h-5"
                                  onClick={() => {
                                    deleteExtraField(field);
                                  }}
                                >
                                  <i className="fa fa-times"></i>
                                </button>
                                <div className="pt-3">{findex + 1}</div>
                              </div>
                              <div className="w-full md:w-11/12">
                                <div className="w-full flex flex-wrap my-2">
                                  <label className="uppercase font-bold text-gray-600 w-full md:w-2/12">
                                    {_("Name")}
                                  </label>
                                  <div className="w-full md:w-10/12 flex">
                                    {usedLanguages.map((lang, eindex) => {
                                      return (
                                        lang.isChecked && (
                                          <input
                                            key={eindex}
                                            value={field.name[lang.language]}
                                            className="form-control"
                                            placeholder={
                                              "Name [" + lang.language + "]"
                                            }
                                            onChange={(e) => {
                                              onChangeExtraField(
                                                "name",
                                                findex,
                                                lang.language,
                                                e
                                              );
                                            }}
                                            required
                                          />
                                        )
                                      );
                                    })}
                                  </div>
                                </div>
                                <div className="w-full flex flex-wrap mb-4">
                                  <label className="uppercase font-bold text-gray-600 w-full md:w-2/12">
                                    {_("Type")}
                                  </label>
                                  <div className="w-full md:w-10/12 flex">
                                    <select
                                      value={field.type}
                                      className="form-control"
                                      placeholder={"Type"}
                                      onChange={(e) => {
                                        onChangeExtraField(
                                          "type",
                                          findex,
                                          null,
                                          e
                                        );
                                      }}
                                      required
                                    >
                                      <option value="">
                                        {_("Select One")}
                                      </option>
                                      <option value="radio">
                                        {_("One choice")}
                                      </option>
                                      <option value="checkbox">
                                        {_("multiple choice")}
                                      </option>
                                    </select>
                                  </div>
                                </div>
                                <div className="w-full flex flex-wrap">
                                  <div className="w-full md:w-2/12 px-1">
                                    <button
                                      type="button"
                                      className="px-2 py-1 border rounded-xl text-xs flex"
                                      onClick={() => {
                                        addFiledOption(findex);
                                      }}
                                    >
                                      {_("New Option")}{" "}
                                      <i className="fa fa-plus p-1"></i>
                                    </button>
                                  </div>
                                  <div className="w-full md:w-10/12 flex flex-wrap">
                                    {field.options &&
                                      field.options.map((option, oindex) => {
                                        return (
                                          <div
                                            className="w-full flex flex-wrap p-1 border rounded-xl"
                                            key={oindex}
                                          >
                                            <div className="w-1/12 text-2xl text-gray-200 p-1">
                                              <button
                                                type="button"
                                                className="absolute text-gray-500 border rounded-full text-xs w-5 h-5"
                                                onClick={() => {
                                                  deleteFieldOption(
                                                    field,
                                                    option,
                                                    oindex
                                                  );
                                                }}
                                              >
                                                <i className="fa fa-times"></i>
                                              </button>
                                              <div className="pt-3">
                                                {oindex + 1}
                                              </div>
                                            </div>
                                            <div className="w-11/12 flex flex-wrap">
                                              <label className="font-bold">
                                                {_("Option Name")}
                                              </label>
                                              <div className="border rounded-xl w-full p-1">
                                                {usedLanguages.map((l, li) => {
                                                  return (
                                                    l.isChecked && (
                                                      <div
                                                        className="w-full"
                                                        key={li}
                                                      >
                                                        <input
                                                          value={
                                                            option["name"][
                                                              l.language
                                                            ]
                                                          }
                                                          onChange={(e) => {
                                                            onChangeFieldOption(
                                                              "name",
                                                              field.id,
                                                              oindex,
                                                              l.language,
                                                              e
                                                            );
                                                          }}
                                                          className="form-control"
                                                          placeholder={
                                                            "Option Name [" +
                                                            l.language +
                                                            "]"
                                                          }
                                                          required
                                                        />
                                                      </div>
                                                    )
                                                  );
                                                })}
                                                <div className="w-full">
                                                  <label className="font-bold">
                                                    {_("Price")}
                                                  </label>
                                                  <div className="w-full">
                                                    <input
                                                      value={option["price"]}
                                                      type="number"
                                                      className="form-control"
                                                      step="0.1"
                                                      placeholder="price"
                                                      onChange={(e) => {
                                                        onChangeFieldOption(
                                                          "price",
                                                          field.id,
                                                          oindex,
                                                          null,
                                                          e
                                                        );
                                                      }}
                                                    />
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        );
                                      })}
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                  <div className="py-8 pb-16">
                    <div className=" ltr:float-right rtl:float-left">
                      <button
                        type="submit"
                        className="btn w-48"
                        onClick={SubmitProduct}
                        disabled={isLoading}
                      >
                        {isLoading && (
                          <i className="animate-spin fa fa-spinner"></i>
                        )}
                        {!isLoading && <span>{_("Save")}</span>}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Admin>
    </>
  );
}
