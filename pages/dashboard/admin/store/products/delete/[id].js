export default function DeleteConfirm()
{
    return <>
<div className="w-full h-full  pt-48">
<div className="mx-auto max-w-xl bg-gray-400 p-6 rounded-xl">
    <h1 className="text-4xl">{('Are you sure?')}</h1>

</div>
</div>
    </>
}
