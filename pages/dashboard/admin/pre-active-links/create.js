import Head from "next/head";
import Admin from "../../../../layouts/Admin";
import { translation } from "../../../../lib/translations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Dropdown, Table } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import Link from "next/link";
import Image from "next/image";
const { Column, HeaderCell, Cell } = Table;

export default function FieldTypeIndex() {
  const { locale } = useRouter();
  const router =useRouter();
  const [link, setLink] = useState();
  const [pin, setPin] = useState();
  const [product, setProduct] = useState();
  const [products, setProducts] = useState([]);
  const [checking, setChecking] = useState(false);
  const [productSeleted, setProductSelected] = useState(false);
  const [canPost,setCanPost]=useState(false);
  const [linkAvalible,setLinkAvalible]=useState(false);
  const [productMissing,setProductMissing]=useState(false);
  const [isCreateing,setIsCreating]=useState(false);
  //======= Translation =========//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  //==== Random Pin ======//
  function makePin(length) {
    var result = "";
    var characters = "1234567890";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random(new Date().getTime()) * charactersLength)
      );
    }
    return result;
  }
  //==== Random slug =====//
  function makeid(length) {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random(new Date().getTime()) * charactersLength)
      );
    }
    return result;
  }
  //====== CheckLink ======//
  async function generateLink() {
    setChecking(true);
    let slug = makeid(12);
    const check = await fetch("/api/cards/check-name/", {
      method: "POST",
      body: JSON.stringify({ name: slug }),
    });
    const res = await check.json();
    setChecking(false);
    if (res.status) {
      setLink(slug);
      checkLink();
    } else {
      generateLink();
    }
  }
  //======= Check Link =========//
  async function checkLink()
  {
    setCanPost(false)
    setLinkAvalible(false)
    let slug = link;
    const check = await fetch("/api/cards/check-name/", {
        method: "POST",
        body: JSON.stringify({ name: slug }),
      });
      const res = await check.json();
      if(res.status)
      {
        setCanPost(true);
        setLinkAvalible(false)
      }
      else
      {
        setCanPost(false);
        setLinkAvalible(true)

      }
    
  }
  //======= get Products =======//
  useEffect(() => {
    fetch("/api/much-products/", {
      method: "post",
      body: JSON.stringify({ locale: locale }),
    })
      .then((res) => res.json())
      .then((data) => setProducts(data));
  }, []);

  //===== Submit From ==========
  function submitForm(e)
  {
    let theForm = document.getElementById("linkForm");
    if (theForm.checkValidity())
    {
        e.preventDefault();
        if(!product)
        {
            setProductMissing(true)
            setProductSelected(true);
        }
        else
        {
            setIsCreating(true)
            fetch('/api/pre-active/create/',{method:"POST",body:JSON.stringify({
                link:link,
                pin:pin,
                product:product
            })}).then(res=>res.json()).then(data=>{
                setIsCreating(false);
                router.replace('/dashboard/admin/pre-active-links')
            })
        }

    }
  }
  return (
    <>
      <Head>
        <title>{_("Create Pre-active Links")}</title>
      </Head>

      <Admin>
        <div className="container px-6">
          <div className="hidden lg:block lg:-m-1   bg-white">
            <div className="text-emerald-800 text-sm hidden lg:block p-4">
              {" "}
              {_("Create Pre-active Links")}
            </div>
          </div>
          <div className="mt-4 bg-white">
            <form id="linkForm">
            <div className="flex flex-wrap mt-2 p-2">
              <div className="w-full md:w-2/12 px-2 text-center">
                <label className="uppercase  text-gray-500 mt-2">
                  {_("Link")}
                </label>
              </div>
              <div className="w-full md:w-8/12 px-2">
                <input onBlur={()=>{checkLink()}} onChange={(e)=>{setLink(e.target.value)}} value={link} className={!linkAvalible?"form-control my-1":"form-control my-1 text-red-500"} required/>
                {linkAvalible &&<span className="text-red-500">{_('Link already exists')}</span>}
              </div>
              <div className="w-full md:w-2/12 px-2">
                <button
                type="button"
                  onClick={() => {
                    generateLink();
                  }}
                  className="bg-emerald-300 text-gray-500 w-full p-2 my-1  rounded"
                >
                  {checking && <i className="animate-spin fa fa-spinner"></i>}{" "}
                  {_("Generate")}
                </button>
              </div>
            </div>
            <div className="flex flex-wrap mt-2 p-2">
              <div className="w-full md:w-2/12 px-2 text-center">
                <label className="uppercase  text-gray-500 mt-2">
                  {_("PIN")}
                </label>
              </div>
              <div className="w-full md:w-8/12 px-2">
                <input
                onChange={(e)=>{setPin(e.target.value)}}
                  value={pin}
                  type="number"
                  className="form-control my-1"
                  required
                />
              </div>
              <div className="w-full md:w-2/12 px-2">
                <button
                type="button"
                  onClick={() => {
                    setPin(makePin(4));
                  }}
                  className="bg-emerald-300 text-gray-500 w-full p-2 my-1  rounded"
                >
                  {" "}
                  {_("Generate")}
                </button>
              </div>
            </div>
            <div className="flex flex-wrap mt-2 p-2">
              <div className="w-full md:w-2/12 px-2 text-center">
                <label className="uppercase  text-gray-500 mt-2">
                  {_("Product")}
                </label>
              </div>
              <div className="w-full md:w-10/12 px-2">
                <Dropdown
                  open={productSeleted}
                  onClick={() => {
                    setProductSelected(true);
                  }}
                  title={
                    product ? (
                      <span className="flex" >
                        {product.cardFront && (
                          <img className="w-10" src={product.cardBack.imgUrl} />
                        )}
                        {product.images && product.images.length && (
                          <img
                            className="w-10"
                            src={product.images[0].imgUrl}
                          />
                        )}
                        {product.name[locale]}
                      </span>
                    ) : (
                        <span className={productMissing?'flex text-red-500 font-bold':'flex'}>
                      {_("Product")}
                      </span>
                    )
                  }
                  
                >
                  {products.map((e) => {
                    return (
                      <Dropdown.Item key={e._id}
                        className="overflow-hidden"
                        onClick={() => {
                          setProduct(e);
                          setProductSelected(false);
                        }}
                      >
                        <div className="flex">
                          {e.cardFront && (
                            <img className="w-10" src={e.cardBack.imgUrl} />
                          )}
                          {e.images && e.images.length && (
                            <img className="w-10" src={e.images[0].imgUrl} />
                          )}
                          {e.name[locale]}
                        </div>
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown>
              </div>
              <div className="w-full p-2">
                <div className="float-right">
                    <button disabled={!canPost || isCreateing} onClick={(e)=>{submitForm(e)}} className="bg-emerald-300 font-bold p-2 rounded text-gray-700 disabled:bg-gray-300 px-4">
                        {isCreateing &&<i className="fa fa-spinner animate-spin"></i>}
                        {_('Create')}</button>
                </div>
              </div>
            </div>
            </form>
          </div>
        </div>
      </Admin>
    </>
  );
}
