import Head from "next/head";
import Admin from "../../../../layouts/Admin";
import { translation } from "../../../../lib/translations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import Link from "next/link";
import Image from "next/image";
const { Column, HeaderCell, Cell } = Table;

export default function FieldTypeIndex() {
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [loading, setLoading] = useState(false);
  const [links, setLinks] = useState([]);
  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    let arr = links.sort((a, b) => {
      return sortType == "desc"
        ? a[sortColumn] < b[sortColumn]
          ? -1
          : 1
        : a[sortColumn] > b[sortColumn]
        ? -1
        : 1;
    });
    setLinks(arr);
    setSortColumn(sortColumn);
    
    setLoading(false);
  };
  useEffect(() => {
    fetch("/api/pre-active/")
      .then((res) => res.json())
      .then((data) => {
        setLinks(data);
      });
  }, []);
  
  
  
  
  
  const EditCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <Link href={"/dashboard/admin/pre-active-links/edit/" + rowData._id}>
        <div className="bg-emerald-300 p-2 rounded-xl text-white duration-300 hover:text-white hover:no-underline hover:font-bold">
          {_("Edit")}
        </div>
      </Link>
    </Cell>
  );
  const ProductCell = ({ rowData, dataKey, ...props }) => {
    return (
      <Cell {...props}>
        <div className="flex">
        {rowData.product && rowData.product.cardFront && (
          <img className="w-10" src={rowData.product.cardFront.imgUrl} />
        )}
        {rowData.product &&
          rowData.product.images &&
          rowData.product.images.length && (
            <img className="w-10" src={rowData.product.images[0].imgUrl} />
          )}
        {rowData.product.name[locale]?rowData.product.name[locale]:rowData.product.name['en']}
        </div>
      </Cell>
    );
  };
  const DateCell = ({ rowData, dataKey, ...props })=>{
    if(rowData[dataKey])
    return <Cell {...props}>{(new Date(rowData[dataKey]).getFullYear()+'-'+(new Date(rowData[dataKey]).getMonth()*1+1)+'-'+new Date(rowData[dataKey]).getDate())}</Cell>
    else
    return '';
    }
  return (
    <>
      <Head>
        <title>{_("Pre-active Links")}</title>
      </Head>

      <Admin>
        <div className="container px-6">
          <div className="hidden lg:block lg:-m-1   bg-white">
            <div className="text-emerald-800 text-sm hidden lg:block p-4">
              {" "}
              {_("Pre-active Links")}
            </div>
          </div>
          <div className="flex flex-wrap mt-4">
            <div className="w-full xl:w-12/12 mb-12 xl:mb-0">
              <div className="py-10  md:py-0 md:pb-12">
                <Link href="/dashboard/admin/pre-active-links/create">
                  <div className="w-full md:w-fit md:rounded-md btn ltr:float-right">
                    {_("Create New")}
                  </div>
                </Link>
              </div>
              <div className="bg-white mt-4 shadow min-h-fit rounded">
                <div className="card-body p-6">
                  <Table
                    height={800}
                    rowHeight={60}
                    data={links}
                    sortColumn={sortColumn}
                    sortType={sortType}
                    onSortColumn={handleSortColumn}
                    loading={loading}
                  >
                    <Column width={350}  align="center">
                      <HeaderCell>{_("Product")}</HeaderCell>
                      <ProductCell dataKey="product" />
                    </Column>
                    <Column width={130} align="center" >
                      <HeaderCell>{_("Link")}</HeaderCell>
                      <Cell dataKey="link" />
                    </Column>

                    <Column width={130} sortable>
                      <HeaderCell>{_("Created At")}</HeaderCell>
                      <DateCell dataKey="created_at" />
                    </Column>
                    <Column width={130} sortable>
                      <HeaderCell>{_("Charged At")}</HeaderCell>
                      <DateCell dataKey="charged_at" />
                    </Column>
                    <Column width={130}>
                      <HeaderCell>{_("Activated At")}</HeaderCell>
                      <DateCell dataKey="activated_at" />
                    </Column>

                    <Column fixed="right" width={60}>
                      <HeaderCell>{_("")}</HeaderCell>
                      <EditCell dataKey="_id" />
                    </Column>
                  </Table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Admin>
    </>
  );
}
