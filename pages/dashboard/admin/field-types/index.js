import Head from "next/head";
import Admin from "../../../../layouts/Admin";
import { translation } from "../../../../lib/translations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Table } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import Link from "next/link";
const { Column, HeaderCell, Cell } = Table;

export default function FieldTypeIndex() {
  const { locale } = useRouter();
  function _(text) {
    
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [loading, setLoading] = useState(false);
  const [fieldTypes, setFieldTypes] = useState([]);
  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    let arr = fieldTypes.sort((a, b) => {
      
      return sortType == "desc"
        ? a[sortColumn] < b[sortColumn]
          ? -1
          : 1
        : a[sortColumn] > b[sortColumn]
        ? -1
        : 1;
    });
    setFieldTypes(arr);
    setSortColumn(sortColumn);
    setSortType(sortType);
    setLoading(false);
  };
  useEffect(() => {
    fetch("/api/field-type/")
      .then((res) => res.json())
      .then((data) => {
        setFieldTypes(data);
      });
  }, []);
  const IconCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      
        <i className={rowData.icon ? rowData.icon+" px-2 text-center text-xs py-2.5 border rounded-xl w-8 h-8" : "px-2 text-xs py-2 border rounded-xl w-8 h-8"}></i>
      
    </Cell>
  );
  const CardTypeCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <div>{_(rowData.cardType)}</div>
    </Cell>
  );
  const SectionCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <div>{_(rowData.section)}</div>
    </Cell>
  );
  const TypeCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <div>{_(rowData.fieldType)}</div>
    </Cell>
  );
  const CheckCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <div className="px-3 text-xs py-2 border rounded-xl w-8 h-8">
        <i
          className={
            dataKey == "withLabel"
              ? rowData.withLabel
                ? "fa fa-check"
                : "fa fa-times"
              : dataKey == "repeatable"
              ? rowData.repeatable
                ? "fa fa-check"
                : "fa fa-times"
              : dataKey == "withImage"
              ? rowData.withImage
                ? "fa fa-check"
                : "fa fa-times"
              : dataKey == "published"
              ? rowData.published
                ? "fa fa-check"
                : "fa fa-times"
              : ""
          }
        ></i>
      </div>
    </Cell>
  );
  const EditCell = ({ rowData, dataKey, ...props }) => (
    <Cell {...props}>
      <Link href={"/dashboard/admin/field-types/edit/" + rowData._id}>
        <div className="bg-emerald-300 p-2 rounded-xl text-white duration-300 hover:text-white hover:no-underline hover:font-bold">
          {_("Edit")}
        </div>
      </Link>
    </Cell>
  );
  return (
    <>
      <Head>
        <title>{_("Field Types")}</title>
      </Head>

      <Admin>
      <div className="container px-6">
        <div className="hidden lg:block lg:-m-1   bg-white">
          <div className="text-emerald-800 text-sm hidden lg:block p-4">
            {" "}
            {_("Field Types")}
          </div>
        </div>
        <div className="flex flex-wrap mt-4">
          <div className="w-full xl:w-12/12 mb-12 xl:mb-0">
            <div className="py-10  md:py-0 md:pb-12">
              <Link href="/dashboard/admin/field-types/create">
              <div
                
                className="w-full md:w-fit md:rounded-md btn ltr:float-right"
              >
                {_("Create New")}
              </div>
              </Link>
            </div>
            <div className="bg-white mt-4 shadow min-h-fit rounded">
              <div className="card-header border-b-2 p-6">
                <h1 className="text-2xl">{_("Field Types")}</h1>
              </div>
              <div className="card-body p-6">
                <Table
                  height={420}
                  data={fieldTypes}
                  sortColumn={sortColumn}
                  sortType={sortType}
                  onSortColumn={handleSortColumn}
                  loading={loading}
                >
                  <Column width={130} align="center"  sortable>
                    <HeaderCell>{_("Card Type")}</HeaderCell>
                    <CardTypeCell dataKey="cardType" />
                  </Column>
                  <Column width={170} align="center"  sortable>
                    <HeaderCell>{_("Section")}</HeaderCell>
                    <SectionCell dataKey="section" />
                  </Column>
                  <Column width={130}  sortable>
                    <HeaderCell>{_("Name")}</HeaderCell>
                    <Cell dataKey="name" />
                  </Column>
                  <Column width={130}  sortable>
                    <HeaderCell>{_("Type")}</HeaderCell>
                    <TypeCell dataKey="fieldType" />
                  </Column>
                  <Column width={130}>
                    <HeaderCell>{_("Icon")}</HeaderCell>
                    <IconCell dataKey="icon" />
                  </Column>
                  <Column width={130}>
                    <HeaderCell>{_("With Label")}</HeaderCell>
                    <CheckCell dataKey="withLabel" />
                  </Column>
                  <Column width={130}>
                    <HeaderCell>{_("With Image")}</HeaderCell>
                    <CheckCell dataKey="withImage" />
                  </Column>
                  <Column width={130}>
                    <HeaderCell>{_("Repeatable")}</HeaderCell>
                    <CheckCell dataKey="repeatable" />
                  </Column>
                  <Column sortable width={130}>
                    <HeaderCell>{_("Published")}</HeaderCell>
                    <CheckCell dataKey="published" />
                  </Column>
                  <Column fixed='right' width={60}>
                    <HeaderCell>{_("")}</HeaderCell>
                    <EditCell dataKey="_id" />
                  </Column>
                </Table>
              </div>
            </div>
          </div>
        </div>
        </div>
      </Admin>
    </>
  );
}
