import Head from "next/head";
import Admin from "../../../../layouts/Admin";
import { translation } from "../../../../lib/translations";
import { useRouter } from "next/router";
import { useState } from "react";
import { TagGroup, Tag, Input, IconButton } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import PlusIcon from "@rsuite/icons/Plus";
import Script from "next/script";
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function CardIndex() {
  const [isLoading, setIsLoading] = useState(false);
  const [section, setSection] = useState();
  const [cardType, setCardType] = useState('business_card');
  const [name, setName] = useState();
  const [icon, setIcon] = useState();
  const [image, setImage] = useState();
  const [initialValue, setInitialValue] = useState();
  const [withLabel, setWithLabel] = useState(false);
  const [withTitle, setWithTitle] = useState(false);
  const [labelSuggestions, SetLabelSuggestions] = useState([]);
  const [titleSuggestions, SetTitleSuggestions] = useState([]);
  const [repeatable, setRepeatable] = useState(false);
  const [withImage, setWithImage] = useState(false);
  const [fieldType, setFieldType] = useState();
  const [sectionError, setSectionError] = useState(false);
  const [nameError, setNameError] = useState(false);
  const [fieldTypeError, setFieldTypeError] = useState(false);

  const [typing, setTyping] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [titleTyping, setTitleTyping] = useState(false);
  const [titleInputValue, setTitleInputValue] = useState("");
  async function handleSubmit() {
    setSectionError(false);
    setNameError(false);
    setFieldTypeError(false);
    let stopSave = false;
    if (section == null) {
      setSectionError(true);
      stopSave = true;
    }
    if (name == null) {
      setNameError(true);
      stopSave = true;
    }
    if (fieldType == null) {
      setFieldTypeError(true);
      stopSave = true;
    }

    if (!stopSave) {
      if (!withLabel) {
        SetLabelSuggestions([]);
        SetTitleSuggestions([]);
      }
      let fieldTypeValue = {
        section: section,
        cardType:cardType,
        name: name,
        icon: icon,
        image: image,
        initialValue: initialValue,
        withLabel: withLabel,
        withTitle:withTitle,
        repeatable: repeatable,
        withImage: withImage,
        fieldType: fieldType,
        labelSuggestions: labelSuggestions,
        titleSuggestions:titleSuggestions
      };
      setIsLoading(true);
      const inseter = await fetch("/api/field-type/create/", {
        method: "POST",
        body: JSON.stringify(fieldTypeValue),
      });
      const newField = await inseter.json();
      setIsLoading(false);
      window.location.href = "/dashboard/admin/field-types";
    }
  }

  const removeSuggestion = (labelSuggestion) => {
    const nextSuggestion = labelSuggestions.filter(
      (item) => item !== labelSuggestion
    );
    SetLabelSuggestions(nextSuggestion);
  };
  const removeTitleSuggestion = (titleSuggestion) => {
    const nextSuggestion = titleSuggestions.filter(
      (item) => item !== titleSuggestion
    );
    SetTitleSuggestions(nextSuggestion);
  };
  const addSuggestion = () => {
    const nextSuggestions = inputValue
      ? [...labelSuggestions, inputValue]
      : labelSuggestions;
    SetLabelSuggestions(nextSuggestions);
    setTyping(false);
    setInputValue("");
  };
  const addTitleSuggestion = () => {
    const nextTitleSuggestions = titleInputValue
      ? [...titleSuggestions, titleInputValue]
      : titleSuggestions;
    SetTitleSuggestions(nextTitleSuggestions);
    setTitleTyping(false);
    setTitleInputValue("");
  };
  const handleButtonClick = () => {
    setTyping(true);
  };
  const handleTitleButtonClick = () => {
    setTitleTyping(true);
  };
  //======== Image Upload ============//
  const openWidget = () => {
    // create the widget
    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 1,
        cropping: true,
        croppingAspectRatio: 1,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 300,
        maxImageHeight: 300,
        theme: "white",
        form: "profilePic",
        showPoweredBy: false,
        resourceType: "image",
      },

      (error, result) => {
        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let pic = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            pic +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          pic += "/" + result.info.public_id;
          setImage(pic);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };
  const renderInput = () => {
    if (typing) {
      return (
        <Input
          className="tag-input"
          size="xs"
          style={{ width: 70 }}
          value={inputValue}
          onChange={setInputValue}
          onBlur={addSuggestion}
          onPressEnter={addSuggestion}
        />
      );
    }

    return (
      <IconButton
        className="tag-add-btn h-8 mt-3"
        onClick={handleButtonClick}
        icon={<PlusIcon />}
        appearance="ghost"
        size="xs"
      />
    );
  };
  const renderTitleInput = () => {
    if (titleTyping) {
      return (
        <Input
          className="tag-input"
          size="xs"
          style={{ width: 70 }}
          value={titleInputValue}
          onChange={setTitleInputValue}
          onBlur={addTitleSuggestion}
          onPressEnter={addTitleSuggestion}
        />
      );
    }

    return (
      <IconButton
        className="tag-add-btn h-8 mt-3"
        onClick={handleTitleButtonClick}
        icon={<PlusIcon />}
        appearance="ghost"
        size="xs"
      />
    );
  };
  return (
    <>
      <Head>
        <title>{_("Create Field Types")}</title>
      </Head>
      <Script
        src="https://widget.Cloudinary.com/v2.0/global/all.js"
        type="text/javascript"
      ></Script>
      <Admin>
        <div className="-mb-24 md:-m-1 md:-mb-20 h-24 bg-white">
          <div className="text-emerald-800 text-sm hidden lg:block pt-14 ltr:pl-16">
            {" "}
            {_("Create Field Types")}
          </div>
        </div>
        <div className="flex flex-wrap mt-24">
          <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-4">
            <div className="bg-white mt-4 shadow min-h-fit rounded-xl">
              <div className="card-header border-b-2 p-6">
                <h1 className="text-2xl">{_("Create Field Types")}</h1>
              </div>
              <div className="card-body p-6">
                <form>
                <div className="flex flex-wrap">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Card Type")}
                    </label>
                    <select
                      value={cardType}
                      onChange={(e) => {
                        setCardType(e.target.value);
                      }}
                      className="w-full md:w-10/12 border border-gray-600 p-2 rounded-xl"
                      placeholder={_("Card Type")}
                    >
                      <option value="">{_("Select One")}</option>
                      <option value="business_card">
                        {_("Business Card")}
                      </option>
                      <option value="company_profile">
                        {_("Company Profile")}
                      </option>
                      <option value="product_card">{_("Product Card")}</option>
                      <option value="event_card">
                        {_("Event Card")}
                      </option>
                      
                    </select>
                    {sectionError && (
                      <span className="text-red-600 text-xs">
                        {_("This Field is Required")}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Section")}
                    </label>
                    <select
                      value={section}
                      onChange={(e) => {
                        setSection(e.target.value);
                      }}
                      className="w-full md:w-10/12 border border-gray-600 p-2 rounded-xl"
                      placeholder="Section"
                    >
                      <option value="">{_("Select One")}</option>
                      <option value="personal_information">
                        {_("Personal Information")}
                      </option>
                      <option value="basic_contact">
                        {_("Basic Contact")}
                      </option>
                      <option value="social_media">{_("Social Media")}</option>
                      <option value="communication">
                        {_("Communication")}
                      </option>
                      <option value="business_page">
                        {_("Business Page")}
                      </option>
                      <option value="payment">{_("Payment")}</option>
                    </select>
                    {sectionError && (
                      <span className="text-red-600 text-xs">
                        {_("This Field is Required")}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Name")}
                    </label>
                    <input
                      value={name}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                      className="w-full md:w-10/12 border border-gray-600 p-3 rounded-xl"
                      placeholder="Name"
                    />
                    {nameError && (
                      <span className="text-red-600 text-xs">
                        {_("This Field is Required")}
                      </span>
                    )}
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Icon")}{" "}
                      {icon && (
                        <i
                          className={
                            icon + " mx-4 text-emerald-300 border p-3 rounded-xl"
                          }
                        ></i>
                      )}
                    </label>
                    <input
                      value={icon}
                      onChange={(e) => {
                        setIcon(e.target.value);
                      }}
                      className="w-full md:w-10/12 border border-gray-600 p-3 rounded-xl"
                      placeholder="Fa Icon"
                    />
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Image")}
                    </label>
                    {image && (
                      <img
                        onClick={openWidget}
                        src={image}
                        className={
                          icon + " mx-4 text-emerald-300 border p-3 rounded-xl"
                        }
                      />
                    )}
                    {!image && (
                      <i
                        onClick={openWidget}
                        className={
                          "fa fa-image mx-4 text-emerald-300 border p-3 rounded-xl"
                        }
                      ></i>
                    )}
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Initial Value")}
                    </label>
                    <input
                      value={initialValue}
                      onChange={(e) => setInitialValue(e.target.value)}
                      className="w-full md:w-10/12 border border-gray-600 p-3 rounded-xl"
                      placeholder="Initial Value"
                    />
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("With Label")}
                    </label>
                    <input
                      checked={withLabel}
                      onChange={(e) => setWithLabel(!withLabel)}
                      type="checkbox"
                      value={true}
                      className="  border border-gray-600 m-2 p-4 rounded-xl"
                    />
                  </div>
                  {withLabel && (
                    <div className="flex flex-wrap mt-4">
                      <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                        {_("Label Suggestions")}
                      </label>
                      <TagGroup className="flex flex-wrap">
                        {labelSuggestions.map((item, index) => (
                          <Tag
                            
                            key={index}
                            closable
                            onClose={() => removeSuggestion(item)}
                          >
                            {item}
                          </Tag>
                        ))}
                        {renderInput()}
                      </TagGroup>
                    </div>
                  )}
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("With Title")}
                    </label>
                    <input
                      checked={withTitle}
                      onChange={(e) => setWithTitle(!withTitle)}
                      type="checkbox"
                      value={true}
                      className="  border border-gray-600 m-2 p-4 rounded-xl"
                    />
                  </div>
                  {withTitle && (
                    <div className="flex flex-wrap mt-4">
                      <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                        {_("Title Suggestions")}
                      </label>
                      <TagGroup className="flex flex-wrap">
                        {titleSuggestions.map((item, index) => (
                          <Tag
                            
                            key={index}
                            closable
                            onClose={() => removeTitleSuggestion(item)}
                          >
                            {item}
                          </Tag>
                        ))}
                        {renderTitleInput()}
                      </TagGroup>
                    </div>
                  )}
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Repeatable")}
                    </label>
                    <input
                      checked={repeatable}
                      onChange={(e) => setRepeatable(!repeatable)}
                      type="checkbox"
                      value={true}
                      className="  border border-gray-600 m-2 p-4 rounded-xl"
                    />
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("With Image")}
                    </label>
                    <input
                      checked={withImage}
                      onChange={(e) => setWithImage(!withImage)}
                      type="checkbox"
                      value={true}
                      className="  border border-gray-600 m-2 p-4 rounded-xl"
                    />
                  </div>
                  <div className="flex flex-wrap mt-4">
                    <label className="uppercase font-bold text-gray-600 w-full md:w-2/12 p-2">
                      {_("Field Type")}
                    </label>
                    <div className="md:w-10/12">
                    <label>
                      <input
                        checked={fieldType == "phone"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="phone"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Phone Number")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "address"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="address"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Address")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "email"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="email"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Email")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "url"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="url"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("URL")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "text"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="text"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Text")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "file"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="file"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("File")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "embeded"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="embeded"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Embeded Video")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "carousel"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="carousel"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Images gallery")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "tags"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="tags"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Tags")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "number"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="number"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Number")}
                    </label>
                    <label>
                      <input
                        checked={fieldType == "date"}
                        onChange={(e) => setFieldType(e.target.value)}
                        type="radio"
                        name="field_type"
                        value="date"
                        className="border border-gray-600 m-2 p-4 rounded-xl"
                      />
                      {_("Date/Time")}
                    </label>
                    {fieldTypeError && (
                      <span className="block w-full text-red-600 text-xs">
                        {_("This Field is Required")}
                      </span>
                    )}
                    </div>
                  </div>
                  <div className="mt-4 h-10">
                    <div className="ltr:float-right rtl:float-left">
                      <button
                        disabled={isLoading}
                        onClick={handleSubmit}
                        type="button"
                        className="btn"
                      >
                        {!isLoading && <span>{_("Save")}</span>}
                        {isLoading && (
                          <i className="animate-pulse fas fa-ellipsis-h"></i>
                        )}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Admin>
    </>
  );
}
