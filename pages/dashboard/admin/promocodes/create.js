import Head from "next/head";
import Admin from "../../../../layouts/Admin";
import { translation } from "../../../../lib/translations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Dropdown, Table } from "rsuite";
import "rsuite/dist/rsuite.min.css";

const { Column, HeaderCell, Cell } = Table;

export default function FieldTypeIndex() {
  const { locale } = useRouter();
  const router = useRouter();
  const [code, setCode] = useState();
  const [percent, setPercent] = useState(0);
  const [minVal, setMinVal] = useState(0);
  const [max, setMax] = useState(0);
  const [startedAt, setStartedAt] = useState();
  const [expired_at, setExpired_at] = useState();

  const [isCreateing, setIsCreating] = useState(false);
  //======= Translation =========//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  //==== Random Pin ======//

  //==== Random slug =====//
  function makeid(length) {
    var result = "";
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random(new Date().getTime()) * charactersLength)
      );
    }
    return result;
  }

  //===== Submit From ==========
  function submitForm(e) {
    let theForm = document.getElementById("linkForm");
    if (theForm.checkValidity()) {
      e.preventDefault();

      setIsCreating(true);
      fetch("/api/promocode/create/", {
        method: "POST",
        body: JSON.stringify({
          code: code,
          percent: percent,
          max: max,
          minVal: minVal,
          started_at: startedAt,
          expired_at: expired_at,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setIsCreating(false);
          router.replace("/dashboard/admin/promocodes");
        });
    }
  }
  return (
    <>
      <Head>
        <title>{_("Create Promo Code")}</title>
      </Head>

      <Admin>
        <div className="container px-6">
          <div className="hidden lg:block lg:-m-1   bg-white">
            <div className="text-emerald-800 text-sm hidden lg:block p-4">
              {" "}
              {_("Create Promo Code")}
            </div>
          </div>
          <div className="mt-4 bg-white">
            <form id="linkForm">
              <div className="flex flex-wrap mt-2 p-2">
                <div className="w-full md:w-2/12 px-2 text-center">
                  <label className="uppercase  text-gray-500 mt-2">
                    {_("Code")}
                  </label>
                </div>
                <div className="w-full md:w-8/12 px-2">
                  <input
                    onChange={(e) => {
                      setCode(e.target.value);
                    }}
                    value={code}
                    className={"form-control my-1"}
                    required
                  />
                </div>
                <div className="w-full md:w-2/12 px-2">
                  <button
                    type="button"
                    onClick={() => {
                      setCode(makeid(6));
                    }}
                    className="bg-emerald-300 text-gray-500 w-full p-2 my-1  rounded"
                  >
                    {_("Generate")}
                  </button>
                </div>
              </div>
              <div className="flex flex-wrap mt-2 p-2">
                <div className="w-full md:w-2/12 px-2 text-center">
                  <label className="uppercase  text-gray-500 mt-2">
                    {_("Percent")}
                  </label>
                </div>
                <div className="w-full md:w-10/12 px-2">
                  <input
                    onChange={(e) => {
                      setPercent(e.target.value);
                    }}
                    value={percent}
                    step="1"
                    type="number"
                    className="form-control my-1"
                    required
                  />
                </div>
              </div>
              <div className="flex flex-wrap mt-2 p-2">
                <div className="w-6/12 flex">
                  <div className="w-full md:w-4/12 px-2 text-center">
                    <label className="uppercase  text-gray-500 mt-2">
                      {_("Minimum Value of invoice")}
                    </label>
                  </div>
                  <div className="w-full md:w-8/12 px-2">
                    <input
                      onChange={(e) => {
                        setMinVal(e.target.value);
                      }}
                      value={minVal}
                      step="1"
                      type="number"
                      className="form-control my-1"
                      required
                    />
                  </div>
                </div>
                <div className="w-6/12 flex">
                  <div className="w-full md:w-4/12 px-2 text-center">
                    <label className="uppercase  text-gray-500 mt-2">
                      {_("Maximum discount Value")}
                    </label>
                  </div>
                  <div className="w-full md:w-8/12 px-2">
                    <input
                      onChange={(e) => {
                        setMax(e.target.value);
                      }}
                      value={max}
                      step="1"
                      type="number"
                      className="form-control my-1"
                      required
                    />
                  </div>
                </div>
              </div>
              <div className="flex flex-wrap mt-2 p-2">
                <div className="w-6/12 flex">
                  <div className="w-full md:w-4/12 px-2 text-center">
                    <label className="uppercase  text-gray-500 mt-2">
                      {_("Start Date")}
                    </label>
                  </div>
                  <div className="w-full md:w-8/12 px-2">
                    <input
                      onChange={(e) => {
                        setStartedAt(e.target.value);
                      }}
                      value={startedAt}
                      type="date"
                      className="form-control my-1"
                      required
                    />
                  </div>
                </div>
                <div className="w-6/12 flex">
                  <div className="w-full md:w-4/12 px-2 text-center">
                    <label className="uppercase  text-gray-500 mt-2">
                      {_("Expiry Date")}
                    </label>
                  </div>
                  <div className="w-full md:w-8/12 px-2">
                    <input
                      onChange={(e) => {
                        setExpired_at(e.target.value);
                      }}
                      value={expired_at}
                      type="date"
                      className="form-control my-1"
                      required
                    />
                  </div>
                </div>
              </div>

              <div className="flex mb-4">
                <div className="w-full p-2">
                  <div className="float-right">
                    <button
                      disabled={isCreateing}
                      onClick={(e) => {
                        submitForm(e);
                      }}
                      className="bg-emerald-300 font-bold p-2 rounded text-gray-700 disabled:bg-gray-300 px-4"
                    >
                      {isCreateing && (
                        <i className="fa fa-spinner animate-spin"></i>
                      )}
                      {_("Create")}
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </Admin>
    </>
  );
}
