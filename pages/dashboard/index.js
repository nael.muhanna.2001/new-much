import React from "react";
import { DateRangePicker, CheckPicker, CustomProvider } from "rsuite";
import { useState } from "react";
import "rsuite/dist/rsuite.min.css";
import ar_EG from "rsuite/locales/ar_EG";
import en_GB from "rsuite/locales/en_GB";
// components

import CardLineChart from "../../components/Cards/CardLineChart";
import CardPageVisits from "../../components/Cards/CardPageVisits.js";
import CardSocialTraffic from "../../components/Cards/CardSocialTraffic.js";
import HeaderStats from "../../components/Headers/HeaderStats";

import Head from "next/head";
// layout for page

import Admin from "../../layouts/Admin";
import { translation } from "../../lib/translations";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import Image from "next/image";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

export default function Dashboard() {
  const {
    allowedMaxDays,
    allowedDays,
    allowedRange,
    beforeToday,
    afterToday,
    combine,
  } = DateRangePicker;
  const { locale } = useRouter();

  const [dateRange, setDateRange] = useState([
    new Date(new Date().setDate(new Date().getDate() - 6)),
    new Date(new Date().setHours(0, 59, 59, 999)),
  ]);
  const [assemblyBy, setAssemblyBy] = useState("day");
  const [chartShap, setChartShap] = useState("line");
  const [cards, setCards] = useState([]);
  const [labels, setLabels] = useState([]);
  const [visits, setVisits] = useState([]);
  const [datasets, setDatasets] = useState([]);
  const [cardCheck, setCardCheck] = useState([]);
  const [selectedCardIds, setSelectedCardIds] = useState([]);
  const colorsArray = [
    "#ebada3",
    "#6b5d7b",
    "#ce6a6c",
    "#e5e0ce",
    "#8ccaae",
    "#00487d",
    "#e3e3e3",
    "#7C3E66",
    "#BDF2D5",
    "#91C483",
    "#FF6464",
  ];
  function getChartInfo() {
    fetch("/api/dashboard/visit-chart/", {
      method: "post",
      body: JSON.stringify({
        dateRange: dateRange,
        assemblyBy: assemblyBy,
        selectCards: selectedCardIds,
        locale: locale,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCards(data.cards);
        setLabels(data.labels);
        setVisits(data.dayVisit);
        setSelectedCardIds(data.card_ids);
        let dataSets = [];
        setCardCheck(
          data.cards.map((e) => ({
            label:
              e.name +
              "(" +
              (e.cardType == "business_card"
                ? "BC"
                : e.cardType == "company_profile"
                ? "CP"
                : e.cardType == "product_card"
                ? "PC"
                : "EC") +
              ")",
            value: e._id,
          }))
        );
        let counter = 0;
        data.cards.forEach((e, i) => {
          if (counter > 11) counter = 0;

          if (data.card_ids.includes(e._id)) {
            let set = {
              label:
                e.name +
                "(" +
                (e.cardType == "business_card"
                  ? "BC"
                  : e.cardType == "company_profile"
                  ? "CP"
                  : e.cardType == "product_card"
                  ? "PC"
                  : "EC") +
                ")",
              tension: 0.5,
              backgroundColor: colorsArray[counter],
              borderColor: colorsArray[counter],

              fill: false,
            };
            let setData = [];
            data.dayVisit.forEach((v, k) => {
              if (v.card_id) {
                setData.push(v.card_id.filter((c) => c == e._id).length);
              } else {
                setData.push(0);
              }
            });
            set.data = setData;
            dataSets.push(set);
          }
          counter++;
        });

        setDatasets(dataSets);
      });
  }

  const { data: session, status } = useSession();
  if (status == "loading") {
    return (
      <>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }
  if (!session) window.location.href = "/signin";
  if (session && status != "loading")
    return (
      <>
        <Head>
          <title>{_("Dashboard")}</title>
        </Head>
        <Admin>
          {/* Header */}
          <div className="container">
            <div className="-mb-24 md:-mb-24">
              <HeaderStats />
            </div>
            <div className="px-1 mb-6">
              <div
                className="relative flex flex-col min-w-0 break-words bg-white w-full mb-2 md:mb-4 rounded-xl"
                style={{ borderBottom: "solid #00487d 3px" }}
              >
                <div className="flex flex-wrap ">
                  <div className="w-full xl:w-12/12 mb-6 xl:mb-0  ">
                    <div className="relative w-full max-w-full flex-grow flex-1">
                      <h3
                        className="font-semibold text-base text-white py-3 rounded-t-xl text-center"
                        style={{ backgroundColor: "#00487d" }}
                      >
                        {_("Performance")}
                      </h3>
                    </div>
                    <div>
                      <div className="mb-2  flex flex-wrap max-w-screen-2xl px-2">
                        <div className="w-full md:w-3/12 mt-2">
                          <DateRangePicker
                            locale={
                              locale == "ar"
                                ? ar_EG.DateRangePicker
                                : en_GB.DateRangePicker
                            }
                            style={{ width: 248 }}
                            value={dateRange}
                            placeholder="Select Date Range"
                            placement="bottomStart"
                            disabledDate={allowedRange(
                              "2022-01-01",
                              new Date()
                            )}
                            onChange={(e) => {
                              setDateRange(e);
                              setLabels([]);
                            }}
                          />
                        </div>
                        <div className="w-full md:w-3/12 mt-2">
                          <CheckPicker
                            value={selectedCardIds}
                            data={cardCheck}
                            style={{ width: 248 }}
                            onChange={(e) => {
                              setSelectedCardIds(e);
                              setLabels([]);
                            }}
                          />
                        </div>

                        <div
                          className="w-6/12 md:w-2/12 mt-2"
                          style={{ minWidth: "max-content" }}
                        >
                          <div>
                            <button
                              className={
                                assemblyBy == "day"
                                  ? "relative bg-gray-200 focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-l rtl:rounded-r focus:font-bold h-9 p-2 w-14 "
                                  : "relative bg-white focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-l rtl:rounded-r focus:font-bold h-9 p-2 w-14 "
                              }
                              onClick={() => {
                                setAssemblyBy("day");
                                setLabels([]);
                              }}
                            >
                              {_("Day")}
                            </button>
                            <button
                              className={
                                assemblyBy == "week"
                                  ? "relative bg-gray-200 focus:bg-gray-200 duration-300 text-blue-500 text-xs  focus:font-bold h-9 p-2 w-16 "
                                  : "relative bg-white focus:bg-gray-200 duration-300 text-blue-500 text-xs  focus:font-bold h-9 p-2 w-16 "
                              }
                              onClick={() => {
                                setAssemblyBy("week");
                                setLabels([]);
                              }}
                            >
                              {_("Week")}
                            </button>
                            <button
                              className={
                                assemblyBy == "month"
                                  ? "relative bg-gray-200 focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-r rtl:rounded-l focus:font-bold h-9 p-2 w-16 "
                                  : "relative bg-white focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-r rtl:rounded-l focus:font-bold h-9 p-2 w-16 "
                              }
                              onClick={() => {
                                setAssemblyBy("month");
                                setLabels([]);
                              }}
                            >
                              {_("Month")}
                            </button>
                          </div>
                        </div>
                        <div
                          className="w-4/12 md:w-1/12 mt-2 mx-1"
                          style={{ minWidth: "max-content" }}
                        >
                          <div>
                            <button
                              className={
                                chartShap == "bar"
                                  ? "relative bg-gray-200 focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-l rtl:rounded-r focus:font-bold h-9 p-2 w-12 "
                                  : "relative bg-white focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-l rtl:rounded-r focus:font-bold h-9 p-2 w-12 "
                              }
                              onClick={() => {
                                setChartShap("bar");
                                window.myLine.type = "bar";
                              }}
                            >
                              <i className="fa fa-chart-simple"></i>
                            </button>
                            <button
                              className={
                                chartShap == "line"
                                  ? "relative bg-gray-200 focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-r rtl:rounded-l focus:font-bold h-9 p-2 w-12 "
                                  : "relative bg-white focus:bg-gray-200 duration-300 text-blue-500 text-xs ltr:rounded-r rtl:rounded-l focus:font-bold h-9 p-2 w-12 "
                              }
                              onClick={() => {
                                setChartShap("line");
                                window.myLine.type = "line";
                              }}
                            >
                              <i className="fa fa-chart-line"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <CardLineChart
                  assemblyBy={assemblyBy}
                  cardCheck={cardCheck}
                  chartShap={chartShap}
                  datasets={datasets}
                  dateRange={dateRange}
                  labels={labels}
                  getChartInfo={getChartInfo}
                />
              </div>
            </div>
            <div className="flex flex-wrap mt-2 md:mt-4 mx-1">
              <div className="w-full xl:w-6/12 mb-6 xl:mb-0 lg:pr-3">
                <CardPageVisits
                  dateRange={dateRange}
                  selectedCardIds={selectedCardIds}
                />
              </div>
              <div className="w-full xl:w-6/12">
                <CardSocialTraffic
                  dateRange={dateRange}
                  selectedCardIds={selectedCardIds}
                />
              </div>
            </div>
          </div>
        </Admin>
      </>
    );
}

Dashboard.layout = Admin;
