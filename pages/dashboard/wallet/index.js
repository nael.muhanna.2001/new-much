import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import WalletHeader from "../../../components/walletheader";
import { translation } from "../../../lib/translations";
import cookie from "js-cookie";
import { useSession } from "next-auth/react";

import "rsuite/dist/rsuite.min.css";
import Vcard from "../../../components/vcard";
import { TagPicker } from "rsuite";
import Admin from "../../../layouts/Admin";
import "rsuite/dist/rsuite.min.css";
function compare(a, b) {
  if (a.saved_at < b.saved_at) {
    return 1;
  }
  if (a.saved_at > b.saved_at) {
    return -1;
  }
  return 0;
}
export default function Wallet() {
  const { locale } = useRouter();
  //=== Translation Function Start
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const [dbuser, setDbUser] = useState({});
  const [cards, setCards] = useState([]);
  const [card, setCard] = useState(null);
  const [card_id, setCard_id] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [tagData, setTagData] = useState([]);
  const [tagArray, setTagArray] = useState([]);
  const { data: session, status } = useSession();
  const [tags, setTags] = useState([]);
  const [cardNotes, setCardNotes] = useState([]);
  const [isAddingNote, setIsAddingNote] = useState(false);
  const [noteText, setNoteText] = useState();
  const [contentLoading, setConentLoading] = useState(false);
  function addTag(user_id, card_id, tag) {
    fetch("/api/wallet/add-tag/?user_id=" + user_id + "&card_id=" + card_id, {
      method: "POST",
      body: JSON.stringify({ tags: tag }),
    })
      .then((res) => res.json())
      .then((data) => {
        setDbUser(data);
        let tagLabels = [];
        if (data.wallet) {
          data.wallet.forEach((x, m) => {
            if (x.tags) {
              x.tags.map((e) => {
                if (
                  tagLabels.filter((t) => {
                    return t.label == e;
                  }).length == 0
                ) {
                  tagLabels.push({ label: e, value: e });
                }
              });
            }
          });
        }

        setTagData(tagLabels);
      });
  }
  
  function deleteNote(note_id, card_id, writer_id) {
    let notes = cardNotes.filter((e) => e._id != note_id);
    setCardNotes(notes);
    fetch("/api/wallet/notes/delete-note/", {
      method: "POST",
      body: JSON.stringify({
        note_id: note_id,
        card_id: card_id,
        writer_id: writer_id,
      }),
    })
      .then((res) => res.json())
      .then((data) => {});
  }
  useEffect(() => {
    setConentLoading(true);
    fetch("/api/wallet/")
      .then((res) => {
        if (res.ok) {
          setConentLoading(false);
        }
        return res.json();
      })
      .then((data) => {
        console.log(data);
        let wallet = data.user.wallet;
        wallet.sort(compare);

        setDbUser(data.user);
        setDbUser((prev) => {
          let theUser = { ...prev };
          theUser.wallet = wallet;
          return theUser;
        });
        let theCards = [];
        data.cards.map((e, k) => {
          let saved_at = data.user.wallet.filter(
            (we, kk) => we.card_id == e._id
          );
          if (saved_at.length) {
            e.saved_at = saved_at[0].saved_at;
            e.tags = saved_at[0].tags ? saved_at[0].tags : [];
            theCards.push(e);
          }
        });

        theCards.sort(compare);
        console.log(theCards);
        setCards(theCards);
        setTagArray(data.tags);
        let tagLabels = [];

        data.tags.map((e) => {
          if (
            tagLabels.filter((t) => {
              return t.label == e;
            }).length == 0
          ) {
            tagLabels.push({ label: e, value: e });
          }
        });
        setTagData(tagLabels);
      });
  }, []);
  async function addNote(note_text) {
    setIsAddingNote(true);
    fetch("/api/wallet/notes/add-note/", {
      method: "POST",
      body: JSON.stringify({
        card_id: card._id,
        writer_id: dbuser._id,
        note_text: note_text,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        let notes = cardNotes;
        notes.push(data);
        setCardNotes(notes);
        setIsAddingNote(false);
        setNoteText("");
      });
  }
  function openCard(slug, card_id) {
    setCard_id(card_id);
    setCardNotes([]);
    setCard(null);
    setIsLoading(true);
    let user = null;
    let src = null;

    if (cookie.get("user") && !session.user) {
      user = JSON.parsecookie.get("user");
    } else {
      user = session && session.user ? JSON.stringify(session.user) : null;
    }

    fetch("/api/post-user/", { method: "post", body: user })
      .then((res) => res.json())
      .then((userData) => {
        fetch("/api/cards/" + slug, {
          method: "POST",
          body: JSON.stringify({
            card_id: card_id,
            location: null,
            src: "wallet",
            referrer: null,
            user:userData,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            setCard(data);

            if (dbuser.wallet.filter((e) => e.card_id == data._id).length) {
              let loadedtags = dbuser.wallet.filter(
                (e) => e.card_id == data._id
              )[0].tags;
              setTags(loadedtags);
            } else {
              setTags([]);
            }
            fetch(
              "/api/wallet/notes/?card_id=" +
                data._id +
                "&writer_id=" +
                dbuser._id
            )
              .then((res) => res.json())
              .then((data) => {
                setCardNotes(data);
                setIsLoading(false);
              });

            if (src != null) {
              window.location.href = "/" + slug;
            }
            return;
          });
      });
  }
  return (
    <>
      <Head>
        <title>{_("My Cardholder")}</title>
        <meta name="viewport" content="width=device-width, initial-scale=0.9" />
      </Head>

      <Admin>
        <div className="md:container md:px-6">
        <div className="hidden lg:block lg:-m-1   bg-white">
          <div className="text-emerald-800 text-sm hidden lg:block p-4">
            {" "}
            {_("My Cardholder")}
          </div>
        </div>
        <div className="lg:mt-1">
          <div
            className="flex grid-cols-12"
            style={{ height: "99vh !important" }}
          >
            <div className="w-full lg:w-1/4  p-2 overflow-y-auto h-screen ">
              {contentLoading && <div className="text-center">
                <div className="mx-auto text-center">
                <h1 className="animate-spin mx-auto">🕛</h1>
                <h4>{_('Loading...')}</h4>
                </div>
                </div>}
                {(!contentLoading && !cards.length) && <div className="text-center"><div><h1>🤷‍♂️</h1><h4>{_('There is no card saved to your cardholder')}</h4></div></div>}
              {(!contentLoading && cards.length) && <div>
                <ul>
                  {cards &&
                    cards.map((e, k) => {
                      return (
                        <button
                          onClick={() => {
                            openCard(e.slug, e._id);
                          }}
                          key={k}
                          className={
                            " w-full border border-l-0 border-r-0 border-t-0 bg-white my-1 rounded-xl shadow-md  hover:bg-gray-100 border-emerald-300 p-2 flex cursor-pointer duration-500" +
                            (card && card._id == e._id
                              ? "bg-gray-200 focus:bg-gray-200 border-b-2 border-emerald-500 font-bold text-black"
                              : isLoading && e._id == card_id
                              ? "focus:bg-gray-100 animate-pulse"
                              : "")
                          }
                        >
                          <div
                            className="w-12 h-12 rounded-full overflow-hidden border ltr:float-left rtl:float-right text-2xl text-center"
                            style={{ minHeight: "50px", minWidth: "50px" }}
                          >
                            {e.pic && <img src={e.pic} />}
                            {!e.pic && e.cardType == "business_card" ? (
                              <i className="fa fa-user p-2"></i>
                            ) : e.cardType == "company_profile" ? (
                              <i className="fa fa-building p-2"></i>
                            ) : e.cardType == "product_card" ? (
                              <i className="fa fa-tag p-2"></i>
                            ) : e.cardType == "event_card" ? (
                              <i className="fa fa-calander p-2"></i>
                            ) : (
                              ""
                            )}
                          </div>
                          <div
                            className="py-3 px-2 text-sm ltr:float-left rtl:float-right ltr:text-left rtl:text-right overflow-x-clip whitespace-nowrap w-8/12 lg:w-6/12"
                            style={{ width: "-webkit-fill-available" }}
                          >
                            {e.name ? e.name : _("Untitled")}
                            <br />
                            {(e.company || e.jobTitle) && (
                              <div className="rtl:float-right ltr:float-left text-xs text-gray-400">
                                {e.jobTitle && e.jobTitle.label
                                  ? e.jobTitle.label
                                  : ""}
                                <br />
                                {e.company && e.company.label
                                  ? "  " + e.company.label
                                  : ""}
                                <br />
                              </div>
                            )}

                            <div className="rtl:float-right ltr:float-left text-xs text-gray-400">
                              {e.cardType == "company_profile"
                                ? _("Company Profile")
                                : e.cardType == "product_card"
                                ? _("Product Card")
                                : e.cardType == "event_card"
                                ? _("Event Card")
                                : ""}
                            </div>
                          </div>
                          <div className="ltr:float-right rtl:float-left  text-gray-400 text-xs min-w-fit">
                            {new Date(e.saved_at).toLocaleDateString(locale, {
                              month: "short",
                              day: "numeric",
                              year: "numeric",
                            })}
                          </div>
                          {isLoading && e._id == card_id && (
                            <i className="ltr:float-right rtl:float-left my-5  fa fa-spinner animate-spin text-gray-400"></i>
                          )}
                        </button>
                      );
                    })}
                </ul>
              </div>}
            </div>
            <div
              className={
                (card == null ? "hidden " : "") +
                (isLoading ? "bg-gray-100" : "") +
                " absolute lg:relative  w-full lg:w-3/4 lg:block z-0 ltr:border-l-2 rtl:border-r-2 border-emerald-300 lg:mt-4  overflow-y-auto "
              }
            >
              {isLoading && (
                <div className="w-full h-full px-96 py-96">
                  <i className="fa fa-spinner animate-spin text-gray-400 text-6xl"></i>
                  <br /> {_("Loading")}...
                </div>
              )}
              <div
                className={
                  (card != null ? "opacity-100 " : "opacity-0 ") + " duration-500"
                }
              >
                {card && (
                  <div className="lg:flex justify-start">
                    <div className="w-full lg:w-2/3 bg-white min-h-screen">
                      <Vcard card={card} />
                      <div className="fixed bottom-0 z-50 left-0 w-full h-12 justify-center lg:hidden shadow shadow-black bg-white">
                        <button
                          onClick={() => {
                            setCard(null);
                          }}
                          className="uppercase w-full p-3 bg-emerald-300 " style={{zIndex:'100'}}
                        >
                          Close
                        </button>
                      </div>
                    </div>
                    <div className=" w-full lg:w-1/3 pt-4  border-l min-h-screen lg:grid bg-white">
                      <div className="pt-4 mx-2">
                        <div className="tex-md font-bold text-gray-400 uppercase">
                          {_("Tags")}
                        </div>
                        <TagPicker
                          value={tags}
                          className="w-full"
                          style={{ minHeight: "50px" }}
                          creatable
                          cleanable={false}
                          searchable={true}
                          noCaret
                          data={tagData}
                          placeholder={_("Tags")}
                          onChange={(value, item) => {
                            setTags(value);
                            addTag(dbuser._id, card._id, value);
                          }}
                          onCreate={(value, item) => {
                            setTags(value);
                            addTag(dbuser._id, card._id, value);
                          }}
                        />
                      </div>
                      <div className="p-2 row-span-5 pb-32 mx-2 mt-10">
                        <div className="tex-md font-bold text-gray-400 uppercase">
                          {_("Notes")}
                        </div>
                        {!isLoading && cardNotes.length == 0 && (
                          <div>{_("There is no note!")}</div>
                        )}
                        {cardNotes.map((e, k) => {
                          return (
                            <div
                              key={k}
                              className="bg-white shadow-md rounded-xl w-full p-2 my-2 note_block"
                              style={{ minHeight: "100px" }}
                            >
                              <button
                                onClick={() => {
                                  deleteNote(e._id, e.card_id, e.writer_id);
                                }}
                                className="delete ltr:float-left rtl:float-right text-red-500 opacity-0 duration-200"
                              >
                                <i className="fa fa-times"></i>
                              </button>
                              <div className="text-xs ltr:float-right rtl:float-left text-gray-400">
                                {new Date(e.created_at).toLocaleTimeString(
                                  locale,
                                  {
                                    weekday: "short",
                                    day: "numeric",
                                    month: "short",
                                    year: "numeric",
                                  }
                                )}
                              </div>
                              <div className="mt-5">
                                <p className="whitespace-pre-line">
                                  {e.note_text}
                                </p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                      <div className="p-2 block w-full  z-10 lg:sticky bottom-12 lg:bottom-0 bg-gray-100 shadow-md px-4">
                        <textarea
                        
                          value={noteText}
                          onChange={(e) => {
                            setNoteText(e.target.value);
                          }}
                          placeholder={_("Write a note")}
                          className="w-full h-28 border rounded-xl border-gray-400 form-control"
                        ></textarea>
                        <button
                          disabled={isAddingNote || !noteText}
                          onClick={() => {
                            addNote(noteText);
                          }}
                          className="w-full bg-emerald-200 border border-emerald-300 p-3 rounded-xl"
                        >
                          {isAddingNote && (
                            <span>
                              {_("Saving")}{" "}
                              <i className="fa fa-spinner animate-spin"></i>
                            </span>
                          )}
                          {!isAddingNote && <span>{_("Save")}</span>}
                        </button>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        </div>
      </Admin>
    </>
  );
}
