import Head from "next/head";
import Admin from "../../../../layouts/Admin";
import { translation } from "../../../../lib/translations";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import "react-toastify/dist/ReactToastify.css";
import QRCode from "easyqrcodejs";

import AsyncCreatableSelect from "react-select/async-creatable";
import { components } from "react-select";
import { countries } from "../../../../lib/countries";
import Script from "next/script";
import { ToastContainer, toast } from "react-toastify";
import { RgbaStringColorPicker, HexColorInput } from "react-colorful";
import { Modal, Button, ButtonToolbar, Placeholder, Toggle } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import Phone from "../../../../components/cardInputs/phone";
import PhoneComponent from "../../../../components/cardComponents/phone";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import Email from "../../../../components/cardInputs/email";
import EmailComponent from "../../../../components/cardComponents/email";
import URL from "../../../../components/cardInputs/url";
import UrlComponent from "../../../../components/cardComponents/url";
import Text from "../../../../components/cardInputs/text";
import TextComponent from "../../../../components/cardComponents/text";
import EmailOutput from "../../../../components/outputComponents/email";
import PhoneOutput from "../../../../components/outputComponents/phone";
import TextOutput from "../../../../components/outputComponents/text";
import UrlOutput from "../../../../components/outputComponents/url";
import Address from "../../../../components/cardInputs/address";
import AddressOutput from "../../../../components/outputComponents/address";
import AddressComponent from "../../../../components/cardComponents/address";
import File from "../../../../components/cardInputs/file";
import "rsuite/dist/rsuite.min.css";
const options = [];
countries.map((country) => {
  options.push({
    value: country.dial,
    label: country.name + " (" + country.dial + ")",
  });
});
const updateSlug = (_id, slug) => {
  updateCard({ _id: _id, slug: slug });
};
//=== Translation Function Start
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
//=== Translation Function End
//=== Company Options Component Start
const { Option } = components;
const IconOption = (props) => (
  <Option {...props}>
    {props.data.pic && (
      <img
        src={props.data.pic}
        style={{
          width: "28px",
          height: "28px",
          float: "left",
          margin: "-2px 4px",
          borderRadius: "100px",
        }}
      />
    )}
    {!props.data.pic && <i className="fa fa-building text-gray-500 p-2"></i>}
    <span> {props.data.label}</span>
  </Option>
);
//=== Company Options Component End

//=== Update Card Handler Start
async function updateCard(request) {
  toast.promise(
    fetch("/api/cards/create/", {
      method: "POST",
      body: JSON.stringify(request),
    }),
    {
      pending: "Updating...",
      success: "Your changes have been saved!",
      error: "Something wrong!!",
    }
  );
}

//=== Update Card Handler End
export default function Card({ params }) {
  const { _id } = params;
  const [card, setCard] = useState();
  const [cardFields, setCardFields] = useState([]);
  const [tab, setTab] = useState("content");
  const [openFieldModal, setOpenFieldModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [oldName, setOldName] = useState();
  const [cardType, setCardType] = useState();
  const [imagePublicId, setImagePublicId] = useState();
  const [coverPublicId, setCoverPublicId] = useState();
  const [cover, setCover] = useState("https://picsum.photos/1125/1152");
  const [imageInfo, setImageInfo] = useState();
  const [coverInfo, setCoverInfo] = useState();
  const [crop, setCrop] = useState("scale");
  const [coverCrop, setCoverCrop] = useState();
  const [contactViewAccess, setContactViewAccess] = useState("public");
  const [pic, setPic] = useState();
  const [name, setName] = useState("");
  const [jobTitle, setJobTitle] = useState("");
  const [department, setDepartment] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [description, setDescription] = useState();
  const [mobilePrefix, setMobilePrefix] = useState();
  const [mobile, setMobile] = useState();
  const [telephonePrefix, setTelephonePrefix] = useState();
  const [telephone, setTelephone] = useState();
  const [whatsappPrefix, setWhatsappPrefix] = useState();
  const [whatsapp, setWhatsapp] = useState();
  const [email, setEmail] = useState();
  const [address, setAddress] = useState();
  const [companiesList, setCompaniesList] = useState();
  const [jobTitleList, setJobTitleList] = useState([]);
  const [departmentList, setDepartmentList] = useState([]);
  const [bgUrl, setBgUrl] = useState("https://picsum.photos/1355/321");
  const [showQR, setShowQR] = useState(false);
  const qrCodeRef = useRef(null);
  const [slug, setSlug] = useState();
  const [QRcolorDark, setQRcolorDark] = useState("#000000");
  const [QRcolorLight, setQrColorLight] = useState("#ffffff");
  const [qrLogo, setQrLogo] = useState();
  const [qrlogoWidth, setQrlogoWidth] = useState();
  const [qrlogoBackgroundTransparent, setQrLogoBackgroundTransparent] =
    useState(false);
  const [qrbackgroundImage, setQrbackgroundImage] = useState();
  const [qrbackgroundImageAlpha, setQrbackgroundImageAlpha] = useState(1);
  const [loadingFieldType, setLoadingFieldType] = useState(false);
  const [fieldTypes, setFieldTypes] = useState([]);
  const [updatedField, setUpdatedField] = useState();
  //desgin variables//
  const [color, setColor] = useState({
    primary: "#333333",
    secondary: "#FFFFFF",
    name: "default",
  });
  const [avatar, setAvatar] = useState("9999px");
  const [contactShape, setContactShape] = useState("horizontal");
  const [buttonShape, setButtonShape] = useState("9999px");

  const [fieldValue, setFieldValue] = useState();
  const [fieldLabel, setFieldLabel] = useState();
  const [fieldPrefix, setFieldPrefix] = useState();
  const [fieldViewAccess, setFieldViewAccess] = useState();
  const [showDeleteArray, setShowDeleteArray] = useState([]);
  const [canPost, setCanPost] = useState(false);
  const [checking, setChecking] = useState(false);
  const [suggest, setSuggest] = useState();
  const router = useRouter();
  function deleteCard(_id)
  {
    fetch('/api/cards/delete/'+_id).then(res=>res.json()).then(data=>{
      router.replace('/dashboard/cards');
    })
  }
  function checkAvailbilty() {
    setChecking(true);
    fetch("/api/cards/check-name", {
      method: "POST",
      body: JSON.stringify({ name: slug }),
    })
      .then((data) => data.json())
      .then((res) => {
        if (res.status) {
          setCanPost(true);
          setChecking(false);
          return;
        } else {
          setCanPost(false);

          setSuggest(res.suggest);
          setChecking(false);
          return;
        }
      });
  }
  //====== get Location =======
  async function getLocation() {
    let countryName = null;
    const location = await fetch(
      "https://ipgeolocation.abstractapi.com/v1/?api_key=8981df21aa7d4dfe9f299632ae1472d8",
      {
        method: "GET",
      }
    );
    let data = await location.json();
    countries.map((country) => {
      if (country.ISO2 == data.country_code2) {
        countryName = {
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        };
      }
    });
    return countryName;
  }
  //==== Get Card Info =====//
  useEffect(() => {
    if (!isLoading) {
      setIsLoading(true);
      getLocation().then((countryCode) => {
        fetch("/api/cards/" + _id, { method: "POST" })
          .then((res) => res.json())
          .then((data) => {
            setCard(data);
            setCardType(data.cardType);
            setPic(data.pic);
            setImagePublicId(data.imagePublicId);
            setCover(data.cover);
            setCoverInfo(data.coverInfo);
            setName(data.name);
            setJobTitle(data.jobTitle);
            setDepartment(data.department);
            setCompanyName(data.company);
            setDescription(data.description);
            setContactViewAccess(data.contactViewAccess);
            setMobilePrefix(
              data.mobilePrefix ? data.mobilePrefix : countryCode
            );
            setMobile(data.mobile);
            setTelephonePrefix(
              data.telephonePrefix ? data.telephonePrefix : countryCode
            );
            setTelephone(data.telephone);
            setWhatsappPrefix(
              data.whatsappPrefix ? data.whatsappPrefix : countryCode
            );
            setWhatsapp(data.whatsapp);
            setEmail(data.email);
            setAddress(data.address);
            setSlug(data.slug);
            data.color != null ? setColor(data.color) : "";
            setAvatar(data.avatar);
            setContactShape(data.contactShape);
            setButtonShape(data.buttonShape);
            setQRcolorDark(data.QRcolorDark);
            setQrColorLight(data.QRcolorLight);
            setCardFields(data.fields || []);

            setIsLoading(false);
          });
      });
    }
  }, []);
  //==== JobTitle Auto Complete ===//
  function handleJobTitleChange(val) {
    setJobTitle(val);

    updateCard({ _id: _id, jobTitle: val });
    if (!val) {
      updateCard({ _id: _id, jobTitle: null });
    }
  }

  async function promiseJobTitleOptions(val) {
    return new Promise((resolve) => {
      fetch("/api/cards/get-job-title/", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          const job_title = [];
          data.map((e) => {
            job_title.push(e.jobTitle);
          });
          setJobTitleList(job_title);
          resolve(job_title);
        });
    });
  }

  async function onCreateJobTitleOption(val) {
    let newOption = { value: val, label: val };
    setJobTitle(newOption);
    updateCard({ _id: _id, jobTitle: newOption });
  }

  //====== Department Auto Complete ======//
  function handleDepartmentChange(val) {
    setDepartment(val);
    updateCard({ _id: _id, department: val });
    if (!val) {
      updateCard({ _id: _id, department: null });
    }
  }

  async function promiseDepartmentOptions(val) {
    return new Promise((resolve) => {
      fetch("/api/cards/get-departments", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          const departments = [];
          data.map((e) => {
            departments.push(e.department);
          });
          setDepartmentList(departments);
          resolve(departments);
        });
    });
  }

  async function onCreateDepartmentOption(val) {
    let newOption = { value: val, label: val };
    setDepartment(newOption);
    updateCard({ _id: _id, department: newOption });
  }

  //===== Company Name Auto Complete =====

  function handleCompanyChange(val) {
    setCompanyName(val);
    if (val && val.source == "seamless" && val._id == null) {
      fetch("/api/cards/create-company", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          updateCard({
            _id: _id,
            company: {
              value: data._id,
              label: data.name,
              pic: data.pic,
              slug: data.slug,
            },
          });
        });
    } else {
      updateCard({ _id: _id, company: val });
    }

    if (!val) {
      updateCard({ _id: _id, company: null });
    }
  }

  async function promiseCompanyOptions(val) {
    return new Promise((resolve) => {
      fetch("/api/cards/get-companies", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          const companiesList = [];
          data.map((e) => {
            companiesList.push({
              value: e._id,
              label: e.name,
              pic: e.pic,
              slug: e.slug,
              source: e.source,
              description: e.description,
            });
          });
          setCompaniesList(companiesList);
          resolve(companiesList);
        });
    });
  }

  async function onCreateCompanyOption(val) {
    let newOption = {
      value: val
        .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
        .toLowerCase(),
      label: val,
      slug: val
        .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
        .toLowerCase(),
    };
    setCompanyName(newOption);
    if (val) {
      const company = await fetch("/api/cards/create-company", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          newOption = { value: data._id, label: data.name, pic: data.pic };
          setCompaniesList([...companiesList, newOption]);

          setCompanyName(newOption);
          updateCard({ _id: _id, company: newOption });
        });
    }

    if (!val) {
      updateCard({ _id: _id, company: true });
    }
  }
  //====== Cover Image Upload =====//
  const openCoverWidget = () => {
    // create the widget
    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 1,
        cropping: true,
        croppingAspectRatio: 0.976,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 1125,
        maxImageHeight: 1152,
        theme: "white",
        form: "coverPic",
        showPoweredBy: false,
        resourceType: "image",
      },

      (error, result) => {
        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let bg = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            bg +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          bg += "/" + result.info.public_id;

          setCover(bg);
          updateCard({ _id: _id, cover: bg, coverInfo: coverPublicId });
          setCoverPublicId(result.info.public_id);
          setCoverInfo(result.info);
          setCoverCrop(result.info.coordinates);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };

  //======== Profile Pic Upload ============//
  const openWidget = () => {
    // create the widget
    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 1,
        cropping: true,
        croppingAspectRatio: 1,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 300,
        maxImageHeight: 300,
        theme: "white",
        form: "profilePic",
        showPoweredBy: false,
        resourceType: "image",
      },

      (error, result) => {
        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let pic = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            pic +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          pic += "/" + result.info.public_id;
          setPic(pic);
          updateCard({
            _id: _id,
            pic: pic,
            imagePublicId: result.info.public_id,
          });
          setImagePublicId(result.info.public_id);
          setImageInfo(result.info);
          setCrop(result.info.coordinates);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };

  //====== Show QR function =====//
  async function showQrFn(option) {
    setShowQR(true);
    let opt = {
      text: "//" + window.location.host + "/" + slug,
      width: 160,
      height: 160,
      logoMaxWidth: 40,
      logoMaxHeight: 40,
      drawer: "canvas",
      autoColor: true,
      colorDark: QRcolorDark ? QRcolorDark : "#000000",
      colorLight: QRcolorLight ? QRcolorLight : "#ffffff",
      correctLevel: QRCode.CorrectLevel.H,
      dotScaleTiming: 0.9,
      dotScale: 1,
      dotScaleTiming_V: 0.9,
      dotScaleTiming_h: 0.9,
      dotScaleA: 0.9,
      dotScaleAO: 0.9,
      dotScaleAI: 0.5,
      quietZone: 3,
      logo: qrLogo,
      logoWidth: qrlogoWidth,
      logoHeight: qrlogoWidth,
      logoBackgroundTransparent: qrlogoBackgroundTransparent,
      backgroundImage: qrbackgroundImage,
      backgroundImageAlpha: qrbackgroundImageAlpha,
    };
    if (option?.QRcolorDark) {
      opt.colorDark = option.QRcolorDark;
    }
    if (option?.QRcolorLight) {
      opt.colorLight = option.QRcolorLight;
    }
    qrCodeRef.current.innerHTML = null;
    new QRCode(qrCodeRef.current, opt);
    let rectArr = [];
    let rects = document.getElementsByTagName("rect");
    setTimeout(() => {
      rectArr = [...rects];
      rectArr.map((e) => {
        e.setAttribute("rx", "10");
        e.setAttribute("ry", "10");
      });
    }, 100);
    updateCard({
      _id: _id,
      QRcolorDark: QRcolorDark,
      QRcolorLight: QRcolorLight,
    });
  }
  function openFieldHandle() {
    setOpenFieldModal(true);
    setLoadingFieldType(true);
    fetch("/api/field-type/", {
      method: "POST",
      body: JSON.stringify({ cardType: cardType }),
    })
      .then((res) => res.json())
      .then((data) => {
        let typeArray = [];
        let showSection = true;
        let prevItem = {};
        data.forEach((v, k) => {
          if (prevItem.section == v.section) {
            v.showSection = false;
          } else {
            v.showSection = true;
          }
          typeArray.push(v);
          prevItem = v;
        });
        setFieldTypes(typeArray);
        setLoadingFieldType(false);
      });
  }

  //===== Handle Field ======//
  function fieldInseted(field) {
    setUpdatedField();
    setOpenFieldModal(false);
    let newCardField = cardFields;
    newCardField.unshift(field);
    setCardFields([...newCardField]);
  }
  function fieldUpdated(field) {
    field.edit = false;
    let newCardField = cardFields;
    newCardField.forEach((e, k) => {
      if (e._id == field._id) {
        newCardField[k] = field;
      }
    });
    setCardFields([...newCardField]);
  }
  //======= Output ==========//

  const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,
    minHeight: "100px",

    // change background colour if dragging
    background: isDragging ? "#eee" : "",
    border: isDragging ? "2px dashed #ddd" : "",

    // styles we need to apply on draggables
    ...draggableStyle,
  });

  const getListStyle = (isDraggingOver) => ({
    border: isDraggingOver ? "#ccc" : "#fff",
    padding: grid,
  });
  const grid = 1;
  function onDragEnd(result) {
    // dropped outside the list

    if (!result.destination) {
      return;
    }

    const items = reorder(
      cardFields,
      result.source.index,
      result.destination.index
    );

    setCardFields(items);
    fetch("/api/card-field/reorder/", {
      method: "POST",
      body: JSON.stringify({
        card_id: _id,
        field_id: result.draggableId,
        old_order: result.source.index,
        new_order: result.destination.index,
      }),
    });
  }
  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };
  function publish(field_id, published) {
    fetch("/api/card-field/publish/", {
      method: "POST",
      body: JSON.stringify({
        card_id: _id,
        field_id: field_id,
        published: published,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCardFields(data);
      });
  }
  function deleteField(id) {
    fetch("/api/card-field/delete/", {
      method: "POST",
      body: JSON.stringify({ field_id: id, card_id: _id }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCardFields(data);
      });
  }

  return (
    <>
      <style jsx>
        {`
          body {
            backgroud: #fff;
          }
          .preview-profile h1,
          .preview-profile h2,
          .preview-profile h3,
          .preview-profile h4,
          .preview-profile h5,
          .preview-profile h6,
          .preview-profile p,
          .preview-profile i {
            color: ${color.primary};
          }
          .preview-profile .card,
          .preview-profile button {
            background-color: ${color.secondary};
          }

          .preview-profile .avatar {
            border-radius: ${avatar};
          }
          .preview-profile button {
            border-radius: ${buttonShape};
          }
        `}
      </style>
      <Head>
        <title>{_("Create New Card")}</title>
        <meta name="apple-mobile-web-app-capable" content="yes"></meta>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        ></meta>
      </Head>

      <Script
        src="https://widget.Cloudinary.com/v2.0/global/all.js"
        type="text/javascript"
      ></Script>
      <Script src="/js/easy.qrcode.min.js" type="text/javascript"></Script>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        className="fixed z-50"
      ></ToastContainer>

      <Admin>
        <div className="-mb-24 md:-m-1 md:-mb-20 h-24 bg-white p-2">
          <div className="text-emerald-800 text-sm hidden lg:block pt-14 ltr:pl-16 rtl:pr-16">
            {" "}
            {_("Create New Card")}
          </div>
          <div
            className="bg-white lg:mt-4"
            style={{ paddingLeft: "0px", paddingRight: "0px" }}
          >
            <div className="grid grid-cols-1 lg:grid-cols-6 bg-white">
              <div className=" bg-white mx-1 rounded-xl mt-24 col-span-4 lg:mt-0">
                <div className="w-full px-4 pt-1 border-b overflow-x-auto">
                  <ul
                    className="flex flex-wrap border-gray-200 border-b mb-0"
                    style={{ width: "max-content" }}
                  >
                    <li className="mx-1" onClick={() => setTab("content")}>
                      <a
                        href="#"
                        className={
                          tab == "content"
                            ? "inline-block duration-200 bg-emerald-100 font-bold focus:bg-emerald-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm  text-center active "
                            : "inline-block duration-200 focus:bg-emerald-100 bg-gray-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm font-medium text-center active "
                        }
                      >
                        {_("Content")}
                      </a>
                    </li>
                    <li className="mx-1" onClick={() => setTab("desgin")}>
                      <a
                        href="#"
                        className={
                          tab == "desgin"
                            ? "inline-block duration-200 bg-emerald-100 font-bold focus:bg-emerald-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm  text-center active "
                            : "inline-block duration-200 focus:bg-emerald-100 bg-gray-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm font-medium text-center active "
                        }
                      >
                        {_("Desgin")}
                      </a>
                    </li>
                    <li
                      className="mx-1"
                      onClick={() => {
                        setTab("qrdesgin");
                        setShowQR(true);
                        showQrFn(null);
                      }}
                    >
                      <a
                        href="#"
                        className={
                          tab == "qrdesgin"
                            ? "inline-block duration-200 bg-emerald-100 font-bold focus:bg-emerald-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm  text-center active "
                            : "inline-block duration-200 focus:bg-emerald-100 bg-gray-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm font-medium text-center active "
                        }
                      >
                        {_("QR Desgin")}
                      </a>
                    </li>
                    <li className="mx-1" onClick={() => setTab("settings")}>
                      <a
                        href="#"
                        className={
                          tab == "settings"
                            ? "inline-block duration-200 bg-emerald-100 font-bold focus:bg-emerald-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm  text-center active "
                            : "inline-block duration-200 focus:bg-emerald-100 bg-gray-100 text-emerald-300 rounded-t-lg py-4 px-4 text-sm font-medium text-center active "
                        }
                      >
                        {_("Settings")}
                      </a>
                    </li>
                  </ul>
                </div>

                {tab == "content" && (
                  <div>
                    <div className="mt-2 grid grid-cols-3 border-b-2">
                      <label
                        className="block uppercase text-blueGray-600 text-xs font-bold mt-3 min-w-32"
                        htmlFor="card-type"
                      >
                        {_("Card Type")}
                      </label>
                      <select
                        value={cardType}
                        onChange={(e) => {
                          setCardType(e.target.value);
                          updateCard({ _id: _id, cardType: e.target.value });
                        }}
                        className="border-0 col-span-2 ltr:border-l-2 border-gray-200 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm  w-full ease-linear transition-all duration-150"
                      >
                        <option value="">{_("Select Card Type")}</option>
                        <option value="business_card">
                          {_("Business Card")}
                        </option>
                        <option value="company_profile">
                          {_("Company Profile")}
                        </option>
                        {/* <option value="product_card">
                          {_("Product Card")}
                        </option> */}
                      </select>
                    </div>
                  </div>
                )}
                {cardType && tab == "content" && (
                  <div id="business_card" className=" duration-200 mb-48">
                    <div className="fixed bottom-0 left-0 lg:left-auto w-full lg:w-max z-50 bg-transparent">
                      <button
                        className="w-full p-4  rounded-t-xl font-bold text-white text-xl bg-emerald-300"
                        onClick={openFieldHandle}
                      >
                        {" "}
                        {_("Add New Field")}
                      </button>
                    </div>
                    <div
                      className=" border shadow-md rounded-xl mt-4 w-full text-center p-10 bg-top bg-cover  bg-no-repeat"
                      style={{
                        backgroundSize: "100%",
                        boxShadow: "inset 0 -100px 50px #00000088",
                        backgroundImage: "url(" + cover + ")",
                      }}
                    >
                      <div className="flex justify-between mb-10">
                        <button
                          onClick={openCoverWidget}
                          className="bg-white  relative w-10 h-10 rounded-full"
                        >
                          <i className="fa fa-image text-emerald-500"> </i>
                        </button>
                        <button
                          onClick={() => {
                            const wtd = 1125 + Math.round(Math.random(9));
                            setCover("https://picsum.photos/1152/" + wtd);
                            updateCard({
                              _id: _id,
                              cover: "https://picsum.photos/1152/1125",
                            });
                          }}
                          className="relative  bg-white  w-10 h-10 rounded-full"
                        >
                          <i className="fa fa-images text-emerald-500"> </i>
                        </button>
                        {cover && (
                          <button
                            onClick={() => {
                              setCover(null);
                              updateCard({ _id: _id, cover: null });
                            }}
                            className="bg-white  relative w-10 h-10 rounded-full"
                          >
                            <i className="fa fa-times text-emerald-500"> </i>
                          </button>
                        )}
                      </div>
                      {!pic && (
                        <div
                          className="w-20 h-20 rounded-full bg-gray-200 mx-auto p-5 overflow-hidden cursor-pointer"
                          onClick={openWidget}
                        >
                          {cardType == "company_profile" && (
                            <i className="fa fa-building duration-300 text-4xl text-emerald-500"></i>
                          )}
                          {cardType == "business_card" && (
                            <i className="fa fa-user duration-300 text-4xl text-emerald-500"></i>
                          )}
                          {cardType == "product_card" && (
                            <i className="fa fa-tag duration-300 text-4xl text-emerald-500"></i>
                          )}
                        </div>
                      )}
                      {pic && (
                        <div>
                          <button
                            onClick={() => {
                              setPic(null);
                              updateCard({ _id: _id, pic: null });
                            }}
                            className=" bg-white rounded-full pt-0 p-1 h-6 w-6 ml-6"
                            style={{ position: "absolute" }}
                          >
                            <i className="fa fa-times"></i>
                          </button>
                          <div
                            className="w-20 h-20 rounded-full bg-gray-200 mx-auto  overflow-hidden cursor-pointer"
                            onClick={openWidget}
                          >
                            <img src={pic} className="h-full w-full" />
                          </div>
                        </div>
                      )}
                    </div>
                    <div className="shadow-md p-1 border-gray-200 mt-4 border rounded-xl">
                      <h6 className="md:min-w-full text-blueGray-500 text-xs uppercase font-bold block pt-2 p-4 no-underline">
                        {_("Basic Information")}
                      </h6>
                      <div className="border-2 rounded-md mt-4">
                        <input
                          onBlur={(e) => {
                            if (oldName !== e.target.value) {
                              updateCard({ _id: _id, name: e.target.value });
                            }
                            setOldName(e.target.value);
                          }}
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                          placeholder={_("Enter your Name")}
                          className="text-center  col-span-2 px-3 py-3  placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-xl    w-full ease-linear transition-all duration-150"
                        ></input>
                      </div>
                      {cardType == "business_card" && (
                        <div>
                          <div className=" mt-4 grid grid-cols-12">
                            <div className="col-span-12 md:col-span-6 grid grid-cols-6 ">
                              <i className="fa fa-briefcase col-span-1  p-2 pt-3 px-3 h-10  text-gray-400"></i>
                              <AsyncCreatableSelect
                                isClearable={true}
                                openMenuOnClick={false}
                                components={{
                                  IndicatorSeparator: () => null,
                                  DropdownIndicator: () => null,
                                }}
                                onChange={handleJobTitleChange}
                                value={jobTitle}
                                options={jobTitleList}
                                onCreateOption={onCreateJobTitleOption}
                                cacheOptions
                                defaultOptions
                                loadOptions={promiseJobTitleOptions}
                                placeholder={_("Your Job Title")}
                                className="col-span-5 h-10  placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm   w-full ease-linear transition-all duration-150"
                              />
                            </div>
                            <div className="col-span-12 md:col-span-6 grid grid-cols-6">
                              <i className="fa fa-users col-span-1 rounded-l-md  p-2 pt-3 px-3 h-10  text-gray-400"></i>
                              <AsyncCreatableSelect
                                isClearable={true}
                                openMenuOnClick={false}
                                components={{
                                  IndicatorSeparator: () => null,
                                  DropdownIndicator: () => null,
                                }}
                                onChange={handleDepartmentChange}
                                value={department}
                                options={departmentList}
                                onCreateOption={onCreateDepartmentOption}
                                cacheOptions
                                defaultOptions
                                loadOptions={promiseDepartmentOptions}
                                placeholder={_("Your Department")}
                                className="col-span-5 h-10  placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm   w-full ease-linear transition-all duration-150"
                              />
                            </div>

                            <i className="fa fa-building col-span-2 md:col-span-1 pt-3 p-2 px-3 h-10  text-gray-400"></i>
                            <AsyncCreatableSelect
                              isClearable={true}
                              openMenuOnClick={false}
                              components={{
                                IndicatorSeparator: () => null,
                                DropdownIndicator: () => null,
                                Option: IconOption,
                              }}
                              onChange={handleCompanyChange}
                              value={companyName}
                              options={companiesList}
                              onCreateOption={onCreateCompanyOption}
                              cacheOptions
                              defaultOptions
                              loadOptions={promiseCompanyOptions}
                              placeholder={_("Your Company Name")}
                              className="col-span-10 md:col-span-11 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm   w-full ease-linear transition-all duration-150"
                            />
                          </div>
                        </div>
                      )}
                      <div className="border-b-2 mt-4">
                        <textarea
                          onBlur={(e) => {
                            updateCard({
                              _id: _id,
                              description: e.target.value,
                            });
                          }}
                          value={description}
                          onChange={(e) => {
                            setDescription(e.target.value);
                          }}
                          placeholder={
                            cardType == "business_card"
                              ? _("Headline")
                              : _("Description")
                          }
                          className="border-0 border-gray-200 ltr:border-l-2 col-span-2 px-3 py-3  placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm   w-full ease-linear transition-all duration-150"
                        ></textarea>
                      </div>
                    </div>

                    {updatedField && (
                      <div
                        id="new-field"
                        className="shadow-md p-2  mt-4 outline-dashed outline-2 outline-blue-400 rounded-xl"
                      >
                        <div className="mb-16">
                          <h5 className="px-2 ltr:float-left rtl:float-right">
                            <i className="text-gray-400  fa fa-link"></i>{" "}
                            {_("New field")}
                          </h5>
                          <button
                            className="ltr:float-right rtl:float-left p-2 text-lg text-blue-600"
                            onClick={() => {
                              setUpdatedField();
                            }}
                          >
                            {_("Cancel")}
                          </button>
                        </div>
                        {updatedField.fieldType == "phone" && (
                          <Phone
                            cardId={_id}
                            field={updatedField}
                            isDone={fieldInseted}
                          />
                        )}
                        {updatedField.fieldType == "email" && (
                          <Email
                            cardId={_id}
                            field={updatedField}
                            isDone={fieldInseted}
                          />
                        )}
                        {updatedField.fieldType == "url" && (
                          <URL
                            cardId={_id}
                            field={updatedField}
                            isDone={fieldInseted}
                          />
                        )}
                        {updatedField.fieldType == "text" && (
                          <Text
                            cardId={_id}
                            field={updatedField}
                            isDone={fieldInseted}
                          />
                        )}
                        {updatedField.fieldType == "address" && (
                          <Address
                            cardId={_id}
                            field={updatedField}
                            isDone={fieldInseted}
                          />
                        )}
                        {updatedField.fieldType == "file" && (
                          <File
                            cardId={_id}
                            field={updatedField}
                            isDone={fieldInseted}
                          />
                        )}
                      </div>
                    )}

                    {cardFields.length > 0 && (
                      <div className="mt-4 border border-gray-200 rounded-xl shadow">
                        <DragDropContext onDragEnd={onDragEnd}>
                          <Droppable droppableId="droppable">
                            {(provided, snapshot) => (
                              <div
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                                style={getListStyle(snapshot.isDraggingOver)}
                              >
                                {cardFields.map((item, index) => (
                                  <Draggable
                                    key={item._id}
                                    draggableId={item._id}
                                    index={index}
                                  >
                                    {(provided, snapshot) => (
                                      <div
                                        className="hover:bg-blue-50 hover:border-dashed hover:border-2 border-blue-500  rounded-xl duration-100"
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        style={getItemStyle(
                                          snapshot.isDragging,
                                          provided.draggableProps.style
                                        )}
                                      >
                                        <div className="flex flex-wrap field">
                                          {!item.edit && (
                                            <div className="flex w-full">
                                              <div className="w-2/12 md:w-1/12 px-2 my-4">
                                                <button
                                                  onClick={() => {
                                                    let temp = cardFields;
                                                    temp[index].edit = true;
                                                    setCardFields([...temp]);
                                                  }}
                                                  className="bg-gray-50 hover:bg-gray-200 font-bold  h-12 w-12  rounded-t-full text-gray-400 hover:text-gray-800 duration-300"
                                                >
                                                  <i className="  fa fa-pencil"></i>
                                                </button>
                                                <button
                                                  onClick={() => {
                                                    let temp = showDeleteArray;
                                                    temp[index] = true;
                                                    setShowDeleteArray([
                                                      ...temp,
                                                    ]);
                                                  }}
                                                  className="bg-gray-50 hover:bg-gray-200 font-bold h-12 w-12  rounded-b-full text-red-300 hover:text-red-600 duration-300"
                                                >
                                                  <i className=" fa fa-trash"></i>
                                                </button>
                                              </div>
                                              <div className="w-8/12 md:w-10/12">
                                                {item.field.fieldType ==
                                                  "phone" && (
                                                  <PhoneComponent
                                                    field={item}
                                                  />
                                                )}
                                                {item.field.fieldType ==
                                                  "email" && (
                                                  <EmailComponent
                                                    field={item}
                                                  />
                                                )}
                                                {item.field.fieldType ==
                                                  "url" && (
                                                  <UrlComponent field={item} />
                                                )}
                                                {item.field.fieldType ==
                                                  "text" && (
                                                  <TextComponent field={item} />
                                                )}
                                                {item.field.fieldType ==
                                                  "address" && (
                                                  <AddressComponent
                                                    field={item}
                                                  />
                                                )}
                                              </div>
                                              <div className=" w-2/12 md:w-1/12 flex">
                                                <div
                                                  className="my-4"
                                                  style={{ height: "100%" }}
                                                >
                                                  {item.published && (
                                                    <button
                                                      onClick={() => {
                                                        publish(
                                                          item._id,
                                                          false
                                                        );
                                                      }}
                                                      className="bg-gray-50 hover:bg-gray-200 font-bold h-12 w-12 my-6 rounded-full text-blue-300 hover:text-blue-600 duration-300"
                                                    >
                                                      <i className="text-xl fa fa-eye"></i>
                                                    </button>
                                                  )}
                                                  {!item.published && (
                                                    <button
                                                      onClick={() => {
                                                        publish(item._id, true);
                                                      }}
                                                      className="bg-gray-50 hover:bg-gray-200 font-bold h-12 w-12 my-6  rounded-full text-gray-300 hover:text-gray-600 duration-300"
                                                    >
                                                      <i className="text-xl fa fa-eye-slash"></i>
                                                    </button>
                                                  )}
                                                </div>
                                                <div className=" move-tag opacity-0 hover:opacity-100 text-blue-500 duration-300 flex">
                                                  <i className="fas fa-ellipsis-v text-4xl"></i>
                                                  <i className="fas fa-ellipsis-v text-4xl"></i>
                                                </div>
                                              </div>
                                            </div>
                                          )}
                                          {item.edit && (
                                            <div
                                              className={
                                                item.edit
                                                  ? "w-full duration-500 opacity-100 h-auto"
                                                  : "w-full duration-500 opacity-0 h-0"
                                              }
                                            >
                                              <div className="mb-16">
                                                <h5 className="px-2 ltr:float-left rtl:float-right">
                                                  <i className="text-gray-400  fa fa-link"></i>{" "}
                                                  {_("Edit field")}
                                                </h5>
                                                <button
                                                  className="ltr:float-right rtl:float-left p-2 text-lg text-blue-600"
                                                  onClick={() => {
                                                    let temp = cardFields;
                                                    temp[index].edit = false;
                                                    setCardFields([...temp]);
                                                  }}
                                                >
                                                  {_("Cancel")}
                                                </button>
                                              </div>
                                              {item.field.fieldType ==
                                                "phone" && (
                                                <Phone
                                                  field={item.field}
                                                  phoneValue={item.phone}
                                                  prefixValue={item.prefix}
                                                  viewAccessValue={
                                                    item.viewAccess
                                                  }
                                                  cardId={_id}
                                                  field_id={item._id}
                                                  isDone={fieldUpdated}
                                                  labelValue={item.label}
                                                  titleValue={item.title}
                                                />
                                              )}
                                              {item.field.fieldType ==
                                                "email" && (
                                                <Email
                                                  field={item.field}
                                                  emailValue={item.email}
                                                  viewAccessValue={
                                                    item.viewAccess
                                                  }
                                                  cardId={_id}
                                                  field_id={item._id}
                                                  isDone={fieldUpdated}
                                                  labelValue={item.label}
                                                  titleValue={item.title}
                                                />
                                              )}
                                              {item.field.fieldType ==
                                                "url" && (
                                                <URL
                                                  field={item.field}
                                                  urlValue={item.url}
                                                  viewAccessValue={
                                                    item.viewAccess
                                                  }
                                                  cardId={_id}
                                                  field_id={item._id}
                                                  isDone={fieldUpdated}
                                                  labelValue={item.label}
                                                  titleValue={item.title}
                                                />
                                              )}
                                              {item.field.fieldType ==
                                                "text" && (
                                                <Text
                                                  field={item.field}
                                                  textValue={item.text}
                                                  viewAccessValue={
                                                    item.viewAccess
                                                  }
                                                  cardId={_id}
                                                  field_id={item._id}
                                                  isDone={fieldUpdated}
                                                  labelValue={item.label}
                                                  titleValue={item.title}
                                                />
                                              )}
                                              {item.field.fieldType ==
                                                "address" && (
                                                <Address
                                                  field={item.field}
                                                  addressValue={item.address}
                                                  countryValue={item.country}
                                                  viewAccessValue={
                                                    item.viewAccess
                                                  }
                                                  cardId={_id}
                                                  field_id={item._id}
                                                  isDone={fieldUpdated}
                                                  labelValue={item.label}
                                                  titleValue={item.title}
                                                />
                                              )}
                                            </div>
                                          )}
                                          <div
                                            className={
                                              showDeleteArray[index]
                                                ? "w-full duration-500 opacity-100 h-auto"
                                                : "w-full duration-1000 opacity-0 h-0"
                                            }
                                            style={{ overflow: "hidden" }}
                                          >
                                            <div
                                              className={
                                                "flex justify-around bg-white p-2 border-t-2"
                                              }
                                            >
                                              <button
                                                onClick={() => {
                                                  let temp = showDeleteArray;
                                                  temp[index] = false;
                                                  setShowDeleteArray([...temp]);
                                                  deleteField(item._id);
                                                }}
                                                className="bg-red-600 h-12 hover:bg-red-900 text-white mx-3 font-bold w-6/12 rounded-xl duration-300"
                                              >
                                                {_("Delete")}
                                              </button>
                                              <button
                                                onClick={() => {
                                                  let temp = showDeleteArray;
                                                  temp[index] = false;
                                                  setShowDeleteArray([...temp]);
                                                }}
                                                className="bg-gray-600 hover:bg-gray-900 text-white mx-3 font-bold w-6/12 rounded-xl duration-300"
                                              >
                                                {_("Cancel")}
                                              </button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    )}
                                  </Draggable>
                                ))}
                                {provided.placeholder}
                              </div>
                            )}
                          </Droppable>
                        </DragDropContext>
                      </div>
                    )}
                  </div>
                )}
                {tab == "desgin" && (
                  <div className="mt-4 border rounded-md mb-4 p-2">
                    <div className="grid grid-cols-12">
                      <div className="col-span-12 md:col-span-2">
                        <label className="block uppercase text-blueGray-600 text-xs font-bold mt-3 min-w-32">
                          {_("Theme Color")}
                        </label>
                      </div>
                      <div className="col-span-12 md:col-span-10 grid grid-cols-4 md:grid-cols-12">
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgba(0,0,0,255)",
                              secondary: "rgba(255,255,255,255)",
                              name: "default",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgba(0,0,0,255)",
                                secondary: "rgba(255,255,255,255)",
                                name: "default",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-black w-full h-full"></div>
                          <div className="bg-white w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(75 85 99,255)",
                              secondary: "rgba(228, 228, 231,255)",
                              name: "gray",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(75 85 99,255)",
                                secondary: "rgba(228, 228, 231,255)",
                                name: "gray",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-gray-600 w-full h-full"></div>
                          <div className="bg-gray-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgba(220, 38, 38,255)",
                              secondary: "rgba(254, 202, 202,255)",
                              name: "red",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgba(220, 38, 38,255)",
                                secondary: "rgba(254, 202, 202,255)",
                                name: "red",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-red-600 w-full h-full"></div>
                          <div className="bg-red-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(234,88,12)",
                              secondary: "rgba(254, 215, 170,255)",
                              name: "orange",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(234,88,12)",
                                secondary: "rgba(254, 215, 170,255)",
                                name: "orange",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-orange-600 w-full h-full"></div>
                          <div className="bg-orange-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(202,138,4)",
                              secondary: "rgb(254,240,138)",
                              name: "yellow",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(202,138,4)",
                                secondary: "rgb(254,240,138)",
                                name: "yellow",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-yellow-600 w-full h-full"></div>
                          <div className="bg-yellow-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(22,163,74)",
                              secondary: "rgb(187,247,208)",
                              name: "green",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(22,163,74)",
                                secondary: "rgb(187,247,208)",
                                name: "green",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-emerald-300 w-full h-full"></div>
                          <div className="bg-emerald-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(13,148,136)",
                              secondary: "rgba(153, 246, 228,255)",
                              name: "teal",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(13,148,136)",
                                secondary: "rgba(153, 246, 228,255)",
                                name: "teal",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-teal-600 w-full h-full"></div>
                          <div className="bg-teal-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(8 145 178)",
                              secondary: "rgb(165 243 252)",
                              name: "cyan",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(8 145 178)",
                                secondary: "rgb(165 243 252)",
                                name: "cyan",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-emerald-300 w-full h-full"></div>
                          <div className="bg-emerald-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(37,99,235)",
                              secondary: "rgb(191,219,254)",
                              name: "blue",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(37,99,235)",
                                secondary: "rgb(191,219,254)",
                                name: "blue",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-blue-600 w-full h-full"></div>
                          <div className="bg-blue-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(79,70,229)",
                              secondary: "rgb(199,210,254)",
                              name: "indigo",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(79,70,229)",
                                secondary: "rgb(199,210,254)",
                                name: "indigo",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-indigo-600 w-full h-full"></div>
                          <div className="bg-indigo-200 w-full h-full"></div>
                        </button>

                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(147,51,234)",
                              secondary: "rgba(233, 213, 255,255)",
                              name: "purple",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(147,51,234)",
                                secondary: "rgba(233, 213, 255,255)",
                                name: "purple",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-purple-600 w-full h-full"></div>
                          <div className="bg-purple-200 w-full h-full"></div>
                        </button>
                        <button
                          onClick={() => {
                            setColor({
                              primary: "rgb(219,39,119)",
                              secondary: "rgb(251,207,232)",
                              name: "pink",
                            });
                            updateCard({
                              _id: _id,
                              color: {
                                primary: "rgb(219,39,119)",
                                secondary: "rgb(251,207,232)",
                                name: "pink",
                              },
                            });
                          }}
                          className="w-10 h-10 rounded-full grid grid-cols-2 overflow-hidden border-2"
                        >
                          <div className="bg-pink-600 w-full h-full"></div>
                          <div className="bg-pink-200 w-full h-full"></div>
                        </button>
                      </div>
                    </div>
                    <div className="grid grid-cols-12 my-10">
                      <div className="col-span-12 md:col-span-6 grid md:grid-cols-6">
                        <div className="col-span-6">
                          <label className="block uppercase text-blueGray-600 text-xs font-bold mt-3 min-w-32">
                            {_("Primary color")}
                          </label>
                        </div>
                        <div className="col-span-6">
                          <HexColorInput
                            color={color.primary}
                            placeholder={_("Enter primary color")}
                            onChange={(e) => {
                              setTimeout(() => {
                                setColor({
                                  secondary: color.secondary,
                                  primary: e,
                                  name: "custom",
                                });
                              }, 50);
                            }}
                            onBlur={() => {
                              updateCard({ _id: _id, color: color });
                            }}
                            className="rounded-sm w-48 h-8 border text-sm"
                          />
                          <RgbaStringColorPicker
                            color={color.primary}
                            onChange={(e) => {
                              setTimeout(() => {
                                setColor({
                                  primary: e,
                                  secondary: color.secondary,
                                  name: "custom",
                                });
                              }, 50);
                            }}
                            onBlur={() => {
                              updateCard({ _id: _id, color: color });
                            }}
                          />
                        </div>
                      </div>
                      <div className="col-span-6 grid grid-cols-6">
                        <div className="col-span-6">
                          <label className="block uppercase text-blueGray-600 text-xs font-bold mt-3 min-w-32">
                            {_("Secondary color")}
                          </label>
                        </div>
                        <div className="col-span-6">
                          <HexColorInput
                            color={color.secondary}
                            placeholder={_("Enter secondary color")}
                            onChange={(e) => {
                              setTimeout(() => {
                                setColor({
                                  primary: color.primary,
                                  secondary: e,
                                  name: "custom",
                                });
                              }, 50);
                            }}
                            onBlur={() => {
                              updateCard({ _id: _id, color: color });
                            }}
                            className="rounded-sm w-48 h-8 border text-sm"
                          />
                          <RgbaStringColorPicker
                            color={color.secondary}
                            onChange={(e) => {
                              setTimeout(() => {
                                setColor({
                                  primary: color.primary,
                                  secondary: e,
                                  name: "custom",
                                });
                              }, 50);
                            }}
                            onBlur={() => {
                              updateCard({ _id: _id, color: color });
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    {pic && (
                      <div className="mt-10 grid md:grid-cols-12">
                        <div className="col-span-2">
                          <label className="block uppercase text-blueGray-600 text-xs font-bold mt-3 min-w-32">
                            {_("Avatar Shape")}
                          </label>
                        </div>
                        <div className="md:col-span-10 grid grid-cols-2 md:grid-cols-4">
                          <button
                            onClick={() => {
                              setAvatar("9999px");
                              updateCard({ _id: _id, avatar: "9999px" });
                            }}
                            className={
                              avatar == "9999px"
                                ? "w-32 h-32 border-2 duration-200 border-black rounded-full overflow-hidden"
                                : "w-32 h-32 border-2 rounded-full overflow-hidden"
                            }
                          >
                            <img src={pic} />
                          </button>
                          <button
                            onClick={() => {
                              setAvatar("1rem");
                              updateCard({ _id: _id, avatar: "1rem" });
                            }}
                            className={
                              avatar == "1rem"
                                ? "w-32 h-32 border-2 duration-200 border-black  rounded-2xl overflow-hidden"
                                : "w-32 h-32 border-2 rounded-2xl overflow-hidden"
                            }
                          >
                            <img src={pic} />
                          </button>
                          <button
                            onClick={() => {
                              setAvatar("0.375rem");
                              updateCard({ _id: _id, avatar: "0.375rem" });
                            }}
                            className={
                              avatar == "0.375rem"
                                ? "w-32 h-32 border-2 duration-200 border-black rounded-md overflow-hidden"
                                : "w-32 h-32 border-2 rounded-md overflow-hidden"
                            }
                          >
                            <img src={pic} />
                          </button>
                        </div>
                      </div>
                    )}

                    <div className="mt-10 grid md:grid-cols-12">
                      <div className="col-span-2">
                        <label className="block text-xs font-bold uppercase">
                          {_("Button Shape")}
                        </label>
                      </div>
                      <div className="col-span-10 grid grid-cols-3">
                        <button
                          onClick={() => {
                            setButtonShape("9999px");
                            updateCard({ _id: _id, buttonShape: "9999px" });
                          }}
                          className={
                            buttonShape == "9999px"
                              ? "bg-white border-2 border-black p-3 m-2 rounded-full h-12 w-12 shadow-md"
                              : "bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md"
                          }
                        >
                          <i className="fa fa-phone"></i>
                        </button>
                        <button
                          onClick={() => {
                            setButtonShape("1rem");
                            updateCard({ _id: _id, buttonShape: "1rem" });
                          }}
                          className={
                            buttonShape == "1rem"
                              ? "bg-white border-2 border-black p-3 m-2 rounded-2xl h-12 w-12 shadow-md"
                              : "bg-white border p-3 m-2 rounded-2xl h-12 w-12 shadow-md"
                          }
                        >
                          <i className="fa fa-phone"></i>
                        </button>
                        <button
                          onClick={() => {
                            setButtonShape("0.375rem");
                            updateCard({ _id: _id, buttonShape: "0.375rem" });
                          }}
                          className={
                            buttonShape == "0.375rem"
                              ? "bg-white border-2 border-black p-3 m-2 rounded-md h-12 w-12 shadow-md"
                              : "bg-white border p-3 m-2 rounded-md h-12 w-12 shadow-md"
                          }
                        >
                          <i className="fa fa-phone"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                )}
                {tab == "qrdesgin" && (
                  <div className="mt-4">
                    <div className="grid md:grid-cols-12">
                      <div className="md:col-span-2">
                        <label className="uppercase font-bold text-xs text-blueGray-600">
                          {_("Foreground Color")}
                        </label>
                      </div>
                      <div className="col-span-10 grid grid-cols-4 md:grid-cols-12">
                        <button
                          onClick={() => {
                            setQRcolorDark("#000000");
                            showQrFn({ QRcolorDark: "#000000" });
                          }}
                          className="h-10 w-10 bg-black border-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#2d3748");
                            showQrFn({ QRcolorDark: "#2d3748" });
                          }}
                          className="h-10 w-10 bg-gray-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#9b2c2c");
                            showQrFn({ QRcolorDark: "#9b2c2c" });
                          }}
                          className="h-10 w-10 bg-red-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#9c4221");
                            showQrFn({ QRcolorDark: "#9c4221" });
                          }}
                          className="h-10 w-10 bg-orange-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#975a16");
                            showQrFn({ QRcolorDark: "#975a16" });
                          }}
                          className="h-10 w-10 bg-yellow-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#276749");
                            showQrFn({ QRcolorDark: "#276749" });
                          }}
                          className="h-10 w-10 bg-emerald-500 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#285e61");
                            showQrFn({ QRcolorDark: "#285e61" });
                          }}
                          className="h-10 w-10 bg-teal-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#155f75");
                            showQrFn({ QRcolorDark: "#155f75" });
                          }}
                          className="h-10 w-10 bg-emerald-500 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#2c5282");
                            showQrFn({ QRcolorDark: "#2c5282" });
                          }}
                          className="h-10 w-10 bg-blue-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#434190");
                            showQrFn({ QRcolorDark: "#434190" });
                          }}
                          className="h-10 w-10 bg-indigo-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#553c9a");
                            showQrFn({ QRcolorDark: "#553c9a" });
                          }}
                          className="h-10 w-10 bg-purple-800 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQRcolorDark("#97266d");
                            showQrFn({ QRcolorDark: "#97266d" });
                          }}
                          className="h-10 w-10 bg-pink-800 broder-2 rounded-full"
                        ></button>
                      </div>
                    </div>
                    <div className="mt-10 grid md:grid-cols-12">
                      <div className="md:col-span-2">
                        <label className="uppercase font-bold text-xs text-blueGray-600">
                          {_("Background Color")}
                        </label>
                      </div>
                      <div className="col-span-10 grid grid-cols-4 md:grid-cols-12">
                        <button
                          onClick={() => {
                            setQrColorLight("#ffffff");
                            showQrFn({ QRcolorLight: "#ffffff" });
                          }}
                          className="h-10 w-10 bg-white border-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#f7fafc");
                            showQrFn({ QRcolorLight: "#f7fafc" });
                          }}
                          className="h-10 w-10 bg-gray-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#fff5f5");
                            showQrFn({ QRcolorLight: "#fff5f5" });
                          }}
                          className="h-10 w-10 bg-red-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#fffaf0");
                            showQrFn({ QRcolorLight: "#fffaf0" });
                          }}
                          className="h-10 w-10 bg-orange-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#fffff0");
                            showQrFn({ QRcolorLight: "#fffff0" });
                          }}
                          className="h-10 w-10 bg-yellow-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#f0fff4");
                            showQrFn({ QRcolorLight: "#f0fff4" });
                          }}
                          className="h-10 w-10 bg-emerald-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#e6fffa");
                            showQrFn({ QRcolorLight: "#e6fffa" });
                          }}
                          className="h-10 w-10 bg-teal-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#cffafe");
                            showQrFn({ QRcolorLight: "#cffafe" });
                          }}
                          className="h-10 w-10 bg-emerald-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#ebf8ff");
                            showQrFn({ QRcolorLight: "#ebf8ff" });
                          }}
                          className="h-10 w-10 bg-blue-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#ebf4ff");
                            showQrFn({ QRcolorLight: "#ebf4ff" });
                          }}
                          className="h-10 w-10 bg-indigo-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#faf5ff");
                            showQrFn({ QRcolorLight: "#faf5ff" });
                          }}
                          className="h-10 w-10 bg-purple-100 broder-2 rounded-full"
                        ></button>
                        <button
                          onClick={() => {
                            setQrColorLight("#fff5f7");
                            showQrFn({ QRcolorLight: "#fff5f7" });
                          }}
                          className="h-10 w-10 bg-pink-100 broder-2 rounded-full"
                        ></button>
                      </div>
                    </div>
                  </div>
                )}
                {tab == "settings" && (
                  <div className="mt-4">
                    <div className="w-full flex flex-wrap">
                      <div className="w-full md:w-2/12 my-2">
                        {_("Update Card username")}
                      </div>
                      <div className="w-full md:w-10/12 flex">
                        <div className="w-10/12">
                          <input
                            onBlur={checkAvailbilty}
                            onChange={(e) => {
                              setSlug(
                                e.target.value
                                  .replace(
                                    /[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g,
                                    "."
                                  )
                                  .toLowerCase()
                              );
                            }}
                            className={
                              canPost
                                ? "form-control font-bold"
                                : "form-control text-red-500"
                            }
                            value={slug}
                          />
                          {!suggest && (
                            <div className="text-red-500">
                              {_(
                                "if slug updated all old QR code and NFC Url will not work"
                              )}
                            </div>
                          )}
                          {suggest && !canPost && (
                            <div className="text-red-500">
                              {_("The slug is already teken you can use:")}{" "}
                              {suggest}
                            </div>
                          )}
                        </div>
                        <div className="w-2/12">
                          <button
                            onClick={() => updateSlug(_id, slug)}
                            disabled={!canPost}
                            className="border disabled:text-gray-500 w-full font-bold border-emerald-300 text-emerald-400 p-2  rounded text-center"
                          >
                            {_("Update")}
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="w-full flex flex-wrap mt-4">
                      <div className="w-full md:w-6/12 my-2">
                        {_("Allow Connections via:")}
                      </div>
                      <div className="w-full md:w-6/12 flex flex-wrap">
                        <div className="w-2/12 mt-4">{_("NFC")}</div>
                        <div className="w-10/12 mt-4">
                          <Toggle
                            onChange={(e) => {
                              setCard((prev) => {
                                prev.nfc?prev.nfc=false:prev.nfc=true;
                                return {...prev};
                              });
                              updateCard({_id:_id,nfc:e})
                            }}
                            checked={card.nfc}
                          ></Toggle>
                        </div>
                        <div className="w-2/12 mt-4">{_("QR Code")}</div>
                        <div className="w-10/12 mt-4">
                          <Toggle
                          onChange={(e) => {
                            setCard((prev) => {
                              prev.qr?prev.qr=false:prev.qr=true;
                              return {...prev};
                            });
                            updateCard({_id:_id,qr:e})
                          }}
                           checked={card.qr}></Toggle>
                        </div>
                      </div>
                    </div>
                    <div className="mt-36 mb-4 w-full flex flex-wrap">
                      <div className="w-full md:w-2/12">
                        {_('Delete Card')}
                      </div>
                      <div className="w-full md:w-10/12 text-center">
                        <p>{_('Deleting Card will remove all card information and insights related to')}</p>
                        <button onClick={()=>deleteCard(_id)} className="mx-auto border border-red-500 p-3 font-bold text-red-500 rounded">{_('Delete Card Permanently')}</button>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className="hidden lg:block col-span-2 xl:p-4 mx-1  rounded-md xl:fixed xl:top-24 ltr:xl:right-16 rtl:xl:left-16 mt-12 lg:mt-0 overflow-hidden">
                <div
                  className="preview-profile mx-auto shadow-lg"
                  style={{
                    width: "375px",
                    height: "812px",
                    overflow: "hidden !important",
                    border: "8px solid #000",
                    borderRadius: "46px",
                  }}
                >
                  <div className="bg-white h-6 hidden lg:block">
                    <div className="bg-emerald-200 h-4 w-8 absolute left-16 top-9 rounded-full text-xs text-center text-white">
                      {new Date().getHours() + ":" + new Date().getMinutes()}
                    </div>
                    <div className="bg-black mx-auto w-36 h-6  absolute left-1/3 rounded-b-2xl"></div>
                    <div>
                      <i className="fa-solid fa-battery-full absolute right-16 top-9 text-lg"></i>
                      <i className="fas fa-signal absolute right-24 top-9 text-sm"></i>
                    </div>
                  </div>
                  <main className="profile-page h-full bg-blueGray-200 overflow-y-auto overflow-x-hidden">
                    <section className="relative block h-2/4">
                      <div
                        className="absolute top-0 w-full h-full bg-top bg-cover  bg-no-repeat"
                        style={{
                          backgroundImage: "url(" + cover + ")",
                          boxShadow: "inset 0 -200px 100px #00000088",
                        }}
                      >
                        <button
                          onClick={() => {
                            showQrFn();
                          }}
                          className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute right-3 top-3"
                        >
                          <i className="fas text-md fa-qrcode"></i>
                        </button>
                        <div
                          className={
                            showQR
                              ? "relative mx-auto mt-10  overflow-hidden qrCodeContainer"
                              : "relative mx-auto mt-10 h-32 w-32 overflow-hidden qrCodeContainer hidden"
                          }
                          ref={qrCodeRef}
                          style={{
                            width: "250px",
                            height: "250px",
                            padding: "34px",
                          }}
                        ></div>
                      </div>
                      <div
                        className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-16"
                        style={{ transform: "translateZ(0)" }}
                      >
                        <svg
                          className="absolute bottom-0 overflow-hidden"
                          xmlns="http://www.w3.org/2000/svg"
                          preserveAspectRatio="none"
                          version="1.1"
                          viewBox="0 0 2560 100"
                          x="0"
                          y="0"
                        >
                          <polygon
                            className="text-blueGray-200 fill-current"
                            points="2560 0 2560 100 0 100"
                          ></polygon>
                        </svg>
                      </div>
                    </section>
                    <section className="relative py-16 bg-blueGray-200">
                      <div className="container mx-auto px-4">
                        <div
                          className={
                            showQR
                              ? "profile-card card relative flex flex-col min-w-0 break-words w-full mb-6 shadow-xl rounded-lg duration-200 -mt-24"
                              : "profile-card card relative flex flex-col min-w-0 break-words w-full mb-6 shadow-xl rounded-lg duration-200 -mt-64"
                          }
                        >
                          <div className="px-6">
                            {pic && (
                              <div className="flex flex-wrap justify-center">
                                <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center pb-10">
                                  <div className="relative">
                                    <div
                                      className="avatar shadow-xl border border-gray-400 bg-gray-50 overflow-hidden  h-auto align-middle absolute -m-16 -ml-20 lg:-ml-16 max-w-150-px"
                                      onClick={openWidget}
                                    >
                                      <img
                                        src={pic}
                                        className="h-full w-full"
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            )}
                            <div className="text-center mt-12">
                              <button className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute ltr:right-3 rtl:left-3 top-3">
                                <i className="fas text-md fa-share-alt"></i>
                              </button>
                              <h3 className="text-2xl lg:text-4xl font-semibold leading-normal mb-1 text-blueGray-700">
                                {name}
                              </h3>
                              {jobTitle && (
                                <h4 className="text-xl lg:text-2xl font-semibold mb-1">
                                  {jobTitle.label}
                                </h4>
                              )}

                              {department && (
                                <h4 className="text-xs lg:text-sm leading-normal mt-0 mb-2 text-blueGray-600 font-bold">
                                  {department.label}
                                </h4>
                              )}
                              {companyName && (
                                <h4 className="mb-2 text-sm lg:text-md text-blueGray-600 mt-2">
                                  {companyName.pic && (
                                    <img
                                      src={companyName.pic}
                                      style={{
                                        height: "32px",
                                        width: "32px",
                                        borderRadius: "100px",
                                        display: "inline",
                                        margin: "0 4px",
                                      }}
                                    />
                                  )}
                                  {companyName.label}
                                </h4>
                              )}
                            </div>
                            <div className="mt-2 py-5 border-t border-blueGray-200 text-center">
                              {description && (
                                <p className="whitespace-pre-line">
                                  {description}
                                </p>
                              )}
                              <div className="w-full  px-1 ltr:text-left">
                                {cardFields.map((item, k) => {
                                  return (
                                    <div key={k}>
                                      {item.field.fieldType == "phone" &&
                                        item.published && (
                                          <PhoneOutput field={item} />
                                        )}
                                      {item.field.fieldType == "email" &&
                                        item.published && (
                                          <EmailOutput field={item} />
                                        )}
                                      {item.field.fieldType == "url" &&
                                        item.published && (
                                          <UrlOutput field={item} />
                                        )}
                                      {item.field.fieldType == "text" &&
                                        item.published && (
                                          <TextOutput field={item} />
                                        )}
                                      {item.field.fieldType == "address" &&
                                        item.published && (
                                          <AddressOutput field={item} />
                                        )}
                                    </div>
                                  );
                                })}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </main>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Admin>
      <Modal
        backdrop={"static"}
        overflow={true}
        size="lg"
        open={openFieldModal}
        onClose={() => {
          setOpenFieldModal(false);
        }}
      >
        <Modal.Header>
          <Modal.Title>{_("New Field")}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {loadingFieldType && (
            <div>
              <Placeholder.Grid />
              <Placeholder.Paragraph />
            </div>
          )}
          {!loadingFieldType && (
            <div className="flex flex-wrap justify-start">
              {fieldTypes.map((e, k) => {
                return (
                  <>
                    {e.showSection && (
                      <h5 className="w-full">{_(e.section)}</h5>
                    )}

                    <div className="w-6/12 md:w-4/12">
                      <button
                        onClick={() => {
                          setUpdatedField(e);
                          setOpenFieldModal(false);
                          setTimeout(() => {
                            window.location.hash = "#new-field";
                          }, 500);
                        }}
                        className="m-2 text-start flex rounded-xl  text-emerald-300 hover:text-white focus:text-white hover:bg-emerald-300 focus:bg-emerald-300 duration-300"
                      >
                        <i
                          className={
                            " border-2 border-emerald-300 rounded-xl p-3 text-xl text-start " +
                            e.icon
                          }
                        ></i>
                        <span className="mx-2 pt-3">{_(e.name)}</span>
                      </button>
                    </div>
                  </>
                );
              })}
            </div>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => {
              setOpenFieldModal(false);
            }}
            appearance="subtle"
          >
            {_("Cancel")}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
export function getServerSideProps(context) {
  return {
    props: { params: context.params },
  };
}
