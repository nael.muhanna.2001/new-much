import Head from "next/head";
import Admin from "../../../layouts/Admin";
import { translation } from "../../../lib/translations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import Image from "next/image";
import "rsuite/dist/rsuite.min.css";
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function CreateCard() {
  const { data: session, status } = useSession();
  const router = useRouter();
  const [slug, setSlug] = useState();
  const [canPost, setCanPost] = useState(false);
  const [suggest, setSuggest] = useState();
  const [checking, setChecking] = useState(false);
  const [creating, setCreating] = useState(false);
  function checkAvailbilty() {
    setChecking(true);
    fetch("/api/cards/check-name", {
      method: "POST",
      body: JSON.stringify({ name: slug }),
    })
      .then((data) => data.json())
      .then((res) => {
        if (res.status) {
          setCanPost(true);
          setChecking(false);
          return;
        } else {
          setCanPost(false);
          
          setSuggest(res.suggest);
          setChecking(false);
          return;
        }
      });
  }
  function create() {
    if (canPost) {
      setCreating(true);
      fetch("/api/cards/create", {
        method: "POST",
        body: JSON.stringify({ slug: slug }),
      })
        .then((res) => res.json())
        .then((data) => {
          setCanPost(false);
          setCreating(false);
          
          router.replace("/dashboard/cards/create/" + data._id);
        });
    } else checkAvailbilty();
  }
  function setName() {
    setSlug(suggest);
    setCanPost(true);
    setSuggest(null);
  }
  if(status=='loading')
  {
    return(<>
    <div className="pt-72 flex justify-center">
        <div className="mx-auto animate-pulse">
    <Image
                  src="/images/logo.svg"
                  alt="Logo"
                  width={"260"}
                  height={"160"}
                  className="cursor-pointer mx-auto"
                />
    </div>
    </div>
    </>)
  }
  if(!session)
  window.location.href='/signin';
  if(session && status!='loading')
  return (
    <>
      <Head>
        <title>{_("Create New Card")}</title>
      </Head>
      <Admin>
        <div className="-mb-24 md:-m-1 md:-mb-20 h-24 bg-white">
          <div className="text-emerald-800 text-sm hidden lg:block pt-14 ltr:pl-16">
            {" "}
            {_("Create New Card")}
          </div>
        </div>
        <div className="flex flex-wrap mt-24">
          <div className="w-full xl:w-12/12 mb-12 xl:mb-0 px-4">
            <div className="bg-white mt-4 shadow min-h-fit rounded">
              <div className="card-header border-b-2 p-6">
                <h1 className="text-2xl">{_("Insert name of your card")}</h1>
              </div>
              <div className="card-body p-6">
                <div className="flex flex-wrap  border p-4   rounded-md">
                  <label className="text-gray-500 text-md col-span-1 text-right w-4/12 md:w-2/12 lg:w-1/12">
                    much.sa/
                  </label>
                  <input
                    onBlur={checkAvailbilty}
                    className="w-8/12 md:w-5/12 lg:w-8/12"
                    value={slug}
                    onChange={(e) => {
                      setSlug(
                        e.target.value
                          .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
                          .toLowerCase()
                      );
                    }}
                  ></input>
                  <button
                    disabled={!slug}
                    className="w-12/12 md:w-3/12 lg:w-3/12 justify-center"
                    onClick={create}
                  >
                    <span className="text-emerald-300">
                      {!creating && !checking && _("Next")}
                    </span>
                    <span className="text-emerald-300">
                      {!creating && checking && _("Checking")}
                    </span>
                    <span className="text-emerald-400">
                      {creating && _("Loading")}
                    </span>
                  </button>

                  {!canPost && suggest && (
                    <div className="w-12/12 ">
                      <p className="text-red-600 text-xs">
                        {_(
                          "Name is not exsits please choose another we suggest you:"
                        )}{" "}
                        <a
                          className="text-emerald-500 cursor-pointer"
                          onClick={setName}
                        >
                          {suggest}
                        </a>
                      </p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Admin>
    </>
  );
}
