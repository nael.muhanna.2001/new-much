import Head from "next/head";
import Admin from "../../../layouts/Admin";
import { translation } from "../../../lib/translations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Link from "next/link";
import { useSession } from "next-auth/react";
import Image from "next/image";
import "rsuite/dist/rsuite.min.css";
function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}
export default function CardIndex() {
  const { data: session, status } = useSession();
  const [cards, setCards] = useState([]);
  const [isLoading, setLoading] = useState();
  useEffect(() => {
    setLoading(true);
    fetch("/api/cards/", { method: "POST" })
      .then((res) => res.json())
      .then((data) => {
        setCards(data);
        setLoading(false);
      });
  }, []);
  if (status == "loading") {
    return (
      <>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }
  if (!session) window.location.href = "/signin";
  if (session && status != "loading")
    return (
      <>
        <Head>
          <title>{_("My Cards")}</title>
        </Head>

        <Admin>
        <div className="container px-6">
        <div className="hidden lg:block lg:-m-1   bg-white">
          <div className="text-emerald-800 text-sm hidden lg:block p-4">
              {" "}
              {_("My Cards")}
            </div>
          </div>
          <div className="flex flex-wrap mt-4">
            <div className="w-full xl:w-full  xl:mb-0">
              <div className="py-6  md:py-0 md:pb-12">
                <Link href="/dashboard/cards/create">
                  <div className="w-full md:w-fit md:rounded-md btn font-bold ltr:float-right">
                    {_("Create New")}
                  </div>
                </Link>
              </div>
              <div className="bg-white mt-4  min-h-fit rounded">
                <div className="card-header border-b-2 p-6">
                  <h1 className="text-2xl p-4">{_("My Cards")}</h1>
                  <hr />
                </div>
                {isLoading && (
                  <div className="grid grid-cols-12 p-6">
                    <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white p-2 m-2 h-40">
                      <div>
                        <h1 className="text-xl m-4 grid grid-cols-12 mt-10">
                          <div className="leading-relaxed nimate-pulse col-span-2 bg-gray-200 shadow-md p-2 h-6 w-6 rounded-md"></div>
                          <div className="leading-relaxed nimate-pulse col-span-10 bg-gray-200 shadow-md h-6 w-30 rounded-md"></div>
                        </h1>
                      </div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white p-2 m-2 h-40">
                      <div>
                        <h1 className="text-xl m-4 grid grid-cols-12 mt-10">
                          <div className="leading-relaxed nimate-pulse col-span-2 bg-gray-200 shadow-md p-2 h-6 w-6 rounded-md"></div>
                          <div className="leading-relaxed nimate-pulse col-span-10 bg-gray-200 shadow-md h-6 w-30 rounded-md"></div>
                        </h1>
                      </div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white p-2 m-2 h-40">
                      <div>
                        <h1 className="text-xl m-4 grid grid-cols-12 mt-10">
                          <div className="leading-relaxed nimate-pulse col-span-2 bg-gray-200 shadow-md p-2 h-6 w-6 rounded-md"></div>
                          <div className="leading-relaxed nimate-pulse col-span-10 bg-gray-200 shadow-md h-6 w-30 rounded-md"></div>
                        </h1>
                      </div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white p-2 m-2 h-40">
                      <div>
                        <h1 className="text-xl m-4 grid grid-cols-12 mt-10">
                          <div className="leading-relaxed nimate-pulse col-span-2 bg-gray-200 shadow-md p-2 h-6 w-6 rounded-md"></div>
                          <div className="leading-relaxed nimate-pulse col-span-10 bg-gray-200 shadow-md h-6 w-30 rounded-md"></div>
                        </h1>
                      </div>
                    </div>
                  </div>
                )}
                <div className="card-body grid grid-cols-12 p-6">
                  {!isLoading &&
                    cards.map((e) => {
                      return (
                        <div
                          key={e._id}
                          className={
                            e.cardType == "business_card"
                              ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white m-2  cursor-pointer border-b-2 border-emerald-300"
                              : e.cardType == "company_profile"
                              ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white m-2  cursor-pointer border-b-2 border-blue-600"
                              : e.cardType == "product_card"
                              ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white  m-2  cursor-pointer border-b-2 border-orange-600"
                              : e.cardType == "event_card"
                              ? "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white  m-2  cursor-pointer border-b-2 border-pink-600"
                              : "card col-span-12 md:col-span-6 lg:col-span-4 xl:col-span-3 shadow-md bg-white  m-2  cursor-pointer"
                          }
                        >
                          <div className="card-header">
                            <Link href={"https://much.sa/" + e.slug}>
                              <div
                                target="_blank"
                                rel="noreferrer"
                                className="w-full text-xs text-gray-400 text"
                                href={"https://much.sa/" + e.slug}
                              >
                                <i className="fa-sharp fa-solid fa-arrow-up-right-from-square"></i>{" "}
                                {"much.sa/" + e.slug}{" "}
                              </div>
                            </Link>
                          </div>
                          <div className="card-body">
                            {
                              <div
                                className={
                                  e.cardType == "business_card"
                                    ? "pr-2 text-emerald-300 text-xs"
                                    : e.cardType == "company_profile"
                                    ? "pr-2 text-gray-600 text-xs"
                                    : e.cardType == "product_card"
                                    ? "pr-2 text-orange-600 text-xs"
                                    : e.cardType == "event_card"
                                    ? "pr-2 text-pink-600 text-xs"
                                    : ""
                                }
                              >
                                {e.cardType == "business_card"
                                  ? _("Businees Card")
                                  : e.cardType == "company_profile"
                                  ? _("Company Profile")
                                  : e.cardType == "product_card"
                                  ? _("Product Card")
                                  : ""}
                              </div>
                            }
                            <Link href={"/dashboard/cards/create/" + e._id}>
                              <div>
                                <h1 className="text-sm">
                                  {!e.pic && (
                                    <i
                                      className={
                                        e.cardType == "business_card"
                                          ? "pr-2 text-emerald-300 mx-1 fa fa-user"
                                          : e.cardType == "company_profile"
                                          ? "pr-2 text-emerald-300 mx-1 fa fa-building"
                                          : e.cardType == "product_card"
                                          ? "pr-2 text-orange-600 mx-1 fa fa-tag"
                                          : e.cardType == "event_card"
                                          ? "pr-2 text-pink-600  mx-1 fa-light fa-calendar-days"
                                          : ""
                                      }
                                    ></i>
                                  )}
                                  {e.pic && (
                                    <img
                                      className="w-10 h-10 ltr:float-left rtl:float-right mx-1 rounded-full"
                                      src={e.pic}
                                    />
                                  )}
                                  {e.name ? e.name : "Untitle"}
                                  {e.cardType == "business_card" &&
                                    e.company && (
                                      <h3 className="text-gray-400 text-xs">
                                        {e.company.label}
                                      </h3>
                                    )}
                                </h1>
                              </div>
                            </Link>
                          </div>
                          <div className="card-footer">
                            <div className="btn-group">
                              <Link href={"/dashboard/cards/create/" + e._id}><button className="bg-emerald-300 p-2 text-gray-600 rounded mx-1">{_('Edit')}</button></Link>
                              <Link href={"/" + e.slug}><button className="bg-emerald-300 p-2 text-gray-600 rounded mx-1">{_('View')}</button></Link>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
            <div className="w-full xl:w-4/12 px-4"></div>
          </div>
          </div>
        </Admin>
      </>
    );
}
