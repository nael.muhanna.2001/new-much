import Address from "../webComponents/Address";
import Contact from "../webComponents/Contact";
import SEO from "../webComponents/Seo";
import { useRouter } from "next/router";
import Header from "../webComponents/new/storeHeader";
import { translation } from "../lib/websiteTranslations";
import Footer from "../webComponents/new/footer";

const items = [
  {
    id: 1,
    title: "Contact Us",
    sub: "Get in touch with us",
    body: "",
    imageUrl: "/images/new/Much-Slider-1.jpg",
  },
];

export default function ContactUs() {
  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  return (
    <>
      <SEO pageTitle={_("much | Contact Us")} />
      <Header items={items} />
      <div className="bg-blue-900 -mt-14">
        <section className="section-6 w-full h-auto pt-10">
          <div
            active
            className="contact react-tabs__tab-panel--selected container"
          >
            <div className="title-section text-start text-sm-center">
              <h2 className="text-center text-3xl font-medium p-5 md:text-4xl md:font-bold w-full text-white">
                {_("Contact")} <span className="text-green">{_("Us")}</span>
              </h2>
            </div>
            <div className="container" data-aos-duration="1200">
              <div className="row">
                {/*  Left Side Starts */}
                <div className="col-12 col-lg-4">
                  <h3 className="text-uppercase custom-title mb-0 ft-wt-600 pb-3 text-white">
                    {"Drop"} a line!
                  </h3>
                  <p className="open-sans-font mb-4 text-white">
                    Feel free to get in touch with us.
                  </p>
                  <Address />
                  {/* End Address */}

                  {/* End Social */}
                </div>
                {/* Left Side Ends */}

                {/*  Contact Form Starts  */}
                <div className="col-12 col-lg-8">
                  <Contact />
                </div>
                {/*  Contact Form Ends */}
              </div>
            </div>
            {/* End .container */}
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
