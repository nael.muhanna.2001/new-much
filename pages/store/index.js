import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Placeholder } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import { translation } from "../../lib/translations";
import CartIcon from "../../webComponents/new/components/cartIcon";
import Product from "../../webComponents/new/components/product";
import Footer from "../../webComponents/new/footer";
import Header from "../../webComponents/new/storeHeader";
import SEO from "../../webComponents/Seo";

export default function Store({ query }) {
  const { locale } = useRouter();
  const [loading, setLoading] = useState(false);
  const [productsData, setProductsData] = useState([]);
  const [category, setCategory] = useState("all");
  const [filteredProducts, setFilteredProducts] = useState([]);
  //====== Translation Function =======//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  const items = [
    {
      id: 1,
      title: "Our Digital Products",
      sub: "Share Details With Style",
      body: "",
      imageUrl: "/images/new/Much-Slider-1.jpg",
    },
  ];

  useEffect(() => {
    setLoading(true);

    fetch("/api/much-products/", {
      method: "POST",
      body: JSON.stringify({ locale: locale }),
    })
      .then((res) => res.json())
      .then((data) => {
        setProductsData(data);
        setFilteredProducts(data);
        if(query.type){
          setCategory(query.type);
          setFilteredProducts(data.filter((e) => e.category == query.type));
        }
        setLoading(false);
        document.body.scrollTo({ top: 400, behavior: "smooth" });
      });
  }, []);

  //==== Select category =====//
  function selectCategory(cat) {
    setCategory(cat);
    if (cat != "all")
      {
        setFilteredProducts(productsData.filter((e) => e.category == cat));
      }
      else setFilteredProducts(productsData);
  }

  return (
    <>
      <SEO pageTitle={_("much... | Store")} />
      <Header items={items} />
      <div className="bg-[url('/images/bgs/pbg.jpg')] bg-contain">
        <div className="container mt-36">
          <div className="relative border-b-2 border-b-emerald-300 mt-16 flex overflow-x-auto">
            <button
              onClick={() => selectCategory("all")}
              className={
                category == "all"
                  ? "bg-blue-900 rounded-t-lg p-2 px-4 font-bold text-emerald-300 mx-1 duration-300 min-w-fit"
                  : "bg-emerald-300 rounded-t-lg p-2 px-4 font-bold text-blue-900 mx-1 duration-300 min-w-fit"
              }
            >
              {_("All")}
            </button>
            <button
              onClick={() => selectCategory("card")}
              className={
                category == "card"
                  ? "bg-blue-900 rounded-t-lg p-2 px-4 font-bold text-emerald-300 mx-1 duration-300 min-w-fit"
                  : "bg-emerald-300 rounded-t-lg p-2 px-4 font-bold text-blue-900 mx-1 duration-300 min-w-fit"
              }
            >
              {_("NFC Cards")}
            </button>
            <button
              onClick={() => selectCategory("tag")}
              className={
                category == "tag"
                  ? "bg-blue-900 rounded-t-lg p-2 px-4 font-bold text-emerald-300 mx-1 duration-300 min-w-fit"
                  : "bg-emerald-300 rounded-t-lg p-2 px-4 font-bold text-blue-900 mx-1 duration-300 min-w-fit"
              }
            >
              {_("NFC Tags")}
            </button>
          </div>
          {loading && (
            <div className="flex flex-wrap mx-auto  ">
              <div className="w-full md:w-6/12 lg:w-4/12 xl:w-4/12 p-4">
                <div className="p-4 border-b-2 border-b-gray-300">
                  <Placeholder.Graph
                    active
                    className="w-[200px] h-[150px] mx-4"
                  />
                  <Placeholder.Paragraph active className="mx-4" />
                </div>
              </div>
              <div className="w-full md:w-6/12 lg:w-4/12 xl:w-4/12 p-4">
                <div className="p-4 border-b-2 border-b-gray-300">
                  <Placeholder.Graph
                    active
                    className="w-[200px] h-[150px] mx-4"
                  />
                  <Placeholder.Paragraph active className="mx-4" />
                </div>
              </div>
              <div className="w-full md:w-6/12 lg:w-4/12 xl:w-4/12 p-4">
                <div className="p-4 border-b-2 border-b-gray-300">
                  <Placeholder.Graph
                    active
                    className="w-[200px] h-[150px] mx-4"
                  />
                  <Placeholder.Paragraph active className="mx-4" />
                </div>
              </div>
            </div>
          )}
          {!loading && (
            <div className="flex flex-wrap mx-auto">
              {filteredProducts.map((e) => {
                return (
                  <div
                    key={e._id}
                    className="w-full md:w-6/12 lg:w-4/12 xl:w-4/12 py-4 mb-5"
                  >
                    <Product key={e._id} product={e} />
                  </div>
                );
              })}
            </div>
          )}
        </div>
      </div>
      {/* <CartIcon /> */}
      <Footer />
    </>
  );
}

export async function getServerSideProps(context) {
  return {
    props: {
      query: context.query,
    },
  };
}
