import Header from "../../../webComponents/new/components/productHeader";
import "rsuite/dist/rsuite.min.css";
import { Placeholder } from "rsuite";
import { useEffect, useState } from "react";
import { Carousel } from "react-bootstrap";
import { useRouter } from "next/router";
import { translation } from "../../../lib/translations";
import Head from "next/head";
import ProductSample from "../../../webComponents/new/components/productSample";
import SEO from "../../../webComponents/Seo";
import { useSession } from "next-auth/react";
import CartIcon from "../../../webComponents/new/components/cartIcon";
import Footer from "../../../webComponents/new/footer";
export default function ProductPage({ params, staticProduct }) {
  const { data: session, status } = useSession();
  const [product, setProduct] = useState(staticProduct);
  const [tabIndex, setTabIndex] = useState(0);
  const [imgArray, setImgArray] = useState([]);
  const [quantity, setQuantity] = useState(1);
  const [showQuantity, setShowQuantity] = useState(false);
  const [relatedProduct, setRelatedProduct] = useState([]);
  const [isPlacingOrder, setIsPlacingOrder] = useState(false);
  const [refresher, setRefresher] = useState(false);
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  //========== Place Order ============
  const placeOrder = async () => {
    setRefresher(false);
    let user = null;
    if (session) {
      user = session.user;
    } else {
      if (typeof window != "undefined") {
        user = JSON.parse(window.localStorage.getItem("user"));
      }
    }
    let order = {
      user_id: user._id,
      product_id: product._id,
      product: product,
      currency: product.currency,
      quantity: quantity,
      payment_status: "unpaid",
      status: "new",
    };
    setIsPlacingOrder(true);
    const orderRes = await fetch("/api/orders/place-order/", {
      body: JSON.stringify(order),
      method: "POST",
    });
    const returnOrder = await orderRes.json();
    setRefresher(true);

    setIsPlacingOrder(false);
    if (orderRes.status == "201") {
    }
  };
  useEffect(() => {
    let user = null;
    if (window.localStorage.getItem("user") && !session) {
      user = window.localStorage.getItem("user");
    } else {
      user = session && session.user ? JSON.stringify(session.user) : null;
    }
    fetch("/api/post-user/", { method: "post", body: user })
      .then((res) => res.json())
      .then((userData) => {
        window.localStorage.setItem("user", JSON.stringify(userData));
        user = window.localStorage.getItem("user");
      });
    setQuantity(1);
    document.body.scrollTo({ top: 0, behavior: "smooth" });
    fetch("/api/much-products/" + params.id + "/")
      .then((res) => res.json())
      .then((data) => {
        setTabIndex(0);
        setProduct(data);
        setImgArray([]);
        if (data.cardFront) {
          setImgArray((prev) => {
            prev.push(data.cardFront);
            return [...prev];
          });
        }
        if (data.cardBack) {
          setImgArray((prev) => {
            prev.push(data.cardBack);
            return [...prev];
          });
        }
        if (data.images && data.images.length > 0) {
          data.images.map((e) => {
            setImgArray((prev) => {
              prev.push(e);
              return [...prev];
            });
          });
        }
        if (data.videos && data.videos.length > 0) {
          data.videos.map((e) => {
            setImgArray((prev) => {
              prev.push(e);
              return [...prev];
            });
          });
        }
        if (data.relatedProducts.length > 1) {
          setRelatedProduct(data.relatedProducts);
        }

        document.body.scrollTo({ top: 0, behavior: "smooth" });
      });
  }, [params]);
  const handleSelect = (selectedIndex) => {
    setTabIndex(selectedIndex);
  };
  return (
    <>
      <SEO
        pageTitle={
          product
            ? product.name[locale]
              ? "much...| " + product.name[locale]
              : "much...| " + product.name["en"]
            : "Loading..."
        }
      />
      <Header refresher = {refresher} />
      <div className="container md:mt-24">
        {!product && (
          <div className="flex flex-wrap">
            <div className="w-full md:w-6/12 px-2">
              <div className="max-w-[500] max-h-[800px] w-96 h-screen mx-auto">
                <Placeholder.Graph active height={300} width={400} />
              </div>
            </div>
            <div className="w-full md:w-6/12 px-2">
              <Placeholder.Paragraph active rowHeight={50} rows={1} />
              <Placeholder.Paragraph active rowHeight={20} rows={6} />
            </div>
          </div>
        )}
        {product && (
          <div>
            <div className="flex flex-wrap">
              <div className="w-full md:w-6/12 md:px-12 mb-2 sticky shadow-none max-h-[200px] md:max-h-[370px]  z-50 left-0 top-0 md:-top-3 md:relative overflow-hidden">
                <div className="max-w-[500] mx-auto">
                  {imgArray.length > 0 && (
                    <Carousel
                      interval={5000}
                      onSelect={handleSelect}
                      activeIndex={tabIndex * 1}
                      className="min-h-[250px] overflow-hidden"
                    >
                      {imgArray.map((img, id) => {
                        if (!img.video)
                          return (
                            <Carousel.Item
                              key={id}
                              className="h-[200px] md:h-[300px] md:max-h-[350px]"
                            >
                              <img className="mx-auto" src={img.imgUrl} />
                            </Carousel.Item>
                          );
                        if (img.video)
                          return (
                            <Carousel.Item
                              key={id}
                              className="h-[200px] md:h-[300px] md:max-h-[350px]"
                            >
                              <video
                                className="rounded-[60px] h-full mx-auto"
                                muted
                                loop
                                autoPlay
                                playsInline
                              >
                                <source src={img.url} />
                              </video>
                            </Carousel.Item>
                          );
                      })}
                    </Carousel>
                  )}
                  <div className="hidden md:block">
                    {imgArray.length > 0 && (
                      <div className="snap-x flex overflow-auto">
                        {imgArray.map((img, id) => {
                          return (
                            <div
                              key={id}
                              className={
                                id == tabIndex
                                  ? "select-none relative snap-center m-1 min-w-[100px] border-2 border-blue-900  w-2/12"
                                  : "select-none relative snap-center m-1 min-w-[100px] w-2/12"
                              }
                              onClick={() => {
                                setTabIndex(id);
                              }}
                            >
                              <img
                                className="mx-auto"
                                src={img.thumbnail_url}
                              />
                              {img.video && (
                                <i className="absolute text-2xl text-green bottom-[20px] left-[40px] fa fa-play"></i>
                              )}
                            </div>
                          );
                        })}
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="w-full md:w-6/12 md:px-2 relative pb-6 md:border-l-2 md:border-l-emerald-300 min-h-[400px]  md:mt-0">
                <h1 className="text-2xl border-b-2 border-b-emerald-300 text-blue-900 mb-4 md:mx-3">
                  {product.name[locale]
                    ? product.name[locale]
                    : product.name["en"]}
                </h1>
                <div className="mb-4">
                  {product.shortDescription && (
                    <p className="my-2 md:mx-3 text-lg text-gray-500">
                      {product.shortDescription[locale]
                        ? product.shortDescription[locale]
                        : product.shortDescription["en"]}
                    </p>
                  )}
                  {product.description && (
                    <p className="text-lg text-gray-500 md:mx-3">
                      {product.description[locale]
                        ? product.description[locale]
                        : product.description["en"]}
                    </p>
                  )}
                </div>
                <div className="mb-28 md:mb-12 z-50">
                  <div className="text-3xl border-b px-1 m-3 py-2 border-b-blue-900 border-dashed w-fit">
                    {product.price != product.originalPrice && (
                      <div>
                        <span className=" w-full line-through text-gray-400 text-sm">
                          {Number(product.originalPrice).toFixed(2)}{" "}
                        </span>
                        <span className="font-bold text-red-700 px-2">
                          {Number(product.price).toFixed(2)} {product.currency}
                        </span>
                      </div>
                    )}
                    {product.price == product.originalPrice && (
                      <div className="font-bold text-blue-900">
                        {Number(product.price).toFixed(2)} {product.currency}
                      </div>
                    )}
                  </div>
                  <div
                    className={
                      showQuantity
                        ? "fixed md:static z-50 left-0 md:bg-transparent  bottom-14 bg-gray-100  w-full pt-1 text-lg flex md:flex-none  md:block shadow"
                        : "fixed md:static z-50 left-0 bg-white md:bg-transparent  bottom-14   w-full pt-1 text-lg hidden md:flex-none  md:block"
                    }
                  >
                    <label className="m-3 w-4/12">{_("Quantity")}</label>
                    <div className="w-8/12 flex md:w-[155px]  md:mx-3 mb-3">
                      <div className="w-3/12">
                        <button
                          disabled={quantity <= 1}
                          onClick={() => {
                            setQuantity((prev) => prev - 1);
                          }}
                          className="w-full disabled:text-gray-400 h-full p-2 border rounded-l text-green"
                        >
                          <i className="fa fa-minus"></i>
                        </button>
                      </div>
                      <div className="w-6/12 text-center p-1 py-2 border text-blue-900 font-semibold bg-white">
                        {quantity}
                      </div>
                      <div className="w-3/12">
                        <button
                          onClick={() => {
                            setQuantity((prev) => prev + 1);
                          }}
                          className="w-full h-full p-2 border rounded-r text-green"
                        >
                          <i className="fa fa-plus"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="fixed z-50 md:absolute bottom-0 left-0 w-full flex">
                  <div className="border-t p-1 pt-2 block md:hidden bg-white">
                    <button
                      onClick={() => {
                        setShowQuantity((prev) => !prev);
                      }}
                      className="bg-white  w-20 border rounded"
                    >
                      {_("QTY")}
                      <br />
                      <span className="text-[16px] font-bold">{quantity}</span>
                    </button>
                  </div>
                  <button
                    disabled={isPlacingOrder}
                    onClick={() => {
                      placeOrder();
                    }}
                    className="disabled:bg-gray-300 disabled:text-gray-700 bg-emerald-300 md:rounded-r-lg w-full md:w-fit p-3  px-4 text-lg text-blue-900 font-bold hover:text-emerald-300 hover:bg-blue-900 duration-300"
                  >
                    {!isPlacingOrder && (
                      <i className="fa fa-shopping-cart animate-marquee duration-300 hover:animate-none"></i>
                    )}{" "}
                    {_("Add to cart")}{" "}
                    {isPlacingOrder && (
                      <i className="fa fa-spinner animate-spin"></i>
                    )}
                  </button>
                </div>
              </div>
            </div>
            <div className="flex flex-wrap mt-4 mb-36 border-t py-1 border-dashed">
              <div className="w-full md:w-6/12"></div>
              <div className="w-full md:w-6/12 ">
                {relatedProduct.length > 0 && (
                  <div className="px-2">
                    <p className="text-lg font-bold">
                      {_("Products related to this item")}
                    </p>
                  </div>
                )}
                <div className="flex overflow-y-auto">
                  {relatedProduct.map((rp) => {
                    return (
                      <div
                        key={rp._id}
                        className={
                          product._id == rp._id
                            ? "mx-2 border-2 border-red-300 rounded-md duration-300"
                            : "mx-2 duration-300"
                        }
                        onClick={() => {
                          setProduct(rp);
                          document.body.scrollTo({
                            top: 0,
                            behavior: "smooth",
                          });
                        }}
                      >
                        <ProductSample product={rp} />
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      {/* <CartIcon flag={refresher} /> */}
      <Footer />
    </>
  );
}
export const getStaticPaths = async () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: "blocking", //indicates the type of fallback
  };
};
export async function getStaticProps(context) {
  const params = context.params;
  const staticProductRequest = await fetch(
    "https://www.much.sa/api/much-products/" + params.id,
    { method: "get" }
  );
  const staticProduct = await staticProductRequest.json();
  return {
    props: { params, staticProduct },
  };
}
