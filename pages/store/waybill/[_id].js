import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Placeholder } from "rsuite";
import { translation } from "../../../lib/translations";
import CartRow from "../../../webComponents/new/components/cartRow";
import Header from "../../../webComponents/new/components/productHeader";
import "rsuite/dist/rsuite.min.css";
import Head from "next/head";
import SEO from "../../../webComponents/Seo";
import Footer from "../../../webComponents/new/footer";
export default function WayBill({ query }) {
  const { locale } = useRouter();
  //====== Translation Function =======//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  const [waybill, setWaybill] = useState();
  const { _id, tap_id } = query;
  useEffect(() => {
    fetch("/api/payment/waybill/", {
      method: "post",
      body: JSON.stringify({
        _id: _id,
        tap_id: tap_id,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setWaybill(data);
      });
  }, []);
  return (
    <>
    
        <SEO pageTitle={_('much... | Waybill')} />
    
    <div className="bg-blue-900">
      <Header />
      {!waybill && (
        <div className="card max-w-2xl mx-auto mt-8">
          <div className="card-header">
            <Placeholder.Paragraph rows={1} rowHeight={30} active />
            <Placeholder.Paragraph rows={1} rowHeight={10} active />
          </div>
          <div className="card-body">
            <div className="border">
              <h5 className=" text-green font-bold text-sm w-32  rounded -m-2 p-2 mb-2">
                <Placeholder.Paragraph rows={1} rowHeight={20} active />
              </h5>
              <Placeholder.Graph rows={1} rowHeight={10} active />
            </div>
            <div className="border mt-4">
              <h5 className=" text-green font-bold text-sm w-32 rounded -m-2 p-2 mb-2">
                <Placeholder.Paragraph rows={1} rowHeight={20} active />
              </h5>
              <Placeholder.Paragraph rows={4} rowHeight={15} active />
            </div>
            <div className="border mt-4">
              <h5 className=" text-green font-bold text-sm w-32 rounded -m-2 p-2 mb-2">
                <Placeholder.Paragraph rows={1} rowHeight={20} active />
              </h5>
              <Placeholder.Paragraph rows={3} rowHeight={10} active />
            </div>
          </div>
        </div>
      )}
      {waybill && (
        <div className="card max-w-2xl mx-auto mt-8">
          <div className="my-2">
            {waybill &&  waybill.status == "CAPTURED" && (
              <h2 className="bg-blue-900 text-green font-bold text-lg w-fit rounded-r p-2">
                {_("Your order has been placed!")}
              </h2>
            )}
            {waybill  &&  waybill.paymentMethod!='transfer' &&  waybill.paymentMethod!='cod' && waybill.status != "CAPTURED" && (
              <h2 className="bg-red-900 text-green font-bold text-lg w-fit rounded-r p-2">
                {_("Your order has not been placed!")}
              </h2>
            )}
            {waybill  && (waybill.paymentMethod=='cod' || waybill.paymentMethod=='transfer') && waybill.status!="CAPTURED" &&
            <h2 className="bg-green text-blue-900 font-bold text-lg w-fit rounded-r p-2">
            {_("Your order has been placed!")}
          </h2>}
            <div className="text-gray-400 p-2 text-xs">
              <span>{_("Placed at")}:</span>{" "}
              {new Date(waybill.created_at).toLocaleString()}
            </div>
          </div>
          <div className="card-body">
            <div className="border">
              <h5 className="bg-blue-900 text-green font-bold text-sm w-fit rounded -m-2 p-2">
                {_("Order details")}:
              </h5>
              {waybill && waybill.orders && (
                <div className="my-5 text-gray-700 ">
                  {waybill.orders.map((e) => (
                    <CartRow key={e._id} order={e} checkedout={true} />
                  ))}
                  <div className="float-right m-2">
                    <b>{_("Total")}</b> {Number(waybill.total).toFixed(2)}{" "}
                    {_("SAR")}
                  </div>
                </div>
              )}
            </div>
            {waybill && waybill.promocode && (
              <div className="border mt-4">
                <h5 className="bg-blue-900 text-green font-bold text-sm w-fit rounded -m-2 p-2">
                  {_("Discount")}:
                </h5>
                <div className="mt-4 text-gray-700 p-2">
                  <p className="text-gray-700">
                    {_("Promo code applied")} :<b>{waybill.promocode.promocode.code}</b>
                  </p>
                  <p className="text-gray-700">
                    {_("Discount")} : <b>{Number(waybill.discount).toFixed(2)}{_('SAR')}</b> (
                    {waybill.promocode.promocode.percent}%)
                  </p>
                </div>
              </div>
            )}
            {waybill && waybill.address && (
              <div className="border mt-4">
                <h5 className="bg-blue-900 text-green font-bold text-sm w-fit rounded -m-2 p-2">
                  {_("Will ship To")}:
                </h5>
                <div className="mt-2 text-gray-700 p-2">
                  <p className="flex">
                    <span className="w-2/12">{_("Name")}:</span>
                    <span className="w-10/12">
                      <b>
                        {waybill.address.fname} {waybill.address.lname}
                      </b>
                    </span>
                  </p>
                  <p className="flex">
                    <span className="w-2/12">{_("Mobile")}:</span>
                    <span className="w-10/12">
                      <b>
                        +{waybill.address.prefix?.value} {waybill.address.phone}
                      </b>
                    </span>
                  </p>
                  <p className="flex my-2">
                    <span className="w-2/12">{_("address")}: </span>
                    <span className="w-10/12">
                      {" "}
                      {waybill.address.address.formatted_address}
                    </span>
                  </p>
                  {waybill.address.city == "Riyadh" && (
                    <span className="text-blue-900 font-bold">
                      <i className="fa fa-info-circle"></i>{" "}
                      {_("The expected delivery time is 4 working days")}
                    </span>
                  )}
                  {waybill.address.city && waybill.address.city != "Riyadh" && (
                    <span className="text-blue-900 font-bold">
                      <i className="fa fa-info-circle"></i>{" "}
                      {_("The expected delivery time is 7 working days")}
                    </span>
                  )}
                </div>
              </div>
            )}
            {waybill && waybill.paymentMethod && (
              <div className="border mt-4">
                <h5 className="bg-blue-900 text-green font-bold text-sm w-fit rounded -m-2 p-2">
                  {_("Paid With")}:
                </h5>
                <div className="mt-2 text-gray-700 p-2">
                  {waybill.paymentMethod == "tap" && (
                    <div>
                      <div className="flex">
                        <img
                          src="https://pbs.twimg.com/profile_images/1214833460067610625/0whceCIX_400x400.jpg"
                          className="h-10 w-10"
                        />
                        <div className="p-2">{_("Tap payment gateway")}</div>
                      </div>
                      {waybill.charge && waybill.charge.card && (
                        <div className="p-2">
                          {_(waybill.charge.card.brand)} (
                          {_(waybill.charge.card.scheme)})
                        </div>
                      )}
                    </div>
                  )}
                  {waybill.paymentMethod == "applepay" && (
                    <div>
                      <div className="flex">
                        
                        <div className="p-2">{_("Apple Pay")}</div>
                      </div>
                      {waybill.charge && waybill.charge.card && (
                        <div className="p-2">
                          {_(waybill.charge.card.brand)} (
                          {_(waybill.charge.card.scheme)})
                        </div>
                      )}
                    </div>
                  )}
                  {waybill.paymentMethod == "transfer" && (
                    <div>
                      <div className="">
                        <div className="font-bold">{_("Bank Transfer to")}:</div>
                        <div className="flex mt-2">
                          <div className="w-3/12">{_("Bank Name:")}</div>
                          <div className="w-9/12 font-bold select-text">
                            {_("Banque Saudi Fransi")}
                          </div>
                        </div>
                        <div>
                          <div className="flex mt-2">
                            <div className="w-3/12">{_("Account Holder")}</div>
                            <div className="w-9/12 font-bold select-all">
                              {_("MUCH MORE INFORMATION TECHNOLOGY EST")}
                            </div>
                          </div>
                          <div className="flex mt-2">
                            <div className="w-3/12">{_("IBAN")}</div>
                            <div className="w-9/12 font-bold select-all">
                              SA5655000000050934500180
                            </div>
                          </div>
                          <div className="flex mt-2">
                            <div className="w-3/12">{_("SwiftCode")}</div>
                            <div className="w-9/12 font-bold select-all">BSFRSARIGEM</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {waybill.paymentMethod == "cod" && (
                    <div>
                      <div className="font-bold">{_('Cash On Delivery')} ({_('COD')})</div>
                      <p>{_('Please prepare an amount of')} <b>{Number(waybill.total).toFixed(2)} {_('SAR')}</b> {_('cash')}</p>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
          <div className="card-footer">
            <Link href="/">
              <button className="w-full p-4 bg-blue-900 text-green font-bold rounded">
                {_("Back home")}
              </button>
            </Link>
          </div>
        </div>
      )}
    </div>
    <Footer />
    </>
  );
}
export function getServerSideProps(context) {
  return {
    props: { query: context.query },
  };
}
