const EXTERNAL_DATA_URL = "https://www.much.sa/store/";
function generateSiteMap(posts) {
  return `<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
	<channel>
		<title>much.sa store</title>
		<link>http://www.much.sa/store</link>
		<description>The New Era Of Digital Business Cards, Sustainability, Creativity, and Easy Data Sharing With A Single Tap, Elevate your professional networking strategy with customizable NFC digital business cards, NFC tags and QR code business card collections at much.sa.</description>
       ${posts
         .map((e) => {
           return `
         <item>
         <g:id>${e._id}</g:id>
         <g:title>${(e.name["en"]).replace('&', '&amp;')}</g:title>
         <g:description>${(e.shortDescription["en"]).replace('&', '&amp;')}</g:description>
         <g:link>${EXTERNAL_DATA_URL + "product/" + e._id+"/"}</g:link>
         <g:image_link>${
           e.cardFront
             ? e.cardFront.imgUrl
             : e.cardBack
             ? e.cardBack.imgUrl
             : e.images && e.images.length
             ? e.images[0].imgUrl
             : ""
         }</g:image_link>
         
         <g:availability>in stock</g:availability>
         <g:price>${e.price} ${e.currency}</g:price>
         
         <g:brand>much</g:brand>
         <g:google_product_category>Digital Cards > NFC Cards</g:google_product_category>
         <g:product_type>${(e.name["en"]).replace('&', '&amp;')}</g:product_type>
         </item>
       `;
         })
         .join("")}
     	</channel>
</rss>
   `;
}

function SiteMap() {
  // getServerSideProps will do the heavy lifting
}

export async function getServerSideProps({ res }) {
  // We make an API call to gather the URLs for our site
  const request = await fetch("https://www.much.sa/api/much-products/", {
    method: "POST",
    body: JSON.stringify({ locale: "en" }),
  });
  const posts = await request.json();

  // We generate the XML sitemap with the posts data
  const sitemap = generateSiteMap(posts);

  res.setHeader("Content-Type", "text/xml");
  // we send the XML to the browser
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
}

export default SiteMap;
