import axios from "axios";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Autocomplete from "react-google-autocomplete";
import Select from "react-select";
import { Placeholder, Radio, RadioGroup, Toggle } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import ApplePayButton from "../../../components/ApplePay/ApplePayButton";
import CreditCard from "../../../components/payment/Card";
import {
  performApplePayDebit,
  performValidation,
} from "../../../components/ApplePay/ApplePayRestClient";
import {
  PaymentStatus,
  performPayment,
} from "../../../components/ApplePay/PaymentApi";
import {
  merchantDisplay,
  merchantIdentifier,
} from "../../../components/ApplePay/PaymentConf";
import { countries } from "../../../lib/countries";
import { translation } from "../../../lib/translations";
import CartRow from "../../../webComponents/new/components/cartRow";
import Header from "../../../webComponents/new/components/productHeader";
import SEO from "../../../webComponents/Seo";
import TapCard from "../../../components/payment/tapCard";
import Footer from "../../../webComponents/new/footer";
const options = [];
countries.map((country) => {
  options.push({
    value: country.dial,
    label: country.name + " (" + country.dial + ")",
  });
});
export default function Cart() {
  const ShoppingCartState = {
    READY: 0,
    PAYMENT_IN_PROGRESS: 1,
    PAYMENT_SUCCESS: 2,
    PAYMENT_FAILURE: 3,
    PAYMENT_CANCEL: 4,
  };
  const [ShoppingCartStatus, setShoppingCartStatus] = useState();
  const { data: session, status } = useSession();
  const { locale } = useRouter();
  const [theUser, setTheUser] = useState();
  const [orders, setOrders] = useState([]);
  const [orderTotal, setOrderTotal] = useState(0);
  const [orderValues, setOrderValue] = useState([]);
  const [total, setTotal] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [loading, setLoading] = useState(false);
  const [code, setCode] = useState();
  const [promocode, setPromocode] = useState();
  const [promoMessage, setPromoMessage] = useState();
  const [checkingPromo, setCheckingPromo] = useState(false);
  const [checkedOut, setCheckedOut] = useState(false);
  const [address, setAddress] = useState();
  const [building, setBuilding] = useState();
  const [appartment, setAppartment] = useState();
  const [phone, setPhone] = useState();
  const [fname, setFname] = useState();
  const [lname, setLname] = useState();
  const [country, setCountry] = useState("sa");
  const [prefix, setPrefix] = useState();
  const [defaultCountry, setDefaultCountry] = useState();
  const [hideApplePay, setHideApplePay] = useState(true);
  const [city, setCity] = useState();
  const [addingAddress, setAddingAddress] = useState(false);
  const [addressSaving, setAddressSaving] = useState(false);
  const [changingAddress, setChangingAddress] = useState(false);

  const [addresses, setAddresses] = useState([]);
  const [defaultAddress, setDefaultAddress] = useState();

  const [paymentMethod, setPaymentMethod] = useState();
  const [selectCcard, setSelectCcard] = useState(false);

  const [placingOrder,setPlacingOrder]=useState(false);
  //====== Translation Function =======//
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  const setQty = (index, qty) => {
    fetch("/api/orders/change-quantity/", {
      method: "POST",
      body: JSON.stringify({
        _id: orders[index]._id,
        quantity: qty,
      }),
    });
    setOrders((prev) => {
      let ords = prev;
      ords[index].quantity = qty;
      let total = 0;
      let theorders = [...ords];
      theorders.forEach((e) => {
        if (e.quantity || e.quantity == 0) {
          total += e.product.price * e.quantity;
        } else {
          total += e.product.price * 1;
        }
      });
      setOrderTotal(total);
      setTotal(total);
      setDiscount(0);
      setCode("");
      setPromoMessage("");
      return [...ords];
    });
  };
  const removeOrder = (index) => {
    let user = null;
    if (session) {
      user = session.user;
    } else {
      if (typeof window != "undefined") {
        user = JSON.parse(window.localStorage.getItem("user"));
      }
    }
    fetch("/api/orders/delete/", {
      method: "post",
      body: JSON.stringify({ user: user, _id: orders[index]._id }),
    })
      .then((res) => res.json())
      .then((data) => {
        data.map((e) => {
          if (!e.quantity) e.quantity = 1;
        });
        setOrders(data);

        let total = 0;
        data.forEach((e) => {
          if (e.quantity || e.quantity == 0) {
            total += e.product.price * e.quantity;
          } else {
            total += e.product.price * 1;
          }
        });
        setOrderTotal(total);
        setTotal(total);
        setDiscount(0);
        setCode("");
        setPromoMessage("");
      });
    setOrders([...orders.filter((order, x) => x != index)]);
    let total = 0;
    let theOrder = [...orders.filter((order, x) => x != index)];
    theOrder.forEach((e) => {
      if (e.quantity || e.quantity == 0) {
        total += e.product.price * e.quantity;
      } else {
        total += e.product.price * 1;
      }
    });
    setOrderTotal(total);
    setTotal(total);
    setDiscount(0);
    setCode("");
    setPromoMessage("");
  };

  const sumQty = (array) => {
    let totalQty = 0;
    array.map((e) => (totalQty += e.quantity));
    return totalQty;
  };

  //====== PromoCode Checker =====//
  const checkPromo = (code) => {
    setPromoMessage("");
    setCheckingPromo(true);
    fetch("/api/promocode/check/", {
      method: "post",
      body: JSON.stringify({ code: code }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.canApply) {
          setPromocode(data.promocode);
          if (orderTotal > data.promocode.minVal * 1) {
            setDiscount(
              Math.round((orderTotal * data.promocode.percent) / 100) <=
                data.promocode.max
                ? Math.round((orderTotal * data.promocode.percent) / 100)
                : data.promocode.max
            );
            setTotal(
              orderTotal -
                (Math.round((orderTotal * data.promocode.percent) / 100) <=
                data.promocode.max
                  ? Math.round((orderTotal * data.promocode.percent) / 100)
                  : data.promocode.max)
            );
          } else {
            setPromoMessage(
              "The invoice amount is less than the applicable discount amount"
            );
          }
        }
        setPromocode(data);
        setCheckingPromo(false);
      });
  };

  // ===== Checkout ========//
  const checkout = () => {
    setCheckedOut(true);
  };

  useEffect(() => {
    if (window.ApplePaySession) {
      setHideApplePay(false);
    }
    console.log(window);
    if (status != "loading") {
      setLoading(true);
      let user = null;
      if (!session) {
        if (typeof window != "undefined") {
          user = JSON.parse(window.localStorage.getItem("user"));
          setTheUser(user);
        }
      } else {
        user = session.user;
        setTheUser(user);
        setDefaultAddress(user.defaultAddress);
      }

      let localUser = window.localStorage.getItem("user")
        ? JSON.parse(window.localStorage.getItem("user"))
        : null;
      console.log(localUser);
      if (localUser && session && localUser._id != session.user._id) {
        fetch("/api/orders/update-orders-ids/", {
          method: "post",
          body: JSON.stringify({
            old_id: localUser._id,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            window.localStorage.setItem("user", JSON.stringify(data.user));
            fetch("/api/orders/my-orders/", {
              method: "post",
              body: JSON.stringify({ user: user }),
            })
              .then((res) => res.json())
              .then((data) => {
                fetch("/api/post-user/addresses/")
                  .then((res) => res.json())
                  .then((data) => {
                    setAddresses(data);
                    setDefaultAddress(data.filter((e) => e.default == true)[0]);
                  });
                data.map((e) => {
                  if (!e.quantity) e.quantity = 1;
                });
                setOrders(data);

                let total = 0;
                data.forEach((e) => {
                  if (e.quantity || e.quantity == 0) {
                    total += e.product.price * e.quantity;
                  } else {
                    total += e.product.price * 1;
                  }
                });

                setOrderTotal(total);
                setTotal(total);
                document.body.scrollTo({ top: 0, behavior: "smooth" });
                setLoading(false);
              });
          });
      } else {
        fetch("/api/orders/my-orders/", {
          method: "post",
          body: JSON.stringify({ user: user }),
        })
          .then((res) => res.json())
          .then((data) => {
            fetch("/api/post-user/addresses/")
              .then((res) => res.json())
              .then((data) => {
                setAddresses(data);

                setDefaultAddress(data.filter((e) => e.default == true)[0]);
              });
            data.map((e) => {
              if (!e.quantity) e.quantity = 1;
            });
            setOrders(data);

            let total = 0;
            data.forEach((e) => {
              if (e.quantity || e.quantity == 0) {
                total += e.product.price * e.quantity;
              } else {
                total += e.product.price * 1;
              }
            });

            setOrderTotal(total);
            setTotal(total);
            document.body.scrollTo({ top: 0, behavior: "smooth" });
            setLoading(false);
          });
      }
    }
  }, [status]);

  //===== Parse Address ======//
  const parseAddress = (type, address) => {
    console.log(typeof address);
    if (typeof address == "object") {
      if (address.address_components) {
        console.log(
          address.address_components.filter((e) => e.types.includes(type))
        );
        return address.address_components.filter((e) => e.types.includes(type))
          .length > 0
          ? address.address_components.filter((e) => e.types.includes(type))[0]
              .long_name
          : "";
      }
      return;
    }
  };
  // ==== Save Addres =====//
  const saveAddress = (e) => {
    let theForm = document.getElementById("addressFrom");
    if (theForm.checkValidity()) {
      e.preventDefault();
      setAddressSaving(true);
      fetch("/api/post-user/save-address/", {
        method: "post",
        body: JSON.stringify({
          address: address,
          city: city,
          building: building,
          appartment: appartment,
          fname: fname,
          lname: lname,
          phone: phone,
          prefix: prefix,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setAddresses(data);
          setDefaultAddress(data.filter((e) => e.default == true)[0]);
          setAddingAddress(false);
          setAddressSaving(false);
        });
    }
  };
  // ==== change defualt Addres =====//
  const changDefualtAddress = (address) => {
    setAddressSaving(true);
    fetch("/api/post-user/save-address/", {
      method: "post",
      body: JSON.stringify(address),
    })
      .then((res) => res.json())
      .then((data) => {
        setAddresses(data);
        setDefaultAddress(data.filter((e) => e.default == true)[0]);
        setAddingAddress(false);
        setAddressSaving(false);
        setChangingAddress(false);
      });
  };

  const getOnPaymentAuthorized = (waybill, session, currencyCode, amount) => {
    return (event) => {
      // amount must be provided in cents
      performApplePayDebit(
        waybill,
        amount * 100,
        currencyCode,
        event.payment.token.paymentData
      )
        .then((response) => {
          if (response.code === 100) {
            session.completePayment(window.ApplePaySession.STATUS_SUCCESS);
            window.location.href='/store/waybill/'+response.waybill._id+'?tap_id='+response.waybill.charge.id;
          } else {
            console.log("Payment error in response ", JSON.stringify(response));
            session.completePayment(window.ApplePaySession.STATUS_FAILURE);
          }
        })
        .catch((err) => {
          console.log("Payment error ", JSON.stringify(err));
          session.completePayment(window.ApplePaySession.STATUS_FAILURE);
        });
    };
  };
  //======= Payment Process =========//
  const processPayment = () => {
    const waybill = {
      orders: orders,
      discount: discount,
      promocode: promocode,
      paymentMethod: paymentMethod,
      address: defaultAddress,
      total:Number(total).toFixed(2)
    };
    const request = {
      countryCode: "SA",
      currencyCode: "SAR",
      merchantCapabilities: ["supports3DS"],
      supportedNetworks: ["visa", "masterCard", "amex", "discover", "mada"],
      displayItems: orders.map((item) => {
        return {
          label: item.product.name["en"],
          amount: {
            value: `${Number(item.product.price * item.quantity).toFixed(2)}`,
            currency: "SAR",
          },
        };
      }),
      total: {
        label: _("PO"),
        type: "final",
        amount: Number(total).toFixed(2),
      },
    };

    // const paymentDetails = getPaymentDetails(
    //   "SAR",
    //   orders.map((item) => {
    //     return {
    //       label: item.product.name["en"],
    //       amount: Number(item.product.price * item.quantity).toFixed(2),
    //     };
    //   }),
    //   "TEST Transaction",
    //   Number(total).toFixed(2)
    // );

    const session = new window.ApplePaySession(14, request);
    session.onvalidatemerchant = async (event) => {
      // Call your own server to request a new merchant session.
      const merchantSession = await post(
        "/api/session/create/",
        { url: event.validationURL },
        "Could not get session"
      );

      console.log(merchantSession);
      session.completeMerchantValidation(merchantSession.statusMessage);
    };

    session.onpaymentauthorized = getOnPaymentAuthorized(
      waybill,
      session,
      "SAR",
      Number(total).toFixed(2)
    );

    session.oncancel = (event) => {
      // Payment cancelled by WebKit
      console.log("session Cancelled!!!!!");
    };

    session.begin();
  };
  const post = (path, data, errorMessage) => {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: path,
        data: data,
        headers: { "Content-Type": "application/json" },
      })
        .then((response) => {
          if (response.status === 200) {
            resolve(response.data);
          } else {
            reject(new Error(errorMessage));
          }
        })
        .catch((error) => {
          console.log(error);
          reject(error);
        });
    });
  };
  const TapPayment = async () => {
    setPlacingOrder(true);
    const waybill = {
      orders: orders,
      discount: discount,
      promocode: promocode,
      paymentMethod: paymentMethod,
      address: defaultAddress,
      total:Number(total).toFixed(2)
    };
    const req = await fetch('/api/payment/charge/',{method:'post',body:JSON.stringify({
      amount:Number(total).toFixed(2),
      description:'Payment for '+sumQty(orders)+' items',
      txn:orders[0]._id,
      order_id:orders[0]._id,
      first_name:defaultAddress.fname,
      last_name:defaultAddress.lname,
      email:session.user.email,
      phoneCode:defaultAddress.prefix?.value,
      phone:defaultAddress.phone,
      waybill:waybill
    })})
    const charge = await req.json();
    setPlacingOrder(false);
    if(charge.transaction)
    {
      window.location.href=charge.transaction.url;
    }
  };
  const placeOrder = async ()=>{
    setPlacingOrder(true);
    const waybill = {
      orders: orders,
      discount: discount,
      promocode: promocode,
      paymentMethod: paymentMethod,
      address: defaultAddress,
      total:Number(total).toFixed(2)
    };
    const req = await fetch('/api/payment/place-order/',{method:'post',body:JSON.stringify({
      waybill:waybill
    })})
    if(req.status==200)
    {
      const charge = await req.json();
      window.location.href="/store/waybill/"+charge.waybill_id;

    }
    setPlacingOrder(false);
    
  }
  const doPayment= async (token)=>{
    const waybill = {
      orders: orders,
      discount: discount,
      promocode: promocode,
      paymentMethod: paymentMethod,
      address: defaultAddress,
      total:Number(total).toFixed(2)
    };
    const req = await fetch('/api/payment/credit-cards/create/',{method:'post',body:JSON.stringify({
      amount:Number(total).toFixed(2),
      description:'Payment for '+sumQty(orders)+' items',
      waybill:waybill,
      token:token
    })})
    const charge = await req.json();
    setPlacingOrder(false);
    if(charge.transaction)
    {
      window.location.href=charge.transaction.url;
    }
    else
    {
      window.location.href=charge.waybill._id;
    }
    
  }
  return (
    <>
      <SEO
        pageTitle={checkedOut ? _("much... | Checkout") : _("much... | Cart")}
      />
      <Header />
      <div className="bg-white min-h-screen container">
        <div className="flex flex-wrap">
          <div className="w-full md:w-8/12 md:min-h-screen">
            <div className="mt-12">
              <div className="cart-header">
                <h1 className="text-lg font-bold bg-white text-gray-700 mb-4 border-b border-b-emerald-300">
                  {!checkedOut && <span>{_("Your Cart")}</span>}
                  {checkedOut && <span>{_("Review your order")}</span>}
                </h1>
              </div>
              <div className="orders-tables">
                <div className="text-gray-500">
                  <div className="card-header p-0 lg:flex hidden ">
                    <div className="w-2/12 border-b p-1 text-center">
                      {_("Image")}
                    </div>
                    <div className="w-4/12 border-b p-1 text-center">
                      {_("Product Name")}
                    </div>
                    <div className="w-2/12 border-b p-1 text-center">
                      {_("Quantity")}
                    </div>
                    <div className="w-2/12 border-b p-1 text-center">
                      {_("Price")}
                    </div>
                    <div className="w-2/12 border-b p-1 text-center"></div>
                  </div>
                </div>
                {loading && (
                  <div>
                    <div className="flex border-b overflow-hidden bg-gray-50 min-h-[120px]">
                      <div className="w-2/12 max-h-[90px] p-2 py-4">
                        <Placeholder.Graph
                          active
                          graph="image"
                          rowHeight={20}
                          className="max-h-[50px] max-w-[90px] mx-3"
                        />
                      </div>
                      <div className="w-4/12 p-2 py-3">
                        <Placeholder.Paragraph
                          active
                          rows={2}
                          rowMargin={10}
                          rowHeight={15}
                        />
                      </div>
                      <div className="2/12 text-center px-4 pt-3 ">
                        <Placeholder.Paragraph
                          className="mx-auto"
                          graph="square"
                        />
                      </div>
                      <div className="w-2/12 pt-3 px-4">
                        <Placeholder.Paragraph
                          active
                          rows={1}
                          rowMargin={20}
                          rowHeight={15}
                        />
                      </div>
                    </div>
                    <div className="flex border-b overflow-hidden  min-h-[120px]">
                      <div className="w-2/12 max-h-[90px] p-2 py-4">
                        <Placeholder.Graph
                          active
                          graph="image"
                          rowHeight={20}
                          className="max-h-[50px] max-w-[90px] mx-3"
                        />
                      </div>
                      <div className="w-4/12 p-2 py-3">
                        <Placeholder.Paragraph
                          active
                          rows={2}
                          rowMargin={10}
                          rowHeight={15}
                        />
                      </div>
                      <div className="2/12 text-center px-4 pt-3 ">
                        <Placeholder.Paragraph
                          className="mx-auto"
                          graph="square"
                        />
                      </div>
                      <div className="w-2/12 pt-3 px-4">
                        <Placeholder.Paragraph
                          active
                          rows={1}
                          rowMargin={20}
                          rowHeight={15}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {!loading &&
                  orders.map((order, index) => {
                    return (
                      <CartRow
                        checkedout={checkedOut}
                        key={index}
                        order={order}
                        index={index}
                        removeOrder={removeOrder}
                        setQty={setQty}
                      />
                    );
                  })}
                <div className="flex flex-wrap font-bold mt-4">
                  <div className="w-6/12 lg:w-8/12 text-end text-gray-600">
                    {_("Total")}:
                  </div>
                  <div className="text-gray-600 w-6/12 lg:w-2/12 text-center">
                    {Number(orderTotal).toFixed(2)} {_("SAR")}
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-full md:w-4/12 md:fixed md:bg-[#00487D] right-0 md:h-screen  p-2 md:p-[20px] md:overflow-y-auto">
            {!checkedOut && (
              <div className="max-w-xl mx-auto mb-20">
                <div className="card lg:mt-12">
                  <div className="card-header">
                    <h1 className="text-xl card-title">{_("Total Summary")}</h1>
                  </div>
                  <div className="card-body">
                    <div className="flex flex-wrap mt-4">
                      <div className="w-full lg:w-4/12 lg:text-end p-2">
                        {_("Sub total")}{" "}
                        <small>
                          ({sumQty(orders)}
                          {sumQty(orders) > 1 ? _("items") : _("item")})
                        </small>
                        :
                      </div>
                      <div className="w-full lg:w-8/12 font-bold p-2 border-b">
                        {Number(orderTotal).toFixed(2)} {_("SAR")}
                      </div>
                    </div>
                    {discount != 0 && (
                      <div className="flex flex-wrap mt-4">
                        <div className="w-full lg:w-4/12 lg:text-end p-2 text-orange-400">
                          {_("Discount")}:
                        </div>
                        <div className="w-full lg:w-8/12 font-bold p-2 text-orange-400 border-b border-b-orange-400">
                          {discount} {_("SAR")}
                        </div>
                      </div>
                    )}
                    <div className="flex flex-wrap mt-4">
                      <div className="w-full lg:w-4/12 lg:text-end p-2 font-bold">
                        {_("Total")}:
                      </div>
                      <div className="w-full lg:w-8/12 font-bold p-2 border-b">
                        {Number(total).toFixed(2)} {_("SAR")}
                      </div>
                    </div>
                    <div className="flex flex-wrap mt-4">
                      <div className="w-full lg:w-4/12 lg:text-end  p-2">
                        {" "}
                        {_("Promo Code?")}
                      </div>
                      <div className="w-8/12 lg:w-6/12">
                        <input
                          value={code}
                          onChange={(e) => setCode(e.target.value)}
                          className="form-control uppercase rounded-none rounded-l m-0 h-12 focus:border-green focus:shadow-none"
                          placeholder={_("Insert Promo Code")}
                        />
                      </div>
                      <div className="w-4/12 md:w-2/12">
                        <button
                          onClick={() => {
                            checkPromo(code);
                          }}
                          className="bg-green px-3 p-2  rounded-r font-bold h-12 text-blue-900 disabled:text-gray-400"
                          disabled={checkingPromo || !code}
                        >
                          {_("Apply")}
                        </button>
                      </div>
                      <div className="w-full">
                        <span className="text-red-500">{_(promoMessage)}</span>
                      </div>
                    </div>

                    <div className="flex mt-4">
                      <div className="w-full p-2">
                        <button
                        disabled={!orders || (orders && orders.length==0)}
                          onClick={() => {
                            checkout();
                          }}
                          className="bg-green p-2 rounded w-full font-bold text-blue-900  disabled:bg-gray-200 disabled:text-gray-500"
                        >
                          {_("Checkout")}{" "}
                          <i className="fa fa-arrow-right float-right  text-white -m-2 rounded-r h-[36px] pt-[12px] bg-blue-900 p-[10px]"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {checkedOut && (
              <div className="max-w-xl mx-auto mb-20">
                <div className="card lg:mt-12">
                  <div className="card-header">
                    <h1 className="text-xl card-title">{_("Checkout")}</h1>
                  </div>
                  <div className="card-body">
                    {!changingAddress && !addingAddress && (
                      <div className="w-full mb-4 mt-10 border rounded p-4">
                        <h4 className=" bg-blue-900 z-50 -mt-10 -ml-4 text-white text-sm w-fit p-2 rounded select-none">
                          {_("Ship To")}
                        </h4>
                        <div className="text-center float-right">
                          {defaultAddress && session && (
                            <button
                              onClick={() => {
                                setChangingAddress(true);
                              }}
                              className="text-blue-900 underline"
                            >
                              {_("Change")}
                            </button>
                          )}
                          {!defaultAddress &&
                            session &&
                            addresses &&
                            addresses.length > 0 && (
                              <button className="text-blue-900 underline">
                                {_("Select")}
                              </button>
                            )}
                          {!defaultAddress &&
                            session &&
                            addresses.length == 0 &&
                            !addingAddress && (
                              <button
                                onClick={() => setAddingAddress(true)}
                                className="text-blue-900 underline"
                              >
                                {_("Add address")}
                              </button>
                            )}
                          {!defaultAddress && !session && (
                            <Link href="/signin/?callback=https://www.much.sa/store/cart/">
                              <button className="text-blue-900 underline">
                                {_("Login")}
                              </button>
                            </Link>
                          )}
                        </div>
                        {defaultAddress && session && !changingAddress && (
                          <div className="w-full">
                            <div className="w-full max-w-full  mt-2 border rounded p-1 whitespace-nowrap overflow-hidden overflow-ellipsis">
                              {defaultAddress.address.formatted_address}
                            </div>
                            {defaultAddress.city &&
                              defaultAddress.city == "Riyadh" && (
                                <span className="text-blue-900 font-bold">
                                  <i className="fa fa-info-circle"></i>{" "}
                                  {_(
                                    "The expected delivery time is 4 working days"
                                  )}
                                </span>
                              )}
                            {defaultAddress.city &&
                              defaultAddress.city != "Riyadh" && (
                                <span className="text-blue-900 font-bold">
                                  <i className="fa fa-info-circle"></i>{" "}
                                  {_(
                                    "The expected delivery time is 7 working days"
                                  )}
                                </span>
                              )}
                          </div>
                        )}
                      </div>
                    )}
                    {addingAddress && (
                      <div className="flex flex-wrap">
                        <div className="w-full mb-4  border rounded p-2 mt-4">
                          <h4 className="bg-blue-900 -m-6 text-white text-sm w-fit p-2 rounded select-none">
                            {_("Add new Address")}
                          </h4>
                          <form
                            id="addressFrom"
                            onSubmit={(e) => {
                              e.preventDefault();
                            }}
                          >
                            <div className="flex flex-wrap mt-8">
                              <div className="w-full lg:w-4/12 my-2">
                                {_("National Address")}:
                              </div>
                              <div className="w-full lg:w-8/12">
                                <Autocomplete
                                  defaultValue={address?.formatted_address}
                                  apiKey={
                                    "AIzaSyBeoXqvPbcjNUkp6FOjmYNkqx6oLmYvcYg"
                                  }
                                  options={{
                                    types: ["geocode", "establishment"],
                                    componentRestrictions: {
                                      country: country,
                                    },
                                  }}
                                  onPlaceSelected={(place) => {
                                    console.log(place);
                                    setAddress(place);
                                    setBuilding(
                                      parseAddress("street_number", place)
                                    );
                                    setCity(parseAddress("locality", place));
                                  }}
                                  className="w-full form-control text-sm"
                                />
                              </div>
                            </div>
                            {address && (
                              <div>
                                <div className="flex flex-wrap mt-2">
                                  <div className="w-full lg:w-4/12 my-2">
                                    {_("Building No.")}
                                  </div>
                                  <div className="w-full lg:w-8/12">
                                    <input
                                      onChange={(e) =>
                                        setBuilding(e.target.value)
                                      }
                                      value={building}
                                      type="text"
                                      className="form-control text-sm"
                                      placeholder={_("Building Number")}
                                      required
                                    />
                                  </div>
                                </div>
                                <div className="flex flex-wrap mt-2">
                                  <div className="w-full lg:w-4/12 my-2">
                                    {_("Apartment No.")}
                                  </div>
                                  <div className="w-full lg:w-8/12">
                                    <input
                                      value={appartment}
                                      onChange={(e) =>
                                        setAppartment(e.target.value)
                                      }
                                      type="text"
                                      className="form-control text-sm"
                                      placeholder={_("Appartment number")}
                                      required
                                    />
                                  </div>
                                </div>
                                <div className="flex flex-wrap mt-2">
                                  <div className="w-full lg:w-4/12 my-2">
                                    {_("Name")}
                                  </div>
                                  <div className="w-6/12 lg:w-4/12">
                                    <input
                                      value={fname}
                                      onChange={(e) => setFname(e.target.value)}
                                      type="text"
                                      className="form-control text-sm rounded-none rounded-l"
                                      placeholder={_("First Name")}
                                      required
                                    />
                                  </div>
                                  <div className="w-6/12 lg:w-4/12">
                                    <input
                                      value={lname}
                                      onChange={(e) => setLname(e.target.value)}
                                      type="text"
                                      className="form-control text-sm rounded-none rounded-r"
                                      placeholder={_("Last Name")}
                                      required
                                    />
                                  </div>
                                </div>
                                <div className="flex flex-wrap mt-2">
                                  <div className="w-full lg:w-4/12 my-2">
                                    {_("Mobile")}
                                  </div>
                                  <div className="w-4/12 lg:w-2/12">
                                    <Select
                                      value={prefix || defaultCountry}
                                      onChange={(e) => {
                                        setPrefix(e);
                                      }}
                                      className="text-xs outline-0 focus:outline-0"
                                      options={options}
                                    ></Select>
                                  </div>
                                  <div className="w-8/12 lg:w-6/12">
                                    <input
                                      value={phone}
                                      onChange={(e) => setPhone(e.target.value)}
                                      required
                                      type="number"
                                      className="form-control text-sm rounded-none rounded-r h-10 outline-0 focus:shadow-none focus:border-gray-300 focus:outline-none"
                                      placeholder={_("5XXXXXXXX")}
                                    />
                                  </div>
                                </div>
                                <div className="w-full">
                                  <button
                                    type="button"
                                    onClick={() => {
                                      setAddingAddress(false);
                                      setAddress(null);
                                    }}
                                    className="bg-blue-900 text-green p-2 font-bold rounded float-right my-2 mx-1"
                                  >
                                    {_("Cancel")}
                                  </button>
                                  <button
                                    onClick={(e) => saveAddress(e)}
                                    className="bg-green text-blue-900 p-2 font-bold rounded float-right my-2"
                                  >
                                    {_("Save")}
                                  </button>
                                </div>
                              </div>
                            )}
                          </form>
                        </div>
                      </div>
                    )}
                    {changingAddress &&
                      addresses &&
                      addresses &&
                      addresses.length > 0 && (
                        <div className="w-full mb-4 mt-4 border rounded p-2">
                          <h4 className="bg-blue-900 -m-6 text-white text-sm w-fit p-2 rounded select-none">
                            {_("Select one of below addresses or add new")}
                          </h4>
                          <div className="mt-8 max-h-[300px] overflow-y-auto">
                            {addresses.map((e, index) => {
                              return (
                                <div
                                  key={index}
                                  className="flex flex-wrap border-b"
                                >
                                  <div className="w-10/12 my-2">
                                    {e.address.formatted_address}
                                  </div>
                                  <div className="w-2/12 my-2">
                                    <button
                                      onClick={() => {
                                        changDefualtAddress(e);
                                      }}
                                      className="bg-green p-2 rounded text-blue-900 w-full text-center"
                                    >
                                      {!addressSaving &&
                                        defaultAddress._id != e._id && (
                                          <i className="fa fa-circle"></i>
                                        )}
                                      {!addressSaving &&
                                        defaultAddress._id == e._id && (
                                          <i className="fa fa-check"></i>
                                        )}
                                      {addressSaving && (
                                        <i className="fa fa-spinner animate-spin"></i>
                                      )}
                                    </button>
                                  </div>
                                </div>
                              );
                            })}
                          </div>
                          <div className="flex flex-wrap mt-2">
                            <div className="w-full">
                              <button
                                onClick={() => {
                                  setChangingAddress(false);
                                  setAddingAddress(true);
                                }}
                                className="bg-green p-2 rounded text-blue-900 w-full text-center"
                              >
                                {_("Add new address")}
                              </button>
                            </div>
                          </div>
                        </div>
                      )}
                    {!paymentMethod && defaultAddress && (
                      <div className="w-full mb-4 mt-10 border rounded p-2 ">
                        <h4 className="bg-blue-900 -mt-8 text-white text-sm w-fit p-2  rounded select-none">
                          {_("Payment Method")}
                        </h4>
                        <div className="mt-2">
                          <RadioGroup
                            value={paymentMethod}
                            onChange={setPaymentMethod}
                          >
                            {!hideApplePay && (
                              <Radio value="applepay">
                                <div
                                  className={`${
                                    paymentMethod == "applepay"
                                      ? "text-blue-900 border-2 border-blue-900"
                                      : "border"
                                  } flex text-sm  rounded p-1 -my-3 w-56`}
                                >
                                  <i className="fab fa-apple-pay text-3xl text-center"></i>{" "}
                                  <div className="mx-1 mt-1">
                                    {_("Apple Pay")}
                                  </div>
                                </div>
                              </Radio>
                            )}
                            <Radio value="creditcard">
                              <div
                                className={`${
                                  paymentMethod == "creditcard"
                                    ? "text-blue-900 border-2 border-blue-900"
                                    : "border"
                                } flex text-sm  rounded p-1 -my-3 w-56`}
                              >
                                <i className="fas fa-credit-card text-3xl text-center"></i>
                                <div className="mx-1 mt-1">
                                  {_("Credit Card")}
                                </div>
                              </div>
                            </Radio>
                            {/* <Radio value="tap">
                              <div
                                className={`${
                                  paymentMethod == "transfer"
                                    ? "text-blue-900 border-2 border-blue-900"
                                    : "border"
                                } flex text-sm  rounded p-1 -my-3 w-56`}
                              >
                                <img src="https://pbs.twimg.com/profile_images/1214833460067610625/0whceCIX_400x400.jpg" className="h-10 w-10" />
                                <div className="mx-1 mt-2">
                                  {_("Tap")}
                                </div>
                              </div>
                            </Radio> */}
                            <Radio value="transfer">
                              <div
                                className={`${
                                  paymentMethod == "transfer"
                                    ? "text-blue-900 border-2 border-blue-900"
                                    : "border"
                                } flex text-sm  rounded p-1 -my-3 w-56`}
                              >
                                <i className="fas fa-money-check-alt text-3xl text-center"></i>{" "}
                                <div className="mx-1 mt-1">
                                  {_("Bank Transfer")}
                                </div>
                              </div>
                            </Radio>
                            <Radio value="cod">
                              <div
                                className={`${
                                  paymentMethod == "cod"
                                    ? "text-blue-900 border-2 border-blue-900"
                                    : "border"
                                } flex text-sm  rounded p-1 -my-3 w-56`}
                              >
                                <i className="fas fa-money-bill text-3xl text-center"></i>{" "}
                                <div className="mx-1 mt-1">
                                  {_("Cash On Delivery")}
                                </div>
                              </div>
                            </Radio>
                          </RadioGroup>
                        </div>
                      </div>
                    )}
                    {paymentMethod=='tap' && <div className="w-full mb-4 mt-10 border rounded p-2 ">
                    <h4 className="bg-blue-900 -mt-8 text-white text-sm w-fit p-2  rounded select-none">
                          {_("Click To Pay")}
                        </h4>
                        <div className="mt-2">
                        <button
                        disabled={placingOrder}
                          className="p-3 w-full bg-blue-900 text-green font-bold rounded disabled:bg-gray-200 disabled:text-gray-400"
                            onClick={() => {
                              TapPayment();
                            }}
                          >{_('Pay')} {Number(total).toFixed(2)} {'SAR'} {_('Now')} {placingOrder &&<i className="fa fa-spinner animate-spin"></i>}
                          </button>
                        </div>
                      </div>}
                      {(paymentMethod=='cod'|| paymentMethod=='transfer') && <div className="w-full mb-4 mt-10 border rounded p-2 ">
                    <h4 className="bg-blue-900 -mt-8 text-white text-sm w-fit p-2  rounded select-none">
                          {_("Place you order")}
                        </h4>
                        <div className="mt-2">
                        <button
                        disabled={placingOrder}
                          className="p-3 w-full bg-blue-900 text-green font-bold rounded disabled:bg-gray-200 disabled:text-gray-400"
                            onClick={() => {
                              placeOrder();
                            }}
                          >{_('Place Order')} {placingOrder &&<i className="fa fa-spinner animate-spin"></i>}</button>
                        </div>
                      </div>}
                    
                      {paymentMethod=='creditcard' &&<div className="w-full mb-4 mt-10 border rounded p-2 ">
                        <h4 className="bg-blue-900 -mt-8 text-white text-sm w-fit p-2  rounded select-none">
                          {_("Inset your card details")}
                        </h4>
                        <div className="mt-2">
                          
                          
                            <TapCard processPayment={doPayment} />
                          
                        </div>
                      </div>}
                    
                    {paymentMethod == "applepay" && (
                      <ApplePayButton
                        merchantIdentifie="merchant.apple_pay"
                        onClick={() => processPayment()}
                      />
                    )}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="z-50">
      <Footer />
      </div>
    </>
  );
}
