
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import Hero from "../webComponents/hero/Hero";
import FeaturesMain from "../webComponents/features";
import Wrapper from "../layout/wrapper";
import SEO from "../webComponents/Seo";
import Products from "../webComponents/products/Products";
import Address from "../webComponents/Address";
import Contact from "../webComponents/Contact";
import SwitchDark from "../webComponents/switch/SwitchDark";
import { useRive,Layout} from '@rive-app/react-canvas';
import { translation } from "../lib/translations";
import { useRouter } from "next/router";
import Accordion from "../components/accordion";
import { useEffect, useState } from "react";

const HomeDark = () => {
  const {query} =useRouter();
  const router = useRouter();
  const { rive, RiveComponent } = useRive({
    src: '/rive/logo.riv',
    autoplay:true,
  })

  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }
  
  const menuItem = [
    { icon: "fa-home", menuName: _("Home"),name:"/" },
    { icon: "fa-star", menuName: _("Features"),name:"features" },
    { icon: "fab fa-product-hunt", menuName: _("Products"),name:'products' },
    { icon: "fas fa-question-circle", menuName: "FAQ",name:'faq' },
    { icon: "fa-envelope", menuName: _("Contact"),name:'contact' },
    
  ];
  
  let dm =1;
  
    

  return (
    <Wrapper>
      <SEO pageTitle={"much... | Digital Cards"} />
      <div className="container hidden lg:block">
        <nav className="fixed">
          <div className="logo w-32 h-20"><RiveComponent /></div>
        </nav>
      </div>
      <div className="blue">
        <SwitchDark />

        {/* End Switcher */}
        <Tabs defaultIndex={dm}>
          <div className="header">
            <TabList className=" icon-menu  revealator-slideup revealator-once revealator-delay1" >
              {menuItem.map((item, i) => (
                <Tab className="icon-box" key={i} onClick={()=>{
                  router.push(item.name);
                }}>
                  <i className={`fa ${item.icon}`}></i>
                  <h2>{item.menuName}</h2>
                </Tab>
              ))}
            </TabList>
          </div>
          {/* End Menu Content */}

          <div className="tab-panel_list">
            <TabPanel className="home ">
            </TabPanel>
            
            <TabPanel className="about">
              <div data-aos-duration="1200">
                <div className="title-section lg:text-start text-center">
                  <h1>
                    <span>{_("Features")}</span>
                  </h1>
                  <span className="title-bg text-6xl -mt-6">{_("What are the benefits?")}</span>
                </div>
                {/* End title */}
                <FeaturesMain />
              </div>
            </TabPanel>
            {/* About Content Ends */}

            {/* Products Content Starts */}
            <TabPanel className="portfolio professional">
            </TabPanel>
            {/* Products Content Ends */}
            {/* Products Content Starts */}
            <TabPanel className="faq professional">
            </TabPanel>
            {/* Products Content Ends */}

            {/* Contact Content Starts */}
            <TabPanel className="contact">
            </TabPanel>
            {/* Contact Content Ends */}

            
          </div>
        </Tabs>
      </div>
    </Wrapper>
  );
};

export default HomeDark;
