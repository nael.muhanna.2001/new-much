import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    // locale is in ctx.locale

    return { ...initialProps, locale: ctx?.locale || "en" };
  }

  render = () => (
    <Html
      dir={this.props.locale === "ar" ? "rtl" : "ltr"}
      lang={this.props.locale}
      style={{ scrollBehavior: "smooth",overflowX :'hidden' }}
    >
      <Head>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;500;600;700;800;900&display=swap"
        ></link>
        {/* <link
          rel="stylesheet"
          type="text/css"
          href="/js/fullpage/fullpage.css"
        /> */}
        <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}

export default MyDocument;
