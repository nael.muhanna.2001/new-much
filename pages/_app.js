import "../styles/index.scss";
import "../styles/globals.css";

import { SessionProvider } from "next-auth/react";

import "@fortawesome/fontawesome-free/css/all.min.css";
import Script from "next/script";

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}) {
  return (
    <>
      <Script
        id="applePay"
        strategy="lazyOnload"
        src="https://applepay.cdn-apple.com/jsapi/v1/apple-pay-sdk.js"
      ></Script>
      <Script
        id="tapCard"
        strategy="lazyOnload"
        src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.4/bluebird.min.js"
      ></Script>
      <Script
        id="tapPayment"
        strategy="lazyOnload"
        src="https://secure.gosell.io/js/sdk/tap.min.js"
      ></Script>
      <Script
        id="google-script"
        strategy="lazyOnload"
        src="https://www.googletagmanager.com/gtag/js?id=G-SQ76QG2VMY"
      ></Script>
      <Script id="google-tag-script" strategy="lazyOnload">
        {`window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-SQ76QG2VMY');`}
      </Script>
      {/* <Script id="Adsense-id"
   data-ad-client="ca-pub-7133076433044697"
   async={true}
   onError={ (e) => { console.error('Script failed to load', e) }}
   strategy="beforeInteractive"  src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" /> */}

      <SessionProvider session={session}>
        {Component.auth ? (
          <Auth>
            <Component {...pageProps} />
          </Auth>
        ) : (
          <Component {...pageProps} />
        )}
      </SessionProvider>
    </>
  );
}
function Auth({ children }) {
  const { status } = useSession({ required: true });
  if (status === "loading") {
    return <div>Loading...</div>;
  }

  return children;
}
