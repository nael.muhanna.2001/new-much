import fs from "fs";
import path from "path";
export default async function handler(req, res) {
  const {color} = req.query;
  const filePath = path.resolve(".", "public/qrbg.svg");
  let svg;
 await fs.readFile(filePath,'utf-8',(err,data)=>{
   svg = data 
   svg = svg.replace('.st0{fill:#FFFFFF;}','path{fill:#'+color+'} .st0{fill:#FFFFFF;}')
   return res
    .setHeader("Content-Type", "image/svg+xml")
    .send(svg);
  });
  
  
}
