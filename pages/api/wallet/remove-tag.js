import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const { user_id, card_id, tag } = req.query;
  const client = await connectToDatabase();
  const user = await client
    .db()
    .collection("users")
    .updateOne(
      { _id: ObjectId(user_id), "wallet.card_id": card_id },
      {
        $pull: {
          "wallet.$.tags": tag,
        },
      }
    );

  return res.status(200).json(user);
}
