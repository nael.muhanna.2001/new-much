import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";
import { getToken } from "next-auth/jwt";
export default async function handler(req, res) {
  const { card_id } = req.query;
  let token = await getToken({ req });

  let user = null;
  if (token) user = token.user;
  if (user && card_id) {
    const user_id = user._id;
    const client = await connectToDatabase();
    let dbUser = await client
      .db()
      .collection("users")
      .findOne({ _id: ObjectId(user_id) });
    if (dbUser.wallet) {
      let wallet = dbUser.wallet;
      if (wallet.filter((e) => e.card_id == card_id).length) {
        return res.status(202).json(dbUser);
      } else {
        wallet.push({ card_id: card_id, saved_at: new Date() });
        dbUser.wallet = wallet;
        delete dbUser._id;
        const userInsertion = await client
          .db()
          .collection("users")
          .updateOne(
            {
              _id: ObjectId(user_id),
            },
            {
              $set: dbUser,
            },
            { upsert: false }
          );

        user = await client
          .db()
          .collection("users")
          .findOne({ _id: ObjectId(user_id) });

        return res.status(201).json(user);
      }
    } else {
      let wallet = [];
      wallet.push({ card_id: card_id, saved_at: new Date() });
      dbUser.wallet = wallet;
      delete dbUser._id;

      const userInsertion = await client
        .db()
        .collection("users")
        .updateOne(
          {
            _id: ObjectId(user_id),
          },
          {
            $set: dbUser,
          },
          { upsert: true }
        );

      dbUser = await client
        .db()
        .collection("users")
        .findOne({ _id: ObjectId(user_id) });

      
      return res.status(201).json(dbUser);
    }
  }
  return res.status(200).json();
}
