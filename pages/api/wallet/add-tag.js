import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const { user_id, card_id, tag } = req.query;
  let tags = [];
  if(req.body)
  {
    
    tags = JSON.parse(req.body).tags;
    
  }
  const client = await connectToDatabase();
  const userisetion = await client
    .db()
    .collection("users")
    .updateOne(
      { _id: ObjectId(user_id), "wallet.card_id": card_id },
      {
        $set: {
          "wallet.$.tags": tags,
        },
      }
    );
    const user = await client.db().collection('users').findOne({_id:ObjectId(user_id)});

  return res.status(200).json(user);
}
