import { connectToDatabase } from "../../../../lib/db";

export default async function notes(req,res)
{
    

    const {card_id,writer_id} = req.query;

    if(card_id,writer_id)
    {
        const client = await connectToDatabase();
        const notes = await client.db().collection('notes').find({
            card_id:card_id,
            writer_id:writer_id,
            $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        }).toArray();
        return res.status(200).json(notes);
    }

}