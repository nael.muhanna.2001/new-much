import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../../lib/db";

export default async function handler(req, res) {
  if (req.body) {
    const { note_id, card_id, writer_id } = JSON.parse(req.body);
    const client = await connectToDatabase();
    const noteDeteting = await client
      .db()
      .collection("notes")
      .updateOne(
        {
          _id: ObjectId(note_id),
          card_id: card_id,
          writer_id: writer_id,
        },
        {
          $set: {
            deleted_at: new Date(),
          },
        },
        { upsert: false }
      );
      const note = await client.db().collection('notes').findOne({_id:ObjectId(note_id)});
      return res.status(201).json(note);    

  }
}
