import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../../lib/db";

export default async function handler(req, res) {
  if (req.body) {
    const {card_id,writer_id,note_text} = JSON.parse(req.body);
    const client = await connectToDatabase();
    const noteInsertion = await client.db().collection("notes").insertOne({
      card_id: card_id,
      writer_id: writer_id,
      note_text: note_text,
      created_at: new Date(),
    });
    const note = await client.db().collection('notes').findOne({
      _id:ObjectId(noteInsertion.insertedId)
    })
    
    return res.status(200).json( note);
  }
  else
  {
    return res.status(404).json();
  }
}
