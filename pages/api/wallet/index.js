import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";
import { getToken } from "next-auth/jwt"
export default async function handler(req, res) {
  
  let card_ids = [];
  let tags = [];
  let user =null;
  const token = await getToken({ req })
  
  if(token && token.user)
    {
      user = token.user;
    }
  if (user) {
    const client = await connectToDatabase();
    const dbUser = await client
      .db()
      .collection("users")
      .findOne({
        _id: ObjectId(user._id),
      });
    if (dbUser.wallet) {
      dbUser.wallet.forEach((e, k) => {
        if (e.tags) {
          
          tags = tags.concat(e.tags);
        }

        if (e.card_id != "undefined") card_ids.push(ObjectId(e.card_id));
      });
      const cards = await client
        .db()
        .collection("cards")
        .find({
          _id: { $in: card_ids },
        })
        
        .toArray();
      return res.status(200).json({ user: dbUser, cards: cards,tags:tags });
    }
    return res.status(200).json({ user: user, cards: [],tags:tags });
  }
  return res.status(404).json();
}
