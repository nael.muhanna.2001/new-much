import { ObjectId } from "mongodb";
import { hashPassword } from "../../../lib/auth";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  if (req.method == "POST") {
    let request = JSON.parse(req.body);
    if (request.link && request.pin) {
      const hashedPin =  await hashPassword(request.pin);
      const client = await connectToDatabase();
      const isExists = await client
        .db()
        .collection("preLinks")
        .findOne({ link: request.link });
      if (!isExists) {
        const createdLink = await client.db().collection("preLinks").insertOne({
          link: request.link,
          pin: hashedPin,
          code:request.pin,
          created_at: new Date(),
          charged_at: request.charged_at,
          activated_at: request.activated_at,
          product: request.product,
        });
        const link = await client
          .db()
          .collection("preLinks")
          .findOne({ _id: ObjectId(createdLink.insertedId) });
        return res.status(201).json(link);
      }
      if(isExists)
      {
        return res.status(204).json({messege:'The link is already exists'})
      }
    }
  }
}
