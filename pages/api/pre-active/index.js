import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req,res)
{
    const token = await getToken({ req })
    console.log(token);
    if(token && token.user.permissions && token.user.permissions.includes('admin'))
    {
        const client = await connectToDatabase();

        const links = await client.db().collection('preLinks').find().toArray();
        return res.status(200).json(links);
    }
    return res.status(500).send('There is something worng!')
}