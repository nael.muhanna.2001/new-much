import { ObjectId } from "mongodb";
import { verifyPassword } from "../../../lib/auth";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  if (req.method == "POST" && req.body) {
    let request = JSON.parse(req.body);
    if (request.link && request.pin) {
      const client = await connectToDatabase();

      const retriveLink = await client.db().collection("preLinks").findOne({
        link: request.link,
      });
      if (retriveLink) {
        const isValid = await verifyPassword(request.pin, retriveLink.pin);
        if (isValid) {
          const activeLink = await client
            .db()
            .collection("preLinks")
            .updateOne(
              { link:request.link },
              { $set: { tested_at: new Date() } },
              { upsert: true }
            );
          return res.status(200).json({ valid: true });
        }
        return res.status(204).json({ message: "Bad PIN" });
      }
      return res.status(404).json({ message: "No valid link" });
    }
    return res.status(500).json({ message: "Bad request" });
  }
  return res.status(500).json({ message: "Bad request method" });
}
