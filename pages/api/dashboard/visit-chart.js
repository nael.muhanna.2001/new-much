import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function (req, res) {
  let { type, dateRange, assemblyBy,selectCards,locale } = JSON.parse(req.body);
  if (!assemblyBy) {
    assemblyBy = "day";
  }
  if(!locale)
  {
    locale='en';
  }

  let token = await getToken({req});
  let user = token.user;
  let query = { user_id: user._id,$or: [{ deleted_at: { $exists: false } }, { deleted_at: null }] };
  if (type) {
    query.cardType = type;
  }

  const client = await connectToDatabase();
  const cards = await client.db().collection("cards").find(query).sort({created_at:-1}).toArray();

  let card_ids = [];
  if (cards.length &&!selectCards.length) {
    for (let i = 0; i < cards.length; i++) {
      card_ids.push(cards[i]._id + "");
    }
  }
  else if(cards.length && selectCards && selectCards.length)
  {
    card_ids = selectCards;
  }

  let start = new Date(dateRange[0]);
  let end = new Date(dateRange[1]);
  let period = (end - start) / 86400000;
  let days = [];
  let dayVisit = [];

  if (assemblyBy == "week") {
    start = new Date(dateRange[0]);
    end = new Date(dateRange[1]);
    period = Math.ceil((end - start) / (86400000 * 7));
    
    days = [];
    dayVisit = [];
  }
  if (assemblyBy == "month") {
    start = new Date(dateRange[0]).setHours(0,0,0,0);
    end = new Date(dateRange[1]).setHours(23,59,59,999);
    period = Math.ceil((end - start) / (86400000 * 30));
    days = [];
    dayVisit = [];
  }
  for (let d = assemblyBy=='day'?-1:0; d <period; d++) {
    let newDay,endDay,actualDay,actualEndDay;
    if(assemblyBy=='day')
    {
        newDay = new Date(start.getTime() + (86400000 * (d+1)));
        newDay.setHours(0, 0, 0, 0)
     endDay = new Date(newDay);
    endDay.setHours(20, 59, 59, 999);
     actualDay = new Date(newDay);
    actualDay.setDate(actualDay.getDate());
    days.push(
      actualDay.toLocaleDateString(locale, {
        weekday: "short",
        month: "short",
        day: "numeric",
      })
    );
    
    }
     
    if (assemblyBy == "week") {
      newDay = new Date(start.getTime() + (86400000 * d*7));
      endDay = new Date(newDay);
      endDay.setDate(endDay.getDate() + 7);
      actualDay = new Date(newDay);
      actualEndDay = new Date(endDay);
      actualDay.setDate(actualDay.getDate());
      actualEndDay.setDate(endDay.getDate());
    days.push(
      actualDay.toLocaleDateString(locale, {
        weekday: "short",
        month: "short",
        day: "numeric",
      })+" - "+actualEndDay.toLocaleDateString(locale, {
        weekday: "short",
        month: "short",
        day: "numeric",
      })
    );
    }
    if (assemblyBy == "month") {
        newDay = new Date(new Date(start).getTime() + (86400000 * d*30));
        endDay = new Date(newDay);
        endDay.setDate(endDay.getDate() + 30);
        actualDay = new Date(newDay);
        actualEndDay = new Date(endDay);
        actualDay.setDate(actualDay.getDate() + 1);
        actualEndDay.setDate(endDay.getDate() + 1);
        
      days.push(
        actualDay.toLocaleDateString(locale, {
          weekday: "short",
          month: "short",
          day: "numeric",
          
        })+" - "+actualEndDay.toLocaleDateString(locale, {
          weekday: "short",
          month: "short",
          day: "numeric",
          year:"numeric"
        })
      );
      }
  
    const visits = await client
      .db()
      .collection("visits")
      .aggregate([
        {
          $match: {
            card_id: { $in: card_ids },
            created_at: { $gte: newDay, $lt: endDay },
          },
        },
        {
          $group: {
            _id: null,
            card_id: { $push: "$card_id" },
            count: { $sum: 1 },
          },
          
        },
      ])
      .toArray();
    dayVisit.push(visits.length ? visits[0] : {});
  }
  let response = {
    cards: cards,
    labels: days,
    dayVisit,
    dayVisit,
    card_ids:card_ids
  };
  return res.status(200).json(response);
}
