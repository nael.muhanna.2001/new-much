import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const {type} = req.query;
  let query =null;
  let token = await getToken({req});
  const user =token.user;
    
    if(type)
    {
         query= {
            user_id:user._id,
            cardType:type,
            $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }]
        }
    }
    else
    {
         query = {
            user_id:user._id
        }
    }
  const client = await connectToDatabase();
  const cards = await client
    .db()
    .collection("cards")
    .find(query)
    .toArray();
  const card_ids = [];
  if (cards.length) {
    for (let i = 0; i < cards.length; i++) {
      card_ids.push(cards[i]._id + "");
    }
  }
  var day_start = new Date();
  day_start.setHours(0, 0, 0, 0);
  var day_end = new Date();
  day_end.setHours(23, 59, 59, 999);
  const day_visit_count = await client
    .db()
    .collection("visits")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
          created_at: { $gte: day_start, $lt: day_end },
        },
      },
      {
        $group: { _id: "$user_id", count: { $sum: 1 } },
      },
    ])
    .toArray();
    var past_day_start = new Date();
    past_day_start.setDate(past_day_start.getDate()-1);
    past_day_start.setHours(0, 0, 0, 0);
    var past_day_end = new Date();
    past_day_end.setDate(past_day_end.getDate()-1);
    past_day_end.setHours(23, 59, 59, 999);
    const past_day_visit_count = await client
      .db()
      .collection("visits")
      .aggregate([
        {
          $match: {
            card_id: { $in: card_ids },
            created_at: { $gte: past_day_start, $lt: past_day_end },
          },
        },
        {
          $group: { _id: "$user_id", count: { $sum: 1 } },
        },
      ])
      .toArray();
  var week_start = new Date();
  week_start.setDate(week_start.getDate() - 6);
  var week_end= new Date();
  
  const week_visit_count = await client
    .db()
    .collection("visits")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
          created_at: { $gte: week_start,$lt:week_end },
        },
      },
      {
        $group: { _id: "$user_id", count: { $sum: 1 } },
      },
    ])
    .toArray();
    var past_week_start = new Date();
  past_week_start.setDate(week_start.getDate() - 13);
  var past_week_end= new Date();
  past_week_end.setDate(past_week_end.getDate()-6)
  
  const past_week_visit_count = await client
    .db()
    .collection("visits")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
          created_at: { $gte: past_week_start,$lt:past_week_end },
        },
      },
      {
        $group: { _id: "$user_id", count: { $sum: 1 } },
      },
    ])
    .toArray();
  var month_start = new Date();
  month_start.setDate(1);
  const month_visit_count = await client
    .db()
    .collection("visits")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
          created_at: { $gte: month_start },
        },
      },
      {
        $group: { _id: "$user_id", count: { $sum: 1 } },
      },
    ])
    .toArray();
    var past_month_start = new Date();
  past_month_start.setMonth(past_month_start.getMonth()-1);
  past_month_start.setDate(1);
  var past_month_end= new Date();
  past_month_end.setDate(0);
  
  const past_month_visit_count = await client
    .db()
    .collection("visits")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
          created_at: { $gte: past_month_start,$lt:past_month_end },
        },
      },
      {
        $group: { _id: "$user_id", count: { $sum: 1 } },
      },
    ])
    .toArray();
    const all_visit_count = await client
    .db()
    .collection("visits")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids }
        },
      },
      {
        $group: { _id: null, count: { $sum: 1 } },
      },
    ])
    .toArray();
    let response = {
        day_visit_count:day_visit_count.length?day_visit_count[0].count:0,
        past_day_visit_count:past_day_visit_count.length?past_day_visit_count[0].count:0,
        week_visit_count:week_visit_count.length?week_visit_count[0].count:0,
        past_week_visit_count:past_week_visit_count.length?past_week_visit_count[0].count:0,
        month_visit_count:month_visit_count.length?month_visit_count[0].count:0,
        past_month_visit_count:past_month_visit_count.length?past_month_visit_count[0].count:0,
        all_visit_count:all_visit_count.length?all_visit_count[0].count:0
    }
  return res.status(200).json(response);
}
