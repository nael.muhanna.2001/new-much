import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function (req, res) {
  let { type, dateRange, assemblyBy, selectCards } = req.body
    ? JSON.parse(req.body)
    : {};
  if (!assemblyBy) {
    assemblyBy = "day";
  }
  let token = await getToken({req});
  const user =token.user;
    
  let query = { user_id: user._id ,$or: [{ deleted_at: { $exists: false } }, { deleted_at: null }]};
  if (type) {
    query.cardType = type;
  }

  const client = await connectToDatabase();
  const cards = await client.db().collection("cards").find(query).toArray();

  let card_ids = [];
  if (cards.length && (!selectCards || (selectCards && !selectCards.length))) {
    for (let i = 0; i < cards.length; i++) {
      card_ids.push(cards[i]._id + "");
    }
  } else if (cards.length && selectCards && selectCards.length) {
    card_ids = selectCards;
  }

  let start = dateRange?new Date((new Date(dateRange[0])).setHours(0,0,0,0)):new Date('2022-09-01');
  let end = dateRange?new Date((new Date(dateRange[1])).setHours(17,59,59,999)):new Date('2022-09-30');
  const traffic = await client
    .db()
    .collection("visits")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
          created_at: { $gte: start, $lt: end },
        },
      },
      {
        $group: {
          _id: { referrer: "$referrer", src: "$src" },
          count: { $sum: 1 },
        },
      },
    ])
    .toArray();
  let facebook = 0;
  let instagram = 0;
  let snapchat=0;
  let twitter=0; //t.co
  let whatsapp=0;//wl.co, whatsapp.com
  let tripadvisor=0;
  let youtube=0;
  let linkedin=0;
  let tiktok=0;
  let messenger = 0; // messenger.com
  let blogger=0;
  let foursquare=0;
  let google=0;
  let totalVists = 0;
  let trafficList = [];
  traffic.forEach((e,i)=>{
    totalVists+=e.count;
    if(e._id.src!=null)
    {
        if(trafficList.filter((fe,k)=>fe.src==e.src).length)
        {
            trafficList.filter((fe,k)=>{
                trafficList[k].count+=e.count;
            })
        }
        else
        trafficList.push({referrer:e._id.src,count:e.count})
    }
    else if(e._id.referrer && (e._id.referrer.includes('facebook') || e._id.referrer.includes('fb')))
    {
        facebook+= e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('instagram'))
    {
        instagram+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('snapchat'))
    {
        snapchat+=e.count;
    }
    else if(e._id.referrer && (e._id.referrer.includes('twitter') || (e._id.referrer.includes('t.co'))))
    {
        twitter+=e.count;
    }
    else if(e._id.referrer && (e._id.referrer.includes('whatsapp') || (e._id.referrer.includes('wl.co'))))
    {
        whatsapp+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('tripadvisor'))
    {
        tripadvisor+=e.count;
    }
    else if(e._id.referrer && (e._id.referrer.includes('youtube')|| e._id.referrer.includes('youtu.be') ))
    {
        youtube+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('linkedin'))
    {
        linkedin+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('tiktok'))
    {
        tiktok+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('messenger'))
    {
        messenger+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('blogger'))
    {
        blogger+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('foursquare'))
    {
        foursquare+=e.count;
    }
    else if(e._id.referrer && e._id.referrer.includes('google'))
    {
        google+=e.count;
    }
    else if(e._id.referrer==null && e._id.src==null)
    {
        trafficList.push({referrer:'direct',count:e.count})
    }
    else if(e._id.referrer!=null)
    {
        trafficList.push({referrer:new URL(e._id.referrer).hostname,count:e.count})
    }
  })
  trafficList.push({referrer:'facebook',count:facebook})
  trafficList.push({referrer:'instagram',count:instagram})
  trafficList.push({referrer:'snapchat',count:snapchat})
  trafficList.push({referrer:'twitter',count:twitter})
  trafficList.push({referrer:'whatsapp',count:whatsapp})
  trafficList.push({referrer:'tripadvisor',count:tripadvisor})
  trafficList.push({referrer:'youtube',count:youtube})
  trafficList.push({referrer:'linkedin',count:linkedin})
  trafficList.push({referrer:'tiktok',count:tiktok})
  trafficList.push({referrer:'blogger',count:blogger})
  trafficList.push({referrer:'foursquare',count:foursquare})
  trafficList.push({referrer:'google',count:google})
  
  function compare( a, b ) {
    if ( a.count > b.count ){
      return -1;
    }
    if ( a.count < b.count ){
      return 1;
    }
    return 0;
  }
  trafficList.sort(compare);
  let response = {
    totalVists:totalVists,
    trafficList:trafficList
  }
  return res.status(200).json(response);
}
