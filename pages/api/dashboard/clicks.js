import { connectToDatabase } from "../../../lib/db";

export default async function (req, res) {
  const user =
    req.cookies && req.cookies.user ? JSON.parse(req.cookies.user) : null;
  const client = await connectToDatabase();
  const cards = await client.db().collection("cards").find({ user_id: user._id,$or: [{ deleted_at: { $exists: false } }, { deleted_at: null }] }).toArray();

  let card_ids = [];
  
    for (let i = 0; i < cards.length; i++) {
      card_ids.push(cards[i]._id + "");
    }
  let uniqueClicks = await client
    .db()
    .collection("clicks")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
        },
      },
      {
        $lookup: {
          from: "cards",
          let: { card_id: { $toObjectId: "$card_id" } },
          pipeline: [
            { $addFields: { card_id: "$_id" } },
            { $match: { $expr: { $eq: ["$card_id", "$$card_id"] } } },
          ],
          as: "card",
        },
      },
      {
        $group: {
          _id: { user_id: "$user_id", card_id: "$card_id",link:"$link" },
          card: {$first: "$card"},
          count: { $sum: 1 },
        },
      },
      {
        $project:{
            card:{$first:"$card"},
            count:"$count",
            link:"$_id.link",
            card_id:"$_id.card_id",
            user_id:"$_id.user_id"
        }
      }
    ])
    .toArray();
    let cardClicks = await client
    .db()
    .collection("clicks")
    .aggregate([
      {
        $match: {
          card_id: { $in: card_ids },
        },
      },
      {
        $lookup: {
          from: "cards",
          let: { card_id: { $toObjectId: "$card_id" } },
          pipeline: [
            { $addFields: { card_id: "$_id" } },
            { $match: { $expr: { $eq: ["$card_id", "$$card_id"] } } },
          ],
          as: "card",
        },
      },
      {
        $group: {
          _id: {card_id: "$card_id",},
          card: {$first: "$card"},
          unique_links:{$addToSet:"$link"},
          links:{$push:"$link"},
          unique_users:{$addToSet:"$user_id"},
          users:{$push:"$user_id"},
          count: { $sum: 1 },
        },
      },
      {
        $project:{
            card:{$first:"$card"},
            count:"$count",
            unique_links:"$unique_links",
            links:"$links",
            card_id:"$_id.card_id",
            unique_users:"$unique_users",
            users:"$users"
        }
      },
      {
        $sort:{count:-1}
      }
    ])
    .toArray();
  
  return res.status(200).send(cardClicks);
}
