import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../../lib/db";
//Product fields
// - slug
// - primaryImages
// - name {}
// - short description {}
// - description {}
// - tags
// - price
// - oldPrice
// - Currency
// - extraFields {}
export default async function create(req, res) {
  console.log(req);
  if (req.body) {
    const client = await connectToDatabase();
    let product = null;
    let request = JSON.parse(req.body);
    if (request._id) {
      const id = request._id;
      delete request._id;
      request.updated_at=new Date();
      if(!request.selectedProductsIds.includes(id))
      {
        request.selectedProductsIds.push(id);
      }
      const updater = await client
        .db()
        .collection("muchProducts")
        .updateOne({ _id: ObjectId(id) }, {$set:request}, { upsert: false });
      product = await client
        .db()
        .collection("muchProducts")
        .findOne({ _id: ObjectId(id) });
    } else {
      request.created_at=new Date();
      request.published=true;
      const inserter = await client
        .db()
        .collection("muchProducts")
        .insertOne(request);
        
         
      product = await client
        .db()
        .collection("muchProducts")
        .findOne({ _id: ObjectId(inserter.insertedId) });
        let selectedProductsIds = product.selectedProductsIds;
        selectedProductsIds.push(product._id);
        const updater = await client
        .db()
        .collection("muchProducts")
        .updateOne({_id:ObjectId(inserter.insertedId)},{$set:{selectedProductsIds:selectedProductsIds}},{upsert:false});
        product = await client
        .db()
        .collection("muchProducts")
        .findOne({ _id: ObjectId(inserter.insertedId) });
    }
    return res.status(201).json(product);
  }
  return res.status(404).send("Not Found!");
}
