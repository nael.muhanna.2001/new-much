import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../../lib/db";

export default async function show(req, res) {
  const client = await connectToDatabase();

  if (req.query) {
    const { id } = req.query;
    const product = await client
      .db()
      .collection("muchProducts")
      .findOne({ _id: ObjectId(id) });
      if(product)
    return res.status(200).json(product);
    else
    return res.status(404).send("Not Found!");
    
  }
  return res.status(404).send("Not Found!");
}
