import { connectToDatabase } from "../../../../lib/db";

export default async function index(req, res) {
  let query = {
    $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
    published: true,
  };
  const client = await connectToDatabase();
  if (req.body) {
    

    let request = JSON.parse(req.body);

    if (request.q) {
      query.name = { $regex: request.q, $options: "i" };
    }
    if(request.unpublished)
    {
        query.published=false;
    }
    const products = await client
      .db()
      .collection("muchProducts")
      .find(query)
      .toArray();
    return res.status(200).json(products);
  } else {
    const products = await client
      .db()
      .collection("muchProducts")
      .find(query).sort({created_at:-1,updated_at:-1})
      .toArray();
    return res.status(200).json(products);
  }
  
}
