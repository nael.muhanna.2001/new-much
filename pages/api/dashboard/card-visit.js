import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function (req, res) {
  let { type, dateRange, assemblyBy, selectCards } =req.body?JSON.parse(req.body):{};
  if (!assemblyBy) {
    assemblyBy = "day";
  }
  let token = await getToken({req});
  const user =token.user;
  let query = { user_id: user._id,$or: [{ deleted_at: { $exists: false } }, { deleted_at: null }] };
  if (type) {
    query.cardType = type;
  }

  const client = await connectToDatabase();
  const cards = await client.db().collection("cards").find(query).toArray();

  let card_ids = [];
  if (cards.length && (!selectCards || (selectCards && !selectCards.length)) ) {
    for (let i = 0; i < cards.length; i++) {
      card_ids.push(cards[i]._id + "");
    }
  } else if (cards.length && selectCards && selectCards.length) {
    card_ids = selectCards;
  }

  let start = dateRange?new Date((new Date(dateRange[0])).setHours(0,0,0,0)):new Date('2022-09-01');
  let end = dateRange?new Date((new Date(dateRange[1])).setHours(23,59,59,999)):new Date('2022-09-30');
  const visits = await client
      .db()
      .collection("visits")
      .aggregate([
        {
          $match: {
            card_id: { $in: card_ids },
            created_at: { $gte: start, $lt: end },
          },
        },
        {
            $lookup: {
              from: "cards",
              let: { card_id: { $toObjectId: "$card_id" } },
              pipeline: [
                { $addFields: { card_id: "$_id" } },
                { $match: { $expr: { $eq: ["$card_id", "$$card_id"] } } },
              ],
              as: "card",
            },
          },
        {
          $group: {
            _id: "$card_id",
            card: {$first: "$card"},
            users:{$push:"$visitor_id"},
            unique_users:{$addToSet:"$visitor_id"},
            count: { $sum: 1 },
          },
        },
        {
            $project:{
                card_id:"$_id",
                count:"$count",
                card:{$first:"$card"},
                unique_users:"$unique_users",
                users:"$users"
            }
          },
        {
            $sort:{count:-1}
        }
      ])
      .toArray();

  return res.status(200).json(visits);
}
