import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../../lib/db";

export default async function (req, res) {
  const token = await getToken({ req });
  if (
    token &&
    token.user &&
    token.user.permissions &&
    token.user.permissions.includes("admin")
  ) {
    const client = await connectToDatabase();
    const totalOrders = await client.db().collection("waybills").find().count();
    const newOrders = await client
      .db()
      .collection("waybills")
      .find({ reviewed_by: null, reviewed_at: null })
      .count();
    const paidOrders = await client
      .db()
      .collection("waybills")
      .find({ status: "CAPTURED" })
      .count();
    const cancelledOrders = await client
      .db()
      .collection("waybills")
      .find({ status: "CANCELLED" })
      .count();
    return res
      .status(200)
      .json({
        totalOrders: totalOrders,
        newOrders: newOrders,
        paidOrders: paidOrders,
        cancelledOrders: cancelledOrders,
      });
  }
  res.status(401).json(token);
}
