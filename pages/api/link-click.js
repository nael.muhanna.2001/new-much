import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../lib/db";

export default async function handler(req,res)
{
    let token = await getToken({req});
    const request = req.query
    if(request.card_id && request.user_id && request.link)
    {
        const user = token?.user;
    const client = await connectToDatabase();
    const linkClickInserter = await client.db().collection('clicks').insertOne({
        card_id:request.card_id,
        card_user:request.user_id,
        user_id:user? user._id:request.visitor_id,
        link:request.link,
        created_at:new Date()
    });
    return res.status(200).json('saved');
    //return res.redirect(307,request.link);
    }
    return res.redirect('/');
    
}