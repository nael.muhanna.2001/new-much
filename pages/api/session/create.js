import fs from "fs";
import https, { Agent } from "https";
import path from "path";
import axios from "axios";
export default async function handler(req, res) {
  const pemkey =
    "-----BEGIN PRIVATE KEY-----\n" +
    "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDV9cAfl8zk3rwm" +
    "N7v5tunzlMzjOMqZ5d2yLJkf+f/IdA1asH8lCVDrE+QoQzm5XRgSz0Uya3R1Cw7Z" +
    "HuHoUBA71A26Hmb7BL5YG9XhtPlcDAdsUIEDbWv+hxmVNNZoKDHlKSMw2Zmzsfe7" +
    "ya08G3Qlh0HV9T0Rgy23MSZueTxrNc1fxRTWPL9F3Bq6ADjqGeeaU2fuH0mbbx94" +
    "bGcVeff57mM4mJAVkR2CxBcx2QkVBzxMzR6rvMyeDOKZwqKi+5CA6y1RYjAWFwlk" +
    "cAuEruPE1R3UK2ynuUCcFKgT6S+sc7ye7n6qGZ5NIuAcHMWXQXfn5kruv50yy83i" +
    "ivF3cSJhAgMBAAECggEBALyvO13hHdUcQ+qkLgZ683hW2hTr6Wes2kSdpfAYJSTe" +
    "i0RV79p2i8Czyvpixo4cdfVoikp7jrkFJnP9ExYvPRG14OEonIJz3XSQH58rXNh6" +
    "1zUDDbM7FQjilYIKlaZHBDFmhN04mJ5qrOETas1eHKBX1oxb+B3whClQk+bKzahw" +
    "rM58CpJrU4XTbnTyz9Y8KxTEttWk1kRlywmT7hv3sx1JgZyC+EJZwYQ8B+hnAhnw" +
    "PIRHjaE94FEh2vDOl1P2uYZ5WOMnnWWbmeLCsGXwKtg+E6HiQ5SMTk0iHOU3Bt5A" +
    "Eb/DKZPjc/IJDtB1wm4/6RzLrYGpTmT1tsqvL7VoAAECgYEA/oMe5U2vQr7PR88Y" +
    "On0iW73Nd2pQ5IMdBH16S3rd298hKuPAFOl5FnK6CJvrJ3HHTOrW+TE94ZSzeBgu" +
    "cokqJEaU7ViTVbrlGYtv1JS25zv6sbVFfuutsr8f5QIjOs4544JrzEZkyaxjnotV" +
    "8Ws0jLfkTiJEWNZOY39ZxxrZiAECgYEA1zXxbtLiOxzeF4qXUj1OW5Jkkd6G+G5D" +
    "2pr9oDMv/JAJnJHc9vp1F0Uh9krquJFtt0Je0I7Z7XIz0q8SRYTq30lPG6cS17Fx" +
    "/3JS28oyP3q9yDaqwy+kFlrJOq9sEJ4KApJepOAAQYc3OnjA088OnjWdysH9LZe9" +
    "/l1fMc80mmECgYAcIMuqVgKXotTvv8CSvj+s2fY6S2KzTpZHZ8K7UtHwOeHFhrQ0" +
    "23TJeVZ8GxdLHbUEGDQ4CCwpoTCQhkOj3ursZWySKqbl46jsQM/UM7dfKO7U8w7C" +
    "LzPcPGk++xR/rEkTjtdxFRqQe1/rJsUR0GJXZv9xbzIUi2LQdkOq0JnYAQKBgQC0" +
    "H44HN7Hpz+uCwJiOWIlw00IhZKKK458fJIIKx0zuko+438yVWmMOKuqV/XYfLHkr" +
    "3P0GicmUlvUQ7T7ZuLnsUKtJdbirlvTW3JTFKKKNxul43WhnHUS1AR9TYueLsPqn" +
    "1GIqlOEh904ePlsF7FNLz4xV269PraVzdCrswYRBgQKBgQCsg4nVYgY236RYyvB8" +
    "cgYkF1THYFpPRBRF+UMs8XE1GOg8BV62R9tM8Yyej7VWyqH/45S6gboVW3cUroER" +
    "mvNcoL6ftO9Jx6Fe6aQ/g3SWOtMoWo3gu50iexS3Cdif/kdIwxoDwkTy0Yuak/wu" +
    "pNgPrd/LgrvodgMVNA7WOc6MmA==\n" +
    "-----END PRIVATE KEY-----";
  const pempem =
    "-----BEGIN CERTIFICATE-----\n" +
    "MIIGCzCCBPOgAwIBAgIQYe7jbMDhwQpIwADE55h83TANBgkqhkiG9w0BAQsFADB1" +
    "MUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBD" +
    "ZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTELMAkGA1UECwwCRzMxEzARBgNVBAoMCkFw" +
    "cGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTIzMDYyNjEyMzUwNVoXDTI1MDcyNTEy" +
    "MzUwNFowgY0xIjAgBgoJkiaJk/IsZAEBDBJtZXJjaGFudC5hcHBsZV9wYXkxNzA1" +
    "BgNVBAMMLkFwcGxlIFBheSBNZXJjaGFudCBJZGVudGl0eTptZXJjaGFudC5hcHBs" +
    "ZV9wYXkxEzARBgNVBAsMClBMWEI2QVVRUkMxGTAXBgNVBAoMEFBpb25lZXJzIE5l" +
    "dHdvcmswggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDV9cAfl8zk3rwm" +
    "N7v5tunzlMzjOMqZ5d2yLJkf+f/IdA1asH8lCVDrE+QoQzm5XRgSz0Uya3R1Cw7Z" +
    "HuHoUBA71A26Hmb7BL5YG9XhtPlcDAdsUIEDbWv+hxmVNNZoKDHlKSMw2Zmzsfe7" +
    "ya08G3Qlh0HV9T0Rgy23MSZueTxrNc1fxRTWPL9F3Bq6ADjqGeeaU2fuH0mbbx94" +
    "bGcVeff57mM4mJAVkR2CxBcx2QkVBzxMzR6rvMyeDOKZwqKi+5CA6y1RYjAWFwlk" +
    "cAuEruPE1R3UK2ynuUCcFKgT6S+sc7ye7n6qGZ5NIuAcHMWXQXfn5kruv50yy83i" +
    "ivF3cSJhAgMBAAGjggJ8MIICeDAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFAn+" +
    "wBWQ+a9kCpISuSYoYwyX7KeyMHAGCCsGAQUFBwEBBGQwYjAtBggrBgEFBQcwAoYh" +
    "aHR0cDovL2NlcnRzLmFwcGxlLmNvbS93d2RyZzMuZGVyMDEGCCsGAQUFBzABhiVo" +
    "dHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHJnMzA5MIIBLQYDVR0gBIIB" +
    "JDCCASAwggEcBgkqhkiG92NkBQEwggENMIHRBggrBgEFBQcCAjCBxAyBwVJlbGlh" +
    "bmNlIG9uIHRoaXMgQ2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IG90aGVyIHRoYW4g" +
    "QXBwbGUgaXMgcHJvaGliaXRlZC4gUmVmZXIgdG8gdGhlIGFwcGxpY2FibGUgc3Rh" +
    "bmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBw" +
    "b2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNwYI" +
    "KwYBBQUHAgEWK2h0dHBzOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhv" +
    "cml0eS8wEwYDVR0lBAwwCgYIKwYBBQUHAwIwHQYDVR0OBBYEFJVNXinn/bukLbGQ" +
    "UxAWPBW8VGhSMA4GA1UdDwEB/wQEAwIHgDBPBgkqhkiG92NkBiAEQgxAODhDNDUz" +
    "OENGOEJFN0VGNzhCNzVDMDIzNjNFRjg1Njk2ODI2Q0QyRTlCNDc2QkJDM0U2RTU2" +
    "MjE5ODRFOTgwMDAPBgkqhkiG92NkBi4EAgUAMA0GCSqGSIb3DQEBCwUAA4IBAQCT" +
    "KQCEH0XO5FAuNF9BW0+WHd8CBioqZxtWaLmcr1kWHAVScajxfAzd5pmLVZedzT1z" +
    "1MmTAM5FUXxC8UstcCM3CrDYTP5a5Kt9DDsqyODs2qglyA3o5CkE07uv7To2Y8xy" +
    "PAvj5K2gqFkfv8b0CFnErFzYs/pAa33UncnaWqoMjNqPJ6wTzMmp97NRVQ7eVK5u" +
    "ItWVUuMk0MB3vXaxeXENcl87/44HSXOKr/LlDqlSgN9+RYhs8aojR7MD2Ap2dDyj" +
    "UuaBqMnVKIHDXyKmRhpV+RM2zqRq048NCKJ+IkfT9k1XmSuUrv2pMIT6mBTy+xbx" +
    "tpH8sjv/SxwxgDZ9RriF\n" +
    "-----END CERTIFICATE-----";

  const cert="-----BEGIN CERTIFICATE-----\n"+
  "MIIEajCCBBCgAwIBAgIIT00ESGOUe0owCgYIKoZIzj0EAwIwgYAxNDAyBgNVBAMM"+
  "K0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENBIC0gRzIxJjAk"+
  "BgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApB"+
  "cHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0yMzA2MjgxNjEyMTlaFw0yNTA3Mjcx"+
  "NjEyMThaMIGbMSIwIAYKCZImiZPyLGQBAQwSbWVyY2hhbnQuYXBwbGVfcGF5MTgw"+
  "NgYDVQQDDC9BcHBsZSBQYXkgUGF5bWVudCBQcm9jZXNzaW5nOm1lcmNoYW50LmFw"+
  "cGxlX3BheTETMBEGA1UECwwKUExYQjZBVVFSQzEZMBcGA1UECgwQUGlvbmVlcnMg"+
  "TmV0d29yazELMAkGA1UEBhMCVVMwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAAQH"+
  "adHcVPVinvp2vL3yxWP6NZ0fgaogSyjczH5f32WcKaKyUgKXM4mLVznjdxi26ud6"+
  "TWNqVVdMty+36PPule3io4ICVTCCAlEwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAW"+
  "gBSEtoTMOoZichZZlOgao71I3zrfCzBHBggrBgEFBQcBAQQ7MDkwNwYIKwYBBQUH"+
  "MAGGK2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGV3d2RyY2EyMDEw"+
  "ggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB/jCBwwYIKwYBBQUHAgIw"+
  "gbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBh"+
  "c3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFy"+
  "ZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGlj"+
  "eSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEF"+
  "BQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkv"+
  "MDYGA1UdHwQvMC0wK6ApoCeGJWh0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxld3dk"+
  "cmNhMi5jcmwwHQYDVR0OBBYEFPOQjdrZectlSCMLWBLaSrMiVa1lMA4GA1UdDwEB"+
  "/wQEAwIDKDBPBgkqhkiG92NkBiAEQgxAODhDNDUzOENGOEJFN0VGNzhCNzVDMDIz"+
  "NjNFRjg1Njk2ODI2Q0QyRTlCNDc2QkJDM0U2RTU2MjE5ODRFOTgwMDAKBggqhkjO"+
  "PQQDAgNIADBFAiB6ePjakr3IHsqcbEUjB4NCbooBOgmiYYM6+vYvd3PxegIhAIVU"+
  "dc3fEa7VdpMMt3pBIwmoPypgBsOHtQtxnAY6iyC8\n"+
  "-----END CERTIFICATE-----";

  const options = {
    //  hostname: require("url").parse(req.body.url).hostname,
    // hostname:'www.much.sa',
    url: req.body.url,
    // path:require("url").parse(req.body.url).path,
    // url:require('url').parse('https://www.much.sa/api/cards/'),

    // method: "post",
    // httpsAgent: new https.Agent({
    //   //   pfx: fs.readFileSync("./keys/therCert.p12"),
    //   // cert: Buffer.from(pempem),
    //   //   key: Buffer.from(pemkey),
    //   pfx: Buffer.from(p12, "utf-8"),
    //   passphrase: "Omran1984-_-",
    //   rejectUnauthorized: false,
    // }),

    data: {
      merchantIdentifier: "merchant.apple_pay",
      displayName: "much.sa",
      initiative: "web",
      initiativeContext: "www.much.sa",
    },
  };
  
    try {
  const request = await axios({
    method: "post",
    url: req.body.url,
    httpsAgent: new https.Agent({
      // pfx:new Buffer.from(p12),
      cert: Buffer.from(pempem, "utf-8"),
      key: Buffer.from(pemkey, "utf-8"),
      rejectUnauthorized: true,
    }),
    data: {
      merchantIdentifier: "merchant.apple_pay",
      displayName: "much.sa",
      initiative: "web",
      initiativeContext: "www.much.sa",
    },
  });

  return res
    .status(200)
    .json({ status: "200", statusMessage: request.data, statusCode: 200 });
    } catch (error) {
      return res.status(500).send({ error: error });
    }
}
