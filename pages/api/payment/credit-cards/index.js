import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../../lib/db";

export default async function handler(req,res)
{
    const token = await getToken({req});
    if(token)
    {
        const client = await connectToDatabase();

        const cards= await client.db().collection('cardTokens').find({user_id:token.user._id}).toArray();

        return res.status(200).json(cards);
    }
    return res.status(401).json({success:false,message:'Bad Request!'});
}