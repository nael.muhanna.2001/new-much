import { getToken } from "next-auth/jwt";
import requestIp from "request-ip";
import { connectToDatabase } from "../../../../lib/db";
export default async function handler(req, res) {
  const token = await getToken({ req });
  if (req.body) {
    const client = await connectToDatabase();
    let request = JSON.parse(req.body);
    const waybill = request.waybill;
    let waybill_id = null;
    if (waybill.orders && waybill.orders.length > 0) {
      waybill_id = waybill.orders[0].waybill_id;
    }

    if (waybill_id) {
      waybill.updated_at = new Date();
      delete waybill._id;
      const waybillupdater = await client
        .db()
        .collection("waybills")
        .updateOne(
          { _id: ObjectId(waybill_id) },
          { $set: waybill },
          { upsert: false }
        );
    } else {
      waybill.created_at = new Date();
      waybill.user_id = token.user._id;
      waybill.status = "PENDING";
      const waybillInserter = await client
        .db()
        .collection("waybills")
        .insertOne(waybill);
      waybill_id = waybillInserter.insertedId;
    }

    const option = {
      amount: Number(request.waybill.total),
      currency: "SAR",
      threeDSecure: true,
      save_card: false,
      customer_initiated: true,
      description: request.description,
      reference: { transaction: waybill_id, order: request.waybill.orders[0]._id },
      receipt: { email: true, sms: true },
      customer: {
        first_name: request.waybill.address.fname,
        last_name: request.waybill.address.lname,
        email: token.user.email,
        phone: {
          country_code: request.waybill.address.prefix.value,
          number: request.waybill.address.phone,
        },
      },
      source: { id: request.token.id },
      post: { url: "https://www.much.sa/api/payment/webhook" },
      redirect: { url: "https://much.sa/store/waybill/" + waybill_id },
    };
    console.log(option);
    const chargeReq = await fetch("https://api.tap.company/v2/charges/", {
      method: "POST",
      headers: {
        Authorization: "Bearer sk_live_WMi20mOC5eExVkYu4GcdRBAj",
        accept: "application/json",
        "content-type": "application/json",
      },
      body: JSON.stringify(option),
    });

    const charge = await chargeReq.json();
    if (chargeReq.status == 200) {
      return res.status(200).json(charge);
    } else {
      return res.status(chargeReq.status).json(charge);
    }
  }
}
