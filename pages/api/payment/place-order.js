import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const token = await getToken({ req });
  if (req.body && token) {
    const client = await connectToDatabase();
    let request = JSON.parse(req.body);
    const waybill = request.waybill;
    let waybill_id = null;
    if (waybill.orders && waybill.orders.length > 0) {
      waybill_id = waybill.orders[0].waybill_id;
    }

    if (waybill_id) {
      waybill.updated_at = new Date();
      delete waybill._id;
      const waybillupdater = await client
        .db()
        .collection("waybills")
        .updateOne(
          { _id: ObjectId(waybill_id) },
          { $set: waybill },
          { upsert: false }
        );
    } else {
      waybill.created_at = new Date();
      waybill.user_id = token.user._id;
      waybill.status = "PENDING";
      const waybillInserter = await client
        .db()
        .collection("waybills")
        .insertOne(waybill);
      waybill_id = waybillInserter.insertedId;
      let orderid = [...waybill.orders.map((e) => ObjectId(e._id))];
      const ordersUpdater = await client
          .db()
          .collection("orders")
          .updateMany(
            { _id: { $in: orderid } },
            { $set: { status: 'PENDING' } },
            { upsert: false }
          );
    }
    return res.status(200).json({waybill_id:waybill_id});
  }
  return res.status(401).json({success:false,message:'Bad Request!'});
}
