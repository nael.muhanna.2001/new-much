import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import requestIp from "request-ip";
import { connectToDatabase } from "../../../../lib/db";
export default async function handler(req, res) {
  const token = await getToken({ req });
  if (req.body) {
    const client = await connectToDatabase();
    const waybill = req.body.waybill;
    let waybill_id = null;
    
    const option = {
      type: "applepay",
      token_data: req.body.payment_data,
      client_ip: requestIp.getClientIp(req),
    };

    const tokenRes = await fetch("https://api.tap.company/v2/tokens", {
      method: "post",
      headers: {
        Authorization: "Bearer sk_live_dGYCpkD3hI4FBRMwe1UZjTcf",
        accept: "application/json",
        "content-type": "application/json",
      },
      body: JSON.stringify(option),
    });

    const cardToken = await tokenRes.json();
    if (tokenRes.status == 200) {
      if (waybill_id) {
        waybill.updated_at = new Date();
        delete waybill._id;
        const waybillupdater = await client
          .db()
          .collection("waybills")
          .updateOne(
            { _id: ObjectId(waybill_id) },
            { $set: waybill },
            { upsert: false }
          );
      } else {
        waybill.created_at = new Date();
        waybill.user_id = token.user._id;
        waybill.status = "PENDING";
        const waybillInserter = await client
          .db()
          .collection("waybills")
          .insertOne(waybill);
        waybill_id = waybillInserter.insertedId;
      }
      const chargeOption = {
        amount: Number(waybill.total).toFixed(2),
        currency: "SAR",
        description: "much product purchase",
        customer: {
          first_name: waybill.address.name,
          last_name: waybill.address.lname,
          email: token.user.email,
          phone: {
            country_code: waybill.address.prefix.value,
            number: waybill.address.phone,
          },
        },
        reference: {
          transaction: waybill_id,
          order: waybill.orders[0].order_id,
        },
        source: { id: cardToken.id },
        post: { url: "https://www.much.sa/api/payment/webhook" },
        redirect: { url: "https://much.sa/store/waybill/" + waybill_id },
      };
      const charge = await fetch("https://api.tap.company/v2/charges/", {
        method: "POST",
        headers: {
          Authorization: "Bearer sk_live_dGYCpkD3hI4FBRMwe1UZjTcf",
          accept: "application/json",
          "content-type": "application/json",
        },
        body: JSON.stringify(chargeOption),
      });
      const chargeRes = await charge.json();
      if(charge.status==200)
      {
        let newWaybill = await client.db().collection('waybills').findOne({_id:ObjectId(waybill_id)});
        newWaybill.charge = chargeRes;
        newWaybill.status=chargeRes.status;
        delete newWaybill._id;
        const waybillUpdate = await client.db().collection('waybills').updateOne({_id:ObjectId(waybill_id)},{$set:newWaybill},{upsert:false});
        newWaybill = await client.db().collection('waybills').findOne({_id:ObjectId(waybill_id)});
        return res
        .status(200)
        .json({ code: 100, success: true, waybill: newWaybill });
      }
      else
      {
        return res.status(charge.status),json(chargeRes);
      }
      
    }
    return res.status(tokenRes.status).json(cardToken);
  }
}
