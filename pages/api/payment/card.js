import requestIp from 'request-ip'
export default async function handler(req, res) {
  const option = {
    "card": {
      "number":4508750015741019,
      "exp_month": 10,
      "exp_year": 2039,
      "cvc": 100,
      "name": "test user",
    },
    "client_ip": "192.168.1.20"
  };
  console.log(option);
  const tokenRes = await fetch("https://api.tap.company/v2/tokens", {
    method:'post',
    headers: {
      Authorization: "Bearer sk_test_Bfe3Ewn4hsMo1TS2C6JDFIrR",
      accept: "application/json",
      "content-type": "application/json",
    },
    body: JSON.stringify(option),
  });
  console.log(tokenRes);
  const cardToken = await tokenRes.json();
  return res.json(cardToken);
}