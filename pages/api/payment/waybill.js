import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  if (req.body) {
    let request = JSON.parse(req.body);
    const client = await connectToDatabase();
    if (request.tap_id) {
      const tapReq = await fetch(
        "https://api.tap.company/v2/charges/" + request.tap_id,
        {
          method: "GET",
          headers: {
            Authorization: "Bearer sk_live_WMi20mOC5eExVkYu4GcdRBAj",
            accept: "application/json",
            "content-type": "application/json",
          },
        }
      );
      if (tapReq.status == 200) {
        const charge = await tapReq.json();
        let waybill = await client
          .db()
          .collection("waybills")
          .findOne({ _id: ObjectId(request._id) });
        let waybill_id = waybill._id;
        delete waybill._id;
        waybill.charge = charge;
        waybill.status = charge.status;
        waybill.updated_at=new Date();
        let orderid = [...waybill.orders.map((e) => ObjectId(e._id))];
        const ordersUpdater = await client
          .db()
          .collection("orders")
          .updateMany(
            { _id: { $in: orderid } },
            { $set: { status: charge.status } },
            { upsert: false }
          );
        const wayBillUpdater = await client
          .db()
          .collection("waybills")
          .updateOne(
            { _id: ObjectId(waybill_id) },
            { $set: waybill },
            { upsert: false }
          );
        const newWaybill = await client
          .db()
          .collection("waybills")
          .findOne({ _id: ObjectId(waybill_id) });
        return res.status(200).json(newWaybill);
      } else {
        const newWaybill = await client
          .db()
          .collection("waybills")
          .findOne({ _id: ObjectId(request._id) });
        return res.status(200).json(newWaybill);
      }
    }
    const newWaybill = await client
      .db()
      .collection("waybills")
      .findOne({ _id: ObjectId(request._id) });
    return res.status(200).json(newWaybill);
  }
  return res.status(401).json({success:false,message:'Bad Request!'});
}
