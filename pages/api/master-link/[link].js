import { connectToDatabase } from "../../../lib/db";

export default async function handler(req,res)
{
    const {link} =req.query;
    if(link)
    {
        let theLink = {};
        const client = await connectToDatabase();
        const masterLinkReq = await client.db().collection('masterLinks').findOne({link:link});
        const preActive = await client.db().collection('preLinks').findOne({link:link});
        if(masterLinkReq)
        {
            theLink = masterLinkReq;
            theLink.type='master-link';
        }
        else if(preActive)
            {
                theLink = preActive;
                theLink.type='new-link';
            }
            else
            {
                res.status(404).json({message:'Not Found!'});
            }
        return res.status(200).json(theLink)
    }
}