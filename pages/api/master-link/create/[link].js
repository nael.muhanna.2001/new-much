import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../../lib/db";

export default async function handler(req, res) {
  const token = await getToken({ req });

  const { link } = req.query;
  console.log(link);
  if ((link && token, req.body)) {
    const { card_id } = JSON.parse(req.body);
    let theLink = {};
    const client = await connectToDatabase();
    const preActive = await client
      .db()
      .collection("preLinks")
      .findOne({ link: link });
    if (preActive) {
      delete preActive._id;
      delete preActive.pin;
      delete preActive.code;
      theLink = preActive;
      theLink.created_at = new Date();
      theLink.user_id = token.user._id;
      theLink.src = "nfc";
      theLink.slug = card_id;
      const createMaterLink = await client
        .db()
        .collection("masterLinks")
        .insertOne(theLink);
      const activeLink = await client
        .db()
        .collection("preLinks")
        .updateOne(
          { _id: ObjectId(preActive._id) },
          { $set: { activated_at: new Date() } },
          { upsert: false }
        );
      return res.status(201).json({ success: true });
    }
  }
  return res.status(404).json({ message: "Bad Request" });
}
