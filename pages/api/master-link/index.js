import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req,res)
{
    const token = await getToken({req});
    
    if(token)
    {
        const client = await connectToDatabase();
        const products = await client.db().collection('masterLinks').find({user_id:token.user._id}).toArray();
        return res.status(200).json(products);

    }
    return res.status(401).json({message:'Bad Request'})
}