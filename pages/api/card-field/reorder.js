import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  if (req.body) {
    let request = JSON.parse(req.body);
    const client = await connectToDatabase();
    const updater = await client
      .db()
      .collection("cardFields")
      .updateOne(
        { cardId: request.card_id, order: request.new_order },
        { $set: { order: request.old_order } },
        {upsert:false}
      );
      
      const updater1 = await client
      .db()
      .collection("cardFields")
      .updateOne(
        {_id:ObjectId(request.field_id) },
        { $set: { order: request.new_order } },
        {upsert:false}
      );
      
      return res.status(200).json('reordered');
  }
  return res.status(404).send('Not Found');
}
