import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function (req, res) {
  if (req.body) {
    let request = JSON.parse(req.body);
    const client = await connectToDatabase();
    const updater = await client
      .db()
      .collection("cardFields")
      .updateOne(
        { _id: ObjectId(request.field_id) },
        { $set: { published: request.published } },
        { upsert: false }
      );
      const fields = await client.db().collection('cardFields').find({cardId:request.card_id,$or: [{ deleted_at: { $exists: false } }, { deleted_at: null }]}).sort('order').toArray();
      return res.status(200).json(fields);
  }
  return res.status(404).send('Not Found');
}
