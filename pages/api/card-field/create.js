import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  if (req.body) {
    const client = await connectToDatabase();
    let field = JSON.parse(req.body);
    let id = null;
    field.created_at= new Date();
    field.published=true;
    field.order=0;
    if (field._id) {
      id = ObjectId(field._id);
      delete field._id;
      const updateField = await client
        .db()
        .collection("cardFields")
        .updateOne({ _id: id }, { $set: field }, { upsert: false });
    } else {
      const insertField = await client
        .db()
        .collection("cardFields")
        .insertOne(field);
        id=ObjectId(insertField.insertedId);
    }
    const newField = await client.db().collection('cardFields').findOne({_id:id});
    return res.status(200).json(newField);
  }
  return res.status(404).send('Not Found');
}
