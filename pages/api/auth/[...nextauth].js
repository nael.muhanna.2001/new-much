import NextAuth from "next-auth";

import GoogleProvider from "next-auth/providers/google";
import FacebookProvider from "next-auth/providers/facebook";
//import TwitterProvider from "next-auth/providers/twitter";
import CredentialsProvider from "next-auth/providers/credentials";
//import AppleProvider from "next-auth/providers/apple";
import LinkedInProvider from "next-auth/providers/linkedin";
import { connectToDatabase } from "../../../lib/db";
import { verifyPassword } from "../../../lib/auth";
export const nextAuthOptions = (req, res) => {
  let client;
  return {
    //  adapter: MongoDBAdapter(clientPromise),
    session: {
      jwt: true,
    },

    providers: [
      CredentialsProvider({
        name: "Credentials",
        credentials: {
          email: { label: "Username", type: "text", placeholder: "jsmith" },
          password: { label: "Password", type: "password" },
        },
        async authorize(credentials) {
          client = await connectToDatabase();
          let usersCollection = client.db().collection("users");
          const user = await usersCollection.findOne({
            email: credentials.email,
          });
          client.close();
          if (!user) {
            client.close();
            throw new Error("No user found!");
          }
          const isValid = await verifyPassword(
            credentials.password,
            user.password
          );
          if (!isValid) {
            client.close();
            throw new Error("Password invalid!");
          }
          client.close();
          
          return user;
        },
      }),
      // EmailProvider({
      //   server: {
      //     host: process.env.EMAIL_SERVER_HOST,
      //     port: process.env.EMAIL_SERVER_PORT,
      //     secure: true,
      //     auth: {
      //       user: process.env.EMAIL_SERVER_USER,
      //       pass: process.env.EMAIL_SERVER_PASSWORD,
      //     },
      //   },
      //   from: process.env.EMAIL_FROM,
      // }),
      LinkedInProvider({
        clientId: process.env.LINKEDIN_CLIENT_ID,
        clientSecret: process.env.LINKEDIN_CLIENT_SECRET,
        token: {
          url: "https://www.linkedin.com/oauth/v2/accessToken",
          async request({ client, params, checks, provider }) {
            const response = await client.oauthCallback(
              provider.callbackUrl,
              params,
              checks,
              {
                exchangeBody: {
                  client_id: process.env.LINKEDIN_CLIENT_ID,
                  client_secret: process.env.LINKEDIN_CLIENT_SECRET,
                },
              }
            );
            return {
              tokens: response,
            };
          },
        },
      }),
      FacebookProvider({
        clientId: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET,
      }),
      // TwitterProvider({
      //   clientId: process.env.TWITTER_CLIENT_ID,
      //   clientSecret: process.env.TWITTER_CLIENT_SECRET,
      // }),
      GoogleProvider({
        clientId: process.env.GOOGLE_ID,
        clientSecret: process.env.GOOGLE_SECRET,
      }),
      // AppleProvider({
      //   clientId: process.env.APPLE_ID,
      //   clientSecret: process.env.APPLE_SECRET,
      // }),
    ],
    callbacks: {
      async redirect({ url, baseUrl }) {
        if (url) return Promise.resolve(url);
        return baseUrl;
      },
      async jwt({ token, user, account, profile, isNewUser }) {
        // if (account) {
        //   client = await connectToDatabase();
        //   let accountCollection = client.db().collection("accounts");
        //   const oldAccount = await accountCollection.findOne({
        //     providerAccountId: account.providerAccountId,
        //   });
        //   client.close();
        //   if (oldAccount) {
        //     client = await connectToDatabase();
        //     accountCollection = client.db().collection("accounts");
        //     const dd = await accountCollection.updateOne(
        //       { providerAccountId: account.providerAccountId },
        //       {
        //         $set: {
        //           provider: account.provider,
        //           type: account.type,
        //           access_token: account.access_token,
        //           expires_at: account.expires_at,
        //           scope: account.scope,
        //           token_type: account.token_type,
        //           id_token: account.id_token,
        //           updated_at: new Date(),
        //         },
        //       }
        //     );
        //   } else {
        //     client = await connectToDatabase();
        //     accountCollection = client.db().collection("accounts");

        //     const dd = await accountCollection.insertOne({
        //       providerAccountId: account.providerAccountId,
        //       provider: account.provider,
        //       type: account.type,
        //       access_token: account.access_token,
        //       expires_at: account.expires_at,
        //       scope: account.scope,
        //       token_type: account.token_type,
        //       id_token: account.id_token,
        //       created_at: new Date(),
        //       updated_at: new Date(),
        //     });
        //     client.close();
        //   }
        // }
        if (account) {
          const oldUser = null;

          client = await connectToDatabase();
          let usersCollection = client.db().collection("users");
          let user1 = await usersCollection.findOne({ email: token.email });
          if (!oldUser && !user1) {
            client = await connectToDatabase();
            let usersCollection = client.db().collection("users");

            const bb = await usersCollection.insertOne({
              name: token.name,
              email: token.email,
              image: token.picture,
              updated_at: new Date(),
              last_login: new Date(),
            });

            user1 = await usersCollection.findOne({ email: token.email });

            

            token.user = user1;
            return token;
          }

          if (oldUser == null && user1) {
            client = await connectToDatabase();
            let usersCollection = client.db().collection("users");

            const bb = await usersCollection.updateOne(
              { email: token.email },
              {
                $set: {
                  updated_at: new Date(),
                  last_login: new Date(),
                },
              },
              { upsert: false }
            );

            user1 = await usersCollection.findOne({ email: token.email });

            

            token.user = user1;
            return token;
          }
          if (oldUser && user1) {
            

            user1 = await usersCollection.findOne({ email: token.email });

            

            token.user = user1;
            return token;
          }
        }

        return token;
        // Persist the OAuth access_token to the token right after signin
        //   if (account) {
        //     token.accessToken = account.access_token;

        //   }
      },
      async session({ session, token, user }) {
        if (session?.user) {
          session.user = token.user;
        }
        return session;
      },
    },
    theme: {
      colorScheme: "light", // "auto" | "dark" | "light"
      brandColor: "#66cccc", // Hex color code
      logo: "", // Absolute URL to image
      buttonText: "", // Hex color code
    },
  };
};
export default (req, res) => {
  
  return NextAuth(req, res, nextAuthOptions(req, res));
};

