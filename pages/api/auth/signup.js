import { ObjectId } from "mongodb";
import { hashPassword } from "../../../lib/auth";
import { connectToDatabase } from "../../../lib/db";

async function handler(req, res) {
  if (req.method !== "POST") {
    return;
  }
  const data = req.body;
  const { name, email, password } = data;
  
  if (
    !name ||
    !email ||
    !email.includes("@") ||
    !password ||
    password.trim().length < 7
  ) {
    res.status(422).json({
      message:
        "Invalid input - password should also be at least 7 characters long.",
    });
    return;
  }
  const client = await connectToDatabase();
  const db = client.db();
  const existingUser = await db.collection('users').findOne({email:email});
  if(existingUser)
  {
    res.status(422).json({message:'User Already exists!'});
    client.close();
    return;
  }
  const hashedPassword =  await hashPassword(password);
  const result = await db.collection("users").insertOne({
    name:name,
    email: email,
    password: hashedPassword,
    created_at:new Date(),
    updated_at:new Date(),
    deleted_at:null
  });
const user = await db.collection('users').findOne({_id:ObjectId(result.insertedId)});
  res.status(201).json(user);
  client.close();
}
export default handler;
