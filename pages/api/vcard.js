import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../lib/db";

export default async function handler(req, res) {
  var vCardsJS = require("vcards-js");

  const imageToBase64 = require("image-to-base64");
  //create a new vCard
  let vCard = vCardsJS();
  const { _id } = req.query;
  var hex = /[0-9A-Fa-f]{6}/g;
  let card = null;
  const user =
    req.cookies && req.cookies.user ? JSON.parse(req.cookies.user) : null;
  const client = await connectToDatabase();
  if (hex.test(_id)) {
    card = await client
      .db()
      .collection("cards")
      .findOne({ _id: ObjectId(_id) });
  } else {
    card = await client.db().collection("cards").findOne({ slug: _id });
  }
  if (card) {
    let firstMeet = await client
      .db()
      .collection("visits")
      .findOne({
        card_id: _id,
        visitor_id: user._id,
        $and: [{ src: { $ne: null } }, { src: { $ne: "wallet" } }],
      });
    card.first_meet = firstMeet;
    let viewAccess = "public";
    if (user == null) {
      viewAccess = "public";
    }
    if (
      card.contactViewAccess == "private" &&
      user != null &&
      card.user_id != user._id
    ) {
      viewAccess = "public";
    }
    if (user != null && card.user_id == user._id) {
      viewAccess == "private";
    }
    if (
      user != null &&
      card.connections &&
      !card.connections.includes(user._id)
    ) {
      viewAccess = "connections";
    }
    card.visitor = user;
    console.log(viewAccess)
    let phones = await client.db().collection('cardFields').find({cardId:card._id+'',$or:[{viewAccess:viewAccess,viewAccess:'public'}],published:true,deleted_at:{"$exists": false},"field.fieldType":'phone'}).toArray();
    let cardPhone = [];
    phones.map((e,i)=>{
      cardPhone.push("+"+e.prefix.value+""+e.phone);
    })
    vCard.cellPhone=cardPhone;
    let emails = await client.db().collection('cardFields').find({cardId:card._id+'',$or:[{viewAccess:viewAccess,viewAccess:'public'}],published:true,deleted_at:{"$exists": false},"field.fieldType":'email'}).toArray();
    
    let cardEmails = [];
    emails.map((e,i)=>{
      cardEmails.push(e.email);
    })
    let urls = await client.db().collection('cardFields').find({cardId:card._id+'',$or:[{viewAccess:viewAccess,viewAccess:'public'}],published:true,deleted_at:{"$exists": false},"field.fieldType":'url'}).toArray();
    urls.map((e,i)=>{
      if(e.field.name=='Company Website')
      vCard.url=e.url;
      if(e.field.name=='Facebook')
      vCard.socialUrls['facebook']=e.url;
      if(e.field.name=='Twitter')
      vCard.socialUrls['twitter']=e.url;
      if(e.field.name=='Linkedin')
      vCard.socialUrls['linkedIn']=e.url;
      if(e.field.name=='Youtube')
      vCard.socialUrls['custom']=e.url;
    })
    let addresses = await client.db().collection('cardFields').find({cardId:card._id+'',$or:[{viewAccess:viewAccess,viewAccess:'public'}],published:true,deleted_at:{"$exists": false},"field.fieldType":'address'}).toArray();
    addresses.map((e,i)=>{
      vCard.homeAddress.countryRegion=e.country.label;
      vCard.homeAddress.street=e.address.formatted_address;
    })
    let photo = null;
    if (card.pic) {
      let cardpic = await imageToBase64(card.pic);
      //let data = await  cardpic.arrayBuffer()
      photo = cardpic;
    }
    vCard.firstName = card.name;
    vCard.photo.embedFromString(photo, "image/png");
    if (card.company) vCard.organization = card.company.label;
    if (card.jobTitle) vCard.title = card.jobTitle.label;
    if (card.department) vCard.department = card.department.label;
    vCard.cellPhone=cardPhone;
    vCard.email=cardEmails;

    if (card.first_meet)
      vCard.note =
        "First Meeting: " +
        new Date(card.first_meet.created_at).toLocaleDateString("en", {
          month: "short",
          day: "numeric",
          year: "numeric",
          hour: "numeric",
          minute: "numeric",
        });
    res
      .setHeader("Content-Type", 'text/vcard; name="' + card._id + '.vcf"')
      .setHeader(
        "Content-Disposition",
        'inline; filename="' + card._id + '.vcf"'
      )
      .send(vCard.getFormattedString());
  }
}
