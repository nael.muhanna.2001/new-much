const { Template } = require("@walletpass/pass-js");
const Imgurl ="https://res.cloudinary.com/pweb/image/upload/c_thumb,w_90,h_87,g_face/logo_nglhx0.png";
const bg ="https://res.cloudinary.com/pweb/image/upload/c_thumb,w_180,h_220,g_face/Screenshot_2023-04-05_at_12.46.24_AM_l7cfrs";

var toBuffer = require('blob-to-buffer')
  
const fs = require("fs");

const pemEncodedPassCertificate =
  "-----BEGIN CERTIFICATE-----" +
  "MIIGAjCCBOqgAwIBAgIQX3TdgHpTSowvy70Iw2HjZTANBgkqhkiG9w0BAQsFADB1" +
  "MUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBD" +
  "ZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTELMAkGA1UECwwCRzQxEzARBgNVBAoMCkFw" +
  "cGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTIyMDkyOTIxMzU0NloXDTIzMTAyOTIx" +
  "MzU0NVowgYQxHjAcBgoJkiaJk/IsZAEBDA5wYXNzLm11Y2guY2FyZDElMCMGA1UE" +
  "AwwcUGFzcyBUeXBlIElEOiBwYXNzLm11Y2guY2FyZDETMBEGA1UECwwKUExYQjZB" +
  "VVFSQzEZMBcGA1UECgwQUGlvbmVlcnMgTmV0d29yazELMAkGA1UEBhMCVVMwggEi" +
  "MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpVmeTP/rpRpdPiLheb1AEPm6h" +
  "/VV2BBg6RIgsDKowIl1fm8reHDBMjCxaJp8pYwisPMs07AFNs7zOiSwGv75uy6oq" +
  "7yD8V0jXHb4KnDxMer73o1MOE1Mm//7AS/Cp7P/30IiSkvNp1rROCXf4324dKr5B" +
  "vcFBAayVGhIuT0OZkWk/0+aBDIoJdxILfmz0W+1p1Zi5sAYaGSH2ZSdsVKTjuOCv" +
  "I9KV8iHmWohutWJE168+Bx1R8tAXlBiN5zfTUnTMr873aYpN3+14N5w9srN18Zcv" +
  "PclDSFWQiRz+nuMn6uEegql7cpL74PaTS71ZCjGPghqzY9SLs/2jeuD7mQzLAgMB" +
  "AAGjggJ8MIICeDAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFFvZ+h3nmhoLo5l2" +
  "IlCGPpHIW3eoMHAGCCsGAQUFBwEBBGQwYjAtBggrBgEFBQcwAoYhaHR0cDovL2Nl" +
  "cnRzLmFwcGxlLmNvbS93d2RyZzQuZGVyMDEGCCsGAQUFBzABhiVodHRwOi8vb2Nz" +
  "cC5hcHBsZS5jb20vb2NzcDAzLXd3ZHJnNDA0MIIBHgYDVR0gBIIBFTCCAREwggEN" +
  "BgkqhkiG92NkBQEwgf8wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhp" +
  "cyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9m" +
  "IHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlv" +
  "bnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24g" +
  "cHJhY3RpY2Ugc3RhdGVtZW50cy4wNwYIKwYBBQUHAgEWK2h0dHBzOi8vd3d3LmFw" +
  "cGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wHgYDVR0lBBcwFQYIKwYBBQUH" +
  "AwIGCSqGSIb3Y2QEDjAyBgNVHR8EKzApMCegJaAjhiFodHRwOi8vY3JsLmFwcGxl" +
  "LmNvbS93d2RyZzQtOS5jcmwwHQYDVR0OBBYEFGhIVYq/fRz77OgznKW5byPC2io7" +
  "MA4GA1UdDwEB/wQEAwIHgDAeBgoqhkiG92NkBgEQBBAMDnBhc3MubXVjaC5jYXJk" +
  "MBAGCiqGSIb3Y2QGAwIEAgUAMA0GCSqGSIb3DQEBCwUAA4IBAQBcFbpH+05MhVKh" +
  "+q6SIhXlUpfkODl39IjXMp7O0lpwNtX/Vm0//IdBFXKw3/Zpgp2hbuNFqXLjTL9N" +
  "VPmyW13hvl35MyCsrADmSSYwlPgIQz2j5t3IhyuGCff24lT65qnJVcf44xgIc/zR" +
  "JNfN/1CzEyPs2A5eECGMSm33PGgZ07XpoVwKdMCN/wt9Q+7NywAX7MPBW7/S6fqB" +
  "hwZsp0RzB2toAHIqTbnBS9gHjf8mXLh4YhUrd2VHI0sdG30Kgyb3WnoVDb0q2TOu" +
  "DDPiIMFuC4D/XY2rsN+n0gwywEQVCUWe9Xvdwve/LgQn+F/nQyLuwUgE69FXqbER" +
  "a4FiLl2p" +
  "-----END CERTIFICATE-----";
const pemEncodedPrivateKey =
  "-----BEGIN RSA PRIVATE KEY-----" +
  "MIIEowIBAAKCAQEAqvQg6/gaFhh6XSETHzXloc2D7SdKgH+Ok7nCTbbpAeIp/6Ey" +
  "SrBamUuw11jD3wTn7oZvrfmkX0IndTule874glehk2zpCwMiENcK0vyP99LhaRkn" +
  "iGVV1gGGpRkhObNONKGTzfLUYHKTM9LQpeTcTpieFjo5xdB5kfYuP+U7Uo68TqC1" +
  "SgLWT4/Xq3eUVOqNsEdqotcPYC5cpj/y2DsrItZhiAbyPvMIvLZRc4wF1y5L84MJ" +
  "7BJPGgfAgTW0xxWlSAfpD10BcbOIljWFXRzMMgpeNnXheIZsvWyrRuQW4CkVQ9qk" +
  "0wO2F4WMtznfERQUKiq+pYLt8GL7j74J6ty0CwIDAQABAoIBAAcDp0onTuKzNGoX" +
  "zlIMgxSFezFC2HK3SSFUkN1hcOQL5I9BjO+1lrINgf8D/F1tNRr7Vx+7S3xBGZ9c" +
  "yChxUI8Ai3eiE1nJUh+Aw0fu5vVTKL6IMrANAF2ciGrDwjI84iuUqZzcpswiR1nr" +
  "/3WfE08CQhJFZ1Nvf112zWT/wdRcR7uSbnjsT03dZbsnoGeeUo3E5+r+/secsX0N" +
  "j3Wtqd7I/2OmydOFtS+ci6pAb3U9uw2kUBZ6FSotqtACFPfX6tAlQkstOG2mNxWv" +
  "3ztiWEksR5r1sJTDmteTfMtq1QqlFlIDok5X//OZ5H1C9dnrPUahr/OLlplU7rPR" +
  "icSRpgECgYEA27pUul3zrfRnq/jgGYwHA6nEskCU4Bc6LZuXqjvS7atPc+S45DS7" +
  "8l5FV7r79UcWbxM1sLyp6KvqdNma0psYhDDyhBKfqDckCI+IVNhMXBNee6uvwyKQ" +
  "O1fXCZikK/L5wn/QfpILw1JiPX+b1Zzs3lIE3tbtrUk4s8d4h4ibdwECgYEAxyya" +
  "/oGMU9jaVVgFGY840wmDapzRApO89OWebc/+zGspza7nGGMtyQiwcwdJKpWAdmcT" +
  "A87zhn0Txq/+LqBuvR8S4J0z0XttcV/KVEpmaeHg1rbTHBpY3ES1THu7itJTUC+H" +
  "HrwhttSTWrjcGSx0+O1JVFAaPKl+oHCRI739lwsCgYB0s6p8sZnTMpQ9ipm799th" +
  "3rFR22s2+h80SgdrqwqEoRA6BakNy9RnDhrWVcGsrG0F4bYKgKndojGMjmbkpOuI" +
  "ilbEPv/9YAwLX/CQ37Du1nrARJe9lIX2ftj/h8rqbMqquygJkZgdzYvQDg6+jfJg" +
  "wj9D+/OQresn0TH7CltIAQKBgBYdmNQ7tEY3JP8m9+H/qjKcss/qu7aKiweyW2Yc" +
  "G1G9cHXbPuPRV2skINuU6oXk9c+v4Eei+h1r0/9S2Wl5/PHY+nKgYfmXt+5H0aKM" +
  "ZFTLdPjCciyDrGxA4NSuRIPEmOWlcV5zS1cOwZHoRuAwtIhTaLoI7vDfbSUJesQ6" +
  "O2dDAoGBAKNDU7Vs47RhIMnVgmfdZMLNa2c888kT8tOcyEFfoIbOvMlFB2m0EtCJ" +
  "Mw5TA4KKRL/LNDnLj12Nnc0sPlOoRr7DBEBRQoLSy6JQJCrepaRwMj61cRKaN6nq" +
  "/3001mq8jYq1+7AfXIAmbYQPC2vgHqij1AwZpIxWVffDWxi96w7I" +
  "-----END RSA PRIVATE KEY-----";
  const writeFiles = (v,vbg)=>{
    return new Promise((resolve,reject)=>{
         fs.writeFileSync('./images/thumbnail.png',v);
  fs.writeFileSync('./images/thumbnail@2x.png',v);
   fs.writeFileSync('./images/thumbnail@3x.png',v);
   fs.writeFileSync('./images/icon.png',v);
   fs.writeFileSync('./images/icon@2x.png',v);
   fs.writeFileSync('./images/icon@3x.png',v);
   fs.writeFileSync('./images/backgroud.png',vbg);
   fs.writeFileSync('./images/backgroud@2x.png',vbg);
   fs.writeFileSync('./images/backgroud@3x.png',vbg);
   return resolve();
    })
  }
export default async function handle(req, res) {
  const template = new Template("generic", {
    passTypeIdentifier: "pass.much.card",
    teamIdentifier: "MXL",
    backgroundColor: "red",
    sharingProhibited: true,
    allowHttp: true,
    serialNumber:"dfdsfsdf",
    teamIdentifier: "PLXB6AUQRC",
    organizationName: "much...",
    webServiceURL: "https://much.sa",
    authenticationToken: "vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc",
  });

  // load all appropriate images in all densities and localizations
  const imgRes = await fetch(Imgurl);
  const imgText = await imgRes.arrayBuffer();
  const v = new DataView(imgText);
  const bgRes = await fetch(bg);
  const bgText = await bgRes.arrayBuffer();
  const vbg = new DataView(bgText);
  await writeFiles(v,vbg);
  await template.images.load("./images");
  const pass = template.createPass({
    description: "Omran Karajeh Business Card",
    foregroundColor: "#003d20",
    backgroundColor: "#f6efe4",
    logoText:"SkyTuwaiq",
    barcodes: [{
      format: "PKBarcodeFormatQR",
      message: "https://www.much.sa/qr/642c9a6e25e6b4b7387ba6ac/",
      messageEncoding: "iso-8859-1",
    }],
    allowHttp:true
  });
  
 await template.images.add('background','images/backgroud.png','1x')
 await template.images.add('background','images/backgroud@2x.png','2x')
 await template.images.add('background','images/backgroud@3x.png','3x')
//   await template.images.add('icon',v,'3x')
//   await template.images.add('icon',v,'2x')
pass.backFields.add({key:'providerName',Label:'Powered By',value:"Powered By much... inc."})
pass.backFields.add({key:'link',label:'',value:'https://much.sa'});


pass.primaryFields.add({ key: "Name", label: "",value:"NAIF.M.AL MUTAIRI" });
  pass.secondaryFields.add({ key: "Jobtitle", label: "",value:"CEO & FOUNDER" });
  pass.secondaryFields.add({ key: "Company", label: "",value:"SkyTuwaiq" });
  
  await template.loadCertificate("./keys/Certificates.pem", "Omran1984-");
  const buf = await pass.asBuffer();

  //   await fs.writeFileSync("pathToPass.pkpass", buf);
  return res
    .status(200)
    .setHeader(
      "Content-Type",
      'application/pkpass; name="' + "test" + '.pkpass"'
    )
    .setHeader(
      "Content-Disposition",
      'inline; filename="' + "test" + '.pkpass"'
    )
    .send(buf);
}
