import { connectToDatabase } from "../../lib/db";

export default async function handler(req, res) {
  if (req.body) {
    let request = JSON.parse(req.body);
    if (request.firstName && request.lastName) {
      const client = await connectToDatabase();
      const suggestions = await client
        .db()
        .collection("seamless_attads")
        .aggregate([
          {
            $match: {
              firstName: { $regex: request.firstName, $options: "i" },
              lastName: { $regex: request.lastName ,$options: "i"}
            },
          },
          {
            $sort: { firstName: 1 },
          },
        ])
        .limit(3)
        .toArray();
      return res.status(200).json(suggestions);
    }
  }
}
