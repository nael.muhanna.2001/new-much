import cookie from "cookie";
import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

let client;
let user = null;
async function handler(req, res) {
  let user = await req.body?JSON.parse(req.body):null;
  client = await connectToDatabase();
  
  if(user!=null && user._id)
  {
    user = await client.db().collection('users').findOne({_id:ObjectId(user._id)});
    
    res.status("201").json(user);
  }
  else
  {
    let insertUser = await client.db().collection('users').insertOne({
      type:'visitor',
      created_at:new Date(),
      updated_at:new Date()
    });
    user = await client.db().collection('users').findOne({_id:ObjectId(insertUser.insertedId)});
    
    res.setHeader(
      "Set-Cookie",
      cookie.serialize("user", JSON.stringify(user), {
        httpOnly: true,
        secure: process.env.NODE_ENV !== "development",
        maxAge: 60 * 60 * 24 * 365,
        sameSite: "strict",
        path: "/",
      })
    );
    res.status("201").json(user);
  }
  
}
export default handler;
