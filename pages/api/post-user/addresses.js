import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function (req, res) {
  const token = await getToken({ req });
  if (token) {
    const client =  await connectToDatabase();
    const addresses = await client.db().collection('addresses').find({user_id:token.user._id}).toArray();
    return res.status(201).json(addresses);
  }
  return res.status(202).json([]);
}
