import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const token = await getToken({ req });
  if (token && req.body) {
    const client = await connectToDatabase();
    let request = JSON.parse(req.body);
    let newAddress = {
      user_id: token.user._id,
      address: request.address,
      building: request.building,
      city:request.city,
      appartment: request.appartment,
      fname: request.fname,
      lname: request.lname,
      phone: request.phone,
      prefix: request.prefix,
      default: true,
      created_at: new Date(),
    };
    let id = null;
    if (request._id) {
      const addressUpdater = await client
        .db()
        .collection("addresses")
        .updateOne(
          { _id: ObjectId(request._id) },
          { $set: newAddress },
          { upsert: false }
        );
      id = request._id;
    } else {
      const addressIserter = await client
        .db()
        .collection("addresses")
        .insertOne(newAddress);
      id = addressIserter.insertedId;
    }
    let addressesUpdater = await client
      .db()
      .collection("addresses")
      .updateMany(
        { user_id: token.user._id, _id: { $ne: ObjectId(id) } },
        { $set: { default: false } },
        { upsert: false }
      );
      const addresses = await client.db().collection('addresses').find({user_id:token.user._id}).toArray();
      return res.status(201).json(addresses);
    
  }
  return res.status(401).json({ message: "Bad Request!" });
}
