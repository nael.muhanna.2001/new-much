import cloudinary from "cloudinary";

cloudinary.config({
  cloud_name: 'pweb',
  api_key: '969553222356618',
  api_secret: 'tIaFy2PQlLPZcuroHd9XeaEZzRQ'
});

export default (request, response) => {
  const image = request.body.image;
  const lang = request.body.lang?request.body.lang:'en';

  return cloudinary.v2.uploader.upload(
    image,
    { ocr: "adv_ocr:"+lang },
    (error, result) => {
      if (error) return response.status(500).json({ error });

      const { textAnnotations } = result.info.ocr.adv_ocr.data[0];

      const extractedText = textAnnotations
        .map((anno, i) => i > 0 && anno.description.replace(/[^0-9a-z]/gi, ""))
        .filter((entry) => typeof entry === "string")
        .join(" ");

      return response.status(200).json({ data: extractedText });
    }
  );
};