import { connectToDatabase } from "../../../lib/db";
export default async function handler(req,res){
    const client = await connectToDatabase();
    const countriesCollection = await client.db().collection("countries")
    .find()
    .toArray();
    
    client.close();
    
    return res.status(200).json(countriesCollection);
}