import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../lib/db";

export default async function handler(req, res) {
  const { _id, geolocation ,src} = JSON.parse(req.body);
  if (_id && geolocation) {
    const client = await connectToDatabase();
    const visitUpdater = await client
      .db()
      .collection("visits")
      .updateOne(
        { _id: ObjectId(_id) },
        { $set: { geoLocation: geolocation } },
        { upsert: true }
      );
    return res.status(200).json(visitUpdater);
  }
}
