import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const token = await getToken({ req });
  let user = null;
  const { _id } = req.query;
  var hex = /[0-9A-Fa-f]{6}/g;
  let card = null;
  if(token)
  {
     user =  token.user;
     
  }
  
  const client = await connectToDatabase();
  if (ObjectId.isValid(_id) && hex.test(_id)) {
    // if (hex.test(_id))
    card = await client
      .db()
      .collection("cards")
      .findOne({
        _id: ObjectId(_id),
        $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
      });
  } else {
    card = await client
      .db()
      .collection("cards")
      .findOne({
        slug: _id,
        $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
      });
  }
  let permissions = ["public"];
  if(user && user._id == card.user_id)
  {
    permissions.push('private');
    permissions.push('connections')
  }
  const cardFields = await client
        .db()
        .collection("cardFields")
        .find({
          cardId: card._id + "",
          "field.cardType": card.cardType,
          viewAccess: { $in: permissions },
          $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        })
        .sort("order")
        .toArray();

      card.fields = cardFields;

  if (req.body) {
    const { location, card_id, src, referrer} = JSON.parse(req.body);
    const reqUser = JSON.parse(req.body)['user'];
     user = reqUser ? reqUser : token?.user;
     console.log('test',user);
    if (card && user) {
      if (src != null && card[src] == true) {
        console.log('test');
        let cardUpdater = await client
          .db()
          .collection("cards")
          .updateOne(
            { _id: ObjectId(card._id) },
            { $addToSet: { connections: user._id } }
          );
        card = await client
          .db()
          .collection("cards")
          .findOne({ _id: ObjectId(card._id) });
      }

      if (user) {
        let firstMeet = await client
          .db()
          .collection("visits")
          .findOne({
            card_id: card_id,
            visitor_id: user._id,
            $and: [{ src: { $ne: null } }, { src: { $ne: "wallet" } }],
          });
        card.first_meet = firstMeet;
      }

      
      if (card.connections && user && card.connections.includes(user._id)) {
        permissions.push("connections");
      }
      if (user && user._id === card.user_id) {
        permissions.push("private");
        permissions.push("connections");
      }
      const cardFields = await client
        .db()
        .collection("cardFields")
        .find({
          cardId: card._id + "",
          "field.cardType": card.cardType,
          viewAccess: { $in: permissions },
          $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        })
        .sort("order")
        .toArray();

      card.fields = cardFields;
      card.visitor = user;
      if (user && card.user_id != user._id && location && card_id) {
        let visitInset = await client.db().collection("visits").insertOne({
          card_id: card_id,
          visitor_id: user._id,
          location: location,
          src: src,
          referrer: referrer,
          created_at: new Date(),
        });
        let visit = await client
          .db()
          .collection("visits")
          .findOne({ _id: ObjectId(visitInset.insertedId) });

        card.visit = visit;
      }
    }
  }

  return res.status(200).json(card);
}
