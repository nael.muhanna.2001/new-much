import { connectToDatabase } from "../../../lib/db";

export default async function handler(req,res){
    const client = await connectToDatabase();
    const cards = await client.db().collection('cards').find().toArray();
    return res.status(200).json(cards);
}