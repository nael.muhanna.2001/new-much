import { connectToDatabase } from "../../../lib/db"
export  default async function  handler(req,res)
{
    const {name}=JSON.parse(req.body);
    const user = JSON.parse(req.cookies.user);
    
    const client = await connectToDatabase();
   let db = await client.db().collection('cards').findOne({slug:name});
   let suggests = user.name.toLowerCase().replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, '.');
   if(db)
   {
    
    let counter = 1;
    let db1 = await client.db().collection('cards').findOne({slug:suggests});
    while(db1!=null)
    {
        suggests=(name+counter); 
        db1 = await client.db().collection('cards').findOne({slug:suggests});
        counter++;
    }
   }
    client.close();
    return res.status(200).json(db==null?{status:true}:{status:false,suggest:suggests});
}