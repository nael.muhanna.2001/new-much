import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const user = JSON.parse(req.cookies.user);
  
  const request = JSON.parse(req.body);
  if(typeof request.inputValue=='string')
  {
    let companyUrl = request.inputValue
    .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
    .toLowerCase();
  const client = await connectToDatabase();
  const checkAvailbility = await client.db().collection('cards').findOne({slug:companyUrl});
  if(checkAvailbility)
  {
    
    let counter = 1;
    let db1 = await client.db().collection('cards').findOne({slug:companyUrl});
    while(db1!=null)
    {
      companyUrl=(companyUrl+counter); 
        db1 = await client.db().collection('cards').findOne({slug:companyUrl});
        counter++;
    }
   }
  const companyInsetion = await client.db().collection("cards").insertOne({
    user_id: user._id,
    slug: companyUrl,
    name: request.inputValue,
    cardType: "company_profile",
    qr:true,
    nfc:true,
    created_at:new Date(),
  });
  const card = await client
    .db()
    .collection("cards")
    .findOne({
      _id: ObjectId(companyInsetion.insertedId),
    });
  client.close();
  return res.status(201).json(card);

  }
  else if(typeof request.inputValue =='object')
  {
    let companyUrl = request.inputValue.label
    .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
    .toLowerCase();
    const client = await connectToDatabase();
  const checkAvailbility = await client.db().collection('cards').findOne({slug:companyUrl});
  if(checkAvailbility)
  {
    
    let counter = 1;
    let db1 = await client.db().collection('cards').findOne({slug:companyUrl});
    while(db1!=null)
    {
      companyUrl=(companyUrl+counter); 
        db1 = await client.db().collection('cards').findOne({slug:companyUrl});
        counter++;
    }
   }
  const companyInsetion = await client.db().collection("cards").insertOne({
    user_id: user._id,
    slug: companyUrl,
    name: request.inputValue.label,
    cardType: "company_profile",
    description:request.inputValue.description,
    pic:request.inputValue.pic,
    created_at:new Date(),
  });
  const card = await client
    .db()
    .collection("cards")
    .findOne({
      _id: ObjectId(companyInsetion.insertedId),
    });
  client.close();
  return res.status(201).json(card);

  }
  
}
