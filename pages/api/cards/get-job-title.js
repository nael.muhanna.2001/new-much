import { connectToDatabase } from "../../../lib/db";
function capitlize(str)
{
  let strArr = str.split(" ");
  strArr.forEach((e,k)=>{
    strArr[k]=e.charAt(0).toUpperCase()+e.slice(1);
  })
  return strArr.join(' ');
}
export default async function handler(req, res) {
  const request = JSON.parse(req.body);
  let jobTitles = [];
  let client = await connectToDatabase();
  const jobtitles = await client
    .db()
    .collection("cards")
    .find(
      {
        cardType: "business_card",
        approved: true,
        "jobTitle.value": { $regex: request.inputValue, $options: "i" },
      },
      { "jobTitle.$": 1 }
    )
    .sort("jobTitle")
    .limit(20)
    .toArray();
  jobTitles = jobtitles;
  if (jobtitles.length == 0) {
    const attendsJobTitle = await client
      .db()
      .collection("seamless_attads")
      .aggregate([
        {
          $sort: { jobTitle: 1 },
        },
        {
          $match: { jobTitle: { $regex: request.inputValue,$options: "i" } },
        },
        {
          $group: {
            _id: { jobTitle: "$jobTitle" },
          },
        },
      ])
      .toArray();

    attendsJobTitle.forEach((e, k) => {
      let item = {
        jobTitle: { value: e._id.jobTitle, label: capitlize(e._id.jobTitle) },
      };
      jobTitles.push(item);
    });
  }

  return res.status(200).json(jobTitles);
}
