import { connectToDatabase } from "../../../lib/db";
export default async function handler(req, res) {
  const request = JSON.parse(req.body);
  let companies = [];
  let client = await connectToDatabase();
  let notEx = false;
  companies = await client
    .db()
    .collection("cards")
    .find({
      cardType: "company_profile",
      name: { $regex: request.inputValue, $options: "i" },
    })
    .sort("name")
    .limit(20)
    .toArray();
  if (companies.length == 0) {
    
    const seamlessCompanies = await client
      .db()
      .collection("seamless")
      .find({ name: { $regex: request.inputValue, $options: "i" } })
      .sort("name")
      .limit(20)
      .toArray();
    if (seamlessCompanies.length == 0) {
      notEx = true;
    } else {
      notEx = false;
    }
    seamlessCompanies.forEach((e, k) => {
      let item = {
        _id: null,
        value: e.id,
        name: e.name,
        pic: e.logoUrl,
        source: "seamless",
        description: e.description,
      };
      companies.push(item);
    });
  }
  if (companies.length == 0 && notEx) {
    const attendsCompanies = await client
      .db()
      .collection("seamless_attads")
      .aggregate([
        {
            $match:{organization:{$regex: request.inputValue, $options: "i"}}
        },
        {
            $sort:{organization:1}
        },
        {
          $group: {
            _id: { organization: "$organization" },
          },
        },
        
      ])
      .toArray();
     attendsCompanies.forEach((e, k) => {
        let item = {
          _id: null,
          value: e._id._id,
          name: e._id.organization,
          pic: null,
          source: "seamless",
          description:null,
        };
        companies.push(item);
      });;
  }

  return res.status(200).json(companies);
}
