import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  if (req.method !== "POST") {
    return res.redirect(307, "/404");
  }
  let token = await getToken({req});
  if(!token)
  {
    return res.status(401).json({message:'No Auth'});
  }
let user = token.user;
  let client = await connectToDatabase();
  const cards = await client
    .db()
    .collection("cards")
    .aggregate([
      {
        $match: {
          user_id: user._id,
          $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        },
      },
      { $sort: { cardType: 1 } },
      {
        $lookup: {
          from: "visits",
          let: { card_id: "$_id" },
          pipeline: [
            { $addFields: { card_id: { $toObjectId: "$card_id" } } },
            { $match: { $expr: { $eq: ["$card_id", "$$card_id"] } } },
            { $group: { _id: "$visitor_id",location:{$push:"$location"}, count: { $sum: 1 } } },
          ],
          as: "vts",
        },
      },
    ])
    .toArray();
  // const cards = await client.db().collection('cards').find({user_id:user._id,$or:[{deleted_at:{$exists:false}},{deleted_at:null}]}).sort('cardType').toArray();
  return res.status(200).json(cards);
}
