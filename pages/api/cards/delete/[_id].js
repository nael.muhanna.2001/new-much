import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../../lib/db";


export default async function handler(req, res) {
  let location = null,
    card_id = null,
    src = null,
    referrer = null;
  if (req.body) {
    location = JSON.parse(req.body)["location"];
    card_id = JSON.parse(req.body)["card_id"];
    src = JSON.parse(req.body)["src"];
    referrer = JSON.parse(req.body)["referrer"];
  }

  const { _id } = req.query;
  var hex = /[0-9A-Fa-f]{6}/g;
  let card = null;
  const client = await connectToDatabase();
  if(ObjectId.isValid(_id))
  // if (hex.test(_id)) 
  {
    
    card = await client
      .db()
      .collection("cards")
      .findOne({ _id: ObjectId(_id) });
  } else {
    card = await client.db().collection("cards").findOne({ slug: _id });
  }
  if(card)
  {
    const deleteCard = await client.db().collection('cards').updateOne({_id:ObjectId(card._id)},{$set:{deleted_at:new Date()}});
  }
  return res.status(200).json(card);
}
