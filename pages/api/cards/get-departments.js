import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const request = JSON.parse(req.body);

  let client = await connectToDatabase();
  const jobtitles = await client
    .db()
    .collection("cards")
    .find(
      {
        cardType: "business_card",
        approved: true,
        "department.value": { $regex: request.inputValue, $options: "i" }
      },
      { "department.$": 1 }
    )
    .sort("department")
    .limit(20)
    .toArray();


  return res.status(200).json(jobtitles);
}
