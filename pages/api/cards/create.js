import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const request = JSON.parse(req.body);
  let user = null;
  let token = await getToken({ req });
  if (token) {
    user = token.user;
  }

  let cardId;
  const client = await connectToDatabase();
  const db = client.db();
  const cardCollection = db.collection("cards");
  const inputCard = {};
  if (user) {
    inputCard.user_id = user._id;
  } else if (request.user_id) {
    inputCard.user_id = request.user_id;
  }
  if(!request._id)
  {
    inputCard.qr = request.qr ? request.qr : true;
    inputCard.nfc = request.nfc ? request.nfc : true;  
  }

  
  //inputCard.cover="https://picsum.photos/1152/1125";
  inputCard.avatar = "1rem";
  if (request.slug) {
    let cardSlug = request.slug;
    const checkAvailbility = await client
      .db()
      .collection("cards")
      .findOne({ slug: cardSlug });
    if (checkAvailbility) {
      let counter = 1;
      let db1 = await client
        .db()
        .collection("cards")
        .findOne({ slug: cardSlug });
      while (db1 != null) {
        cardSlug = cardSlug + "" + counter;
        db1 = await client.db().collection("cards").findOne({ slug: cardSlug });
        counter++;
      }
      inputCard.slug = cardSlug;
    }
    inputCard.slug = cardSlug;
  }
  if (request.cardType) inputCard.cardType = request.cardType;
  if (request.name) inputCard.name = request.name;
  if (
    request.jobTitle ||
    (typeof request.jobTitle != "undefined" && request.jobTitle == null)
  )
    inputCard.jobTitle = request.jobTitle;
  if (
    request.department ||
    (typeof request.department != "undefined" && request.department == null)
  )
    inputCard.department = request.department;
  if (
    request.company ||
    (typeof request.company != "undefined" && request.company == null)
  ) {
    if (request.company != null && request.company.value) {
      inputCard.company = request.company;
    } else {
      if (request.company != null && request.company.label) {
        const findCompany = await client
          .db()
          .collection("cards")
          .findOne({ name: request.company.label });
        if (findCompany) {
          inputCard.company = {
            value: findCompany._id + "",
            label: findCompany.name,
            pic: findCompany.pic,
            slug: findCompany.slug,
          };
        } else {
          let companyUrl = request.company.label
            .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
            .toLowerCase();
          const checkAvailbility = await client
            .db()
            .collection("cards")
            .findOne({ slug: companyUrl });
          if (checkAvailbility) {
            let counter = 1;
            let db1 = await client
              .db()
              .collection("cards")
              .findOne({ slug: companyUrl });
            while (db1 != null) {
              companyUrl = companyUrl + counter;
              db1 = await client
                .db()
                .collection("cards")
                .findOne({ slug: companyUrl });
              counter++;
            }
          }
          const createCompany = await client
            .db()
            .collection("cards")
            .insertOne({
              user_id: user._id,
              slug: companyUrl,
              name: request.company.label,
              cardType: "company_profile",
              description: request.company.description,
              qr: true,
              nfc: true,
              pic: null,
            });
          let createdCompany = await client
            .db()
            .collection("cards")
            .findOne({ _id: ObjectId(createCompany.insertedId + "") });
          inputCard.company = {
            value: createdCompany._id + "",
            label: createdCompany.name,
            pic: createdCompany.pic,
            slug: createdCompany.slug,
          };
        }
      }
    }
  }

  if (request.description) inputCard.description = request.description;
  if (request.pic || (typeof request.pic != "undefined" && request.pic == null))
    inputCard.pic = request.pic;
  if (
    request.cover ||
    (typeof request.cover != "undefined" && request.cover == null)
  )
    inputCard.cover = request.cover;
  if (request.mobilePrefix) inputCard.mobilePrefix = request.mobilePrefix;
  if (request.mobile || typeof request.mobile != "undefined")
    inputCard.mobile = request.mobile;
  if (request.telephonePrefix)
    inputCard.telephonePrefix = request.telephonePrefix;
  if (request.telephone || typeof request.telephone != "undefined")
    inputCard.telephone = request.telephone;
  if (request.whatsappPrefix) inputCard.whatsappPrefix = request.whatsappPrefix;
  if (request.whatsapp || typeof request.whatsapp != "undefined")
    inputCard.whatsapp = request.whatsapp;
  if (request.email || typeof request.email != "undefined")
    inputCard.email = request.email;
  if (request.address || typeof request.address != "undefined")
    inputCard.address = request.address;
  if (request.primaryColor) inputCard.primaryColor = request.primaryColor;
  if (request.secondaryColor) inputCard.secondaryColor = request.secondaryColor;
  if (request.contactViewAccess)
    inputCard.contactViewAccess = request.contactViewAccess;
  if (request.color) inputCard.color = request.color;
  if (request.contactShape) inputCard.contactShape = request.contactShape;
  if (request.buttonShape) inputCard.buttonShape = request.buttonShape;
  if (request.avatar) inputCard.avatar = request.avatar;
  if (request.QRcolorDark) inputCard.QRcolorDark = request.QRcolorDark;
  if (request.QRcolorLight) inputCard.QRcolorLight = request.QRcolorLight;
  if (request.nfc || request.nfc==false) inputCard.nfc = request.nfc;
  if (request.qr || request.qr==false) inputCard.qr = request.qr;
  if (request._id) {
    inputCard.updated_at = new Date();
    cardId = await cardCollection.updateOne(
      { _id: ObjectId(request._id) },
      { $set: inputCard },
      { upsert: true }
    );
  } else {
    inputCard.create_at = new Date();
    cardId = await cardCollection.insertOne(inputCard);
  }

  const card = await cardCollection.findOne({
    _id: ObjectId(cardId.insertedId),
  });
  if (request.mobile && request.mobilePrefix && card) {
    let field = await client
      .db()
      .collection("fieldTypes")
      .findOne({ fieldType: "phone", name: "Phone" });
    field._id = field._id + "";
    const cardField = await client
      .db()
      .collection("cardFields")
      .insertOne({
        field: field,
        cardId: card._id + "",
        prefix: request.mobilePrefix,
        phone: request.mobile,
        label: "Mobile",
        viewAccess: "public",
        order: 0,
        published: true,
        created_at: new Date(),
      });
  }
  if (request.email && card) {
    let field = await client
      .db()
      .collection("fieldTypes")
      .findOne({ fieldType: "email", name: "Email" });
    field._id = field._id + "";
    const cardField = await client
      .db()
      .collection("cardFields")
      .insertOne({
        field: field,
        cardId: card._id + "",
        email: request.email,
        label: "Work",
        viewAccess: "public",
        order: 0,
        published: true,
        created_at: new Date(),
      });
  }

  return res.status(201).json(card);
}
