import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req,res)
{
    const {id}=req.query;
    const client = await connectToDatabase();
    let product = await client.db().collection('muchProducts').findOne({_id:ObjectId(id)});
    
    if(product.selectedProductsIds)
    {
        let ids = [];
    product.selectedProductsIds.map((e)=>{
        ids.push(ObjectId(e));
    })
    const products = await client
      .db()
      .collection("muchProducts")
      .find({_id:{$in:ids}}).toArray();
      product.relatedProducts = products;

    }
    else
    {
        product.relatedProducts = [];
    }
    
      
    return res.status(200).json(product);
}