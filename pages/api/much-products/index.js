import { connectToDatabase } from "../../../lib/db";

export default async function handler(req,res)
{
    let request = null;
    if(req.body)
    {
        request = JSON.parse(req.body);

    }
    const client = await connectToDatabase();
    const products = await client.db().collection('muchProducts').find({published:true,languages:request.locale}).toArray();
    return res.status(200).json(products);
}