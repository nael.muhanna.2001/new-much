import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  if (req.body) {
    let request = JSON.parse(req.body);
    let ids = [];
    request.productsIds.map((e)=>{
        ids.push(ObjectId(e));
    })
    const client = await connectToDatabase();
    const products = await client
      .db()
      .collection("muchProducts")
      .find({_id:{$in:ids}}).toArray();
      return res.status(200).json(products);
  }
  return res.status(401).json({message:'Bad Reques'});
}
