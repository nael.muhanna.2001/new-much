import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  

  if (req.body) {
    let request = JSON.parse(req.body);
    const client = await connectToDatabase();
    const order = await client
      .db()
      .collection("orders")
      .findOne({ _id: ObjectId(request._id) });
    
      const changeQuantity = await client
        .db()
        .collection("orders")
        .updateOne(
          { _id: ObjectId(request._id) },
          { $set: { quantity: request.quantity } },
          { upsert: false }
        );
      return res.status(201).json(changeQuantity);
    
    
  }
  return res.status(401).json({ message: "bad request" });
}
