import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const token = await getToken({ req });
  let request = JSON.parse(req.body)
  if (token && token.user || request.user) {
    let user = null;
    if(!token)
    {
      user = request.user;
    }
    else
    {
      user = token.user;
    }
    const client = await connectToDatabase();
    const orders = await client
      .db()
      .collection("orders")
      .find({
        user_id: user._id,
        status: "new",
        $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
      })
      .toArray();
    return res.status(200).json(orders);
  }
  return res.status(200).json([]);
}
