import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";
export const config = {
  api: {
    bodyParser: {
      sizeLimit: "4mb", // Set desired value here
    },
  },
};
export default async function handler(req, res) {
  const token = getToken({ req });
  let user = null;
  if (token) {
    user = token.user;
  }
  const client = await connectToDatabase();

  let request = null;
  if (req.body && req.method == "POST") {
    request = JSON.parse(req.body);
    if (!request.user_id && token) {
      request.user_id = token.user._id;
    }
    request.created_at = new Date();
    const productChecker = await client
      .db()
      .collection("orders")
      .findOne({
        user_id: request.user_id,
        product_id: request.product_id,
        $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        status: "new",
      });
      let pid=null;
    if (productChecker) {
      const updateOrder = await client
        .db()
        .collection("orders")
        .updateOne(
          { _id: ObjectId(productChecker._id) },
          {
            $set: { quantity: productChecker.quantity * 1 + request.quantity },
          },
          { upsert: false }
        );
        pid = productChecker._id;
    }
    else
    {
        const insertOrder = await client
      .db()
      .collection("orders")
      .insertOne(request);
      pid=insertOrder.insertedId;
    }
    
    const order = await client
      .db()
      .collection("orders")
      .findOne({ _id: ObjectId(pid) });
    return res.status(201).json(order);
  }
  return res.status(422).json({ message: "Bad Request!" });
}
