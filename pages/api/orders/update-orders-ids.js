import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const client = await connectToDatabase();
  const token = await getToken({ req });
  const user = await client
    .db()
    .collection("users")
    .findOne({ _id: ObjectId(token.user._id) });
  if (token && req.body) {
    let request = JSON.parse(req.body);

    const orderUpdate = await client
      .db()
      .collection("orders")
      .updateMany(
        { user_id: request.old_id, status: "new" },
        { $set: { user_id: token.user._id } },
        { upsert: false }
      );

    return res.status(202).json({ success: true, user: user });
  }
  return res.status(200).json({ success: false, user: user });
}
