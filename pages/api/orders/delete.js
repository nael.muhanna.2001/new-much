import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const token = await getToken({ req });
  let request = JSON.parse(req.body);
  if ((req.body && token) || request.user) {
    let user = null;
    if (token) {
      user = token.user;
    } else {
      user = request.user;
    }
    
    const client = await connectToDatabase();
    const order = await client
      .db()
      .collection("orders")
      .findOne({ _id: ObjectId(request._id) });
    if (order.user_id == user._id) {
      const deleteOrder = await client
        .db()
        .collection("orders")
        .updateOne(
          { _id: ObjectId(request._id) },
          { $set: { deleted_at: new Date() } },
          { upsert: false }
        );
      const orders = await client
        .db()
        .collection("orders")
        .find({
          user_id: user._id,
          status: "new",
          $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        })
        .toArray();
      return res.status(202).json(orders);
    }
    return res.status(422).json({ message: "No Auth" });
  }
  return res.status(401).json({ message: "bad request" });
}
