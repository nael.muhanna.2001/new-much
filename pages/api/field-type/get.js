import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db"

export default async function handler(req,res)
{
    const _id = req.query.id
    if(_id)
    {
        const client = await connectToDatabase();
        const fieldType = await client.db().collection('fieldTypes').findOne({
            _id:ObjectId(_id)
        });
        
        return res.status(200).json(fieldType);
    }
    return res.status(404).send('Not Found');
}