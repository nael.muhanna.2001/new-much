import { ObjectId } from "mongodb";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const user = JSON.parse(req.cookies.user);
  if (req.body && user) {
    let id =null
    let linkType = JSON.parse(req.body);
    const client = await connectToDatabase();
    linkType.created_at = new Date();
    linkType.published = 1;
    linkType.user_id = user._id;
    if (linkType._id) {
       id = ObjectId(linkType._id);
      delete linkType._id;
      const updateLinkType = await client
        .db()
        .collection("fieldTypes")
        .updateOne({ _id: id }, { $set: linkType }, { upsert: true });
    } else {
      const insertLinkType = await client
        .db()
        .collection("fieldTypes")
        .insertOne(linkType);
       id = ObjectId(insertLinkType.insertedId);
    }

    linkType = await client.db().collection("fieldTypes").findOne({
      _id: id,
    });
    return res.status(200).json(linkType);
  }
  return res.status(404).send("Not Found");
}
