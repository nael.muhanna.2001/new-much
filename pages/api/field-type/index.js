import { getToken } from "next-auth/jwt"
import { connectToDatabase } from "../../../lib/db";
export default async function handler(req, res) {
    const token = await getToken({ req })
    
    if(token && token.user)
    {
        let query = {
            $or: [{ deleted_at: { $exists: false } }, { deleted_at: null }],
        };
        if(req.body)
        {
            const {cardType} = JSON.parse(req.body);
            query.cardType=cardType;
            
        }
        const client = await connectToDatabase();
        const fieldTypes = await client.db().collection('fieldTypes').find(query).sort('section').toArray();

        return res.status(200).json(fieldTypes);
    }
    return res.status(404).send('Not Found')
  
}
