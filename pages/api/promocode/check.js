import { connectToDatabase } from "../../../lib/db";

export default async function handler(req,res)
{
    if(req.body)
    {
        let request = JSON.parse(req.body);
        const client = await connectToDatabase();
        const checker = await client.db().collection('promoCodes').findOne({code:{ $regex: new RegExp("^" + request.code.toLowerCase(), "i") }});
        if(checker && checker.expired_at < new Date())
        {
            return res.status(203).json({canApply:false,message:'The promo code is expired!'});
        }
        if(!checker)
        {
            return res.status(203).json({canApply:false,message:'No Pomo codo found'});
        }
        return res.status(200).json({canApply:true,promocode:checker});
    }
    return res.status(500).json({message:'Bad Request!'});
}