import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function (req,res)
{
    const token = await getToken({req});
    if (
        token &&
        token.user &&
        token.user.permissions &&
        token.user.permissions.includes("admin")
      )
      {
        const client = await connectToDatabase();
        const promocodes = await client.db().collection('promoCodes').find({$or: [{ deleted_at: { $exists: false } }, { deleted_at: null }]}).toArray();
        return res.status(200).json(promocodes);
      }
      return res.status(500).json({message:'Bad Request!'})
}
