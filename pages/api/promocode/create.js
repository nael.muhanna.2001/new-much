import { ObjectId } from "mongodb";
import { getToken } from "next-auth/jwt";
import { connectToDatabase } from "../../../lib/db";

export default async function handler(req, res) {
  const token = await getToken({ req });
  if (
    token &&
    token.user &&
    token.user.permissions &&
    token.user.permissions.includes("admin")
  ) {
    const client = await connectToDatabase();
    let request = JSON.parse(req.body);
    request.code = request.code.toUpperCase();
    if (request._id) {
      request.updated_at = new Date();
      request.updated_by = token.user._id;
      let id = request._id;
      delete request._id;
      const updater = await client
        .db()
        .collection("promoCodes")
        .updateOne({ _id: ObjectId(id) }, { $set: request }, { upsert: false });
        const promocode = await client.db().collection('promoCodes').findOne({_id:ObjectId(id)})
        return res.status(202).json(promocode);
    }
    else
    {
    request.user_id = token.user._id;
    request.created_at = new Date();
    const inserter = await client.db().collection('promoCodes').insertOne(request);
    const promocode = await client.db().collection('promoCodes').findOne({_id:ObjectId(inserter.insertedId)});
    return res.status(201).json(promocode);
    }
  }
  return res.status(500).json({message:'Bad Request'});
}
