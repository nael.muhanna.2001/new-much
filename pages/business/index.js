import Head from "next/head";
import Admin from "../../layouts/BusinessLayout";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { translation } from "../../lib/translations";
import Image from "next/image";

import "rsuite/dist/rsuite.min.css";
import Link from "next/link";
export default function BusinessCards() {
  const { data: session, status } = useSession();
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  if (status == "loading") {
    return (
      <>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }

  if (!session) window.location.href = "/signin";
  if (session && status != "loading" && session.user.admin_of)
    window.location.href = "/business/dashboard";
  if (session && status != "loading" && !session.user.admin_of)
    return (
      <>
        <Head>
          <title>{_("Create Your Business Account")}</title>
        </Head>
        <div className="w-full px-4" style={{ backgroundColor: "#084874" }}>
          <div className="logo">
            <Link href="/">
              <div>
                <Image
                  src="/images/logo.svg"
                  alt="Logo"
                  width={"160"}
                  height={"80"}
                ></Image>
                <span
                  className="absolute ltr:left-6 rtl:right-6 top-14 text-lg font-bold"
                  style={{ color: "#fff" }}
                >
                  {_("Enterprise")}
                </span>
                </div>
            </Link>
          </div>
        </div>
        <div className="flex flex-wrap">
          <div className="w-full md:w-6/12  p-4">
            <div className="mx-auto p-6 text-lg text-justify border rounded">
              <div
                className="pb-4 font-bold text-center text-4xl"
                style={{ color: "#084874" }}
              >
                {_("Attract and retain customers with ")}
                <b style={{ color: "#7FCBAE" }}>{_("much.sa")}</b>
              </div>
              {_(
                "much.sa's customizable NFC digital business cards and NFC tags."
              )}{" "}
              <br />{" "}
              {_(
                "Our trackable products and advanced analytics feature assist businesses in:"
              )}
              <ul className="list-decimal ltr:pl-10 py-2">
                <li className="py-1">{_("Collect sales leads")}</li>
                <li className="py-1">{_("Making valuable connections")}</li>
                <li className="py-1">{_("Driving revenue!")}</li>
              </ul>
              <div className="w-full h-16">
                <Link href='/business/create'>
              <button className="border-2 rounded p-3 float-right font-bold" style={{borderColor:"#7FCBAE",color:"#084874"}}>{_('Get Started')}</button>
              </Link>
              </div>
            </div>
          </div>
          <div className="w-full md:w-6/12 p-5">
            <img src="/images/BDB.png" />
          </div>
        </div>
        <div className="flex flex-wrap mx-auto p-2" style={{color:'#084874'}}>
          <div className="w-6/12 md:w-1/12 mx-auto text-center text-sm font-bold">
            {" "}
            <img className="" src="/images/elevatebrand.png" />
            {_("Elevate Brand Presence")}
          </div>
          <div className="w-6/12 md:w-1/12 mx-auto text-center text-sm font-bold">
            {" "}
            <img className="" src="/images/sustainable.png" />
            {_("Sustainable & Cost-effective")}
          </div>
          <div className="w-6/12 md:w-1/12 mx-auto text-center text-sm font-bold">
            {" "}
            <img className="" src="/images/customizable.png" />
            {_("Fully Customizable Card Designs")}
          </div>
          <div className="w-6/12 md:w-1/12 mx-auto text-center text-sm font-bold">
            {" "}
            <img className="" src="/images/trackable.png" />
            {_("Trackable Team Performance")}
          </div>
          <div className="w-6/12 md:w-1/12 mx-auto text-center text-sm font-bold">
            {" "}
            <img className="" src="/images/geolocation.png" />
            {_("Geolocation Records")}
          </div>
          <div className="w-6/12 md:w-1/12 mx-auto text-center text-sm font-bold">
            {" "}
            <img className="" src="/images/leadscollect.png" />
            {_("Sales Lead Collection")}
          </div>
        </div>
      </>
    );
}
