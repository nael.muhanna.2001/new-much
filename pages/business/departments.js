import Head from "next/head";
import Admin from "../../layouts/BusinessLayout";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { translation } from "../../lib/translations";
import Image from "next/image";

import "rsuite/dist/rsuite.min.css";
export default function Departments() {
  const { data: session, status } = useSession();
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  if (status == "loading") {
    return (
      <>
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }

  if (!session) window.location.href = "/signin";
  if (session && status != "loading")
    return (
      <>
        <Head>
          <title>{_("Eamil Signatures")}</title>
        </Head>
        <Admin></Admin>
      </>
    );
}
