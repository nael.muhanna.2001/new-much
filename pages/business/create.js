import { useEffect } from "react";

export default function CreateBusiness() {
  const getCompanyInfobyWeb = async (weburl) => {
    const url = 'https://linkedin-company-data.p.rapidapi.com/linkedInCompanyDataByDomainJson';
const options = {
  method: 'POST',
  headers: {
    'content-type': 'application/json',
    'X-RapidAPI-Key': 'f4a104453dmshb198e8a7d935a7dp12f0ddjsnde803193778d',
    'X-RapidAPI-Host': 'linkedin-company-data.p.rapidapi.com'
  },
  body:JSON.stringify({
    domains: [
        weburl,
    ]
  })
};

try {
	const response = await fetch(url, options);
	const result = await response.text();
	console.log(result);
} catch (error) {
	console.error(error);
}
  };
  useEffect(() => {
    getCompanyInfobyWeb('foodics.com');
  }, []);
}
