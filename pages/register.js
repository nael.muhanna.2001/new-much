import Image from "next/image";
import Link from "next/link";
import { translation } from "../lib/translations";
import { useRouter } from "next/router";
import { useRef, useState } from "react";
import { useSession,getCsrfToken,getProviders,signIn } from "next-auth/react";
import Head from "next/head";
import SEO from "../webComponents/Seo";
async function createUser(name, email, password) {
  const response = await fetch("/api/auth/signup", {
    method: "POST",
    body: JSON.stringify({ name, email, password }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return response;
}
export default function RegisterPage({ csrfToken, providers,query }) {
  const { data: session, status } = useSession();
  const nameInputRef = useRef();
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  const [error, setError] = useState();
  const { locale } = useRouter();
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  async function submitHandler(event) {
    event.preventDefault();
    setError();
    const inputName = nameInputRef.current.value;
    const inputEmail = emailInputRef.current.value;
    const inputPassword = passwordInputRef.current.value;
    try {
      const result = await createUser(inputName, inputEmail, inputPassword);
      const message = await result.json();
      if (result.status == 422) {
        setError(message["message"]);
      }
      else
      {
        if(query.callback)
      {
        window.location.href=query.callback
      }
      else
      {
       signIn("credentials",{email:inputEmail,password:inputPassword})
      }
      }
      
    } catch (error) {}
  }
  if (status == "loading") {
    return (
      <>
      
    <SEO pageTitle={"much... | Register"} />
      
        <div className="pt-72 flex justify-center">
          <div className="mx-auto animate-pulse">
            <Image
              src="/images/logo.svg"
              alt="Logo"
              width={"260"}
              height={"160"}
              className="cursor-pointer mx-auto"
            />
          </div>
        </div>
      </>
    );
  }
  if (session) window.location.href = "/dashboard";
  if (!session)
    return (
      <>
      <div className="bg-white min-h-screen">
      <Head>
        <title>much.. | Register</title>
      </Head>
        <header id="header-wrap" className="relative">
          <div className="navigation  top-0 left-0 w-full z-50 duration-300">
            <div className="container text-center">
              <nav
                className={
                  "navbar py-4 navbar-expand-lg flex justify-center md:justify-between items-center relative duration-300 "
                }
              >
                <Link className="navbar-brand h-96 " href="/">
                  <Image
                    src="/images/logo.svg"
                    alt="Logo"
                    width={"160"}
                    height={"60"}
                    className="cursor-pointer"
                  />
                </Link>
              </nav>
            </div>
          </div>
        </header>
        <main>
          
            <div className="register-page text-center container ">
              <div className="lg:grid lg:grid-cols-2">
                <div className="lg:my-12 lg:px-28  lg:bg-gray-50 lg:shadow-md lg:rounded-xl md:mx-28 lg:mx-8 ">
                  <h1 className="text-2xl pt-5 font-bold">
                    {_("Create Your Business Card Now!")}
                  </h1>
                  <div className="justify-center">
                  <form onSubmit={submitHandler}>
                    <div className="mt-4">
                      <input
                        ref={nameInputRef}
                        className="border border-gray-500 text-gray-600 h-12 w-full rounded-md p-1 placeholder-gray-500"
                        placeholder={_("Your Full Name")}
                      ></input>
                    </div>
                    <div className="mt-4">
                      <input
                        ref={emailInputRef}
                        className="border border-gray-500 text-gray-600 h-12 w-full rounded-md p-1 placeholder-gray-500"
                        placeholder={_("Your Email")}
                      ></input>
                    </div>
                    <div className="mt-4">
                      <input
                        ref={passwordInputRef}
                        className="border border-gray-500 text-gray-600 h-12 w-full rounded-md p-1 placeholder-gray-500"
                        placeholder={_("Password")}
                        type="password"
                      ></input>
                    </div>
                    <div className="mt-4">
                      <p className="text-red-600">{error}</p>
                      <button
                        type="submit"
                        className="bg-emerald-300 text-white w-full h-12 rounded-md hover:bg-emerald-500 duration-200"
                      >
                        {_("Create")}
                      </button>
                      <p className="ltr:text-left rtl:text-right mt-4 text-xs text-gray-600">
                        {_("By clicking Create, you agree to out")}{" "}
                        <span className="text-blue-600">
                          <Link href={"/terms"}>{_("Terms of Service")}</Link>
                        </span>{" "}
                        {_("and that you have read out")}{" "}
                        <span className="text-blue-600">
                          <Link href={"privacy-policy"}>
                            {_("Privacy Policy")}
                          </Link>
                        </span>
                      </p>
                    </div>
                    </form>
                    <div className="mt-4 justify-start grid grid-cols-12">
                      <div className="col-span-5 pt-3">
                        <hr className="border-gray-500"></hr>
                      </div>
                      <div className="col-span-2 text-gray-500">{_("OR")}</div>
                      <div className="col-span-5 pt-3">
                        <hr className="border-gray-500"></hr>
                      </div>
                    </div>
                    <div className="mt-4  flex justify-center">
                    {Object.values(providers).map((provider) =>
                      provider.id == "facebook" ? (
                        <button
                          onClick={() => signIn(provider.id)}
                          key={provider.name}
                          className="facebook w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i
                            className={
                              "fab fa-facebook-f text-blue-600  text-lg"
                            }
                          ></i>
                        </button>
                      ) : provider.id == "google" ? (
                        <button
                          key={provider.name}
                          onClick={() => signIn(provider.id)}
                          className="facebook w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-google text-red-500  text-lg"></i>
                        </button>
                      ) : provider.id == "linkedin" ? (
                        <button
                          key={provider.name}
                          onClick={() => signIn(provider.id)}
                          className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-linkedin-in text-blue-800  text-lg"></i>
                        </button>
                      ) : provider.id == "apple" ? (
                        <button
                          key={provider.name}
                          onClick={() => signIn(provider.id)}
                          className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-apple text-gray-800  text-lg"></i>
                        </button>
                      ) : provider.id == "twitter" ? (
                        <button
                          key={provider.name}
                          onClick={() => signIn(provider.id)}
                          className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                        >
                          <i className="fab fa-twitter text-cyan-400  text-lg"></i>
                        </button>
                      ) : (
                        ""
                      )
                    )}
                  </div>
                    <div className="my-4">
                      <p className="text-sm text-gray-600">
                        {_("Already have an account?")}{" "}
                        <span className="text-blue-600 font-bold">
                          <Link href={"/signin/"+(query.callback?"?callback="+query.callback:"")}>{_("Sign-in")}</Link>
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
        </main>
        </div>
      </>
    );
}
export async function getServerSideProps(context) {
    const providers = await getProviders();
  
    return {
      props: {
        query: context.query,
        csrfToken: await getCsrfToken(context),
        providers,
      },
    };
  }