import Head from "next/head";
import { Dropdown, IconButton, Steps } from "rsuite";
import { useRouter } from "next/router";
import { translation } from "../lib/translations";
import QRCode from "easyqrcodejs";
import AsyncCreatableSelect from "react-select/async-creatable";
import { components } from "react-select";
import "rsuite/dist/rsuite.min.css";
import { useEffect, useRef, useState } from "react";
import Script from "next/script";
import Link from "next/link";
import cookie from "js-cookie";
import {
  useSession,
  getCsrfToken,
  getProviders,
  signIn,
} from "next-auth/react";
import Select from "react-select";
import { countries } from "../lib/countries";
const options = [];
countries.map((country) => {
  options.push({
    value: country.dial,
    label: country.name + " (" + country.dial + ")",
  });
});
export default function CreateYourCard({ csrfToken, providers, query }) {
  const { data: session, status } = useSession();
  const { Option } = components;
  const IconOption = (props) => (
    <Option {...props}>
      {props.data.pic && (
        <img
          src={props.data.pic}
          style={{
            width: "28px",
            height: "28px",
            float: "left",
            margin: "-2px 4px",
            borderRadius: "100px",
          }}
        />
      )}
      {!props.data.pic && <i className="fa fa-building text-gray-500 p-2"></i>}
      <span> {props.data.label}</span>
    </Option>
  );
  //desgin variables//
  const [color, setColor] = useState({
    primary: "#333333",
    secondary: "#FFFFFF",
    name: "default",
  });
  const [nameSuggestion, setNameSuggestion] = useState([]);
  const [avatar, setAvatar] = useState("9999px");
  const [contactShape, setContactShape] = useState("vertical");
  const [buttonShape, setButtonShape] = useState("9999px");
  const [step, setStep] = useState(0);
  const [card, setCard] = useState({});
  const [createdCard, setCreatedCard] = useState({});
  const [error, setError] = useState();
  const [name, setName] = useState();
  const [showQR, setShowQR] = useState(false);
  const { locale } = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [oldName, setOldName] = useState();
  const [cardType, setCardType] = useState();
  const [imagePublicId, setImagePublicId] = useState();
  const [coverPublicId, setCoverPublicId] = useState();
  const [cover, setCover] = useState("https://picsum.photos/1125/1152");
  const [imageInfo, setImageInfo] = useState();
  const [coverInfo, setCoverInfo] = useState();
  const [crop, setCrop] = useState("scale");
  const [coverCrop, setCoverCrop] = useState();
  const [contactViewAccess, setContactViewAccess] = useState("public");
  const [pic, setPic] = useState();
  const [jobTitle, setJobTitle] = useState("");
  const [department, setDepartment] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [description, setDescription] = useState();
  const [mobilePrefix, setMobilePrefix] = useState();
  const [mobile, setMobile] = useState();
  const [telephonePrefix, setTelephonePrefix] = useState();
  const [telephone, setTelephone] = useState();
  const [whatsappPrefix, setWhatsappPrefix] = useState();
  const [whatsapp, setWhatsapp] = useState();
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState();
  const [companiesList, setCompaniesList] = useState();
  const [jobTitleList, setJobTitleList] = useState([]);
  const [departmentList, setDepartmentList] = useState([]);
  const [bgUrl, setBgUrl] = useState("https://picsum.photos/1355/321");
  const qrCodeRef = useRef(null);
  const [slug, setSlug] = useState();
  const [QRcolorDark, setQRcolorDark] = useState("#000000");
  const [QRcolorLight, setQrColorLight] = useState("#ffffff");
  const [qrLogo, setQrLogo] = useState();
  const [qrlogoWidth, setQrlogoWidth] = useState();
  const [qrlogoBackgroundTransparent, setQrLogoBackgroundTransparent] =
    useState(false);
  const [qrbackgroundImage, setQrbackgroundImage] = useState();
  const [qrbackgroundImageAlpha, setQrbackgroundImageAlpha] = useState(1);
  const nameInputRef = useRef();
  let emailInputRef = useRef();
  const [registerEmail, setRegisterEmail] = useState();
  const passwordInputRef = useRef();
  const [isRegistering, setIsRegistering] = useState(false);
  //==== Random slug =====//
  function makeid(length) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random(new Date().getTime()) * charactersLength)
      );
    }
    return result;
  }
  //====== get Location =======
  async function getLocation() {
    const location = await fetch(
      "https://api.ipgeolocation.io/ipgeo?apiKey=8981df21aa7d4dfe9f299632ae1472d8",
      {
        method: "GET",
      }
    );
    let data = await location.json();
    countries.map((country) => {
      if (country.ISO2 == data.country_code2) {
        setMobilePrefix({
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        });
        setTelephonePrefix({
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        });
        setWhatsappPrefix({
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        });
        let tempCard = card;
        tempCard.mobilePrefix = {
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        };
        tempCard.telephonePrefix = {
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        };
        tempCard.whatsappPrefix = {
          value: country.dial,
          label: country.name + " (" + country.dial + ")",
        };
      }
    });
    return location;
  }
  let user = null;
  if (cookie.get("user") && !session.user) {
    user = JSON.parsecookie.get("user");
  } else {
    user = session && session.user ? JSON.stringify(session.user) : null;
  }
  useEffect(() => {
    getLocation();
    
    if (
      typeof window !== "undefined" &&
      window.localStorage.getItem("card") != null
    ) {
      let card = JSON.parse(window.localStorage.getItem("card"));
      setCard(card);
      setName(card.name);
      setJobTitle(card.jobTitle);
      setCompanyName(card.company);
      setEmail(card.email);
      setMobile(card.mobile);
      setMobilePrefix(card.mobilePrefix);
      setColor(card.color);
      setAvatar(card.avatar);
      setPic(card.pic);
      setCover(card.cover);
      if (card.name && card.email && session) {
        card.slug = card.name.replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
        .toLowerCase();
        card.cardType = "business_card";
        card.contactShape = "vertical";
        fetch("/api/cards/create", {
          method: "POST",
          body: JSON.stringify(card),
        })
          .then((res) => res.json())
          .then((data) => {
            setCreatedCard(data);
            showQrFn(data._id, null);
            showQrFn(data._id, null);

            setTimeout(() => {
              showQrFn(data._id, null);
            }, 300);
            window.localStorage.removeItem("card");
            setStep(3);
          });
      }
    }
    
    if (
      typeof window !== "undefined" &&
      window.localStorage.getItem("card") == null &&
      session
    ) {
      window.location.href = "/dashboard/cards/";
    }
  }, [session]);
  // ===== Create User =========//
  async function createUser(name, email, password) {
    const response = await fetch("/api/auth/signup", {
      method: "POST",
      body: JSON.stringify({ name, email, password }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response;
  }
  //====== Registeration ========//
  async function submitHandler(event) {
    event.preventDefault();
    setError();
    const inputName = name;
    const inputEmail = emailInputRef.current.value;
    const inputPassword = passwordInputRef.current.value;
    try {
      setIsRegistering(true);
      const result = await createUser(inputName, inputEmail, inputPassword);
      const message = await result.json();

      if (result.status == 422) {
        setError(message["message"]);
      } else {
        if (query.callback) {
          signIn("credentials", {
            email: inputEmail,
            password: inputPassword,
          }).then(() => {
            setIsRegistering(false);
            window.location.href = query.callback;
          });
        } else {
          signIn("credentials", {
            email: inputEmail,
            password: inputPassword,
          }).then((data) => {
            setIsRegistering(false);
            
          });
        }
      }
    } catch (error) {}
  }

  // ===== Downlaod QR Code =====//
  const downloadQRCode = () => {
    // Generate download with use canvas and stream
    const canvas = document.getElementById("qr-gen").firstChild;
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = `${createdCard.name}.png`;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };
  //====== Show QR function =====//
  async function showQrFn(slug, option) {
    setShowQR(true);
    let opt = {
      text: "https://" + window.location.host + "/" + slug,
      width: 160,
      height: 160,
      logoMaxWidth: 40,
      logoMaxHeight: 40,
      drawer: "canvas",
      autoColor: true,
      colorDark: QRcolorDark ? QRcolorDark : "#000000",
      colorLight: QRcolorLight ? QRcolorLight : "#ffffff",
      correctLevel: QRCode.CorrectLevel.H,
      dotScaleTiming: 0.9,
      dotScale: 1,
      dotScaleTiming_V: 0.9,
      dotScaleTiming_h: 0.9,
      dotScaleA: 0.9,
      dotScaleAO: 0.9,
      dotScaleAI: 0.5,
      quietZone: 3,
      logo: qrLogo,
      logoWidth: qrlogoWidth,
      logoHeight: qrlogoWidth,
      logoBackgroundTransparent: qrlogoBackgroundTransparent,
      backgroundImage: qrbackgroundImage,
      backgroundImageAlpha: qrbackgroundImageAlpha,
    };
    if (option?.QRcolorDark) {
      opt.colorDark = option.QRcolorDark;
    }
    if (option?.QRcolorLight) {
      opt.colorLight = option.QRcolorLight;
    }
    new QRCode(qrCodeRef.current, opt);
    let rectArr = [];
    let rects = document.getElementsByTagName("rect");
    setTimeout(() => {
      rectArr = [...rects];
      rectArr.map((e) => {
        e.setAttribute("rx", "10");
        e.setAttribute("ry", "10");
      });
    }, 100);
  }
  //====== Translation =========
  function _(text) {
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  //====== Handle Submit ==========//
  function handleSubmit() {
    if (step === 0) {
      if (name && card.name && email && card.email) {
        setStep(1);
      } else {
        setError(_("Insert Your Name and email Please"));
      }
    }
    if (step === 1) {
      setStep(2);
      setRegisterEmail(email);
    }
  }
  //===== Suggest Info ============//
  async function suggestInfo() {
    if (name && name.split(" ").length > 1) {
      let firstName = name.split(" ")[0];
      let lastName = name.split(" ")[1];
      fetch("/api/get-info/", {
        method: "POST",
        body: JSON.stringify({ firstName: firstName, lastName: lastName }),
      })
        .then((res) => res.json())
        .then((data) => {
          setNameSuggestion([...data]);
        });
    }
  }

  function selectSuggestion(e) {
    setName(e.firstName + " " + e.lastName);
    setJobTitle({ value: e.jobTitle, label: e.jobTitle });
    if (e.photoUrl) {
      setPic(e.photoUrl);
    }
    else
    {
      setPic();
    }
    
        setCompanyName({
          value: null,
          label: e.organization,
          pic: null,
          slug: null,
        });
        let tempCard = card;
        tempCard.company = {
          value: null,
          label: e.organization,
          pic: null,
          slug: null,
        };
        tempCard.name = e.firstName + " " + e.lastName;
        tempCard.jobTitle = { value: e.jobTitle, label: e.jobTitle };
        if (e.photoUrl) {
          tempCard.pic = e.photoUrl;
        }
        else
        {
          tempCard.pic=null;
        }
        setCard(tempCard);
        window.localStorage.setItem("card", JSON.stringify(tempCard));
        setNameSuggestion([]);
      
  }
  //==== JobTitle Auto Complete ===//
  function handleJobTitleChange(val) {
    setJobTitle(val);
    let tempCard = card;
    tempCard.jobTitle = val;
    setCard(tempCard);
    window.localStorage.setItem("card", JSON.stringify(tempCard));
  }

  async function promiseJobTitleOptions(val) {
    return new Promise((resolve) => {
      fetch("/api/cards/get-job-title", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          const job_title = [];
          data.map((e) => {
            job_title.push(e.jobTitle);
          });
          setJobTitleList(job_title);
          resolve(job_title);
        });
    });
  }
  async function onCreateJobTitleOption(val) {
    let newOption = { value: val, label: val };
    setJobTitle(newOption);
    let tempCard = card;
    tempCard.jobTitle = newOption;
    setCard(tempCard);
    window.localStorage.setItem("card", JSON.stringify(tempCard));
  }
  //===== Company Name Auto Complete =====

  function handleCompanyChange(val) {
    setCompanyName(val);
    if (val && val.source == "seamless" && val._id == null) {
      fetch("/api/cards/create-company/", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          setCompanyName({
            value: data._id,
            label: data.name,
            pic: data.pic,
            slug: data.slug,
          });
          let tempCard = card;
          tempCard.company = {
            value: data._id,
            label: data.name,
            pic: data.pic,
            slug: data.slug,
          };
          setCard(tempCard);
          window.localStorage.setItem("card", JSON.stringify(tempCard));
        });
    } else {
      let tempCard = card;
      tempCard.company = val;
      setCard(tempCard);
      window.localStorage.setItem("card", JSON.stringify(tempCard));
    }

    if (!val) {
      let tempCard = card;
      tempCard.company = val;
      setCard(tempCard);
      window.localStorage.setItem("card", JSON.stringify(tempCard));
    }
  }

  async function promiseCompanyOptions(val) {
    return new Promise((resolve) => {
      fetch("/api/cards/get-companies/", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          const companiesList = [];
          data.map((e) => {
            companiesList.push({
              value: e._id,
              label: e.name,
              pic: e.pic,
              slug: e.slug,
              source: e.source,
              description: e.description,
            });
          });
          setCompaniesList(companiesList);
          resolve(companiesList);
        });
    });
  }

  async function onCreateCompanyOption(val) {
    let newOption = {
      value: val
        .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
        .toLowerCase(),
      label: val,
      slug: val
        .replace(/[&\/\\#,+()$~%.'":*?^@_\-\!\s<>{}]/g, ".")
        .toLowerCase(),
    };
    setCompanyName(newOption);
    let tempCard = card;
    tempCard.company = newOption;
    setCard(tempCard);
    window.localStorage.setItem("card", JSON.stringify(tempCard));
    if (val) {
      fetch("/api/cards/create-company", {
        method: "POST",
        body: JSON.stringify({ inputValue: val }),
      })
        .then((res) => res.json())
        .then((data) => {
          newOption = { value: data._id, label: data.name, pic: data.pic };
          setCompaniesList([...companiesList, newOption]);

          setCompanyName(newOption);
        });
    }
  }
  //====== Cover Image Upload =====//
  const openCoverWidget = () => {
    // create the widget
    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 1,
        cropping: true,
        croppingAspectRatio: 0.976,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 1125,
        maxImageHeight: 1152,
        theme: "white",
        form: "coverPic",
        showPoweredBy: false,
        resourceType: "image",
      },

      (error, result) => {
        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let bg = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            bg +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          bg += "/" + result.info.public_id;

          setCover(bg);
          let tempCard = card;
          tempCard.cover = bg;
          tempCard.coverInfo = coverPublicId;
          setCard(tempCard);
          window.localStorage.setItem("card", JSON.stringify(tempCard));
          setCoverPublicId(result.info.public_id);
          setCoverInfo(result.info);
          setCoverCrop(result.info.coordinates);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };

  //======== Profile Pic Upload ============//
  const openWidget = () => {
    // create the widget
    const widget = window.cloudinary.createUploadWidget(
      {
        cloudName: "pweb",
        uploadPreset: "ml_default",
        maxFiles: 1,
        cropping: true,
        croppingAspectRatio: 1,
        croppingCoordinatesMode: "face",
        showSkipCropButton: false,
        maxImageWidth: 300,
        maxImageHeight: 300,
        theme: "white",
        form: "profilePic",
        showPoweredBy: false,
        resourceType: "image",
      },

      (error, result) => {
        if (
          result.event === "success" &&
          result.info.resource_type === "image"
        ) {
          let pic = "https://res.cloudinary.com/pweb/image/upload/";
          if (result.info.coordinates) {
            pic +=
              "c_crop,h_" +
              result.info.coordinates.faces[0][3] +
              ",w_" +
              result.info.coordinates.faces[0][2] +
              ",x_" +
              result.info.coordinates.faces[0][0] +
              ",y_" +
              result.info.coordinates.faces[0][1];
          }
          pic += "/" + result.info.public_id;
          setPic(pic);
          let tempCard = card;
          tempCard.pic = pic;
          setCard(tempCard);
          window.localStorage.setItem("card", JSON.stringify(tempCard));
          setImagePublicId(result.info.public_id);
          setImageInfo(result.info);
          setCrop(result.info.coordinates);
        }
      }
    );
    widget.open(); // open up the widget after creation
  };
  if (status == "loading") {
    return _("Loading...");
  }
  if (!session || (session && card))
    return (
      <>
        <Head>
          <title>{_("Create Your Card")}</title>
          <meta charset="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=.85, shrink-to-fit=no"
          />
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="apple-touch-fullscreen" content="yes" />
          <meta name="HandheldFriendly" content="true" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="viewport" content="width=device-width, initial-scale=0.8" />
        </Head>
        <Script
          src="https://widget.Cloudinary.com/v2.0/global/all.js"
          type="text/javascript"
        ></Script>
        <div className="bg-white min-h-screen">
          <div className="flex">
            <div className="w-12/12 lg:w-8/12 pt-10 px-4">
              <Steps current={step} style={{ zoom: "0.66" }}>
                <Steps.Item
                  onClick={() => {
                    setStep(0);
                  }}
                  title={_("Personal details")}
                />
                <Steps.Item title={_("Customization")} />
                <Steps.Item title={_("Account")} />
                <Steps.Item title={_("Dashboard")} />
              </Steps>
              <div className="mt-6 lg:max-w-3xl lg:mx-auto">
                {step == 0 && (
                  <div className="container step1">
                    <h1 className="text-4xl font-bold mb-6">
                      {_("Create Your First Card")}
                    </h1>
                    <p className="text-xl">
                      {_(
                        "You can add your all basic information with many other information and track your card interactivities through your dashboard!"
                      )}
                    </p>
                    <div className="registeration-form my-6">
                      <form>
                        <div className="name-field px-1">
                          <label className="uppercase text-gray-600 font-semibold p-2">
                            {_("Full Name")}
                          </label>
                          <input
                            value={name}
                            placeholder={_("Your Full Name")}
                            type="text"
                            className="w-full border border-gray-300 text-xl rounded p-2 font-bold"
                            onChange={(e) => setName(e.target.value)}
                            onBlur={(e) => {
                              setError();
                              suggestInfo();
                              setName(e.target.value);
                              let tempCard = card;
                              tempCard.name = e.target.value;
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                          />
                          {nameSuggestion.length > 0 && (
                            <div>
                              <h3 className="text-xl font-bold p-3">
                                {nameSuggestion.length > 1 && (
                                  <label>
                                    {_(
                                      "Which of the following information matches your information?"
                                    )}
                                  </label>
                                )}
                                {nameSuggestion.length == 1 && (
                                  <label>{_("Are you?")}</label>
                                )}
                              </h3>
                              {nameSuggestion.map((e, k) => {
                                return (
                                  <div
                                    key={k}
                                    className="w-full flex p-3 rounded-xl border my-1"
                                  >
                                    <div className="avatar w-1/12 m-2">
                                      {e.photoUrl && (
                                        <img
                                          src={e.photoUrl}
                                          className="w-12 h-12 rounded-full"
                                        />
                                      )}
                                      {!e.photoUrl && (
                                        <img
                                          src={
                                            "https://ui-avatars.com/api/?name=" +
                                            e.firstName +
                                            "+" +
                                            e.lastName
                                          }
                                          className="w-12 h-12 rounded-full"
                                        />
                                      )}
                                    </div>
                                    <div className="w-full">
                                      <h2 className="text-xl text-gray-800">
                                        {e.firstName} {e.lastName}
                                      </h2>
                                      <small>
                                        {e.jobTitle} @ {e.organization}
                                      </small>
                                    </div>
                                    <div className="w-2/12">
                                      <button
                                        onClick={() => {
                                          selectSuggestion(e);
                                        }}
                                        type="button"
                                        className="text-emerald-300 p-3 font-bold text-xl"
                                      >
                                        {_("Yes")}
                                      </button>
                                    </div>
                                    {nameSuggestion.length == 1 && (
                                      <div className="w-2/12">
                                        <button
                                          type="button"
                                          onClick={() => {
                                            setNameSuggestion([]);
                                          }}
                                          className="text-red-600 p-3 font-bold text-xl"
                                        >
                                          {_("No")}
                                        </button>
                                      </div>
                                    )}
                                  </div>
                                );
                              })}
                              {nameSuggestion.length > 1 && (
                                <div className="w-full">
                                  {" "}
                                  <button
                                    onClick={() => {
                                      setNameSuggestion([]);
                                    }}
                                    className="p-3 border text-xl font-bold rounded-xl w-full border-gray-500"
                                  >
                                    {_("There is no correct option!")}
                                  </button>
                                </div>
                              )}
                            </div>
                          )}
                        </div>
                        <div className="flex flex-wrap mt-4">
                          <div className="w-full md:w-6/12 px-1">
                            <label className="uppercase text-gray-600 font-semibold p-2">
                              {_("Job Title")}
                            </label>
                            <AsyncCreatableSelect
                              id="qwertyuio"
                              instanceId="qwertyuio"
                              isClearable={true}
                              openMenuOnClick={false}
                              components={{
                                IndicatorSeparator: () => null,
                                DropdownIndicator: () => null,
                              }}
                              onChange={handleJobTitleChange}
                              value={jobTitle}
                              options={jobTitleList}
                              onCreateOption={onCreateJobTitleOption}
                              cacheOptions
                              defaultOptions
                              loadOptions={promiseJobTitleOptions}
                              placeholder={_("Your Job Title")}
                              className="h-10  placeholder-blueGray-300 text-blueGray-600 bg-white rounded-xl border-gray-300 text-sm   w-full ease-linear transition-all duration-150"
                            />
                          </div>
                          <div className="w-full md:w-6/12 px-1">
                            <label className="uppercase text-gray-600 font-semibold p-2">
                              {_("Company Name")}
                            </label>
                            <AsyncCreatableSelect
                              id="asdfghjk"
                              instanceId="asdfghjk"
                              isClearable={true}
                              openMenuOnClick={false}
                              components={{
                                IndicatorSeparator: () => null,
                                DropdownIndicator: () => null,
                                Option: IconOption,
                              }}
                              onChange={handleCompanyChange}
                              value={companyName}
                              options={companiesList}
                              onCreateOption={onCreateCompanyOption}
                              cacheOptions
                              defaultOptions
                              loadOptions={promiseCompanyOptions}
                              placeholder={_("Your Company Name")}
                              className="col-span-10 md:col-span-11 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm   w-full ease-linear transition-all duration-150"
                            />
                          </div>
                        </div>
                        <div className="flex flex-wrap">
                          <div className="email-field mt-4 px-1 w-full lg:w-6/12">
                            <label className="uppercase text-gray-600 font-semibold p-2">
                              {_("Email")}
                            </label>
                            <input
                              value={email}
                              placeholder={_("Your Email")}
                              type="email"
                              className="w-full border border-gray-300 text-md rounded p-2 h-9.5 mt-1"
                              onChange={(e) => setEmail(e.target.value)}
                              onBlur={(e) => {
                                if (
                                  e.target.value
                                    .toLowerCase()
                                    .match(
                                      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                    )
                                ) {
                                  e.target.className =
                                    "w-full border border-gray-300 text-md rounded p-2 ";
                                  setEmail(e.target.value);
                                  let tempCard = card;
                                  tempCard.email = e.target.value;
                                  setCard(tempCard);
                                  window.localStorage.setItem(
                                    "card",
                                    JSON.stringify(tempCard)
                                  );
                                  
                                } else
                                  e.target.className =
                                    "w-full border  text-md rounded p-2 text-red-600 border-red-600";
                              }}
                            />
                          </div>
                          <div className="email-field mt-4 px-1 w-full lg:w-6/12">
                            <label className="uppercase text-gray-600 font-semibold p-2">
                              {_("Mobile")}
                            </label>
                            <div className="flex">
                              <Select
                                value={mobilePrefix}
                                onChange={(e) => {
                                  setMobilePrefix(e);
                                  let tempCard = card;
                                  tempCard.mobilePrefix = e;
                                  setCard(tempCard);
                                }}
                                className="text-xs w-10/12  pt-1 ltr:mr-1 rtl:ml-1  rounded-sm outline-0 focus:outline-0"
                                options={options}
                              ></Select>
                              <input
                                value={mobile}
                                placeholder={_("Your Mobile Number")}
                                type="number"
                                className="w-full border border-gray-300 h-9.5 mt-1 text-md rounded p-2"
                                onChange={(e) => setMobile(e.target.value)}
                                onBlur={(e) => {
                                  setMobile(e.target.value);
                                  let tempCard = card;
                                  tempCard.mobile = e.target.value;
                                  setCard(tempCard);
                                  window.localStorage.setItem(
                                    "card",
                                    JSON.stringify(tempCard)
                                  );
                                  
                                }}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="flex flex-wrap mt-6">
                          {error && (
                            <div className="text-red-600 w-full">{error}</div>
                          )}
                          <div className="px-1 text-center w-6/12 pt-2">
                            <Link href="/">
                              <div className="text-emerald-300 hover:no-underline text-xl  rounded w-full">
                                {_("Cancel")}
                              </div>
                            </Link>
                          </div>
                          <div className="px-1 text-center w-6/12">
                            <button
                              onClick={handleSubmit}
                              type="button"
                              className="bg-emerald-300 hover:bg-emerald-200 duration-300 border hover:border-emerald-300 text-white text-xl font-semibold p-2 rounded w-full"
                            >
                              {_("Next")}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                )}
                {step == 1 && (
                  <div className="container step2">
                    <h1 className="text-4xl font-bold mb-6">
                      {_("Time to customize your card!")}
                    </h1>
                    <p className="text-xl">
                      {_(
                        "Great Job! now choose colors and add other information to your card"
                      )}
                    </p>
                    <div className="main-content mt-10">
                      <div className="grid grid-cols-12">
                        <div className="col-span-12 md:col-span-2">
                          <label className="block uppercase text-blueGray-600 text-xs font-bold mt-3 min-w-32">
                            {_("Theme Color")}
                          </label>
                        </div>
                        <div className="col-span-12 md:col-span-10 grid grid-cols-4 md:grid-cols-12">
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgba(0,0,0,255)",
                                secondary: "rgba(255,255,255,255)",
                                name: "default",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgba(0,0,0,255)",
                                secondary: "rgba(255,255,255,255)",
                                name: "default",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "default"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-black w-full h-full"></div>
                            <div className="bg-white w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(75 85 99,255)",
                                secondary: "rgba(228, 228, 231,255)",
                                name: "gray",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(75 85 99,255)",
                                secondary: "rgba(228, 228, 231,255)",
                                name: "gray",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "gray"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-gray-600 w-full h-full"></div>
                            <div className="bg-gray-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgba(220, 38, 38,255)",
                                secondary: "rgba(249, 250, 250,255)",
                                name: "red",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgba(220, 38, 38,255)",
                                secondary: "rgba(249, 250, 250,255)",
                                name: "red",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "red"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-red-600 w-full h-full"></div>
                            <div className="bg-red-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(234,88,12)",
                                secondary: "rgba(255, 242, 242,255)",
                                name: "orange",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(234,88,12)",
                                secondary: "rgba(255, 242, 242,255)",
                                name: "orange",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "orange"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-orange-600 w-full h-full"></div>
                            <div className="bg-orange-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(202,138,4)",
                                secondary: "rgb(255,247,237)",
                                name: "yellow",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(202,138,4)",
                                secondary: "rgb(255,247,237)",
                                name: "yellow",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "yellow"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-yellow-600 w-full h-full"></div>
                            <div className="bg-yellow-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(22,163,74)",
                                secondary: "rgb(240,253,244)",
                                name: "green",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(22,163,74)",
                                secondary: "rgb(240,253,244)",
                                name: "green",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "green"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-emerald-300 w-full h-full"></div>
                            <div className="bg-emerald-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(13,148,136)",
                                secondary: "rgba(240, 253, 244,255)",
                                name: "teal",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(13,148,136)",
                                secondary: "rgba(240, 253, 244,255)",
                                name: "teal",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "teal"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-teal-600 w-full h-full"></div>
                            <div className="bg-teal-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(8 145 178)",
                                secondary: "rgb(240 253 250)",
                                name: "cyan",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(8 145 178)",
                                secondary: "rgb(240 253 250)",
                                name: "cyan",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "cyan"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-cyan-600 w-full h-full"></div>
                            <div className="bg-cyan-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(37,99,235)",
                                secondary: "rgb(239,246,255)",
                                name: "blue",
                              });
                            }}
                            className={
                              color && color.name == "blue"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-blue-600 w-full h-full"></div>
                            <div className="bg-blue-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(79,70,229)",
                                secondary: "rgb(238,242,255)",
                                name: "indigo",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(79,70,229)",
                                secondary: "rgb(238,242,255)",
                                name: "indigo",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "indigo"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-indigo-600 w-full h-full"></div>
                            <div className="bg-indigo-50 w-full h-full"></div>
                          </button>

                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(147,51,234)",
                                secondary: "rgba(250, 245, 255,255)",
                                name: "purple",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(147,51,234)",
                                secondary: "rgba(250, 245, 255,255)",
                                name: "purple",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "purple"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-purple-600 w-full h-full"></div>
                            <div className="bg-purple-50 w-full h-full"></div>
                          </button>
                          <button
                            onClick={() => {
                              setColor({
                                primary: "rgb(219,39,119)",
                                secondary: "rgb(253,242,248)",
                                name: "pink",
                              });
                              let tempCard = card;
                              tempCard.color = {
                                primary: "rgb(219,39,119)",
                                secondary: "rgb(253,242,248)",
                                name: "pink",
                              };
                              setCard(tempCard);
                              window.localStorage.setItem(
                                "card",
                                JSON.stringify(tempCard)
                              );
                            }}
                            className={
                              color && color.name == "pink"
                                ? "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-4 border-black"
                                : "w-10 h-10 m-4 rounded-full grid grid-cols-2 overflow-hidden border-2"
                            }
                          >
                            <div className="bg-pink-600 w-full h-full"></div>
                            <div className="bg-pink-50 w-full h-full"></div>
                          </button>
                        </div>
                      </div>
                      {pic && (
                        <div className="mt-10 grid md:grid-cols-12">
                          <div className="col-span-2">
                            <label className="block uppercase text-blueGray-600 text-xs font-bold mt-3 min-w-32">
                              {_("Picture Shape")}
                            </label>
                          </div>
                          <div className="md:col-span-10 grid grid-cols-2 md:grid-cols-4">
                            <button
                              onClick={() => {
                                setAvatar("9999px");
                                let tempCard = card;
                                tempCard.avatar = "9999px";
                                setCard(tempCard);
                                window.localStorage.setItem(
                                  "card",
                                  JSON.stringify(tempCard)
                                );
                              }}
                              className={
                                avatar == "9999px"
                                  ? "w-32 h-32 border-2 duration-200 border-black rounded-full overflow-hidden"
                                  : "w-32 h-32 border-2 rounded-full overflow-hidden"
                              }
                            >
                              <img src={pic} />
                            </button>
                            <button
                              onClick={() => {
                                setAvatar("1rem");
                                let tempCard = card;
                                tempCard.avatar = "1rem";
                                setCard(tempCard);
                                window.localStorage.setItem(
                                  "card",
                                  JSON.stringify(tempCard)
                                );
                              }}
                              className={
                                avatar == "1rem"
                                  ? "w-32 h-32 border-2 duration-200 border-black  rounded-2xl overflow-hidden"
                                  : "w-32 h-32 border-2 rounded-2xl overflow-hidden"
                              }
                            >
                              <img src={pic} />
                            </button>
                            <button
                              onClick={() => {
                                setAvatar("0.375rem");
                                let tempCard = card;
                                tempCard.avatar = "0.375rem";
                                setCard(tempCard);
                                window.localStorage.setItem(
                                  "card",
                                  JSON.stringify(tempCard)
                                );
                              }}
                              className={
                                avatar == "0.375rem"
                                  ? "w-32 h-32 border-2 duration-200 border-black rounded-md overflow-hidden"
                                  : "w-32 h-32 border-2 rounded-md overflow-hidden"
                              }
                            >
                              <img src={pic} />
                            </button>
                          </div>
                        </div>
                      )}
                    </div>
                    <div className="flex fixed w-full lg:w-8/12 h-20 shadow-[0px_-5px_5px_0px_rgba(0,0,0,.3)] bg-white bottom-0 left-0 pt-4 px-6">
                      <div className="px-1 text-center w-6/12 pt-2">
                        <button
                          onClick={() => {
                            setStep(0);
                          }}
                          className="text-emerald-300 hover:no-underline text-xl  rounded w-full"
                        >
                          {_("Previous")}
                        </button>
                      </div>
                      <div className="px-1 text-center w-6/12">
                        <button
                          onClick={handleSubmit}
                          type="button"
                          className="bg-emerald-300 hover:bg-emerald-200 duration-300 border hover:border-emerald-300 text-white text-xl font-semibold p-2 rounded w-full"
                        >
                          {_("Next")}
                        </button>
                      </div>
                    </div>
                  </div>
                )}
                {step == 2 && (
                  <div className="mx-auto">
                    <div className="container mt-10">
                      <div className="">
                        <div>
                          <h1 className="text-4xl">{_("Create an account")}</h1>
                          <p className="text-xl mt-6">
                            {_(
                              "It's time to create an account and customize your card any time you want, and add new cards for your company, products and events"
                            )}
                          </p>
                          <div className="mt-10 ">
                            <form onSubmit={submitHandler}>
                              <div className="mt-4">
                                <input
                                  value={registerEmail}
                                  onChange={(e) =>
                                    setRegisterEmail(e.target.value)
                                  }
                                  ref={emailInputRef}
                                  className="border border-gray-500 h-12 w-full rounded-md p-1 placeholder-gray-500"
                                  placeholder={_("Your Email")}
                                ></input>
                              </div>
                              <div className="mt-4">
                                <input
                                  ref={passwordInputRef}
                                  className="border border-gray-500 h-12 w-full rounded-md p-1 placeholder-gray-500"
                                  placeholder={_("Password")}
                                  type="password"
                                ></input>
                              </div>
                              <div className="mt-4">
                                <p className="text-red-600">{error}</p>
                                <div className="flex">
                                  <div className="w-6/12">
                                    <button
                                      onClick={() => {
                                        setStep(1);
                                      }}
                                      className="text-emerald-300 hover:no-underline text-xl  rounded w-full"
                                    >
                                      {_("Previous")}
                                    </button>
                                  </div>
                                  <div className="w-6/12">
                                    <button
                                      type="submit"
                                      className="bg-emerald-300 text-center text-white w-full h-12 rounded-md hover:bg-emerald-500 duration-200"
                                    >
                                      {!isRegistering && (
                                        <span>{_("Create Account")}</span>
                                      )}
                                      {isRegistering && (
                                        <i className="animate-pulse fas fa-ellipsis-h"></i>
                                      )}
                                    </button>
                                  </div>
                                </div>
                                <p className="ltr:text-left rtl:text-right mt-4 text-xs text-gray-600">
                                  {_("By clicking Create, you agree to out")}{" "}
                                  <span className="text-blue-600">
                                    <Link href={"/terms"}>
                                      {_("Terms of Service")}
                                    </Link>
                                  </span>{" "}
                                  {_("and that you have read out")}{" "}
                                  <span className="text-blue-600">
                                    <Link href={"privacy-policy"}>
                                      {_("Privacy Policy")}
                                    </Link>
                                  </span>
                                </p>
                              </div>
                            </form>
                            <div className="mt-4 justify-start grid grid-cols-12">
                              <div className="col-span-5 pt-3">
                                <hr className="border-gray-500"></hr>
                              </div>
                              <div className="col-span-2 text-gray-500 text-center">
                                {_("OR")}
                              </div>
                              <div className="col-span-5 pt-3">
                                <hr className="border-gray-500"></hr>
                              </div>
                            </div>
                            <div className="mt-4  flex justify-center">
                              {Object.values(providers).map((provider) =>
                                provider.id == "facebook" ? (
                                  <button
                                    onClick={() => signIn(provider.id)}
                                    key={provider.name}
                                    className="facebook w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                                  >
                                    <i
                                      className={
                                        "fab fa-facebook-f text-blue-600  text-lg"
                                      }
                                    ></i>
                                  </button>
                                ) : provider.id == "google" ? (
                                  <button
                                    key={provider.name}
                                    onClick={() => signIn(provider.id)}
                                    className="facebook w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                                  >
                                    <i className="fab fa-google text-red-500  text-lg"></i>
                                  </button>
                                ) : provider.id == "linkedin" ? (
                                  <button
                                    key={provider.name}
                                    onClick={() => signIn(provider.id)}
                                    className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                                  >
                                    <i className="fab fa-linkedin-in text-blue-800  text-lg"></i>
                                  </button>
                                ) : provider.id == "apple" ? (
                                  <button
                                    key={provider.name}
                                    onClick={() => signIn(provider.id)}
                                    className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                                  >
                                    <i className="fab fa-apple text-gray-800  text-lg"></i>
                                  </button>
                                ) : provider.id == "twitter" ? (
                                  <button
                                    key={provider.name}
                                    onClick={() => signIn(provider.id)}
                                    className="linkedin w-12 h-12 rounded-md bg-white p-2 mx-2 border shadow"
                                  >
                                    <i className="fab fa-twitter text-cyan-400  text-lg"></i>
                                  </button>
                                ) : (
                                  ""
                                )
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {step == 3 && (
                  <div className="container mt-10">
                    <h1 className="text-4xl">
                      {_("Here is Your Card QR Code!")}
                    </h1>
                    <div className="mx-auto text-center">
                      <div ref={qrCodeRef} id="qr-gen"></div>
                      <Link
                        href={"https://much.sa/" + createdCard._id}
                        target="_blank"
                      >
                        {_("Preview Your card")}
                      </Link>
                      <div
                        onClick={downloadQRCode}
                        className="text-center mt-10 pb-40"
                      >
                        <button className="btn ">
                          {_("Download QR Code")}
                        </button>
                      </div>
                    </div>
                    <div className="flex fixed w-full lg:w-8/12 h-20 shadow-[0px_-5px_5px_0px_rgba(0,0,0,.3)] bg-white bottom-0 left-0 pt-4 px-6">
                      <div className="px-1 text-center w-full">
                        <Link href="/dashboard/">
                          <div
                            type="button"
                            className="bg-emerald-300 hover:bg-emerald-200 duration-300 border hover:border-emerald-300 text-white text-xl font-semibold p-2 rounded w-full"
                          >
                            {_("Dashoard")}
                          </div>
                        </Link>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className="hidden lg:block lg:w-4/12 bg-emerald-300 min-h-screen">
              <div className="ltr:float-right p-4">
                <img src="/images/logo-white.svg" className="w-28" />
              </div>

              {!name && (
                <div className="my-48">
                  <h1 className="text-4xl font-bold text-white mb-6 px-12">
                    {_("Let's go!")}
                  </h1>
                  <p className="text-xl text-white text-start px-12">
                    {_(
                      "With in less than 2 minutes you will have your digital business card, and take your communication to the new Era"
                    )}
                  </p>
                </div>
              )}
              {name && (
                <div>
                  <style jsx>
                    {`
                      h1,
                      h2,
                      h3,
                      h4,
                      h5,
                      h6,
                      p,
                      i {
                        color: ${color?.primary || "#333"};
                      }
                      .card,
                      button {
                        background-color: ${color?.secondary || "#FFF"};
                      }

                      .avatar {
                        border-radius: ${avatar || "9999px"};
                      }
                      button {
                        border-radius: ${buttonShape};
                      }
                    `}
                  </style>
                  <div className="col-span-2 xl:p-4 mx-1 rounded-md xl:fixed xl:top-24 ltr:xl:right-16 rtl:xl:left-16 ltr:lg:left-auto rtl:lg:right-auto">
                    <div
                      className="preview-profile mx-auto shadow-lg"
                      style={{
                        width: "375px",
                        height: "812px",
                        overflow: "hidden",
                        border: "8px solid #000",
                        borderRadius: "46px",
                        zoom: "0.65",
                      }}
                    >
                      <div className="bg-white h-6 hidden xl:block">
                        <div className="bg-emerald-200 h-4 w-8 absolute left-16 top-9 rounded-full text-xs text-center text-white">
                          {new Date().getHours() +
                            ":" +
                            new Date().getMinutes()}
                        </div>
                        <div className="bg-black mx-auto w-36 h-6  absolute left-1/3 rounded-b-2xl"></div>
                        <div>
                          <i className="fa-solid fa-battery-full absolute right-16 top-9 text-lg"></i>
                          <i className="fas fa-signal absolute right-24 top-9 text-sm"></i>
                        </div>
                      </div>
                      <main className="profile-page h-full bg-blueGray-200 overflow-y-auto">
                        <section className="relative block h-2/4">
                          <div
                            onClick={openCoverWidget}
                            className="absolute top-0 w-full h-full bg-top bg-cover  bg-no-repeat"
                            style={{
                              backgroundImage: "url(" + card.cover + ")",
                              boxShadow: "inset 0 -200px 100px #00000088",
                            }}
                          >
                            {!card.cover && step == 1 && (
                              <div
                                onClick={openCoverWidget}
                                className="mx-auto font-bold bg-white px-1 py-4 rounded-xl drop-shadow text-center mt-20 cursor-pointer w-40"
                              >
                                <i className="fa fa-image px-2"></i>{" "}
                                {_("Add Cover Photo")}
                              </div>
                            )}
                            <button
                              onClick={() => {
                                showQrFn();
                              }}
                              className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute right-3 top-3"
                            >
                              <i className="fas text-md fa-qrcode"></i>
                            </button>
                            <div
                              className={
                                showQR
                                  ? "relative mx-auto mt-10  rounded-full bg-white border-4 border-black overflow-hidden qrCodeContainer"
                                  : "relative mx-auto mt-10 h-32 w-32 overflow-hidden qrCodeContainer hidden"
                              }
                              ref={qrCodeRef}
                              style={{
                                width: "250px",
                                height: "250px",
                                padding: "34px",
                                backgroundImage:
                                  'url("/api/qrbg/?color=' +
                                  card.QRcolorDark?.replace("#", "") +
                                  'aa")',
                              }}
                            ></div>
                          </div>
                          <div
                            className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-16"
                            style={{ transform: "translateZ(0)" }}
                          >
                            <svg
                              className="absolute bottom-0 overflow-hidden"
                              xmlns="http://www.w3.org/2000/svg"
                              preserveAspectRatio="none"
                              version="1.1"
                              viewBox="0 0 2560 100"
                              x="0"
                              y="0"
                            >
                              <polygon
                                className="text-blueGray-200 fill-current"
                                points="2560 0 2560 100 0 100"
                              ></polygon>
                            </svg>
                          </div>
                        </section>
                        <section className="relative py-16 bg-blueGray-200">
                          <div className="container mx-auto px-4">
                            <div
                              className={
                                showQR
                                  ? "profile-card card relative flex flex-col min-w-0 break-words w-full mb-6 shadow-xl rounded-lg duration-200 -mt-24"
                                  : "profile-card card relative flex flex-col min-w-0 break-words w-full mb-6 shadow-xl rounded-lg duration-200 -mt-64"
                              }
                            >
                              <div className="px-6">
                                {(card.pic != null || step == 1) && (
                                  <div className="flex flex-wrap justify-center">
                                    <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center pb-10">
                                      <div className="relative">
                                        <div
                                          className="avatar shadow-xl border border-gray-400 bg-gray-50 overflow-hidden  h-auto align-middle absolute -m-16 -ml-20 lg:-ml-16 max-w-150-px"
                                          onClick={openWidget}
                                        >
                                          {card.pic && (
                                            <img
                                              src={card.pic}
                                              className="h-full w-full"
                                            />
                                          )}
                                          {!card.pic && (
                                            <i className="fa fa-camera text-4xl p-12 cursor-pointer text-gray-400"></i>
                                          )}
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                )}
                                <div className="text-center mt-12">
                                  <button className="bg-white shadow-md h-10 w-10 border-gray-500 rounded-full absolute ltr:right-3 rtl:left-3 top-3">
                                    <i className="fas text-md fa-share-alt"></i>
                                  </button>
                                  <h3 className="text-4xl font-semibold leading-normal mb-1 text-blueGray-700">
                                    {name}
                                  </h3>
                                  {card.jobTitle && (
                                    <h4 className="text-2xl font-semibold mb-1">
                                      {card.jobTitle.label}
                                    </h4>
                                  )}

                                  {card.department && (
                                    <h4 className="text-sm leading-normal mt-0 mb-2 text-blueGray-600 font-bold">
                                      {card.department.label}
                                    </h4>
                                  )}
                                  {card.company && (
                                    <h4 className="mb-2 text-blueGray-600 mt-2">
                                      {card.company.pic && (
                                        <img
                                          src={card.company.pic}
                                          style={{
                                            height: "32px",
                                            width: "32px",
                                            borderRadius: "100px",
                                            display: "inline",
                                            margin: "0 4px",
                                          }}
                                        />
                                      )}
                                      {card.company.label}
                                    </h4>
                                  )}
                                </div>
                                <div className="mt-2 py-5 border-t border-blueGray-200 text-center">
                                  {card.description && (
                                    <p className="whitespace-pre-line">
                                      {card.description}
                                    </p>
                                  )}
                                  {contactShape == "horizontal" && (
                                    <div className="flex flex-wrap justify-center">
                                      <div className="w-full  px-1">
                                        {card.telephonePrefix &&
                                          card.telephone && (
                                            <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                              <i className="fa fa-phone text-xl"></i>
                                            </button>
                                          )}
                                        {card.mobilePrefix && card.mobile && (
                                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                            <i className="fa fa-mobile text-xl"></i>
                                          </button>
                                        )}
                                        {card.whatsappPrefix && card.whatsapp && (
                                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                            <i className="fab fa-whatsapp text-xl"></i>
                                          </button>
                                        )}
                                        {email && (
                                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                            <i className="fas fa-envelope text-xl"></i>
                                          </button>
                                        )}
                                      </div>
                                    </div>
                                  )}
                                  {contactShape == "vertical" && (
                                    <div className="w-full  px-1 ltr:text-left">
                                      {card.telephonePrefix && card.telephone && (
                                        <div>
                                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                            <i className="fa fa-phone text-xl"></i>
                                          </button>
                                          <h5 className="inline-block font-bold">
                                            {"(" +
                                              card.telephonePrefix.value +
                                              ") " +
                                              card.telephone}
                                          </h5>
                                        </div>
                                      )}
                                      {card.mobilePrefix && card.mobile && (
                                        <div className="flex">
                                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                            <i className="fa fa-mobile text-xl"></i>
                                          </button>
                                          <h5 className="my-3 inline-block font-bold">
                                            {"(" +
                                              card.mobilePrefix.value +
                                              ") " +
                                              card.mobile}
                                          </h5>
                                        </div>
                                      )}
                                      {card.whatsappPrefix && card.whatsapp && (
                                        <div>
                                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                            <i className="fab fa-whatsapp text-xl"></i>
                                          </button>
                                          <h5 className="inline-block font-bold">
                                            {"(" +
                                              card.whatsappPrefix.value +
                                              ") " +
                                              card.whatsapp}
                                          </h5>
                                        </div>
                                      )}
                                      {email && (
                                        <div className="flex">
                                          <button className="bg-white border p-3 m-2 rounded-full h-12 w-12 shadow-md">
                                            <i className="fas fa-envelope text-xl"></i>
                                          </button>
                                          <h5 className="inline-block my-3 font-bold overflow-hidden text-ellipsis whitespace-nowrap w-48">
                                            {email}
                                          </h5>
                                        </div>
                                      )}
                                    </div>
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>
                      </main>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </>
    );
}
export async function getServerSideProps(context) {
  const providers = await getProviders();

  return {
    props: {
      query: context.query,
      csrfToken: await getCsrfToken(context),
      providers,
    },
  };
}
