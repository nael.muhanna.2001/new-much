import { translation } from "../lib/translations";
import { useRouter } from "next/router";
import Link from "next/link";
import Footer from "../webComponents/new/footer";
import Header from "../webComponents/new/storeHeader";

const items = [
  {
    id: 1,
    title: "Our Teame",
    sub: "",
    body: "",
    imageUrl: "/images/new/Much-Slider-3.jpg",
  },
];

const TeamCard = ({ data }) => {
  return (
    <div
      className="min-h-[395px] min-w-[315px]  max-w-[315px] rounded-3xl bg-white relative mx-auto"
      style={{ height: "100%" }}
    >
      <div className=" absolute h-[195px] w-[195px] top-[-58px] left-[58px] bg-[#bcbec0] rounded-full">
        <img
          src={data.img}
          className="w-full h-full object-contains rounded-full"
        />
      </div>
      <div className="relative top-[145px] ">
        <h2 className="  text-[#004c7f] text-2xl font-bold mb-0 tracking-normal">
          {data.name}
        </h2>
        <div className="mx-12 text-gray-800">
          <h4 className=" !pb-2 text-xl text-center">{data.job_title}</h4>
          <p className=" text-sm">{data.description}</p>
        </div>
      </div>
      <div className="flex absolute w-full bottom-4">
        <div className="w-6/12">
          <div className="w-[28px] h-[28px] mx-auto">
            <Link href={data.linkedin} target="_blank">
              <img src="/assets/img/lkicon.svg" alt="linkedin" />
            </Link>
          </div>
        </div>
        <div className="w-6/12">
          <div className="w-[28px] h-[28px] mx-auto">
            <Link href={data.much} target="_blank">
              <img src="/assets/img/micon.svg" alt="linkedin" />
            </Link>
          </div>
        </div>
      </div>

      <div className="w-[85px] absolute top-[378px] left-[115px]">
        <svg id="a" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84.2 17">
          <path
            d="M0,17c4.52,0,8.96-1.23,12.83-3.56L29.27,3.56c7.9-4.75,17.77-4.75,25.67,0l16.43,9.88c3.88,2.33,8.31,3.56,12.83,3.56H0Z"
            fill="#7fcaad"
          />
        </svg>
      </div>
    </div>
  );
};

const Teams = () => {
  function _(text) {
    const { locale } = useRouter();
    if (translation[locale][text]) {
      return translation[locale][text];
    }
    return text;
  }

  const data = [
    {
      name: "Ebtihal Alosaimi",
      job_title: "CEO & CO Founder",
      description:
        "10+ yrs experience in eCommerce and Fintech (Ex- Amazon, BNPL, eWallet) and strategic consulting in U.S.A.",
      img: "https://res.cloudinary.com/pweb/image/upload/c_crop,h_243,w_242,x_0,y_29/WhatsApp_Image_2020-11-24_at_2.22.15_PM_gv5ibz",
      much: "https://www.much.sa/ebtihal.alosaimi/",
      linkedin: "https://www.linkedin.com/in/ebtihal-alosaimi-mba-8638a450/",
    },
    {
      name: "Omran Karajeh",
      job_title: "CTO & CO Founder",
      description:
        "12+ yrs experience in IT, Recruitment & Training. He started his first company in 2014, Pioneers Network, and then co-found- ed Mehnati, MBSHR, Papion.",
      img: "https://res.cloudinary.com/pweb/image/upload/c_crop,h_244,w_244,x_23,y_0/Untitled-1_ibeerc",
      much: "https://www.much.sa/omran.karajeh",
      linkedin: "https://www.linkedin.com/in/omran-karajeh-43288527/",
    },
    {
      name: "Muhannad Mahayni",
      job_title: "Chief Creative Officer",
      description:
        "18+ yrs experience in advertising, branding, localisation and copywriting. Founder of Moodban creative agency.",
      img: "https://res.cloudinary.com/pweb/image/upload/c_crop,h_220,w_220,x_0,y_0/5db7d855-d4a4-4004-9ddd-a8e72e51923c_w8mbgf",
      much: "https://www.much.sa/muhannad.mahayni/",
      linkedin: "https://www.linkedin.com/in/muhannadmahayni/",
    },
  ];

  return (
    <div className="bg-[url('/images/bgs/tbg.jpg')] bg-cover bg-right">
      <Header items = {items} />
      <div className="container mb-5">
        <div className="text-center ">
          <h2 className="text-3xl md:text-5xl md:font-bold text-green p-5">
            {_("Our Team")}
          </h2>
        </div>
        <div className=" flex flex-wrap overflow-x-hidden   pt-20 pb-4 justify-center mx-auto">
          {data.map((val, index) => {
            return (
              <div key={index} className="w-full lg:w-4/12 mt-16 lg:mt-0">
                <TeamCard key={index} data={val} />
              </div>
            );
          })}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Teams;
