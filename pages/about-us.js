import { useRive, Layout } from "@rive-app/react-canvas";
import { translation } from "../lib/websiteTranslations";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { BsList, BsX } from "react-icons/bs";
import Header from "../webComponents/new/storeHeader";
import SEO from "../webComponents/Seo";
import Contact from "../webComponents/Contact";
import { TabPanel } from "react-tabs";
import Address from "../webComponents/Address";
import SharedHeader from "../webComponents/new/sharedHeader";

function _(text) {
  const { locale } = useRouter();
  if (translation[locale][text]) {
    return translation[locale][text];
  }
  return text;
}

const HomeDark = () => {
  const [tap, setTap] = useState(2);
  useEffect(() => {
    document.body.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  const items = {
    title: "About Us",
    sub: "",
    body: "",
    imageUrl: "/images/new/Much-Slider-1.jpg",
  };

  return (
    <>
      <SEO pageTitle={"much... | About Us"} />

      <div className="">
        <SharedHeader items={items} />

        <div className="bg-[#004c7e]">
          <section className="section-2 w-full h-auto pt-28">
            <div className="px-10 sm:px-0">
              <div className="w-full mx-auto flex flex-col sm:flex-row justify-between items-center">
                <div className="w-full sm:w-1/2 h-full flex flex-col mx-auto items-center pb-5">
                  <div className="video-1 h-auto w-64 lg:w-80 relative">
                    <img
                      className="w-16  lg:w-24 absolute right-[80%] -top-1 lg:right-64 lg:top-16 animate-[popup_7s_ease-in-out_infinite]"
                      src="/images/new/imgmuch/4.png"
                      alt=""
                    />
                    <img
                      className="  w-16 lg:w-24 absolute left-[90%] -bottom-[-16rem] lg:left-64 lg:bottom-16 animate-[popup_10s_ease-in-out_infinite]"
                      src="/images/new/imgmuch/5.png"
                      alt=""
                    />
                    <img
                      className=" w-16 lg:w-24 absolute right-[80%]  bottom-0 lg:right-64 lg:bottom-16 animate-[popup_5s_ease-in-out_infinite]"
                      src="/images/new/imgmuch/3.png"
                      alt=""
                    />
                    <video
                      className="rounded-[60px]"
                      muted
                      loop
                      autoPlay
                      playsInline
                    >
                      <source
                        src="https://pioneers.network/much/noapp.m4v"
                        type="video/mp4"
                      />
                    </video>
                  </div>
                </div>
                <div className="w-full sm:w-1/2 h-full items-center !p-4 mx-auto">
                  <h2 className="text-center md:text-justify text-3xl text-[26px] !p-1 text-green md:text-4xl font-bold !pb-4 w-full">
                    {_("No App? No Problem!")}
                  </h2>
                  <p className="text-center md:text-justify font-light !p-1 md:text-lg text-white">
                    {_(
                      "Share your business details with potential clients with just a tap using an app free digital business card solution offered by much.sa"
                    )}
                  </p>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div className="bg-[#004c7e]">
          <section className="section-1 w-full h-auto py-28 -mt-24 xl:min-h-[105vh] bg-right bg-cover bg-[url('https://res.cloudinary.com/pweb/image/upload/v1688605466/Much_Banners_02_1_jra6rf.jpg')]">
            <div className="container">
              <div className="w-full lg:w-1/2 h-full mt-24 p-4 py-5 bg-[#004c7eaa]   bg-opacity-40 rounded xl:bg-transparent">
                <h2 className="text-3xl  py-2  md:text-4xl font-bold w-full text-white">
                  {_("About")} <span className="text-green">{_("Us")}</span>
                </h2>
                <p className=" lg:text-left font-light md:text-lg text-lg p-6 text-white">
                  {_(
                    "much.sa is your trusted partner for NFC digital business cards and other innovative products and services. Our B2C and B2B solutions are well designed to help individuals and businesses boost their full potential in an increasingly connected economy."
                  )}
                </p>
              </div>
            </div>
          </section>

          <section className="section-2 w-full h-auto  lg:min-h-[100vh] bg-[#7fcbae] bg-right bg-cover md:bg-no-repeat md:bg-contain md:bg-[url('https://res.cloudinary.com/pweb/image/upload/v1688699451/Much_Banners_02_1_copy_wcxd2l.jpg')]">
            <div className=" md:hidden">
              <img
                src="https://res.cloudinary.com/pweb/image/upload/v1688699583/Much-story-m_e0ih23.jpg"
                alt="much story"
              />
            </div>
            <div className="container">
              <div className="w-full  flex">
                <div className="md:w-1/2  h-full  p-4 md:bg-[#004c7edd]   md:px-12  text-center md:min-h-screen">
                  <h2 className=" text-3xl  p-2 md:p-4 md:text-4xl font-bold w-full text-white">
                    {_("Our")}{" "}
                    <span className="text-blue-900 md:text-green">
                      {_("Story")}
                    </span>
                  </h2>
                  <div className="lg:text-left  text-lg text-justify font-medium md:text-left">
                    <p className=" md:text-left font-light md:text-lg pb-6 text-gray-800 md:text-white">
                      {
                        "Inspired by a lifelong commitment to sustainability, much.sa was founded by a group of entrepreneurs who were frustrated by the limitations of traditional business cards."
                      }
                    </p>
                    <p className="md:text-left font-light md:text-lg pb-6 text-gray-800 md:text-white">
                      {_(
                        "Attending a conference and collecting over 100 paper business cards in a single session gave us a loud reminder of our childhood lessons on recycling and sustainability. Our parents taught us how to make efficient use of paper, placing it in marked recycling boxes, and donating it to local charities for reuse. We grew up thinking that this was how people disposed of used paper, but as adults, reality hit us hard with a major struggle."
                      )}
                    </p>
                    <p className="md:text-left font-light md:text-lg pb-6 text-gray-800 md:text-white">
                      {_(
                        "That's were the idea of connecting people in a more efficient way to build meaningful relationships in the digital age was uncovered."
                      )}
                    </p>
                    <p className="md:text-left font-light md:text-lg pb-6 text-gray-800 md:text-white">
                      {_(
                        "Today, much.sa is a company dedicated to providing cutting-edge communication tools like NFC digital business cards to entrepreneurs and businesses in Saudi Arabia and beyond to boost digital transformation."
                      )}
                    </p>
                  </div>
                </div>
                <div className="md:w-1/2"></div>
              </div>
            </div>
          </section>
          <section className="section-3 w-full h-auto  xl:min-h-screen bg-center lg:bg-left bg-[#004c7e] md:bg-[url('https://res.cloudinary.com/pweb/image/upload/v1688606418/Our-Mission_dnp9dx.jpg')]">
            <div className="md:hidden">
              <img
                src="https://res.cloudinary.com/pweb/image/upload/v1688606418/Our-Mission_dnp9dx.jpg"
                alt="much mission"
              />
            </div>
            <div className="container">
              <div className="w-full mx-auto flex flex-col sm:flex-row justify-between items-center">
                <div className="w-full md:w-1/2 "></div>
                <div className="md:w-1/2 xl:h-screen flex flex-col  items-center p-4 pt-32 md:bg-[#004c7edd]   text-center">
                  <h2 className="text-center text-3xl  p-2 xl:my-20 md:text-4xl font-bold w-full text-white">
                    {_("Our")}{" "}
                    <span className="text-green">{_("Mission")}</span>
                  </h2>
                  <p className="md:text-left font-light md:text-lg text-white lg:text-left text-justify">
                    {_(
                      "To enable our clients to build stronger relationships and track their success in an efficient and reliable environment."
                    )}
                  </p>
                </div>
              </div>
            </div>
          </section>
          <section className="section-2 w-full h-auto  xl:min-h-screen bg-center bg-cover lg:bg-left bg-[#7fcbae] md:bg-[url('https://res.cloudinary.com/pweb/image/upload/v1688606898/Our-Vision_kkvzty.jpg')]">
            <div className="md:hidden">
              <img
                src="https://res.cloudinary.com/pweb/image/upload/v1688606898/Our-Vision_kkvzty.jpg"
                alt="much vision"
              />
            </div>
            <div className="container">
              <div className="w-full  flex flex-col sm:flex-row justify-between items-center">
                <div className="md:w-1/2  h-full  p-4 md:bg-[#004c7edd]   px-12  text-center xl:min-h-screen">
                  <h2 className="text-center text-3xl  p-2 xl:my-20 md:text-4xl font-bold w-full text-white">
                    {_("Our")}{" "}
                    <span className="md:text-green text-blue-900">
                      {_("Vision")}
                    </span>
                  </h2>
                  <div className="lg:text-left text-justify">
                    <p className=" font-light md:text-lg text-gray-800 md:text-white">
                      {_(
                        "We picture a world where all businesses are able to aquire the most advanced communication innovations that allow them to efficiently connect with clients and partners."
                      )}
                    </p>
                    <p className="font-light md:text-lg text-gray-800 md:text-white">
                      {_(
                        "By constantly evolving and leading the frontline of emerging technologies, much.sa has a strong vision in playing an important role in shaping the future of communication in KSA and around the globe.."
                      )}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="section-3 w-full h-auto  xl:min-h-screen bg-center bg-cover lg:bg-right bg-[#004c7e] md:bg-[url('https://res.cloudinary.com/pweb/image/upload/v1688701799/value_syehup.jpg')]">
            <div className="md:hidden">
              <img
                src="https://res.cloudinary.com/pweb/image/upload/v1688701799/value_syehup.jpg"
                alt="much values"
              />
            </div>
            <div className="container">
              <div className="w-full mx-auto flex flex-col sm:flex-row justify-between items-center">
                <div className="w-full md:w-1/2 "></div>
                <div className="md:w-1/2 xl:h-screen flex flex-col  items-center p-4  pt-32 lg:bg-[#004c7edd] bg-[#004c7eaa]  text-center">
                  <h2 className="text-center text-3xl  p-2 xl:my-20 md:text-4xl font-bold w-full text-white">
                    {_("Our")} <span className="text-green">{_("Values")}</span>
                  </h2>
                  <div className="lg:text-left text-justify">
                    <p className=" font-light md:text-lg text-white">
                      {
                        "At much.sa, sustainability is at the core of everything we do. We believe in using creative business solutions to reduce waste and promote environmental responsibility, while also empowering businesses and individuals to achieve their potential."
                      }
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>

          {/* <section className="section-6 w-full h-auto pt-20 bg-green md:bg-[#004c7e]">
            <div
              active
              className="contact react-tabs__tab-panel--selected container"
            >
              <div className="title-section text-start text-sm-center">
                <h2 className="text-center text-3xl p-5 md:text-4xl font-bold w-full text-white">
                  {_("Contact")}{" "}
                  <span className="text-blue-900 md:text-green">{_("Us")}</span>
                </h2>
              </div>
              <div className="container">
                <div className="row">
                  <div className="col-12 col-lg-4">
                    <h3 className="text-uppercase custom-title mb-0 ft-wt-600 pb-3 text-gray-800 md:text-white">
                      {"Drop"} a line!
                    </h3>
                    <p className="open-sans-font mb-4 text-gray-800 md:text-white">
                      Feel free to get in touch with us.
                    </p>
                    <Address />

                  </div>

                  <div className="col-12 col-lg-8">
                    </div>
                </div>
              </div>
            </div>
          </section> */}
        </div>
      </div>
    </>
  );
};

export default HomeDark;
