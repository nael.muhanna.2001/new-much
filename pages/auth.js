import AuthForm from "../components/auth/auth-form"
import { useState } from "react"
import Login from "../components/auth/login";
export default function Auth()
{
    const[isLogin,setIsLogin]= useState(true);
    if(!isLogin)
    return(
        <>
        <AuthForm />
        <button className="btn" onClick={()=>{setIsLogin(!isLogin)}}> Switch</button>
        </>
    )
    else
    return(
        <>
        <Login />
        <button className="btn" onClick={()=>{setIsLogin(!isLogin)}}> Switch</button>
        </>
    )
    
}